
/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

XB.P.Util = function ()
{
	
	// Time field combo stores
	
	var data = [];
	for (var i = 0; i < 24; i++)
	{
		data.push([String.leftPad(i, 2, '0')]);
	}
	
	var timeFieldHourComboStore = new Ext.data.SimpleStore
	(
		{
			fields: ['value'],
			data: data
		}
	);
	
	var data = [];
	for (var i = 0; i < 60; i++)
	{
		data.push([String.leftPad(i, 2, '0')]);
	}
	
	var timeFieldMinuteComboStore = new Ext.data.SimpleStore
	(
		{
			fields: ['value'],
			data: data
		}
	);

	var timeFieldSecondComboStore = new Ext.data.SimpleStore
	(
		{
			fields: ['value'],
			data: data
		}
	);


	return {
		/**
		* store, dependency, [grid], [cb] - backward compat order
		*  OR
		* dependency, (store | (store [, grid [, cb]]), ... )
		*/
		dep: function (dependency, stores, grid, cb)
		{
			cb = cb || null;
			
			var legacy = false;
			
			if (! Ext.isArray(stores)) // single mode: store, dependency, [grid], [cb]
			{
				// exchange store and dependency
				
				var d = stores;
				stores = dependency;
				dependency = d;
				
				legacy = true;
			}
				
			var depends = Ext.util.JSON.encode(dependency);
			
			
			var setd = function (store, grid, cb)
			{
				store.baseParams.depends = depends;
				if (grid)
				{
					if (cb) store.on('load', cb, {single: true});
					grid.getBottomToolbar().doLoad(0);
				}
				else
					store.load({callback: cb});
			}

			if (legacy)
				setd(stores, grid, cb)
			else
			{
				for (var i = 0; i < stores.length; i++)
				{
					var store = stores[i], grid = null, cb = null;

					if (Ext.isArray(store)) { grid = store[1]; cb = store[2] || null; store = store[0]; }

					setd(store, grid, cb)
				}
			}
		},
		
		/**
		* store OR (store, ... )
		*/
		undep: function (stores)
		{
			if (! Ext.isArray(stores)) stores = [stores];
				
			for (var i = 0; i < stores.length; i++)
			{
				var store = stores[i];
				
				delete store.baseParams.depends;
				store.removeAll();
			}
		},		
		
		timeFieldHourComboStore: timeFieldHourComboStore,
		timeFieldMinuteComboStore: timeFieldMinuteComboStore,
		timeFieldSecondComboStore: timeFieldSecondComboStore,
		
		alertErrors: function(errors)
		{
			var msg = '';
			
			for (var errorType in errors)
			{
				msg += errorType + ': ' + errors[errorType] + "\n";
			}
		
			alert(msg);
		},
		

		/*
			Show closable message box with OK button.
			Optional type (info|warning|error) customizes alert by type.
			o is applied to overall config if any
		*/
		showAlertBox: function(msg, type, o)
		{
			o = o || {};
			
			var c =
			{
				closable: true,
				buttons: Ext.MessageBox.OK,
				
				msg: msg
			};
			
			switch (type)
			{
				case 'i': 
				case 'info':     c.icon = 'INFO';     c.title = _('Info');     break;
				case 'w': 
				case 'warning':  c.icon = 'WARNING';  c.title = _('Warning');  break;
				case 'e': 
				case 'error':    c.icon = 'ERROR';    c.title = _('Error');    break;
			}
			c.icon = c.icon ? Ext.MessageBox[c.icon] : null;
			
			Ext.apply(c, o);
			
			Ext.Msg.show(c);
		},

		alert: function (msg, o)
		{
			this.showAlertBox(msg, null, o);
		},
		alertInfo: function (msg, o)
		{
			this.showAlertBox(msg, 'info', o);
		},
		alertWarning: function (msg, o)
		{
			this.showAlertBox(msg, 'w', o);
		},
		alertError: function (msg, o)
		{
			this.showAlertBox(msg, 'e', o);
		},
		
		
		/**
		*
		*/
		applyCustomConfigs: function (configs, customConfigs)
		{
			var unshiftConfigs = [];
			var pushConfigs = [];
			
			for (var k in customConfigs)
			{
				var c = customConfigs[k];
				
				
				if (k == '+')
				{
					if (Ext.isArray(c))
					{
						for (var i = 0; i < c.length; i++)
						{
							pushConfigs.push(c[i]);
						}
					}
					else
					{
						pushConfigs.push(c);
					}
				}
				else if (k == '-')
				{
					if (Ext.isArray(c))
					{
						for (var i = 0; i < c.length; i++)
						{
							unshiftConfigs.push(c[i]);
						}
					}
					else
					{
						unshiftConfigs.push(c);
					}
				}
				else
				{
					var index = parseInt(k);

					if (Ext.isArray(c))
					{
						for (var i = 0; i < c.length; i++)
						{
							configs.splice(index, 0, c[i]);
							
							index++;
						}
					}
					else
					{
						configs.splice(index, 0, c);
					}
				}
			}
			
			
			if (unshiftConfigs.length > 0)
			{
				var index = 0;
				
				for (var i = 0; i < unshiftConfigs.length; i++)
				{
					var c = unshiftConfigs[i];
					
					configs.splice(index, 0, c);
					
					index++;
				}
			}
			
			if (pushConfigs.length > 0)
			{
				for (var i = 0; i < pushConfigs.length; i++)
				{
					var c = pushConfigs[i];
					
					configs.push(c);
				}
			}
			
			
			return configs;
		},
		
		/**
		* @param array ps array of parameter objects where each represents a request
		*/
		batchLoad: function (ps, o)
		{
			var params = {batch: Ext.util.JSON.encode(ps)};
			
			XB.P.requestModule
			(
				'Default',
				'Batch',				
				{
					params: params,
					callback: function ()
					{
						if (o.callback) o.callback();
					},
					success: function (response)
					{
						if (o.success) o.success(response.results);
					},
					failure: function ()
					{
						if (o.failure) o.failure();
					}
				}
			);
		},
		
		/**
		* @param array ss array of stores to load in a batch
		*/
		storeBatchLoad: function (ss, o)
		{
			var ps = [];
			for (var i = 0; i < ss.length; i++)
			{
				var s = ss[i];
				var p = s.getFullParams();
				ps.push(p);
			}

			this.batchLoad
			(
				ps,
				{
					success: function (results)
					{
						for (var i = 0; i < results.length; i++)
						{
							var result = results[i];
							
							if (result.success)
							{
								ss[i].loadData(result);
							}
							else
							{
								XB.P.Util.alertErrors(result.errors);
							}
						}
						
						if (o.success) o.success(results);
					},
					failure: function ()
					{
						if (o.failure) o.failure(response);
					}
				}
			);
			
		},
		
		/**
		* Set slave grid view for dual master-slave grid-to-grid UI (master record and slave grid) 
		*/
		setSlaveGridView: function (masterEntityName, masterRecord, slaveGrid)
		{
			var slaveStore = slaveGrid.getStore();
	
			
			var depends = {};
			depends[masterEntityName.toLowerCase()] = {id: masterRecord.get('id')};

			slaveStore.baseParams.depends = Ext.util.JSON.encode(depends);
			
			
			if (slaveStore.baseParams.paging) // paged store
			{
				var tb = slaveGrid.getBottomToolbar();
				tb.doLoad(0);
			}
			else // plain store
			{
				slaveStore.load();
			}
	
			
			slaveGrid.show();
		},
		
		/**
		* Reset slave grid view for dual master-slave grid-to-grid UI 
		*/
		resetSlaveGridView: function (slaveGrid)
		{
			var slaveStore = slaveGrid.getStore();
		
			slaveGrid.hide();
		
			delete slaveStore.baseParams.depends;
			slaveStore.removeAll();
		}
	}
}();
