
/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

Ext.namespace('XB', 'XB.Components', 'XB.Platform', 'XB.ApplicationModules');


function _(text)
{
	return LC_MESSAGES[text] ? LC_MESSAGES[text] : text;
}


var localizeExtJs = function()
{
	Ext.MessageBox.buttonText.ok     = _('OK');	
	Ext.MessageBox.buttonText.yes    = _('Yes');
	Ext.MessageBox.buttonText.no     = _('No');
	Ext.MessageBox.buttonText.cancel = _('Cancel');
	
	Ext.override
	(
		Ext.grid.GridView,
		{
			sortAscText:  _('Sort Ascending'),
			sortDescText: _('Sort Descending'),
			columnsText:  _('Columns')
		}
	);
	
	Ext.override
	(
		Ext.ux.grid.GridFilters,
		{
			menuFilterText: _('Filters')
		}
	);
	
	Ext.override
	(
		Ext.grid.GroupingView,
		{
			groupByText: _('Group by this field'),
			showGroupsText: _('Show in groups')
		}
	);

	Date.dayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
	Date.monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
	Date.monthNumbers =
	{
		'Янв': 0,
		'Фев': 1,
		'Мар': 2,
		'Апр': 3,
		'Май': 4,
		'Июн': 5,
		'Июл': 6,
		'Авг': 7,
		'Сен': 8,
		'Окт': 9,
		'Ноя': 10,
		'Дек': 11
	};

	if(Ext.DatePicker){
		Ext.apply(Ext.DatePicker.prototype, {
			todayText          : "Сегодня",
			minText            : "Эта дата раньше минимальной даты",
			maxText            : "Эта дата позже максимальной даты",
			disabledDaysText   : "",
			disabledDatesText  : "",
			monthNames         : Date.monthNames,
			dayNames           : Date.dayNames,
			nextText           : 'Следующий месяц (Control+Вправо)',
			prevText           : 'Предыдущий месяц (Control+Влево)',
			monthYearText      : 'Выбор месяца (Control+Вверх/Вниз для выбора года)',
			todayTip           : "{0} (Пробел)",
			format             : "d.m.y",
			okText             : "&#160;OK&#160;",
			cancelText         : "Отмена",
			startDay           : 1
		});
	}
	
	
	Ext.override
	(
		Ext.form.ComboBox,
		{
			loadingText: _('Loading...')
		}
	);


	Ext.override
	(
		Ext.PagingToolbar,
		{
			displayMsg:      _('from {0} to {1} shown out of {2}'),
			emptyMsg:        _('empty'),
			beforePageText:  _('page') + '&nbsp;',
			afterPageText:   '&nbsp;' + _('of {0}'),
			firstText:       _('First page'),
			prevText:        _('Previous page'),
			nextText:        _('Next page'),
			lastText:        _('Last page'),
			refreshText:     _('Refresh')
		}
	);
}


var localizeXBComponents = function()
{
	/*
	Ext.override
	(
		XB.Components.YesNoComboBox,
		{
		}
	);
	*/
}

var localizeXBPlatform = function()
{
	
}

var localize = function ()
{
	localizeExtJs();
	localizeXBComponents();
	localizeXBPlatform();
}




var fileBrowser = null;
var createFileBrowser = function()
{
	fileBrowser = new Ext.ux.FileBrowser
	(
		{
			renderTo: Ext.getBody(),

			title: 'Выбор файла...',

			width: 970,
			height: 470,
			resizable: true,
			
			modal: true,
			
			closeAction: 'hide',

			url: 'index.php',
			baseParams: {module: self.moduleName},
			
			// rootDirectoryName: 'icons',
			
			imagePath: XB.P.Config.extjsPluginUrl + 'filebrowser/',
			
			GetDirectoryListActionName: 'GetFileBrowserDirectoryList',
			GetFileListActionName: 'GetFileBrowserFileList',
			UploadFileActionName: 'FileBrowserUploadFile',
			
			listeners:
			{
				fileclick: function (fileBrowser, fileRecord)
				{
					var path = fileRecord.get('path');
					
					fileBrowser.targetTextField.setValue(path);
					
					fileBrowser.hide();
				},
				show: function (fileBrowser)
				{
					// var root = 'image/';
					
					
					var path = fileBrowser.targetTextField.getValue();
					
					if (path)
					{
						//var re = new RegExp('^' + root);
						//path = path.replace(re, '');
						
						// var re = new RegExp('/[^/]*$');
						// path = path.replace(re, '');

						fileBrowser.selectPath(path);
					}
				}
			}
		}
	);
	
	return fileBrowser;
}

var getFileBrowser = function()
{
	if (! fileBrowser) throw 'getFileBrowser(): fileBrowser is null';

	return fileBrowser;
}


// Ext interversion compatible object

ExtCompatible = {
	EventConstants: Ext.EventObject
}


// ExtJS 6.2.0 forward stubs

Ext.define = function (name, cfg)
{
	var parent = cfg.extend
	delete cfg.extend
	
	var xtype
	if (cfg.xtype)
	{
		xtype = cfg.xtype
		delete cfg.xtype
	}
	
	var cmp = Ext.extend(parent, cfg)

	if (xtype) Ext.reg(xtype, cmp)
	
	eval(String.format('{0} = cmp', name))
}
