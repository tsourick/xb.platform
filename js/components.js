
/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/


XB.Components = {};


// Standalone components


Ext.define
(
	'XB.Components.ButtonTextField',
	{
		extend: Ext.form.TextField,
		
		xtype: 'xb_buttontextfield',
		
		buttonText: 'Push the button',
		buttonOffset: 3,
		
		autoSize: Ext.emptyFn,
		
		initComponent: function()
		{
			Ext.apply
			(
				this,
				{
					cls: 'ButtonTextFieldInput'
				}
			);
			
			XB.Components.ButtonTextField.superclass.initComponent.call(this);
		},
		
		onRender : function(ct, position)
		{
			XB.Components.ButtonTextField.superclass.onRender.call(this, ct, position);
	
			
			this.el.setStyle({marginRight: this.buttonOffset + 'px'});
			
			this.wrap = this.el.wrap({cls: 'x-form-field-wrap'});
			
			
			var btnCfg = Ext.applyIf
			(
				this.buttonCfg || {},
				{
					text: this.buttonText
				}
			);
			
			this.button = new Ext.Button
			(
				Ext.apply
				(
					btnCfg,
					{
						renderTo: this.wrap,
						
						ownerTextField: this
					}
				)
			);
		},
	
		// private
		getResizeEl : function()
		{
			return this.wrap;
		},
	
		// private
		getPositionEl : function()
		{
			return this.wrap;
		},
	
		// private
		onResize : function(w, h)
		{
			XB.Components.ButtonTextField.superclass.onResize.call(this, w, h);
	
			this.wrap.setWidth(w);
			
			var w = this.wrap.getWidth() - this.button.getEl().getWidth() - this.buttonOffset * 2;
			this.el.setWidth(w);
		},
		
		// private
		preFocus : Ext.emptyFn,
	
		// private
		alignErrorIcon : function()
		{
			this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
		}
	}
);


Ext.define
(
	'XB.Components.YesNoComboBox',
	{
		extend: Ext.form.ComboBox,
		
		xtype: 'xb_yesnocombo',
		
		store: new Ext.data.SimpleStore
		(
			{
				fields: ['id', 'title'],
				data:
				[
					[1, _('Yes')],
					[0, _('No')]
				]
			}
		),
		
		editable: false,
		displayField: 'title',
		valueField: 'id',

		mode: 'local',
		triggerAction: 'all',
		
		width: 60
	}
);

Ext.define
(
	'XB.Components.IntegerField',
	{
		extend: Ext.form.NumberField,
		
		xtype: 'xb_integerfield',
		
		allowDecimals: false
	}
);


Ext.define
(
	'XB.Components.FloatField',
	{
		extend: Ext.form.NumberField,
		
		xtype: 'xb_floatfield'
	}
);


Ext.define
(
	'XB.Components.MoneyField', 
	{
		extend: XB.Components.FloatField,
		
		xtype: 'xb_moneyfield',
		
		unsigned: true
	}
);



Ext.define
(
	'XB.Components.ParametersField',
  {
  	extend: Ext.form.TextField,
  	
  	xtype: 'xb_paramsfield',
	
  	grid: null,
  	
    autoSize: Ext.emptyFn,
		
    // private
    initComponent: function(){
        XB.Components.ParametersField.superclass.initComponent.call(this);
    },
    
    updateFieldValue: function()
		{
			var records = this.grid.getStore().getRange();
			var params = {};
			Ext.each
			(
				records,
				function (r)
				{
					params[r.data.name] = r.data.value;
				}
			);
			var rawParams = XB.Util.implodeParams(params);
			this.setRawValue(rawParams);
		},
		
		addNewParam: function ()
		{
			this.grid.getStore().loadData([['new_name','new_value']], true);
			
			this.updateFieldValue();
		},
						
    // private
    onRender: function(ct, position)
    {
			XB.Components.ParametersField.superclass.onRender.call(this, ct, position);
			
			//this.el.setStyle({display: 'none'});
			
			var self = this;
			
			this.grid = new Ext.grid.EditorGridPanel
			(
				{
					title: 'parameters',
					stripeRows: true,
					renderTo: ct,
					
					width: 400,
					height: 150,
					frame: true,
					
					tbar: [{text: 'add', handler: function () {self.addNewParam();}}],

					contextMenu: new Ext.menu.Menu
					(
						{
							parentField: self,
							defaults: {listeners: {click: function() {this.parentMenu.hide();} } },
							items:
							[
								{
									text: _('Add'),
									handler: function ()
									{
										self.addNewParam();
									}
								},
								{
									text: _('Delete'),
									handler: function ()
									{
										var m = this.parentMenu;
										m.parentField.grid.getStore().remove(m.contextRecord);
										
										self.updateFieldValue();
									}
								},
								'-',
								{
									text: _('Cancel')
								}
							]
						}
					),
	
					listeners:
					{
						rowcontextmenu: function(grid, rowIndex, e)
						{
							e.preventDefault();
							var c = grid.contextMenu || null;
							c.contextRecord = grid.getStore().getAt(rowIndex);
							c.showAt(e.getXY());
						},
						afteredit: (function (e)
						{
							e.record.commit();
							
							this.updateFieldValue();
						}).createDelegate(self)
					},
					store: new Ext.data.ArrayStore
					(
						{
							autoSave: true,
							fields: ['name', 'value'],
							// idIndex: 0 // id for each record will be the first element
							data:
							[
								['qqq', 'www'],
								['qqq', 'www']
							]
						}
					),
					columns:
					[
						{header: 'name', dataIndex: 'name', editor: new Ext.form.TextField()},
						{header: 'value', width: 200, dataIndex: 'value', editor: new Ext.form.TextField()}
					]
				}
			);
    }

    
    
    /*
    // private
    onResize : function(w, h){
        Ext.form.FileUploadField.superclass.onResize.call(this, w, h);

        this.wrap.setWidth(w);
        
        if(!this.buttonOnly){
            var w = this.wrap.getWidth() - this.button.getEl().getWidth() - this.buttonOffset;
            this.el.setWidth(w);
        }
    },
    
    // private
    preFocus : Ext.emptyFn,
    
    // private
    getResizeEl : function(){
        return this.wrap;
    },

    // private
    getPositionEl : function(){
        return this.wrap;
    },

    // private
    alignErrorIcon : function(){
        this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
    },
    
		//private
		reset : function(){
			this.fileInput.removeAllListeners();
			this.fileInput.remove();
			this.createFileInput();
			this.addFileListener();
			Ext.form.FileUploadField.superclass.reset.call(this);
		}
		*/
		
		,  setValue : function(v)
		{
			XB.Components.ParametersField.superclass.setValue.apply(this, arguments);
			
			var p = XB.Util.explodeParams(v);
			var a = XB.Util.objectToArrays(p);
			
			this.grid.getStore().loadData(a);
			
			return this;
		}
});



XB.Components.CTCButton = Ext.extend
(
	Ext.Button,
	{
		createZeroClipboard: function ()
		{
			var clip = new ZeroClipboard.Client();
			clip.setHandCursor( true );

			clip.addEventListener('mouseDown', this.onCopy);
			
			clip.button = this;
			this.clip = clip;
		},
		onCopy: function (clip)
		{
			if (clip.button.fireEvent('beforeCopy'))
			{
				var value = clip.button.getValue();
				clip.setText(value);
				clip.button.fireEvent('afterCopy', value);
			}
		},
		getValue: function ()
		{
			var value = null;
			
			if (this.value)
			{
				value = this.value;
			}
			else if (this.field)
			{
				value = this.field.getValue();
			}
			
			return value;
		},
		clipMove: function ()
		{
			this.clip.reposition();
		},
		clipShow: function ()
		{
			this.clip.show();
		},
		clipHide: function ()
		{
			this.clip.hide();
		},

		constructor: function (config)
		{
			XB.Components.CTCButton.superclass.constructor.call(this, config);
			
			this.createZeroClipboard();
		},
		
		onRender: function(ct, position)
		{
			XB.Components.CTCButton.superclass.onRender.call(this, ct, position);

			this.clip.glue(this.getEl().dom);
			this.clip.div.style.zIndex = 9999;

			this.on('move', this.clipMove, this);
			this.refOwner.on('move', this.clipMove, this);
			this.on('show', this.clipShow, this);
			this.refOwner.on('show', this.clipShow, this);
			this.on('hide', this.clipHide, this);
			this.refOwner.on('hide', this.clipHide, this);
		}
	}
);
