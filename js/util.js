
/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

// Standalone utils

XB.Util =
{
	convertDateTimeFromSQLToDMY: function(value)
	{
		if (value)
		{
			return Date.parseDate(value, "Y-m-d h:i:s").format("d.m.Y H:i");
		}
		
		return '';
	},	
	
	roundNumber: function(num, dec)
	{
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		return result;
	},
	
	/**
	* @param string params
	* @param string|array separators
	* @param bool allowMulti make value as array if more than one name found
	* @param bool alwaysMulti always make value as array
	*
	* @return object;
	*/
	explodeParams: function (params, separators, allowMulti, alwaysMulti)
	{
		separators = separators || ';=';
		
		var r = {};
	
		if (params)
		{
			if (! Ext.isArray(separators)) separators = separators.split('');
			
			var pairs = params.split(separators[0]);
			Ext.each
			(
				pairs,
				function (pair)
				{
					var kv = pair.split(separators[1]);
					var k = kv[0];
					var v = kv[1];
					
					if (allowMulti && alwaysMulti)
					{
						if (! r[k]) // new name
						{
							r[k] = []; // create empty array
						}
						
						(r[k]).push(v); // save in array
					}
					else if (allowMulti)
					{
						if (r[k]) // existing name
						{
							if (! Ext.isArray(r[k]))
							{
								r[k] = [r[k]]; // transform scalar to array
							}
							
							(r[k]).push(v); // save in array
						}
						else // new name
						{
							r[k] = v; // save as scalar
						}
					}
					else
					{
						r[k] = v; // save as scalar
					}
				}
			);
		}	
		
		return r;
	},	
	/**
	* @param object params
	* @param string|array separators
	*
	* @return string;
	*/
	implodeParams: function (params, separators)
	{
		separators = separators || ';=';
		
		var r = '';
	
		if (params)
		{
			if (! Ext.isArray(separators)) separators = separators.split('');

			var pairs = [];
			Ext.iterate
			(
				params,
				function (k, v)
				{
					pairs.push(k + separators[1] + v);
				}
			);
			
			r = pairs.join(separators[0]);
		}	
		
		return r;
	},
	arraysToObject: function (a)
	{
		var o = {};
		
		Ext.each
		(
			a,
			function (row)
			{
				var k = row[0];
				var v = row[1];
				
				o[k] = v;
			}
		);
		
		return o;
	},
	objectToArrays: function (o)
	{
		var a = [];
		
		Ext.iterate
		(
			o,
			function (k, v)
			{
				a.push([k, v]);
			}
		);
		
		return a;
	},
	
	JS:
	{
		ucfirst: function (s)
		{
			return s.charAt(0).toUpperCase() + s.slice(1);
		},
		// FIXME: this assignment does not work
		capitalize: this.ucfirst 
	}
};


