	
/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/


XB.P.Components = {};


// Platform components		


Ext.define
(
	'XB.P.Components.Button',
	{
		extend: Ext.Button,
		
		xtype: 'xbp_button',
		
		minWidth: 100,
		cls: 'x-btn-text-icon',
		
		initComponent: function ()
		{
			if (this.iconName)
			{
				this.icon = XB.P.Skin.getIconUrl(this.iconName);
			}


			XB.P.Components.Button.superclass.initComponent.apply(this, arguments);
		}
	}
);


Ext.define
(
	'XB.P.Components.ButtonSave',
	{
		extend: XB.P.Components.Button,
		
		xtype: 'xbp_button_save',
		
		text: _('Save'),
		iconName: 'disk'
	}
);


Ext.define
(
	'XB.P.Components.ButtonCreate',
	{
		extend: XB.P.Components.Button,
		
		xtype: 'xbp_button_create',
		
		text: _('Create'),
		iconName: 'add'
	}
);



Ext.define // deprecated
(
	'XB.P.Components.ButtonCancel', 
	{
		extend: XB.P.Components.Button,
		
		xtype: 'xbp_button_cancel',

		style: 'margin-left: 7px',
					
		text: _('Cancel'),
		iconName: 'cross'
	}
);


Ext.define
(
	'XB.P.Components.ButtonClose',
	{
		extend: XB.P.Components.Button,
		
		xtype: 'xbp_button_close',
		
		style: 'margin-left: 7px',
					
		text: _('Close'),
		iconName: 'cross'
	}
);


XB.P.Components.CTCButton = Ext.extend
(
	XB.Components.CTCButton,
	{
		minWidth: 100,
		cls: 'x-btn-text-icon',
		
		initComponent: function ()
		{
			if (this.iconName)
			{
				this.icon = XB.P.Skin.getIconUrl(this.iconName);
			}


			XB.P.Components.CTCButton.superclass.initComponent.apply(this, arguments);
		},
		
		getValue: function ()
		{
			var value = XB.P.Components.CTCButton.superclass.getValue();

			if (value === null && this.fieldName)
			{
				var f = this.refOwner.formPanel.getForm();
				
				value = f.findField(this.fieldName).getValue();
			}
			
			return value;
		}
	}
);


Ext.define
(
	'XB.P.Components.ToolbarButton',
	{
		extend: Ext.Button,
		
		minWidth: 100,
		ctCls: 'x-btn-over',
		cls: 'x-btn-text-icon',
		
		initComponent: function ()
		{
			if (this.iconName)
			{
				this.icon = XB.P.Skin.getIconUrl(this.iconName);
			}


			XB.P.Components.ToolbarButton.superclass.initComponent.apply(this, arguments);
		}
	}
);


Ext.define
(
	'XB.P.Components.Toolbar',
	{
		extend: Ext.Toolbar,
		
		isButtonConfig: function (o)
		{
			return (!o.isFormField && !o.render && (typeof o != "string") && !o.tagName && (typeof o == "object") && !o.xtype);
		},
		
		constructor: function (config)
		{
			if (Ext.isArray(config))
			{
				for(var i = 0; i < config.length; i++)
				{
					var c = config[i];
					
					if (this.isButtonConfig(c)) // must be button config?
					{
						config[i] = new XB.P.Components.ToolbarButton(c);
					} 
				}
			}
			else if (config.items)
			{
				for(var i = 0; i < config.items.length; i++)
				{
					var c = config.items[i];
					
					if (this.isButtonConfig(c)) // must be button config?
					{
						config.items[i] = new XB.P.Components.ToolbarButton(c);
					} 
				}
			}


			XB.P.Components.Toolbar.superclass.constructor.call(this, config);
		}
	}
);


Ext.define
(
	'XB.P.Components.MenuItem',
	{
		extend: Ext.menu.Item,
		
		_isAction: function (o)
		{
			return o.isAction;
		},
		
		constructor: function (config)
		{
			if (this._isAction(config)) // assumethat action can not contain submenus, only end-point menu item
			{
				var ic = config.initialConfig; // when action, original config is kept in initialConfig
				if (ic.iconName)
				{
					ic.icon = XB.P.Skin.getIconUrl(ic.iconName);
					
					delete ic.iconName;
				}
			}
			else
			{
				if (config.menu && !config.menu.events) // menu config?
				{
					config.menu = new XB.P.Components.Menu(config.menu);
				}
				
				
				if (config.iconName)
				{
					config.icon = XB.P.Skin.getIconUrl(config.iconName);
					
					delete config.iconName;
				}
			}


			XB.P.Components.MenuItem.superclass.constructor.call(this, config);
		}
	}
);


Ext.define
(
	'XB.P.Components.Menu',
	{
		extend: Ext.menu.Menu,
		
		_isMenuItemConfig: function (o)
		{
			return (!o.render && (typeof o != "string") && !o.tagName && !o.el && (typeof o == "object"));
		},
		
		_isAction: function (o)
		{
			return o.isAction;
		},
		
		constructor: function (config)
		{
			if (Ext.isArray(config))
			{
				for(var i = 0; i < config.length; i++)
				{
					var c = config[i];
					
					if (this._isMenuItemConfig(c) || this._isAction(c)) // must be button config?
					{
						config[i] = new XB.P.Components.MenuItem(c);
					} 
				}
			}
			else if (config.items)
			{
				for(var i = 0; i < config.items.length; i++)
				{
					var c = config.items[i];
					
					if (this._isMenuItemConfig(c) || this._isAction(c)) // must be button config?
					{
						config.items[i] = new XB.P.Components.MenuItem(c);
					} 
				}
			}


			XB.P.Components.Menu.superclass.constructor.call(this, config);
		}
	}
);

/*
XB.P.Components.Action = Ext.extend
(
	Ext.Action,
	{
    constructor: function(config)
    {
			XB.P.Components.Action.superclass.constructor.call(this, config);
			
        this.initialConfig = config;
        this.itemId = config.itemId = (config.itemId || config.id || Ext.id());
        this.items = [];
    }
  }
);
*/

Ext.define
(
	'XB.P.Components.ItemSelector',
	{
		extend: Ext.ux.ItemSelector,
		
		xtype: 'xbp_itemselector',
		
		constructor: function (config)
		{
			config.imagePath = XB.P.Config.extjsPluginUrl + 'multiselect';
		
			XB.P.Components.ItemSelector.superclass.constructor.call(this, config);
		}
	}
);


Ext.define
(
	'XB.P.Components.TabPanel',
	{
		extend: Ext.TabPanel,
		
		xtype: 'xbp_tabpanel',
		
		activeTab: 0,
		plain: true,
		deferredRender: false, // render on create instead of on access (this provides access to GridView's this.grid)
		defaults: {autoScroll: true},
		
		enableTabScroll: true,
		
		listeners: 
		{
			tabchange: function (tabPanel, tab)
			{
				tab.doLayout();
			}
		}
	}
);


Ext.define
(
	'XB.P.Components.PopupWindow',
	{
		extend: Ext.Window,
		
		xtype: 'xbp_popupwindow',
		
		renderTo: 'body', // this is the ID of the node; 'body' ID is further assigned to the body tag on Ready

		resizable: false,

		layout: 'fit',

		modal: true,
		closeAction: 'hide',
		
		title: '',
		/*
		previouslyFocusedElement: null,
		show: function ()
		{
			this.previouslyFocusedElement = document.activeElement;
			
			XB.P.Components.PopupWindow.superclass.show.apply(this, arguments);
		},
		hide: function ()
		{
			XB.P.Components.PopupWindow.superclass.hide.apply(this, arguments);
			
			if (this.previouslyFocusedElement)
			{
				var pfe = this.previouslyFocusedElement;
				(function ()
				{
					pfe.focus();
				}).defer(300);
			}
		},
		*/
		previouslyFocusedElement: null,
		listeners:
		{
			beforeshow: function ()
			{
				this.previouslyFocusedElement = document.activeElement;
			},
			hide: function ()
			{
				if (this.previouslyFocusedElement) this.previouslyFocusedElement.focus();
			}
		}
	}
);


Ext.define
(
	'XB.P.Components.PopupFormWindow',
	{
		extend: XB.P.Components.PopupWindow,
		
		constructor: function (config)
		{
			// Apply config default values
			
			var configDefaults =
			{
				width: 490,
			
				fieldLabelWidth: 85,
				fieldDefaults: {width: 373},
				fieldDefaultType: 'textfield',
				
				formWaitMsg: _('Please wait'),
				formParams: {}
			}
			
			Ext.applyIf(config, configDefaults);		
		
			
			// Setup handlers
			
			var onSaveDefault = function () // called in popup scope
			{
				if (this.canClose()) this.hide();
			}
			
			var onSave = 
				config.onSave ?
				onSaveDefault.createInterceptor
				(
					config.onSave
				) :
				onSaveDefault;
			
			delete config.onSave;
			
			
			var onFailureDefault = function(form, action)
			{
				switch (action.failureType)
				{
					case Ext.form.Action.CLIENT_INVALID:
						Ext.Msg.alert(_('Form data error'), _('Please, check the form data'));
					break;
					
					case Ext.form.Action.CONNECT_FAILURE:
						Ext.Msg.alert(_('Connect failure'), _('Response') + ': ' + action.response.status + ': ' + action.response.statusText);				
					break;
					
					default:
					case Ext.form.Action.LOAD_FAILURE:
					case Ext.form.Action.SERVER_INVALID:
						if (action.result)
						{
							XB.P.Util.alertErrors(action.result.errors);
						}
						else
						{
							var title, msg;
							switch (action.failureType)
							{
								case Ext.form.Action.LOAD_FAILURE:   title = _('Unknown error'); msg = 'LOAD FAILURE'; break;	
								case Ext.form.Action.SERVER_INVALID: title = _('Unknown error'); msg = 'SERVER FAILURE'; break;
							}
							Ext.Msg.alert(title, msg);
						}
					break;
				}
			}
			
			var onFailure = config.onFailure || onFailureDefault; delete config.onFailure;
		
			
			var onBeforeSave = config.onBeforeSave || null; delete config.onBeforeSave;
			
			var onSaveClickDefault = function ()
			{
				var popup = this;
				
				if (onBeforeSave)
				{
					if (onBeforeSave.call(popup) === false)
						return;
				}
				
				popup.formPanel.getForm().submit
				(
					{
						waitMsg: popup.formWaitMsg,
						
						params: Ext.apply({}, popup.formParams), // pass a copy of params because Action injects baseParams into original params object
						success: onSave, // function(form, action)
						failure: onFailure,
						scope: popup
					}
				);
			}
		
			var onCancelClickDefault = function ()
			{
				this.hide(); // hide window
			}
			
			// must be set to true between hide()/show() calls when navigating thru records
			this.navigating = false;
			this.goNavigating = function ()
			{
				this.navigating = true;
			}
			
			var onShowInterceptor = function()
			{
				this.navigating = false; // reset if any; must be set before every hide/show cycle when navigating
			}
			
			var onHideInterceptor = function()
			{
				if (! this.navigating) this.canCloseReset(); // do not reset between hide()&show() when navigating thru records 
			}
			
			var onShow = config.onShow ? onShowInterceptor.createInterceptor(config.onShow) : onShowInterceptor; delete config.onShow;
			var onHide = config.onHide ? onHideInterceptor.createInterceptor(config.onHide) : onHideInterceptor; delete config.onHide;
			
			var onSaveClick   = config.onSaveClick   || onSaveClickDefault;   delete config.onSaveClick;
			var onCancelClick = config.onCancelClick || onCancelClickDefault; delete config.onCancelClick;
																								 
			var formConfig =
			{
				xtype: 'form',
				
				url: XB.P.Config.serverUrl,
				method: 'POST',
				
				autoHeight: true,
				frame: true
			};
			if (config.height) // fixed height, big content assumed
			{
				// modify defaults
				formConfig.autoScroll = true;
				formConfig.autoHeight = false;
			}
		
			// Optional form params
			if (! Ext.isEmpty(config.formBaseParams)) { formConfig.baseParams = config.formBaseParams; delete config.formBaseParams; }
			if (! Ext.isEmpty(config.formFileUpload)) { formConfig.fileUpload = config.formFileUpload; delete config.formFileUpload; }
			if (! Ext.isEmpty(config.formAutoHeight)) { formConfig.autoHeight = config.formAutoHeight; delete config.formAutoHeight; }
			if (! Ext.isEmpty(config.formAutoScroll)) { formConfig.autoScroll = config.formAutoScroll; delete config.formAutoScroll; }
		
			// Required form params
			formConfig.labelWidth   = config.fieldLabelWidth;   delete config.fieldLabelWidth;
			formConfig.defaults     = config.fieldDefaults;     delete config.fieldDefaults;
			formConfig.defaultType  = config.fieldDefaultType;  delete config.fieldDefaultType;
			formConfig.items        = config.fieldConfigs;      delete config.fieldConfigs;
			
			// add form to window
			
			config.items = [];
			
			config.items.push(formConfig);  // add form
			
			// form button controls (save/cancel/nav)
		
			// column stub, has children
			var cstub = function(w, cw, f) 
			{
				var cfg =  
				{
					xtype: 'panel',
					unstyled: true
				};
				
				if (w)  cfg.width = w;
				if (cw) cfg.columnWidth = cw;
		
				return Ext.apply(cfg, f);
			}
			// column stub w/width
			var cstubw  = function (w, f) { return cstub(w, null, f); } 
			// column stub w/columnWidth
			var cstubcw = function (cw, f) { return cstub(null, cw, f); }
			
			// column gap, no children, empty box
			var cgap    = function (w, cw, f) { return cstub(w, cw, Ext.apply({html: '&nbsp;'}, f)); }
			// column gap w/width
			var cgapw   = function (w, f) { return cgap(w, null, f); }
			// column gap w/columnWidth
			var cgapcw  = function (cw, f) { return cgap(null, cw, f); }
							
			
			var bbarColumnsCfg =
			[
				cgapcw(.5),
				{
					width: 100,
					xtype: 'panel',
					unstyled: true,
					items:
					[
						{
							xtype: 'xbp_button_create',
							
							handler: onSaveClick,
							scope: this,
							
							_id: 'create',
							hidden: true
						},
						{
							xtype: 'xbp_button_save',
							
							handler: onSaveClick,
							scope: this,
		
							_id: 'save',
							hidden: true
						}
					]
				},
				{
					width: 107,
					xtype: 'panel',
					unstyled: true,
					items:
					{
						xtype: 'xbp_button_close', 
						
						handler: onCancelClick,
						scope: this
					}
				},
				cgapcw(.5)
			];				
			
			if (config.useNav)
			{
				this.canNav = config.navCheckHandler || Ext.emptyFn;
				
				this.adjustNav = function ()
				{
					var adjustNavBtn = function (btn, prev) { if (this.canNav(prev)) btn.enable(); else btn.disable(); };
					
					adjustNavBtn.call(this, this.navPrevBtn, true);
					adjustNavBtn.call(this, this.navNextBtn);
				}
				
				var onPrevClick = config.onPrevClick || Ext.emptyFn; delete config.onPrevClick;
				var onNextClick = config.onNextClick || Ext.emptyFn; delete config.onNextClick;
			
				var nwidth = 68, nbwidth = 53
				var bbarColumnsCfg =
				[
					{
						width: nwidth,
						xtype: 'panel',
						unstyled: true,
						bodyStyle: 'min-height: 1px', //CSS trick: force empty DIV have width, when nav buttons hidden
						items:
						{
							xtype: 'xbp_button',
							
							_id: 'prev',
							
							style: 'margin-left: 14px',
							
							text: _('P.Components.PopupFormWindow.Prev'),
							minWidth: nbwidth,
							iconName: 'resultset_previous',
							
							handler: onPrevClick,
							scope: this,
							
							hidden: true,
							disabled: true
						}
					},
					{
						columnWidth: 1,
						
						xtype: 'panel',
						layout: 'column',
						unstyled: true,
						items: bbarColumnsCfg
					},
					{
						width: nwidth,
						xtype: 'panel',
						unstyled: true,
						bodyStyle: 'min-height: 1px', //CSS trick: force empty DIV have width, when nav buttons hidden
						items:
						{
							xtype: 'xbp_button', 
							
							_id: 'next',
							
							text: _('P.Components.PopupFormWindow.Next'),
							minWidth: nbwidth,
							iconName: 'resultset_next',
							
							handler: onNextClick,
							scope: this,
		
							hidden: true,
							disabled: true
						}
					}
				];
			}
			
			var bbarConfig =
			{
				xtype: 'panel',
				// autoWidth: true,
				// height: 50,
				// html: 'fixed panel',
				// style: 'background-color: #DFE8F6',
				unstyled: true,
				bodyStyle: 'background-color: #DFE8F6; padding-top: 15px; padding-bottom: 15px',
				items: 
				[
					{
						xtype: 'panel',
						layout: 'column',
		
						unstyled: true,
						items: bbarColumnsCfg
					},
					{
						xtype: 'panel',
						layout: 'column',
						unstyled: true,
						bodyStyle: 'padding-top: 3px;',
						items:
						[
							cgapcw(.5),
							cstubw(100,
							{
								layout: {type: 'hbox', pack: 'center'},
								items: 
								{
									// width: 100,
			
									xtype: 'checkbox',
									
									'_id': 'canclose',
									
									hideLabel: true,
									boxLabel: 'и закрыть',
									
									inputValue: 1,
									
									checked: true
								}
							}),
							cgapw(107),
							cgapcw(.5)
						]
					}
				]
			};
			
			config.bbar = bbarConfig;
			
			// set listeners
			
			var defaultListeners = 
			{
				show: onShow,
				hide: onHide
			};
			
			config.listeners = config.listeners || {};
			
			Ext.applyIf(config.listeners, defaultListeners);
			
			// set keys
			config.keys = 
			[
				{
					key: ExtCompatible.EventConstants.ENTER,
					handler: onSaveClick,
					scope: this
				},
				{
					key: ExtCompatible.EventConstants.LEFT,
					ctrl:  true,
					alt:   true,
					shift: false,
					handler: onPrevClick,
					scope: this,
					stopEvent: true
				},
				{
					key: ExtCompatible.EventConstants.RIGHT,
					ctrl:  true,
					alt:   true,
					shift: false,
					handler: onNextClick,
					scope: this,
					stopEvent: true
				}
			];
		
			
			// Call constructor
			XB.P.Components.PopupFormWindow.superclass.constructor.call(this, config);
		
		
			this.formPanel = this.findByType('form')[0];
			
		
			this.formPanel.getForm().waitMsgTarget = this.formPanel.getEl();
			this.formPanel.getForm().waitMsgTarget = this.el;
			this.formPanel.getForm().waitMsgTarget = this.bwrap;
			
			this.btc = function (_id) { return (this.getBottomToolbar().find('_id', _id))[0] }; // find bottom toolbar component
			
			this.canCloseCheckbox = this.btc('canclose');
			
			if (this.useNav)
			{
				this.navPrevBtn = this.btc('prev');
				this.navNextBtn = this.btc('next');
				this.navEnabled = false;
			}
		
			this.saveBtn   = this.btc('save');
			this.createBtn = this.btc('create');
			
			this.save = onSaveClick;
			
			this.getPopup = function (context)
			{
				context = context || this; // if called without params try 'this'
				
				return context.formPanel ? context : context.ownerCt.ownerCt.ownerCt;
			}
		},

		showNav: function ()
		{
			if (! this.navVisible)
			{
				this.navPrevBtn.show();
				// this.navPrevBtn.enable();
				this.navNextBtn.show();
				// this.navNextBtn.enable();
				
				this.navVisible = true;
			}
		},
		hideNav: function ()
		{
			if (this.navVisible)
			{
				this.navVisible = false;
				
				this.navPrevBtn.disable();
				this.navPrevBtn.hide();
				this.navNextBtn.disable();
				this.navNextBtn.hide();
			}
		},
		
		canCloseReset: function () { this.canCloseCheckbox.setValue(true); },
		canClose: function () { return this.canCloseCheckbox.getValue(); },
		
		show: function (mode)
		{
			if (mode == 'add')
			{
				if (this.useNav) this.hideNav();
				
				this.createBtn.show();
				this.saveBtn.hide();
			}
			else
			{
				if (this.useNav)
				{
					this.showNav();
					this.adjustNav();
				}

				this.createBtn.hide();
				this.saveBtn.show();
			}


			// skip first custom argument 'mode'
			var args = [];
			for (var i = 1; i < arguments.length; i++)
			{
				args.push(arguments[i]);
			}
			
			XB.P.Components.PopupFormWindow.superclass.show.apply(this, args);
		}
	}
);


Ext.define
(
	'XB.P.Components.StorePopup',
	{
		extend: XB.P.Components.PopupFormWindow,
	
		constructor: function (config)
		{
			if (! config.store) throw 'Store not set';
		
			this.fieldNames = [];
			this.record = null;
			
			
			var onShowDefault = function(window)
			{
				// var p = editCityFormPanel;
				// var f = p.getForm();
				var p = this.formPanel;
				var f = this.formPanel.getForm();
				var r = this.record;
		
		
				if (r) f.loadRecord(r);
		
				// Focus first focusable field if applicable
				f.items.each
				(
					function (item, index, length)
					{
						var xtype = item.getXType();
						
						// if (xtype && ['infofield'].indexOf(xtype) == -1)
						if (xtype && ['displayfield'].indexOf(xtype) == -1)
						{
							item.focus(false, 500);
							
							return false;
						}
						
						return true;
					}
				);
			}
			
			var onShow = 
				config.onShow ?
				onShowDefault.createSequence
				(
					config.onShow
				) :
				onShowDefault;
				
			config.onShow = onShow;
			
			
			var onHideDefault = function()
			{
				var p = this.formPanel;
				var f = this.formPanel.getForm();
				// var r = this.record;
				
				
				if (p.baseParams)
				{
					for (var param in p.baseParams)
					{
						p.baseParams[param] = null;
					}
				}
				
				f.reset();
				
				this.record = null;
			}
			
			var onHide = 
				config.onHide ?
				onHideDefault.createInterceptor
				(
					config.onHide
				) :
				onHideDefault;
				
			config.onHide = onHide;
		
			
			var onSaveDefault = function(form, action)
			{
				var r = this.record;
				var s = this.store;
				var fn = this.fieldNames;
				
				
				if (r && ! s.reloadOnSave && ! this.reloadOnSave)
				{
					var values = form.getValues();
					
		
					r.beginEdit();
					for (var i = 0; i < fn.length; i++)
					{
						var name = fn[i];
						
						r.set(name, values[name]);
					}
		
		
					r.endEdit();
		
					r.commit();
				}
				else
				{
					s.reload();
				}
			}
			
			var onSave = 
				config.onSave ?
				onSaveDefault.createInterceptor
				(
					config.onSave
				) :
				onSaveDefault;
				
			config.onSave = onSave;
		
			
			// Experimantal
			// set 'navigation' to custom show handler or true for default handler
			if (config.navigation && ! config.store.hasPaging())
			{
				config.useNav = true;
				config.navShowHandler = Ext.isFunction(config.navigation) ? config.navigation : null;
				
				var onNavClick = function (prev)
				{
					// debugger;
					var popup = this;
					var record = popup.record;
					var store = popup.store;
					
					var nextRecord = store.getPrevNextRecord(record, prev);
					
					if (nextRecord)
					{
						popup.goNavigating();
						
						popup.hide();
						
						popup.navShowHandler ? popup.navShowHandler(nextRecord) : popup.show(nextRecord);
					}
					else throw 'No more records';
				}
				var onPrevClickDefault = function () { onNavClick.call(this, true); }
				var onNextClickDefault = function () { onNavClick.call(this); }
				
				config.onPrevClick = config.onPrevClick || onPrevClickDefault;
				config.onNextClick = config.onNextClick || onNextClickDefault;
				// config.onNavShow = config.onNavShow || onNextClickDefault;
				
				var navCheckHandler = function (prev)
				{
					// debugger;
					var popup = this;
					var record = popup.record;
					var store = popup.store;
		
					return store.getPrevNextRecord(record, prev);
				}
				var navCheckHandlerDefault = function (prev) { return navCheckHandler.call(this, prev); }
				
				config.navCheckHandler = config.navCheckHandler || navCheckHandlerDefault;
			}
			else
			{
				config.useNav = false;
			}
			
			
			// Call constructor
			XB.P.Components.StorePopup.superclass.constructor.call(this, config);
		},

		show: function (record)
		{
			this.record = record || null;
			
			
			var mode = record ? 'edit' : 'add';

			
			var args = [mode];
			for (var i = 1; i < arguments.length; i++)
			{
				args.push(arguments[i]);
			}

			XB.P.Components.StorePopup.superclass.show.apply(this, args);
		}
	}
);


Ext.define
(
	'XB.P.Components.TextArea',
	{
		extend: Ext.form.TextArea,
		xtype: 'xbp_textarea',
		
		enableKeyEvents: true,
		listeners:
		{
			keydown: function (field, e)
			{
				var key = Ext.EventObject.getKey();
				if (key === 13)
				{
					e.stopPropagation();
				}
			}
		}
	}
);


Ext.define
(
	'XB.P.Components.RTEPopup',
	{
		extend: Ext.form.Field,
		xtype: 'xbp_rtepopup',
		
		editorBrowserWindow: null,
	
		// Prototype Defaults, can be overridden by user's config object
		width: 400,
		
		height: 100,
		
		// loadingText: _('loading text...'),
		
		buttonText: _('Edit...'),
 
		
		initComponent: function()
		{
			// Before parent code

			Ext.apply
			(
				this,
				{
					autoCreate: {tag: "input", type: "hidden", size: "20", autocomplete: "off"},
					widgets:
					{
						panel: null,
						preview: null,
						button: null
					}
				}
			);


			// Call parent (required)
			XB.P.Components.RTEPopup.superclass.initComponent.apply(this, arguments);

			// After parent code
			// e.g. install event handlers on rendered component

			// ATTENTION! This will not de processed on browser window close
			this.addListener
			(
				'destroy',
				function ()
				{
					if (this.editorBrowserWindow) this.editorBrowserWindow.masterObject = null;
				},
				this
			);
		},
 
		
		storageBaseURL: null, // specify an url to turn feature on
		
		packURLs: function (html)
		{
			var baseURL = this.storageBaseURL;
			
			if (baseURL)
			{
				var replacer = function (match, pBefore, pURL, pAfter, offset)
				{
					if (pURL.indexOf(baseURL) == 0)
					{
						pURL = pURL.replace(baseURL, '{$application.url}');
					}
					
					return pBefore + pURL + pAfter;
				}
 
				var re = new RegExp('(<img\\s.*?src=[\'"])([^\'"]+)([\'"])', 'gi');
				html = html.replace(re, replacer);

				var re = new RegExp('(<a\\s.*?href=[\'"])([^\'"]+)([\'"])', 'gi');
				html = html.replace(re, replacer);
			}
			
			
			return html;
		},
		
		resolveURLs: function (html)
		{
			var baseURL = this.storageBaseURL;
			
			if (baseURL)
			{
				var resolver = function (match, pBefore, pURL, pAfter, offset)
				{
					pURL = pURL.replace('{$application.url}', baseURL);
					
					return pBefore + pURL + pAfter;
				}
				
				var re = new RegExp('(<img\\s.*?src=[\'"])([^\'"]+)([\'"])', 'gi');
				html = html.replace(re, resolver);

				var re = new RegExp('(<a\\s.*?href=[\'"])([^\'"]+)([\'"])', 'gi');
				html = html.replace(re, resolver);
			}
			
			
			return html;
		},
		
		
		setValue: function(value)
		{
			XB.P.Components.RTEPopup.superclass.setValue.apply(this, arguments);


			var resolvedValue = this.resolveURLs(value);
			
			this.widgets.preview.body.update(resolvedValue);			
		},
		
		// deprecated
		getPreviewHtml: function ()
		{
			// return this.getRawValue();
			return this.getResolvedValue();
		},
		
		// deprecated
		setPreviewHtml: function (html)
		{
			// this.setValue(html)
			this.setResolvedValue(html);
		},
		
		getResolvedValue: function ()
		{
			// return this.getRawValue();
			return this.widgets.preview.body.dom.innerHTML;
		},
		
		setResolvedValue: function (html)
		{
			html = this.packURLs(html);
			
			this.setValue(html)
		},
		
		startEditor: function ()
		{
			// Ext.Msg.show({title: CMS.name, msg: 'Запускается редактор...', closable: false});
			Ext.Msg.show({title: XB.P.Config.name, msg: 'Запускается редактор...', closable: false});
			
			editorId = new Date().format('His');


			// var w = window.open(Config.serverUrl + '?module=Default&action=FCK&editor_id=' + editorId, editorId, 'width=800,height=600,resizable=yes,fillscreen=yes');
			var w = window.open(XB.P.Config.serverUrl + '?module=Default&action=CK&editor_id=' + editorId, editorId, 'width=800,height=600,resizable=yes,fillscreen=yes');
			if (w)
			{
				w.masterObject = this;
				
				this.editorBrowserWindow = w;
			}
			else
			{
				Ext.Msg.hide();

				Ext.Msg.alert('Warning', 'Editor window seems to be blocked with popup-blocker');
			}
		},
		stopEditor: function()
		{
			if (this.editorBrowserWindow) this.editorBrowserWindow.close();
		},
		startEditing: function ()
		{
			Ext.Msg.show({title: 'Редактор', msg: 'Идёт редактирование...', closable: false, buttons: {cancel: 'Прервать'}, fn: this.stopEditor.createDelegate(this)});

			this.editorBrowserWindow.focus();
		},
		finishEditing: function ()
		{
			Ext.Msg.hide();
		},
		
		// Override other inherited methods 
		onRender: function(ct, position)
		{
			// Before parent code

			this.widgets.preview = new Ext.Panel
			(
				{
					frame: true,
	
					width: this.width,
					height: this.height - 29,
					
					autoScroll: true,
	
					cls: 'rte-preview-panel'

					// html: this.loadingText
				}
			);
			
			
			var itemConfigs = 
			[
				this.widgets.preview
			];
			
			// if (! this.disabled)
			// {
				itemConfigs.push
				(
					{
						xtype: 'button',

						text: this.buttonText,
						cls: 'x-btn-text-icon',
						icon: XB.P.Config.silkUrl + 'edit.gif',
						minWidth: 90,
						
						hidden: this.disabled,
						
						handler: this.startEditor.createDelegate(this)
					}
				);
			// }
			
			
			this.widgets.panel = new Ext.Panel
			(
				{
					xtype: 'panel',

					items: itemConfigs
				}
			);
			
			
			// Call parent (required)
			XB.P.Components.RTEPopup.superclass.onRender.apply(this, arguments);

			// After parent code
			
			this.widgets.panel.render(ct);
			
			this.widgets.button = this.widgets.panel.findByType('button')[0];
		},
		

    onDisable: function()
    {
    	this.widgets.button.hide();
    	
    	XB.P.Components.RTEPopup.superclass.onDisable.call(this);
    },
    
    onEnable: function()
    {
    	XB.P.Components.RTEPopup.superclass.onEnable.call(this);
    	
    	this.widgets.button.show();
    }		
  }
);


Ext.define
(
	'XB.P.Components.PagingToolbar',
	{
		extend: Ext.ux.SensitivePagingToolbar,
	
		pageSize: 20,
		
		displayInfo: true,
		
		displayMsg:      _('from {0} to {1} shown out of {2}'),
		emptyMsg:        _('empty'),
		beforePageText:  _('page') + '&nbsp;',
		afterPageText:  '&nbsp;' + _('of {0}'),
		firstText:      _('First page'),
		prevText:       _('Previous page'),
		nextText:       _('Next page'),
		lastText:       _('Last page'),
		refreshText:    _('Refresh'),
		
		/*
		items:
		[
			'-',
			_('show by '),
		],
		*/
		
		// onRender: function(ct, position)
		initComponent: function(ct, position)
		{
			// XB.P.Components.PagingToolbar.superclass.onRender.call(this, ct, position);
			
			XB.P.Components.PagingToolbar.superclass.initComponent.call(this);
			
			
			var combo = new Ext.form.ComboBox
			(
				{
					width: 45,
					// style: 'margin-left: 5px',
					
					hideLabel: true,
					labelSeparator: '',
					// fieldLabel: _('Type'),
					name: 'limit',
				
					store: new Ext.data.SimpleStore
					(
						{
							fields: ['limit'],
							data: [[10], [20], [30], [40], [50], [60], [70], [80], [90], [100]]
						}
					),
					editable: true,
					displayField: 'limit',
					valueField: 'limit',
	
					mode: 'local',
					triggerAction: 'all',
					disableKeyTrigger: true,
					
					applyChanges: function ()
					{
						var p = this.parentToolbar;
						
						p.pageSize = parseInt(this.getRawValue());
						p.doLoad(0);
					},
					
					listeners:
					{
						select: function (combo, record, index)
						{
							this.applyChanges();
						},
						specialkey: function (combo, e)
						{
							var k = e.getKey();
							if (k == e.RETURN)
							{
								this.applyChanges();

								e.stopEvent();
							}
						}
					}
				}							
			);


			combo.parentToolbar = this;
			combo.setValue(this.pageSize);
			
			this.insertButton(6, new Ext.Toolbar.TextItem(_('by')));
			/*
			var ti = this.insertField(7, combo);
			// combo.setSize(50, 18);
			// combo.setHeight(18);
			var domEl = ti.getEl();
			var extEl = Ext.get(domEl);
			// extEl.setWidth(50, 18);
			combo.setWidth(45);
			extEl.setWidth(55);
			*/
			this.insert(7, combo);
		}
		
		/*
		insertField: function(index, field)
		{
			var td = this.insertBlock(index);
			field.render(td);
			var ti = new Ext.Toolbar.Item(td.firstChild);
			ti.render(td);
			this.items.insert(index, field);
			return ti;
		},
		
		insertBlock: function(index)
		{
			var td = document.createElement("td");
			this.tr.insertBefore(td, this.tr.childNodes[index]);
			return td;
		}
		*/
	}
);


Ext.define
(
	'XB.P.Components.ImageField',
	{
		extend: Ext.form.TextField,
		xtype: 'xbp_imagefield',
	
		width: 100,
		height: 100,
	
		// emptyUrl: XB.P.Config.serverUrl + 'icons/no.gif',
		url: '',
		baseUrl: '',
		paramName: '',
		
		
		constructor: function (config)
		{
			config.emptyUrl = XB.P.Config.serverUrl + 'icons/no.gif',
		
			XB.P.Components.ImageField.superclass.constructor.call(this, config);
		},
		
		onRender : function(ct, position)
		{
			XB.P.Components.ImageField.superclass.onRender.call(this, ct, position);
	
			this.wrap = this.el.wrap();
			
			this.el.set({disabled: 'disabled'});
			this.el.setVisibilityMode(Ext.Element.DISPLAY);
			this.el.hide();
			
			this.image = this.wrap.insertFirst
			(
				{
					src: this.url || this.emptyUrl,
					tag: 'img'
				}
			);
		},
	
		// private
		getResizeEl : function()
		{
			return this.wrap;
		},
	
		// private
		getPositionEl : function()
		{
			return this.wrap;
		},
	
		// private
		alignErrorIcon : function()
		{
			this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
		},
	
		setValue: function(v)
		{
			XB.P.Components.ImageField.superclass.setValue.call(this, v);
			
			if (v > 0)
			{
				var anticache = (new Date()).format('His');
	
				var paramName = this.paramName || this.name;
	
				var src = this.baseUrl + '&' + paramName + '=' + v + '&anticache=' + anticache;
			}
			else
			{
				var src = this.url || this.emptyUrl;
			}
	
			this.image.set({src: src});
		},
		
		reset : function()
		{
			XB.P.Components.ImageField.superclass.reset.call(this);
			
			var src = this.url || this.emptyUrl;
			this.image.set({src: src});
		}
	}
);



Ext.define
(
	'XB.P.Components.ImageUploadField',
	{
		extend: Ext.Panel,
		xtype: 'xbp_imageuploadfield',
		
		constructor: function(c)
		{
			var baseConfig = 
			{
				name: '', // must be defined on usage
				// fieldLabel: '',
				
				labelWidth: null, // must be defined on usage
				baseUrl: '', // must be defined on usage
				paramName: 'id', // must be defined on usage
				
				ffName: '',
				ffWidth: 360, // must be defined on usage
				// ffLabel: '', // file field: (optional) can be defined on usage
				ffLabelWidth: null, // file field: (optional) can be defined on usage
		
				ifName: '',
				ifWidth: 100, // image field: (optional) can be defined on usage
				ifHeight: 100, // image field: (optional) can be defined on usage
				// ifLabel: '', // image field: (optional) can be defined on usage
				ifLabelWidth: null, // image field: (optional) can be defined on usage
		
				
				// image panel column
				// ipWidth: null,
				ipWidth: 120,
				ipColumnWidth: null,
		
				// file panel column
				fpWidth: null,
				// fpColumnWidth: null,
				fpColumnWidth: 1,
				
				layout: 'column',
		
				autoWidth: true,
				
				items:
				[
					{
						// width: 120,
						
						layout: 'form',
						labelAlign: 'top',
						// labelWidth: 85,
						
						items:
						{
							xtype: 'xbp_imagefield'
							// fieldLabel: _('Image 1'),
							// name: 'image1_id',
			 
							// baseUrl: CMS.serverUrl + '?module=' + self.moduleName + '&action=GetFileInline',
							// paramName: 'id'
						}	
					},
					{
						// columnWidth: 1, // all remaining container
						
						layout: 'form',
						labelAlign: 'top',
						// labelWidth: 85,
						
						items:
						[
							{
								xtype: 'fileuploadfield',
								// fieldLabel: _('Load new image'),
								// name: 'image1_id',
								
								buttonCfg:
								{
									text: _('File...')
								}
							},
							{
								xtype: 'checkbox',
								
								hideLabel: true,
		
								inputValue: 1,
								
								checked: false
								
								// boxLabel: _('delete')
								// fieldLabel: _('Load new image'),
								// name: 'image1_id',
							}
						]
					}
				]
			};
			
			
			Ext.applyIf(c, baseConfig);
			
			
			var ifLabelDefault = _('Image');
			var ffLabelDefault = _('Load new image');
			var cfLabelDefault = _('delete');
		
			
			// image panel config
		
			var ipc = c.items[0];
			
			if (c.labelWidth || c.ifLabelWidth)
			{
				ipc.labelWidth = c.ifLabelWidth ? c.ifLabelWidth : c.labelWidth;
			}
			
			if (c.ipWidth)
			{
				ipc.width = c.ipWidth;
			}
			else if (c.ipColumnWidth)
			{
				ipc.columnWidth = c.ipWidth;
			}
		
			
			// image field config
		
			var ifc = c.items[0].items;
			
			if (c.ifLabel || c.fieldLabel)
			{
				ifc.fieldLabel = c.ifLabel ? c.ifLabel : c.fieldLabel;
			}
			else
			{
				ifc.fieldLabel = ifLabelDefault;
			}
			
			if (c.ifName)
			{
				ifc.name = c.ifName;
			}
			else
			{
				ifc.name = c.name;
			}
			
			ifc.baseUrl = c.baseUrl;
			ifc.paramName = c.paramName;
			
			ifc.width = c.ifWidth;
			ifc.height = c.ifHeight;
		
			
			// file panel config
			
			var fpc = c.items[1];
			
			if (c.labelWidth || c.ffLabelWidth)
			{
				fpc.labelWidth = c.ffLabelWidth ? c.ffLabelWidth : c.labelWidth;
			}
		
			if (c.fpWidth)
			{
				fpc.width = c.fpWidth;
			}
			else if (c.fpColumnWidth)
			{
				fpc.columnWidth = c.fpColumnWidth;
			}
		
			
			// file field config
			
			var ffc = c.items[1].items[0];
			
			if (c.ffLabel || c.fieldLabel)
			{
				ffc.fieldLabel = c.ffLabel ? c.ffLabel : c.fieldLabel;
			}
			else
			{
				ffc.fieldLabel = ffLabelDefault;
			}
			
			if (c.ffName)
			{
				ffc.name = c.ffName;
			}
			else
			{
				ffc.name = c.name;
			}
			
			if (c.ffWidth)
			{
				ffc.width = c.ffWidth;
			}
		
				
			// checkbox field config
			
			var cfc = c.items[1].items[1];
			
			if (c.cfLabel || c.fieldLabel)
			{
				cfc.boxLabel = c.cfLabel ? c.cfLabel : (c.fieldLabel ? c.fieldLabel : cfLabelDefault);
			}
			else
			{
				cfc.boxLabel = cfLabelDefault;
			}
			
			if (c.ffName)
			{
				cfc.name = c.ffName + '__delete';
			}
			else
			{
				cfc.name = c.name + '__delete';
			}
		
			
			delete c.name;
			delete c.fieldLabel;
			delete c.labelWidth;
			delete c.baseUrl;
			delete c.paramName;
			delete c.ffWidth;
			delete c.ifLabelWidth;
			delete c.ffLabelWidth;
			delete c.ipWidth;
			delete c.ipColumnWidth;
			delete c.fpWidth;
			delete c.fpColumnWidth;
			
			
			if (c.orientation == 'vertical')
			{
				c.layout = 'form';
				c.labelAlign = 'top';
				var ifc = c.items[0].items;
				var ffc = c.items[1].items[0];
				var cfc = c.items[1].items[1];
				c.items = [ifc, ffc, cfc];
				delete c.autoWidth;
			}
			
		
			// Call constructor
			XB.P.Components.ImageUploadField.superclass.constructor.call(this, c);
		}
	}
);


Ext.define
(
	'XB.P.Components.DownloadableFileField',
	{
		extend: Ext.form.FileUploadField,
		xtype: 'xbp_dfilefield',
		
		href: '?',
		baseHref: '?',
		idParamName: 'id',
		
		useSetValue: false,
	
		
		initComponent: function()
		{
			// This provides that the field will be FOUND with Ext.form.BasicForm.findField
			// inside Ext.form.BasicForm.setValues and setValues will be TRIGGERED for this field
			// (initially, 'name' attribute is removed in Ext.form.FileUploadField, perhaps because file field
			// value can not be set with JS, so the field can not be found to set value)
			this.dataIndex = this.name;	
			// this.autoHeight = true;	
	
			XB.P.Components.DownloadableFileField.superclass.initComponent.call(this);
			
			this.on
			(
				{
					fileselected: function(field, value)
					{
						// Call superclass' method directly to just set internal value and not modify "attached file indicator"
						XB.P.Components.DownloadableFileField.superclass.setValue.call(this, value);
					}
				}
			);
			
			
			this.resetInProgress = false;
		},
										
		onRender : function(ct, position)
		{
			XB.P.Components.DownloadableFileField.superclass.onRender.call(this, ct, position);
	
			this.wrap2 = this.wrap.wrap({cls:'x-form-field-wrap'});
			
			
			this.fileArea = this.wrap2.createChild
			(
				{
					tag: 'div',
					style: {height: 22},
					children:
					[
						{
							tag: 'a',
							href: '?',
							html: _('download')
						},
						{
							tag: 'span',
							html: _('no file attached')
						}
					]
				}
			);
	
	
			this.downloadAnchor = this.fileArea.child('a');
			this.downloadAnchor.setVisibilityMode(Ext.Element.DISPLAY);
			this.noFileLabel = this.fileArea.child('span');
			this.noFileLabel.setVisibilityMode(Ext.Element.DISPLAY);
			
			// ct.autoHeight();
			this.downloadAnchor.hide();
			this.noFileLabel.show();
		},
	
		// private
		getResizeEl : function()
		{
			return this.wrap2;
		},
	
		// private
		getPositionEl : function()
		{
			return this.wrap2;
		},
	
		// private
		alignErrorIcon : function()
		{
			this.errorIcon.alignTo(this.wrap2, 'tl-tr', [2, 0]);
		},
	
		setValue: function(v)
		{
			// Set internal value by superclass' method call only when reset is triggered,
			// otherwise just further modify "attached file indicator"
			if (this.resetInProgress)
			{
				XB.P.Components.DownloadableFileField.superclass.setValue.call(this, v);
			}
			
			if (v)
			{
				if (this.idParamName)
				{
					this.downloadAnchor.dom.href = this.baseHref + '&' + this.idParamName + '=' + v;
				}
				else
				{
					this.downloadAnchor.dom.href = this.href;
				}
				
				this.downloadAnchor.show();
				this.noFileLabel.hide();
			}
			else
			{
				this.downloadAnchor.dom.href = '';
				this.downloadAnchor.hide();
				this.noFileLabel.show();
			}
		},
		
		reset : function()
		{
			this.resetInProgress = true; // enables parent setValue call, see setValue()
			XB.P.Components.DownloadableFileField.superclass.reset.call(this);
			this.resetInProgress = false; // disables parent setValue call, see setValue()
			
			/*
			These are called in setValue when v == false which is true on reset
			
			this.downloadAnchor.dom.href = '';
			this.downloadAnchor.hide();
			this.noFileLabel.show();
			*/
		}
	
	}
);


Ext.define
(
	'XB.P.Components.DownloadableImageField',
	{
		extend: Ext.form.FileUploadField,
		xtype: 'xbp_dimagefield',
		
		href: '?',
		baseHref: '?',
		idParamName: 'id',
		
		useSetValue: false,
	
		
		initComponent: function()
		{
			// This provides that the field will be FOUND with Ext.form.BasicForm.findField
			// inside Ext.form.BasicForm.setValues and setValues will be TRIGGERED for this field
			// (initially, 'name' attribute is removed in Ext.form.FileUploadField, perhaps because file field
			// value can not be set with JS, so the field can not be found to set value)
			this.dataIndex = this.name;	
			// this.autoHeight = true;	
	
			XB.P.Components.DownloadableImageField.superclass.initComponent.call(this);
			
			this.on
			(
				{
					fileselected: function(field, value)
					{
						XB.P.Components.DownloadableImageField.superclass.setValue.call(this, value);
					}
				}
			);
			
		},
										
		onRender : function(ct, position)
		{
			XB.P.Components.DownloadableImageField.superclass.onRender.call(this, ct, position);
	
			this.wrap2 = this.wrap.wrap({cls:'x-form-field-wrap'});
			
			
			this.fileArea = this.wrap2.createChild
			(
				{
					tag: 'div',
					style: {height: 22},
					children:
					[
						{
							tag: 'a',
							href: '?',
							html: _('download')
						},
						{
							tag: 'span',
							html: _('no file attached')
						}
					]
				}
			);
	
	
			this.downloadAnchor = this.fileArea.child('a');
			this.downloadAnchor.setVisibilityMode(Ext.Element.DISPLAY);
			this.noFileLabel = this.fileArea.child('span');
			this.noFileLabel.setVisibilityMode(Ext.Element.DISPLAY);
			
			// ct.autoHeight();
			this.downloadAnchor.hide();
			this.noFileLabel.show();
		},
	
		// private
		getResizeEl : function()
		{
			return this.wrap2;
		},
	
		// private
		getPositionEl : function()
		{
			return this.wrap2;
		},
	
		// private
		alignErrorIcon : function()
		{
			this.errorIcon.alignTo(this.wrap2, 'tl-tr', [2, 0]);
		},
	
		setValue: function(v)
		{
			
			// XB.P.Components.DownloadableImageField.superclass.setValue.call(this, v);
			
			if (v)
			{
				if (this.idParamName)
				{
					this.downloadAnchor.dom.href = this.baseHref + '&' + this.idParamName + '=' + v;
				}
				else
				{
					this.downloadAnchor.dom.href = this.href;
				}
				
				this.downloadAnchor.show();
				this.noFileLabel.hide();
			}
			else
			{
				this.downloadAnchor.dom.href = '';
				this.downloadAnchor.hide();
				this.noFileLabel.show();
			}
		},
		
		reset : function()
		{
			XB.P.Components.DownloadableImageField.superclass.reset.call(this);
			
			this.downloadAnchor.dom.href = '';
			this.downloadAnchor.hide();
			this.noFileLabel.show();
		}
	
	}
);


Ext.define
(
	'XB.P.Components.TimeField',
	{
		extend: Ext.Panel,
		xtype: 'timefield2',
		
		constructor: function(c)
		{
			var updateRawValue = function ()
			{
				var h = this.hourCombo.getValue();
				var m = this.minuteCombo.getValue();
				var s = this.secondCombo.getValue();
				
				var v = h + ':' + m + ':' + s;
				
				this.value = v;
				
				this.hiddenField.setValue(v);
			}
		
		
			var separatorConfig = 
			{
				width: 5,
				
				html: ':',
				
				style: 'padding: 4px 0 0 1px'
			};
			
			var baseConfig = 
			{
				isFormField: true,
				
				// name: '',
				fieldLabel: '',
		
				autoWidth: true,
				layout: 'column',
				
				items:
				[
					{
						xtype: 'hidden',
						
						isFormField: false,
						
						name: c.name ? c.name : ''
					},
					{
						layout: 'form',
						items:
						{
							xtype: 'combo',
							
							isFormField: false,
							
							width: 45,
							
							_id: 'hour',
							
							hideLabel: true,
							labelSeparator: '',
						
							store: XB.P.Util.timeFieldHourComboStore,
							editable: false,
							displayField: 'value',
							valueField: 'value',
			
							mode: 'local',
							triggerAction: 'all',
							
							listeners:
							{
								select: updateRawValue.createDelegate(this)
							}
						}
					},
					separatorConfig,
					{
						layout: 'form',
						items:
						{
							xtype: 'combo',
							
							isFormField: false,
		
							width: 45,
							
							_id: 'minute',
							
							hideLabel: true,
							labelSeparator: '',
						
							store: XB.P.Util.timeFieldMinuteComboStore,
							editable: false,
							displayField: 'value',
							valueField: 'value',
			
							mode: 'local',
							triggerAction: 'all',
							
							listeners:
							{
								select: updateRawValue.createDelegate(this)
							}
						}
					},
					separatorConfig,
					{
						layout: 'form',
						items:
						{
							xtype: 'combo',
							
							isFormField: false,
		
							width: 45,
						
							_id: 'second',
							
							hideLabel: true,
							labelSeparator: '',
						
							store: XB.P.Util.timeFieldSecondComboStore,
							editable: false,
							displayField: 'value',
							valueField: 'value',
			
							mode: 'local',
							triggerAction: 'all',
							
							listeners:
							{
								select: updateRawValue.createDelegate(this)
							}
						}
					},
					{
						// xtype: 'panel',
						
						columnWidth: 1
					}
				]
			};
			
			
			Ext.applyIf(c, baseConfig);
			
			/*
			if (c.name)
			{
				// hour field name
				c.items[0].hiddenName = c.name + c.postfixes[0];
				// minute field name
				c.items[1].hiddenName = c.name + c.postfixes[1];
				// second field name
				c.items[2].hiddenName = c.name + c.postfixes[2];
				
				
				// delete c.name;
			}
			*/
		
			// Call constructor
			XB.P.Components.TimeField.superclass.constructor.call(this, c);
		
		
			/*
			this.hourCombo = this.items.find(function (item) { return (item._id && item._id == 'hour') ? true : false; });
			this.minuteCombo = this.items.find(function (item) { return (item._id && item._id == 'minute') ? true : false; });
			this.secondCombo = this.items.find(function (item) { return (item._id && item._id == 'second') ? true : false; });
			*/
			this.hourCombo = (this.find('_id', 'hour'))[0];
			this.minuteCombo = (this.find('_id', 'minute'))[0];
			this.secondCombo = (this.find('_id', 'second'))[0];
		
			this.hiddenField = (this.find('xtype', 'hidden'))[0];
		},
		
		getName: function(){
				 return this.name;
		},

		
		initValue: function(){
				if(this.value !== undefined){
						this.setValue(this.value);
				}
				
				// reference to original value for reset
				this.originalValue = this.getValue();
		},

		isDirty: function() {
				if(this.disabled) {
						return false;
				}
				return String(this.getValue()) !== String(this.originalValue);
		},

		// private
		afterRender: function(){
				XB.P.Components.TimeField.superclass.afterRender.call(this);
				
				this.initValue();
		},


		getRawValue : function(){
			return this.value;
		},

		getValue : function(){
			return this.getRawValue();
		},

		setRawValue : function(v){
			// var d = Date.parseDate(v, 'H:i:s');
			// alert('TimeField::setRawValue('+v+')');
			if (v)
			{
				re = /(\d+):(\d+):(\d+)/;
				var matches = re.exec(v);
	 
				this.hourCombo.setValue(matches[1]);
				this.minuteCombo.setValue(matches[2]);
				this.secondCombo.setValue(matches[3]);

				this.value = v;
				
				this.hiddenField.setValue(v);
			}
			else
			{
				this.hourCombo.clearValue();
				this.minuteCombo.clearValue();
				this.secondCombo.clearValue();

				this.value = '';
				
				this.hiddenField.setValue('');
			}
		},

		setValue : function(v){
			this.setRawValue(v);
		},

		reset : function(){
				this.setValue(this.originalValue);
				// this.clearInvalid();
		},


		isValid: function() {return true;},
		validate: function() {return true;},

		markInvalid: Ext.emptyFn,
		getErrorCt: Ext.emptyFn,
		alignErrorIcon: Ext.emptyFn,
		clearInvalid: Ext.emptyFn,
		onFocus: Ext.emptyFn,
		beforeBlur: Ext.emptyFn,
		onBlur: Ext.emptyFn,
		fireKey:  Ext.emptyFn
		// adjustSize: Ext.emptyFn,
		/*
		adjustSize: function(w, h)
		{
				var s = Ext.form.Field.superclass.adjustSize.call(this, w, h);
				s.width = this.adjustWidth(this.el.dom.tagName, s.width);
				return s;
		}
		*/
		
		// adjustWidth: Ext.emptyFn
	}
);


Ext.define('XB.P.Components.ButtonTextField', {extend: XB.Components.ButtonTextField, xtype: 'xbp_buttontextfield'}, {});


Ext.define 
(
	'XB.P.Components.GridRowSelectionModel',
	{
		extend: Ext.grid.RowSelectionModel,
		
		singleSelect: true
	}
);


/*
XB.P.Components.MasterGridView = function (config)
{
	XB.P.Components.MasterGridView.superclass.constructor.call(this, config);

	this.addEvents('rowsremoved');
}

Ext.extend
(
	XB.P.Components.MasterGridView, 
	Ext.grid.GridView,
	{
		initData : function(ds, cm)
		{
					if(this.ds){
							this.ds.un("load", this.onLoad, this);
							this.ds.un("datachanged", this.onDataChange, this);
							this.ds.un("add", this.onAdd, this);
							this.ds.un("remove", this.onRemove, this);
									this.ds.un("removerecords", this.onRemoveRecords, this);
							this.ds.un("update", this.onUpdate, this);
							this.ds.un("clear", this.onClear, this);
					}
					if(ds){
							ds.on("load", this.onLoad, this);
							ds.on("datachanged", this.onDataChange, this);
							ds.on("add", this.onAdd, this);
							ds.on("remove", this.onRemove, this);
									ds.on("removerecords", this.onRemoveRecords, this);
							ds.on("update", this.onUpdate, this);
							ds.on("clear", this.onClear, this);
					}
					this.ds = ds;
	
					if(this.cm){
							this.cm.un("configchange", this.onColConfigChange, this);
							this.cm.un("widthchange", this.onColWidthChange, this);
							this.cm.un("headerchange", this.onHeaderChange, this);
							this.cm.un("hiddenchange", this.onHiddenChange, this);
							this.cm.un("columnmoved", this.onColumnMove, this);
							this.cm.un("columnlockchange", this.onColumnLock, this);
					}
					if(cm){
							delete this.lastViewWidth;
							cm.on("configchange", this.onColConfigChange, this);
							cm.on("widthchange", this.onColWidthChange, this);
							cm.on("headerchange", this.onHeaderChange, this);
							cm.on("hiddenchange", this.onHiddenChange, this);
							cm.on("columnmoved", this.onColumnMove, this);
							cm.on("columnlockchange", this.onColumnLock, this);
					}
					this.cm = cm;
		},
		
		onRemoveRecords : function(ds, records, indexes)
		{
			this.fireEvent("rowsremoved", this, indexes, records);
		}
	}
);
*/

XB.P.Components.MasterGridRowSelectionModel = function(config)
{
	// config = config || {};
	
	// config.view = new XB.P.Components.MasterGridView();
		
	// Call constructor
	XB.P.Components.MasterGridRowSelectionModel.superclass.constructor.call(this, config);
}

Ext.extend 
(
	XB.P.Components.MasterGridRowSelectionModel,
	Ext.grid.RowSelectionModel,
	{
		singleSelect: false,
		
		skipMasterRowEvents: false, // private

	
		initEvents : function ()
		{
			XB.P.Components.MasterGridRowSelectionModel.superclass.initEvents.call(this);
			
			var view = this.grid.view;
			view.on("rowsremoved", this.onRemoveRows, this);
		},
		
		onRefresh : function(){
			
					this.skipMasterRowEvents = true; // tsourick

				var ds = this.grid.store, index;
				var s = this.getSelections();
				this.clearSelections(true);
				for(var i = 0, len = s.length; i < len; i++){
						var r = s[i];
						if((index = ds.indexOfId(r.id)) != -1){
								this.selectRow(index, true);
						}
				}
				
					this.skipMasterRowEvents = false; // tsourick
					
					var sc = this.selections.getCount(); // tsourick
					
				// if(s.length != this.selections.getCount()){
				if(s.length != sc){
						this.fireEvent("selectionchange", this);
						
							// [ tsourick						
							if (sc > 1) // now is > 1
							{
							// THIS SHOULD NEVER HAPPEN !!!
							if (s.length == 1) // was 1 
							{
								alert('bug');
							}
							}
							else if (sc == 1) // now is 1
							{
							if (s.length > 1) // was > 1 
							{
								this.grid.fireEvent("setmasterrow", this.selections.first());
							}
							else if (s.length == 1) // was 1
							{
								// compare rows
								if (this.selections[0] != s[0])
								{
									this.grid.fireEvent("setmasterrow", this.selections.first());
								}
								else
								{
									// nothing changed
								}
							}
							// THIS SHOULD NEVER HAPPEN !!!
							else // i.e. was 0   
							{
								alert('bug');
							}
							
							this.grid.fireEvent("blurmasterrow");
							}
							else // i.e. now is 0
							{
							if (s.length == 1) // was 1 
							{
								this.grid.fireEvent("blurmasterrow");
							}
							}
							// tsourick ]						
				}
		},
		
		onRemove : function(v, index, r){
				if(this.selections.remove(r) !== false){
						this.fireEvent('selectionchange', this);

						
						var s = this.getSelections();
						var new_sc = s.length;
						
						if (new_sc == 0) // one record was selected and it was removed
						{
							this.grid.fireEvent("blurmasterrow");
						}
						else if (new_sc == 1) // two or more records were selected and all were removed except one (one left selected)
						{
							this.grid.fireEvent("setmasterrow", s[0]);
						}
						
				}
		},
		/*			
		onRemoveRows : function(view, indexes, records)
		{
			var sm = this;

			var old_sc = this.selections.getCount();

			if (old_sc > 0) // has records
			{
				Ext.each
				(
					records,
					function (record, index, allRecords)
					{
						if (sm.selections.remove(record) !== false)
						{
							sm.fireEvent('selectionchange', sm);
						}
					}
				);
				

				var s = this.getSelections();
				var new_sc = s.length;
				
				if (old_sc == 1 && new_sc == 0) // one record was selected and it was removed
				{window.status += ' onRemove: blur';
					this.grid.fireEvent("blurmasterrow");
				}
				else if (old_sc > 1 && new_sc == 1) // two or more records were selected and all were removed except one (one left selected)
				{window.status += ' onRemove: set';
					this.grid.fireEvent("setmasterrow", s[0]);
				}
			}
		},
		*/	
	
		selectRecords : function(records, keepExisting){
			
					this.skipMasterRowEvents = true; // tsourick

				if(!keepExisting){
						this.clearSelections();
				}
				var ds = this.grid.store;
				for(var i = 0, len = records.length; i < len; i++){
						this.selectRow(ds.indexOf(records[i]), true);
				}
				
					this.skipMasterRowEvents = false; // tsourick
					
					// [ tsourick
					var s = this.getSelections();
					if (s.length == 1)
					{
					this.grid.fireEvent("setmasterrow", s[0]);
					}
					else // i.e. >0 , (if existing rows were selected) or 0 (if non-existing rows were selected)
					{
					this.grid.fireEvent("blurmasterrow");
					}
					// tsourick ]
					
		},
	
		clearSelections : function(fast){
				if(this.locked) return;

				if(fast !== true){
						var ds = this.grid.store;
						var s = this.selections;
						
							var skipMasterRowEvents = this.skipMasterRowEvents; // tsourick
							this.skipMasterRowEvents = true; // tsourick
						
						s.each(function(r){
								this.deselectRow(ds.indexOfId(r.id));
						}, this);
						s.clear();
						
							this.skipMasterRowEvents = skipMasterRowEvents; // tsourick

				}else{
						this.selections.clear();
				}
				this.last = false;
					
					// [ tsourick				
					if (! this.skipMasterRowEvents)
					{
					this.grid.fireEvent("blurmasterrow");
					}
					// tsourick ]
					
		},
	
		selectAll : function(){
				if(this.locked) return;
				
					this.skipMasterRowEvents = true; // tsourick

				this.selections.clear();
				for(var i = 0, len = this.grid.store.getCount(); i < len; i++){
						this.selectRow(i, true);
				}
				
					this.skipMasterRowEvents = false; // tsourick
					
					// [ tsourick
					var s = this.getSelections();
					if (s.length == 1)
					{
					this.grid.fireEvent("setmasterrow", s[0]);
					}
					else // i.e. >0 , (if existing rows were selected) or 0 (if non-existing rows were selected)
					{
					this.grid.fireEvent("blurmasterrow");
					}
					// tsourick ]

		},
	
		selectRows : function(rows, keepExisting){
				
					this.skipMasterRowEvents = true; // tsourick

				if(!keepExisting){
						this.clearSelections();
				}
				for(var i = 0, len = rows.length; i < len; i++){
						this.selectRow(rows[i], true);
				}
				
					this.skipMasterRowEvents = false; // tsourick
					
					// [ tsourick
					var s = this.getSelections();
					if (s.length == 1)
					{
					this.grid.fireEvent("setmasterrow", s[0]);
					}
					else // i.e. >0 , (if existing rows were selected) or 0 (if non-existing rows were selected)
					{
					this.grid.fireEvent("blurmasterrow");
					}
					// tsourick ]
		},
	
		selectRange : function(startRow, endRow, keepExisting){
				if(this.locked) return;

					this.skipMasterRowEvents = true; // tsourick

				if(!keepExisting){
						this.clearSelections();
				}
				if(startRow <= endRow){
						for(var i = startRow; i <= endRow; i++){
								this.selectRow(i, true);
						}
				}else{
						for(var i = startRow; i >= endRow; i--){
								this.selectRow(i, true);
						}
				}
				
					this.skipMasterRowEvents = false; // tsourick
					
					// [ tsourick				
					var s = this.getSelections();
					if (s.length == 1)
					{
					this.grid.fireEvent("setmasterrow", s[0]);
					}
					else // i.e. >0 , (if existing rows were selected) or 0 (if non-existing rows were selected)
					{
					this.grid.fireEvent("blurmasterrow");
					}
					// tsourick ]

		},

		deselectRange : function(startRow, endRow, preventViewNotify){
				if(this.locked) return;
				
					this.skipMasterRowEvents = true; // tsourick

				for(var i = startRow; i <= endRow; i++){
						this.deselectRow(i, preventViewNotify);
				}
				
					this.skipMasterRowEvents = false; // tsourick
					
					// [ tsourick				
					var s = this.getSelections();
					if (s.length == 1)
					{
					this.grid.fireEvent("setmasterrow", s[0]);
					}
					else // i.e. >0 , (if some rows became selected) or 0 (if no rows became selected)
					{
					this.grid.fireEvent("blurmasterrow");
					}
					// tsourick ]
					
		},
	
		selectRow : function(index, keepExisting, preventViewNotify){
				if(this.locked || (index < 0 || index >= this.grid.store.getCount()) || this.isSelected(index)) return;
				var r = this.grid.store.getAt(index);
				if(r && this.fireEvent("beforerowselect", this, index, keepExisting, r) !== false){
					

						if(!keepExisting || this.singleSelect){
							
									var skipMasterRowEvents = this.skipMasterRowEvents; // tsourick
									this.skipMasterRowEvents = true; // tsourick

								this.clearSelections();
								
									this.skipMasterRowEvents = skipMasterRowEvents; // tsourick

						}
						this.selections.add(r);
						this.last = this.lastActive = index;
						if(!preventViewNotify){
								this.grid.getView().onRowSelect(index);
						}
						this.fireEvent("rowselect", this, index, r);
						this.fireEvent("selectionchange", this);
						
							// [ tsourick						
							if (! this.skipMasterRowEvents)
							{
							var s = this.getSelections();
							if (s.length == 1)
							{
								this.grid.fireEvent("setmasterrow", r);
							}
							else // i.e. >0 , because we process only existing records and min length AFTER select can be only 1
							{
								this.grid.fireEvent("blurmasterrow");
							}
							}
							// tsourick ]
				}
		},
		
		deselectRow : function(index, preventViewNotify){
				if(this.locked) return;
				if(this.last == index){
						this.last = false;
				}
				if(this.lastActive == index){
						this.lastActive = false;
				}
				var r = this.grid.store.getAt(index);
				if(r){
						this.selections.remove(r);
						if(!preventViewNotify){
								this.grid.getView().onRowDeselect(index);
						}
						this.fireEvent("rowdeselect", this, index, r);
						this.fireEvent("selectionchange", this);
						
							// [ tsourick					
							if (! this.skipMasterRowEvents)
							{
							var s = this.getSelections();
							if (s.length == 1)
							{
								this.grid.fireEvent("setmasterrow", s[0]);
							}
							else if (s.length == 0)
							{
								this.grid.fireEvent("blurmasterrow");
							}
							}
							// tsourick ]
				}
		}
		
		
	}
);


XB.P.Components.GridContextToolbox = function (config)
{
}

XB.P.Components.GridPanel = function (config)
{
	// set context menus
	
	if (config.contextMenuConfig)
	{
		// Add 'cancel' item

		var len = config.contextMenuConfig.length;
		if (len > 0)
		{
			// Add separator if not already there
			if (config.contextMenuConfig[len - 1] != '-') config.contextMenuConfig.push('-');
				
			config.contextMenuConfig.push
			(
				{
					text: _('Cancel'),
					iconName: 'cross'
				}
			);
		}


		// Create menu
		// config.contextMenu = new Ext.menu.Menu
		config.contextMenu = new XB.P.Components.Menu
		(
			{defaults: {listeners: {click: function() {this.parentMenu.hide();} } }, items: config.contextMenuConfig}
		);
		
		config.contextMenu.grid = this;
		
		
		delete config.contextMenuConfig;
	}
	
	if (config.groupContextMenuConfig)
	{
		// Add 'cancel' item
		
		var len = config.groupContextMenuConfig.length;
		if (len > 0)
		{
			// Add separator if not already there
			if (config.groupContextMenuConfig[len - 1] != '-') config.groupContextMenuConfig.push('-');
			
			config.groupContextMenuConfig.push
			(
				{
					text: _('Cancel'),
					iconName: 'cross'
				}
			);
		}

		
		// Create menu
		
		// config.groupContextMenu = new Ext.menu.Menu
		config.groupContextMenu = new XB.P.Components.Menu
		(
			{defaults: {listeners: {click: function() {this.parentMenu.hide();} } }, items: config.groupContextMenuConfig}
		);
		
		config.groupContextMenu.grid = this;
		
		
		delete config.groupContextMenuConfig;
	}
	
	if (config.contextMenu || config.groupContextMenu)
	{
		config.listeners = config.listeners || {};
		config.listeners.rowcontextmenu = function(grid, rowIndex, e)
		{
			e.preventDefault();
			
			
			if (grid.getSelectionModel().getSelections().length > 1)
			{
				var c = grid.groupContextMenu || null;
			}
			else
			{
				var c = grid.contextMenu || null;
			}
			
			
			if (c)
			{
				c.contextRecord = grid.getStore().getAt(rowIndex);
				
				if (grid.fireEvent('beforerowcontextmenu', grid, rowIndex, e))
				{
					c.showAt(e.getXY());
				}
			}
		}
	}


	if (! config.selModel)
	{
		config.selModel = new XB.P.Components.MasterGridRowSelectionModel();
	}

	
	/*
	if (config.hasSlaveGrid)
	{
		config.selModel = new XB.P.Components.MasterGridRowSelectionModel();
	}
	else
	{
		config.selModel = new XB.P.Components.GridRowSelectionModel();
	}
	*/
	

	// Add refresh button onto the top toolbar (and toolbar itself is not exists)
	
	if (config.refreshable)
	{
		this.refresh_arg1 = '->';
		this.refresh_arg2 = 
		{
			_id: 'refresher',
			
			minWidth: 25,
			tooltip: _('Refresh'),
			ctCls: 'x-btn-over',
			
			// icon: CMS.extjsUrl + 'resources/images/default/grid/refresh.gif',
			iconCls: "x-tbar-loading",
			
			handler: (function(button, event)
			{
				this.getStore().reload();
			}).createDelegate(this)
		};
		

		if (! config.tbar)
		{
			config.tbar = [];
		}
		

		if (Ext.isArray(config.tbar))
		{
			config.tbar.push(this.refresh_arg1, this.refresh_arg2);
			
			// delete config.refreshable;
			delete this.refresh_arg1;
			delete this.refresh_arg2;
			
			this.dynamicallyAddRefresher = false;
		}
		else
		{
			// Can not add buttons to unrendered Ext.Toolbar object
			// Schedule adding for afterRender
			
			this.dynamicallyAddRefresher = true;
		}
	}
	
	
	// Create filters if any
	var filters = [];
	for (var i = 0; i < config.columns.length; i++)
	{
		var cc = config.columns[i];
		if (cc.filter)
		{
			cc.filter.dataIndex = cc.dataIndex;
			
			filters.push(cc.filter);
			
			// delete cc.filter;
		}
	}

	if (filters.length > 0)
	{
		if (config.store.baseParams.paging)
		{
			var filtersConfig =
			{
				autoReload: true,
				local: false,
				filters: filters
			};
		}
		else
		{
			var filtersConfig =
			{
				autoReload: false,
				local: true,
				filters: filters
			};
		}


		var filtersPlugin = new Ext.ux.grid.GridFilters(filtersConfig);

		config.plugins = filtersPlugin;
	}


	// Call constructor
	XB.P.Components.GridPanel.superclass.constructor.call(this, config);
	
	
	//if (config.hasSlaveGrid)
	//{
		// Split click from dblclick facility

		this.processMouseDownEvent = function (e)
		{
			this.processEvent("mousedown", e);
		}
		
		
		this.mouseDownTask = new Ext.util.DelayedTask();
		
		// override
		this.onMouseDown = function(e)
		{
			var ee = Ext.apply({}, e);
			this.mouseDownTask.delay(400, this.processMouseDownEvent, this, [ee]);
		}
		
		
		// override
		this.onDblClick = function(e)
		{
			this.mouseDownTask.cancel();

			// this.stopSelectEvent = true;

			this.processEvent("dblclick", e);
		}
	//}
}

Ext.extend
(
	XB.P.Components.GridPanel,
	Ext.grid.GridPanel,
	{
		stripeRows: true,
		loadMask: {msg: _('Loading...')},
		

		initComponent: function()
		{
			XB.P.Components.GridPanel.superclass.initComponent.call(this);

			if (this.refreshable)
			{
				this.store.on("beforeload", this.beforeRefreshableLoad, this);
				this.store.on("load", this.afterRefreshableLoad, this);
				this.store.on("loadexception", this.afterRefreshableException, this);
			}
		},
		
		onDestroy: function ()
		{
			if (this.refreshable)
			{
				this.store.un("beforeload", this.beforeRefreshableLoad, this);
				this.store.un("load", this.afterRefreshableLoad, this);
				this.store.un("loadexception", this.afterRefreshableException, this);
			}
			
			XB.P.Components.GridPanel.superclass.onDestroy.call(this);
		},
	
		beforeRefreshableLoad: function()
		{
			if (this.refresher) this.refresher.disable();
			return true;
		},
		
		afterRefreshableLoad: function()
		{
			if (this.refresher) this.refresher.enable();
			return true;
		},

		afterRefreshableException: function(e)
		{
			if (this.refresher) this.refresher.enable();
			return true;
		},
		
		onRender: function(ct, position)
		{
			XB.P.Components.GridPanel.superclass.onRender.call(this, ct, position);
		// onRender: function()
		// {
			// XB.P.Components.GridPanel.superclass.onRender.call(this);
			
			// console.log('rendering ' + this.entityName);
			// Add refresh button onto the top toolbar (if toolbar if already here, and buttons could not be added before render - extjs bug)
			
			if (this.refreshable)
			{
				if (this.dynamicallyAddRefresher)
				{
					this.getTopToolbar().add(this.refresh_arg1, this.refresh_arg2);
					
					// delete this.refreshable;
					delete this.refresh_arg1;
					delete this.refresh_arg2;
				}

				
				this.refresher = this.getTopToolbar().items.find(function (item) { return (item._id && item._id == 'refresher') ? true : false; });
			}
		}
	}
);


XB.P.Components.MasterTreeSelectionModel = Ext.extend
(
	Ext.tree.DefaultSelectionModel,
	{
		listeners:
		{
			selectionchange: function(selectionModel, newNode)
			{
				var sm = selectionModel;
				var smTree = sm.tree;
				

				var scheduledBlurSelection = function ()
				{
					if (this.stopBlurSelectionEvent) this.stopBlurSelectionEvent = false;
					else
					{
						this.tree.fireEvent('blurselection');
					}
					
					this.scheduledBlurSelection = false;
				}

				
				var node = sm.getSelectedNode();

				if (node)
				{
					// record selected
					
					if (sm.scheduledBlurSelection)
					{
						sm.scheduledBlurSelection = false;
						
						sm.stopBlurSelectionEvent = true;
					}
					
					smTree.fireEvent('setselection', node);
				}
				else
				{
					// nothing selected

					sm.scheduledBlurSelection = true;
					
					scheduledBlurSelection.defer(600, sm);
				}
			}
		}
	}
);


XB.P.Components.TreePanel = Ext.extend
(
	Ext.tree.TreePanel,
	{
		constructor: function (config)
		{
			if (config.contextMenuConfig)
			{
				// config.contextMenu = new Ext.menu.Menu
				config.contextMenu = new XB.P.Components.Menu
				(
					Ext.applyIf
					(
						config.contextMenuConfig,
						{
							defaults: {listeners: {click: function() {this.parentMenu.hide();} } }
						}
					)
				);
				
				delete config.contextMenuConfig;
			}
		
			if (config.rootContextMenuConfig)
			{
				// config.rootContextMenu = new Ext.menu.Menu
				config.rootContextMenu = new XB.P.Components.Menu
				(
					Ext.applyIf
					(
						config.rootContextMenuConfig,
						{
							defaults: {listeners: {click: function() {this.parentMenu.hide();} } }
						}
					)
				);
				
				delete config.rootContextMenuConfig;
			}
			
			
			if (config.contextMenu || config.rootContextMenu)
			{
				config.listeners = config.listeners || {};
				config.listeners.contextmenu = function(node, e)
				{
					var tree = node.getOwnerTree();
					
					// alert('node.id: ' + node.id);
					// if (tree.rootContextMenu && node.id == 1)
					if (tree.rootContextMenu && node == tree.root)
					{
						var menu = tree.rootContextMenu;
					}
					else
					{
						var menu = tree.contextMenu;
					}
		
		
					if (tree.fireEvent('beforecontextmenu', menu, node, e))
					{
						menu.contextNode = node;
						menu.showAt(e.getXY());
					}
				}
			}
		
		
			config.selModel = new XB.P.Components.MasterTreeSelectionModel();
			
			/*
			if (config.hasSlaveGrid)
			{
				config.selModel = new XB.P.Components.MasterGridRowSelectionModel();
			}
			else
			{
				config.selModel = new XB.P.Components.GridRowSelectionModel();
			}
			*/
			
		
			// Add refresh button onto the top toolbar (and toolbar itself is not exists)
			
			if (config.refreshable)
			{
				this.refresh_arg1 = '->';
				this.refresh_arg2 = 
				{
					_id: 'refresher',
		
					minWidth: 25,
					tooltip: _('Refresh'),
					ctCls: 'x-btn-over',
		
					// icon: CMS.extjsUrl + 'resources/images/default/grid/refresh.gif',
					iconCls: "x-tbar-loading",
					
					handler: (function(button, event)
					{
						if (this.root.load)
						{
							this.root.collapse(false, false);
							this.root.ui.beforeLoad(this); // this will add 'x-tree-node-loading' CSS class to the node to indicate loading;
																						 // afterLoad() should get called after root.reload() if no errors occur											
							
							this.root.load
							(
								(
									function ()
									{
										this.root.reload();
									}
								).createDelegate(this)
							);
						}
						else
						{
							this.root.reload();
						}
					}).createDelegate(this)
				};
				
		
				if (! config.tbar)
				{
					config.tbar = [];
				}
				
		
				if (Ext.isArray(config.tbar))
				{
					config.tbar.push(this.refresh_arg1, this.refresh_arg2);
					
					// delete config.refreshable;
					delete this.refresh_arg1;
					delete this.refresh_arg2;
					
					this.dynamicallyAddRefresher = false;
				}
				else
				{
					// Can not add buttons to unrendered Ext.Toolbar object
					// Schedule adding for afterRender
					
					this.dynamicallyAddRefresher = true;
				}
			}
			
			
			// Create top toolbar
			
			if (Ext.isArray(config.tbar))
			{
				config.tbar = new XB.P.Components.Toolbar(config.tbar);
			}
			
			
			// Enable node drag-n-drop and appropriate processing 
			
			if (config.movable)
			{
				config.enableDD = true;
				/*
				config.listeners = config.listeners || {};
				config.listeners.beforemovenode = function(tree, node, oldParent, newParent, newIndex)
				{
					return onBeforeMoveGroup(tree, node, oldParent, newParent, newIndex);
				}
				*/
			}
			
			
			// Call constructor
			XB.P.Components.TreePanel.superclass.constructor.call(this, config);
		},

		autoScroll: true,
		
		initComponent: function()
		{
			XB.P.Components.TreePanel.superclass.initComponent.call(this);
			
			if (this.refreshable)
			{
				this.root.on("beforeloadroot", this.beforeRefreshableLoadRoot, this);
				this.root.on("loadroot", this.afterRefreshableLoadRoot, this);
				this.root.on("beforeload", this.beforeRefreshableLoad, this);
				this.root.on("load", this.afterRefreshableLoad, this);
			}
			
			if (this.movable)
			{
				this.on('beforemovenode', this.onBeforeMoveNode, this);
			}
		},
		
		beforeDestroy: function ()
		{
			if (this.movable)
			{
				this.un('beforemovenode', this.onBeforeMoveNode, this);
			}
			
			if (this.refreshable)
			{
				this.root.un("beforeloadroot", this.beforeRefreshableLoadRoot, this);
				this.root.un("loadroot", this.afterRefreshableLoadRoot, this);
				this.root.un("beforeload", this.beforeRefreshableLoad, this);
				this.root.un("load", this.afterRefreshableLoad, this);
			}
			
			XB.P.Components.TreePanel.superclass.beforeDestroy.call(this);
		},
		
		beforeRefreshableLoadRoot: function()
		{
			this.refresher.disable();
			return true;
		},
		
		afterRefreshableLoadRoot: function()
		{
			this.refresher.enable();
			return true;
		},
		
		beforeRefreshableLoad: function()
		{
			this.refresher.disable();
			return true;
		},
		
		afterRefreshableLoad: function()
		{
			this.refresher.enable();
			return true;
		},
		
		onRender: function(ct, position)
		{
			XB.P.Components.TreePanel.superclass.onRender.call(this, ct, position);
		// onRender: function()
		// {
			// XB.P.Components.GridPanel.superclass.onRender.call(this);
			

			// Add refresh button onto the top toolbar (if toolbar if already here, and buttons could not be added before render - extjs bug)
			
			if (this.refreshable)
			{
				if (this.dynamicallyAddRefresher)
				{
					this.getTopToolbar().add(this.refresh_arg1, this.refresh_arg2);
					
					// delete this.refreshable;
					delete this.refresh_arg1;
					delete this.refresh_arg2;
				}
				

				this.refresher = this.getTopToolbar().items.find(function (item) { return (item._id && item._id == 'refresher') ? true : false; });
			}
		},
		
		
		stopBeforeMoveEvent: false,
		
		moveNodeAfter: function(node, siblingNode, asChild)
		{
			var tree = this;
			
			XB.Platform.requestModule
			(
				this.moduleName,
				'MoveEntityNodeAfter',
				{
					params: {entity_name: this.entityName, node_id: node.id, sibling_node_id: siblingNode.id, as_child: asChild},
					success: function ()
					{
						tree.stopBeforeMoveEvent = true;
						
						
						var oldParentNode = node.parentNode;
						
						
						if (asChild)
						{
							var newNode = siblingNode.insertBefore(node, siblingNode.firstChild);

							// siblingNode.reload();
						}
						else
						{
							if (siblingNode.isLast())
							{
								var newNode = siblingNode.parentNode.appendChild(node);
							}
							else
							{
								var newNode = siblingNode.parentNode.insertBefore(node, siblingNode.nextSibling);
							}
							
							// node.reload();
						}
						
						
						var newParentNode = newNode.parentNode;
						
						
						if (oldParentNode != newParentNode)
						{
							delete newParentNode.attributes.children; // force load from server instead in doPreload()
							
							newParentNode.reload();
						}
						else
						{
							newNode.reload();
						}
						
						// node.load(function () {node.reload();});
						// node.ui.updateExpandIcon(); // eliminate bug after node move
					}
				}
			);
				
			return true;
		},
		
		onBeforeMoveNode: function(tree, node, oldParent, newParent, newIndex)
		{
			if (tree.stopBeforeMoveEvent)
			{
				tree.stopBeforeMoveEvent = false;
				
				return true;
			}
			
			var oldIndex = oldParent.indexOf(node);
			
			var siblingNode = null;
			var asChild = null;
			
			if (newIndex == 0)
			{
				siblingNode = newParent;
				asChild = 1;
			}
			else
			{
				siblingNode = newParent.childNodes[newIndex - 1];
				asChild = 0;
			}
			
			if (siblingNode != node)
			{
				var m = new Ext.menu.Menu
				(
					{
						defaults: {listeners: {click: function() {this.parentMenu.hide();} } },
						items:
						[
							/*
							// FIXME: copying implemented for tree node only without copying bindings
							{
								text: _('Copy'),
								icon: XB.P.Config.silkUrl + 'page_copy.gif',
								
								handler: function ()
								{
									copyNode(this.parentMenu.node, this.parentMenu.siblingNode, this.parentMenu.asChild);
								}
							},
							*/
							{
								text: _('Move'),
								// icon: CMS.silkUrl + 'page_go.gif',
								icon: XB.P.Config.silkUrl + 'page_go.gif',
								
								handler: function ()
								{
									var m = this.parentMenu;
									
									Ext.MessageBox.confirm
									(
										_('Moving'),
										_('Are you sure?'),
										function (buttonId)
										{
											if (buttonId == 'yes')
											{
												tree.moveNodeAfter(m.node, m.siblingNode, m.asChild);
											}
										}
									);
								}
							},
							'-',
							{
								text: _('Cancel'),
								icon: XB.P.Config.silkUrl + 'cross.gif'
							}
						]
					}
				);
				
				m.node = node;
				m.siblingNode = siblingNode;
				m.asChild = asChild;
							
				m.showAt(Ext.EventObject.getXY());
			}
			
			return false;
		}
	}
);


XB.P.Components.ButtonBar = Ext.extend
(
	Ext.Panel,
	{
		constructor: function (config)
		{
			// config.bodyStyle = 'margin-top: 15px; margin-bottom: 7px; padding-top: 15px; padding-bottom: 7px;';
			config.bodyStyle = 'padding-top: 15px; padding-bottom: 7px;';
			
			
			// Make column layout and put overridden button configs at the center
			
			config.layout = 'column';
			

			// Override button configs
			
			var gapSize = 7; // button margin in pixels
			var buttonContainerWidth = 100; // button's parent panel width in pixels
			
			var buttonConfigs = []; 
			for (var i = 0; i < config.items.length; i++)
			{
				var bc = config.items[i];

				// Add gap if not first
				if (i > 0)
				{
					bc.style = 'margin-left: ' + gapSize + 'px';
				}
				
				// Create button
				var b = new XB.P.Components.Button(bc);
				
				// Override config
				var width = (i > 0) ? buttonContainerWidth + gapSize : buttonContainerWidth;
				bc =
				{
					width: width,
					
					xtype: 'panel',

					unstyled: true,

					items: b
				};
				
				// Add to button configs
				buttonConfigs.push(bc);
			}


			// Make resulting config
			
			var emptyColumnConfig = // leftmost and rightmost columns; to implement centering
			{
				columnWidth: .5,

				xtype: 'panel',
				html: '&nbsp;',
				
				unstyled: true
			};
			
			var items = [];
			items.push(emptyColumnConfig);
			items = items.concat(buttonConfigs);
			items.push(emptyColumnConfig);
			
			config.items = items;
			
			
			XB.P.Components.ButtonBar.superclass.constructor.call(this, config);
		}
	}
);

Ext.reg('xbp_buttonbar', XB.P.Components.ButtonBar);


Ext.define('XB.P.Components.YesNoComboBox', {extend: XB.Components.YesNoComboBox, xtype: 'xbp_yesnocombo'},   {});

Ext.define('XB.P.Components.IntegerField',  {extend: XB.Components.IntegerField,  xtype: 'xbp_integerfield'}, {});

Ext.define('XB.P.Components.FloatField',    {extend: XB.Components.FloatField,    xtype: 'xbp_floatfield'},   {});

Ext.define('XB.P.Components.MoneyField',    {extend: XB.Components.MoneyField,    xtype: 'xbp_moneyfield'},   {});
