
/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

// Platform

XB.Platform = 
{
	Config:
	{
		// Config options
		/*
		name:            'XB.CMS',
		version:         '2.2.0',
		
		serverUrl:       '<!--{$application.url}-->', 
		extjsUrl:        extjsUrl,
		extjsPluginUrl:  '<!--{$extjs_plugin_url}-->', 
		silkUrl:         '<!--{$silk_url}-->',
		LANG:            '<!--{$application.language}-->', 
		debug:           '<!--{$application.debug}-->',
		
		authModuleName:  'Users'
		*/
	},
	
	createRequestModuleUrl: function (moduleName, actionName, params)
	{
		var _params = {};
		
		_params.module = moduleName;
		if (actionName) _params.action = actionName;
		if (params) _params = Ext.apply(_params, params);
			
		var pairs = [];
		Ext.iterate
		(
			_params,
			function (key, value)
			{
				pairs.push(key + '=' + value);
			}
		);
		
		var serverUrl = this.Config.serverUrl;
		
		var url = serverUrl + '?' + pairs.join('&');
		
		return url;
	},
	
	requestModuleDirect: function(moduleName, actionName, params)
	{
		var url = this.createRequestModuleUrl(moduleName, actionName, params);
		
		window.location.href = url;
	},
	
	requestModule: function(moduleName, actionName, options)
	{
		var o = options;
		
		o.params = o.params || {};
		o.params.module = moduleName;
		o.params.action = actionName;
		
		o.callback = o.callback || null;
		o.success = o.success || null;
		o.failure = o.failure || null;
		o.async = !(o.sync ? true : false);
		o.timeout = o.timeout || 30000; // msec
		
		options = o;
		
		
		var successFlag = false;
		var serverUrl = this.Config.serverUrl;
	
		Ext.Ajax.request
		(
			{
				async: options.async,
				timeout: options.timeout,
				url: serverUrl,
				params: options.params,
				callback: function (_options, success, XHR) // success here is an Ext indicator
				{
					if (options.callback) options.callback();
					
					if (successFlag == true)
					{
						var response = Ext.util.JSON.decode(XHR.responseText);
						
						if (options.success) options.success(response);
					}
					else
					{
						if (options.failure) options.failure();
					}
				},
				success: function (XHR, options)
				{
					var response = {};
					try
					{
						response = Ext.util.JSON.decode(XHR.responseText);
					}
					catch (e) {}
					
					switch (response.success) // this is a flag within recieved data
					{
						case true:
							successFlag = true;
						break;
						
						case false:
							XB.P.Util.alertErrors(response.errors);
						break;
	
						default:
							alert('Protocol error: no \'success\' flag received');
					}
				},
				failure: function ()
				{
					alert(_('Network error') + '.' + "\n" + _('Application will now be reloaded...'));
					
					document.location = serverUrl;					
				}
			}
		);
	},
	
	loadJS: function (config)
	{
		if (! config)
		{
			config = {url: this.Config.serverUrl, params: {}, callback: null};
		}
		else
		{
			config.params = config.params ? config.params : {};
			config.callback = config.callback ? config.callback : null;
		}
		

		var src = config.url + '?' + Ext.urlEncode(config.params);
		loadScript(src, function (){if (config.callback) config.callback();});
	},
	
	loadApplicationModule: function (config)
	{
		var loadSource = function (config)
		{
			// Load base JS
			XB.P.loadJS
			(
				{
					url: serverUrl,
					params: {module: config.name, action: 'GetJSBase', build: window.xb_cms_build || 0},
					callback: function ()
					{
						// Load lang JS
						XB.P.loadJS
						(
							{
								url: serverUrl,
								params: {module: config.name, action: 'GetJSLang', build: window.xb_cms_build || 0},
								callback: function ()
								{
									// Load application base JS
									XB.P.loadJS
									(
										{
											url: serverUrl,
											params: {module: config.name, action: 'GetJSApplicationBase', build: window.xb_cms_build || 0},
											callback: function ()
											{
												// Load main JS
												XB.P.loadJS
												(
													{
														url: serverUrl,
														params: {module: config.name, action: 'GetJS', build: window.xb_cms_build || 0},
														callback: config.callback
													}
												);
											}
										}
									);
								}
							}
						);
					}
				}
			);
		}
		
			
		var serverUrl = this.Config.serverUrl;
		/*
		if (! this.Config.debug)
		{
			loadSource(config);
		}
		else
		{
			if (! XB.ApplicationModules[config.name])
			{
				loadSource(config);
			}
			else
			{
				// Assume already loaded directly from html/head
				config.callback();
			}
		}
		*/
		if (! XB.ApplicationModules[config.name])
		{
			loadSource(config);
		}
		else
		{
			// Assume already loaded directly from html/head
			config.callback();
		}
	}
}

Ext.namespace('XB.Platform.Components');


XB.P = XB.Platform; // Platform shortcut


XB.P.Skin = function ()
{
	return {
		getIconUrl: function(name)
		{
			return XB.P.Config.silkUrl + name + '.gif';
		}
	}
}();


XB.P.ModuleManager = function ()
{
	var launchedModules = new Ext.util.MixedCollection();
	var launchedInteractiveModules = new Ext.util.MixedCollection();

	
	var isLaunched = function (moduleName)
	{
		return launchedModules.containsKey(moduleName);
	}
	
	var isLaunchedInteractive = function (moduleName)
	{
		return launchedInteractiveModules.containsKey(moduleName);
	}
	
	
	var addModRef = function (moduleName)
	{
		var meta = launchedModules.get(moduleName);
		
		meta.refCount++;
	}
	
	var releaseModRef = function (moduleName)
	{
		var meta = launchedModules.get(moduleName);

		if (meta.refCount > 0) meta.refCount--;
		
		return (meta.refCount > 0);
	}

	
	var beforeLaunchInteractive = function()
	{
		Ext.Msg.show({title: _('Please wait'), msg: _('Starting application...'), closable: false});
	}

	var afterLaunchInteractive = function()
	{
		Ext.Msg.hide();
	}
	
	var updateLaunchProgress = function (text)
	{
		if (Ext.Msg.isVisible()) Ext.Msg.updateText(text);
	}
	
	
	var launch = function(moduleName, params, callback)
	{
		if (! params) params = {};
		

		var _moduleName = Ext.util.Format.capitalize(moduleName); // UCFirst

		if (isLaunched(moduleName))
		{
			if (callback)
			{
				var meta = launchedModules.get(moduleName);
				
				callback(meta.ref);
			}
		}
		else
		{
			XB.P.loadApplicationModule
			(
				{
					name: _moduleName,
					callback: function ()
					{
						eval('var m = new XB.ApplicationModules.' + _moduleName + '();');

						launchedModules.add(moduleName, {ref: m, refCount: 0});

						m.onLaunch(params, callback); // <module>.onLaunch MUST call callback
					}
				}
			);
		}
	}
	
	var launchInteractive = function (moduleName, params, callback)
	{
		moduleName = moduleName.toLowerCase();
		// debugger;
		if (isLaunchedInteractive(moduleName))
		{
			var meta = launchedModules.get(moduleName);
			
			meta.ref.onInteractiveActivate
			(
				// onInteractiveReady
				function (m, w) // module, main window
				{
					if (callback) callback(m, w);
				}
			);
		}
		else
		{
			beforeLaunchInteractive();
			
			launch
			(
				moduleName, params,
				function (m)
				{
					addModRef(moduleName);
					
					
					launchedInteractiveModules.add(moduleName, true);
					
					// debugger;
					// XB.P.UIPath.clear();

					
					m.onInteractiveStart
					(
						params,
						// afterInteractiveStart
						function()
						{
							afterLaunchInteractive();
						},
						// onInteractiveReady
						function (m, w) // module, main window
						{
							if (callback) callback(m, w);
						}
					);
				}
			);
		}
	}
	
	var terminateInteractive = function (moduleName)
	{
		if (isLaunchedInteractive(moduleName))
		{
			var meta = launchedModules.get(moduleName);
			
			meta.ref.onInteractiveStop();
			
			launchedInteractiveModules.removeKey(moduleName);


			releaseModRef(moduleName);
		}
	}
	
	var terminate = function (moduleName)
	{
		if (isLaunched(moduleName))
		{
			terminateInteractive(moduleName); // this should not be ever needed because terminate is only run when no more refs exist,
																				// while interactive running mod has at least one ref
			
			var meta = launchedModules.get(moduleName);
			
			meta.ref.onTerminate();

			launchedModules.removeKey(moduleName);
		}
	}
	

	var loadResource = function (moduleName, type, name, params, callback)
	{
		moduleName = moduleName.toLowerCase();
		
		if (isLaunched(moduleName))
		{
			var meta = launchedModules.get(moduleName);

			meta.ref.getResource
			(
				type, name, params,
				function(resource, success)
				{
					if (success) addModRef(moduleName);

					callback(resource, success);
				}
			);
		}
		else
		{
			launch
			(
				moduleName, null,
				function (m)
				{
					m.getResource
					(
						type, name, params,
						function (resource, success)
						{
							if (success) addModRef(moduleName);
							
							callback(resource, success);
						}
					);
				}
			);
		}
	}
	
	var releaseResource = function (moduleName)
	{
		moduleName = moduleName.toLowerCase();
		
		if (! isLaunched(moduleName)) throw 'Could not release resource. Module "' + moduleName + '" was not launched';


		releaseModRef(moduleName);
	}
	
	
	var moduleStore = null;
	var createModuleStore = function()
	{
		moduleStore = new Ext.data.JsonStore
		(
			{
				url: XB.P.Config.serverUrl,
				baseParams: {module: 'Modules', action: 'LoadModuleList'},

				remoteSort: false,
				sortInfo: {field: 'name', direction: 'ASC'},
				
				root: 'list',
				fields:
				[
					{name: 'id', type: 'int'},
					{name: 'name', type: 'string'},
					{name: 'title', type: 'string'},
					{name: 'version', type: 'string'},
					{name: 'type', type: 'string'},
					{name: 'files_enabled', type: 'int'},
					{name: 'available_in_desktop', type: 'int'},
					{name: 'show_in_desktop', type: 'int'},
					{name: 'debug_desktop_load_js', type: 'int'},
					{name: 'debug_builder_load_js', type: 'int'}
				]
			}
		);
		
		return moduleStore;
	}

	var getModuleStore = function()
	{
		if (! moduleStore) throw 'getModuleStore(): moduleStore is null';
	
		return moduleStore;
	}
	


	var moduleEventStore = null;
	var createModuleEventStore = function()
	{
		moduleEventStore = new Ext.data.JsonStore
		(
			{
				url: XB.P.Config.serverUrl,
				baseParams: {module: 'Modules', action: 'LoadAllModuleEventList'},
				// autoLoad: true,

				root: 'list',
				fields:
				[
					{name: 'id', type: 'int'},
					{name: 'module_id', type: 'int'},
					{name: 'name', type: 'string'},
					{name: 'description', type: 'string'}
				]
			}
		);
		
		return moduleEventStore;
	}
	
	var getModuleEventStore = function()
	{
		if (! moduleEventStore) throw 'getModuleEventStore(): moduleEventStore is null';
	
		return moduleEventStore;
	}


	var moduleActionStore = null;
	var createModuleActionStore = function()
	{
		moduleActionStore = new Ext.data.JsonStore
		(
			{
				url: XB.P.Config.serverUrl,
				baseParams: {module: 'Modules', action: 'LoadAllModuleActionList'},
				// autoLoad: true,

				root: 'list',
				fields:
				[
					{name: 'id', type: 'int'},
					{name: 'module_id', type: 'int'},
					{name: 'name', type: 'string'},
					{name: 'description', type: 'string'}
				]
			}
		);
		
		return moduleActionStore;
	}
	
	var getModuleActionStore = function()
	{
		if (! moduleActionStore) throw 'getModuleActionStore(): moduleActionStore is null';
	
		return moduleActionStore;
	}


	function init()
	{
		createModuleStore();
		createModuleEventStore();
		createModuleActionStore();
	}
	
	return {
		init: init,
		
		launchInteractive: launchInteractive, 
		launch: launch,
		terminate: terminate,
		terminateInteractive: terminateInteractive,
		loadResource: loadResource,
		releaseResource: releaseResource,
		updateLaunchProgress: updateLaunchProgress,
		
		getModuleStore: getModuleStore,
		getModuleEventStore: getModuleEventStore,
		getModuleActionStore: getModuleActionStore
	}
}();


XB.P.Desktop = function ()
{
	var self = this; // public
	var _self = {}; // private
	
	var desktop = null;
	// var moduleCollection = new Ext.util.MixedCollection();
	var moduleCollection = null;

	_self.accModuleName = ''; // must be defined on init

		
	var setModuleCollection = function (mcol)
	{
		moduleCollection = mcol.clone();
		
		
		// sort by (core/plugged, localised title)
		moduleCollection.sort('ASC', function (a, b)
		{
			// var aType = (a.type == 'core') ? 1 : 0;
			var aTitle = _(a.title);
			var _a = aTitle;
			
			// var bType = (b.type == 'core') ? 1 : 0;
			var bTitle = _(b.title);
			var _b = bTitle;
			

			return (_a >= _b ? 1 : -1);
		});
	}
	

	var MsgBox = function(text)
	{
		Ext.MessageBox.alert('Application alert', text);
	}


	var logout = function()
	{
		XB.P.requestModule(XB.P.Config.authModuleName, 'Logout', { success: function() {document.location = XB.P.Config.serverUrl;} });
	}


	var initStateManager = function ()
	{
		// Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
	}
		

	var openModule = function(name, params)
	{
		XB.P.ModuleManager.launchInteractive(name, params);
	}
	
	var initDesktop = function()
	{
		// var mcol = XB.P.ModuleManager.getModuleStore().queryBy( function (record, id) {return record.get('show_in_desktop') == '1';} );
		
		var mcol = moduleCollection;
		
		var menuItemConfigs = [];
		mcol.each
		(
			function (item, index, length)
			{
				var moduleName = item.name;
				var moduleTitle = _(item.title);
				var tip = [item.name, item.type, item.version].join(', ');
				
				// if (permission('module_' + moduleName, 'ui', 1))
				// {
					menuItemConfigs.push
					(
						{
							// text: _(item.data.title),
							text: moduleTitle,
							// tooltip: tip,
							handler: function ()
							{
								// window.location.hash = moduleName;
								
								openModule(moduleName);
							},
							icon: XB.P.Config.serverUrl + '?module=' + moduleName + '&action=GetIconImage'
						}
					);
				// }
				
			}
		);

		menuItemConfigs.push
		(
			'-',
			{
				// text: _('Account') + ' ("' + CMS.User.login + '")',
				text: _('Account'),
				icon: XB.P.Config.silkUrl + 'group.gif',

				handler: function ()
				{
					openModule(_self.accModuleName, {interactiveMode: 'profile'});
				}
			},
			'-',
			{
				text: _('Logout'),
				icon: XB.P.Config.silkUrl + 'door_out.gif',

				handler: function ()
				{
					Ext.MessageBox.confirm
					(
						_('Logout'),
						_('Are you sure?'),
						function (buttonId) {if (buttonId == 'yes') logout();}
					);
				}
			}
		);
		
		
		var shortcutConfigs = [];
		mcol.each
		(
			function (item, index, length)
			{
				var moduleName = item.name;
				var moduleTitle = _(item.title);
				
				
				// if (permission('module_' + moduleName, 'ui', 1))
				// {
					shortcutConfigs.push
					(
						{
							id: 'desktopShortcut' + moduleName,
							
							imageSrc: XB.P.Config.serverUrl + '?module=' + moduleName + '&action=GetShortcutImage',
							text: moduleTitle,
							
							callback: function ()
							{
								// window.location.hash = moduleName;
								
								openModule(moduleName);
							}
						}
					);
				// }
			}
		);
		
			
		desktop = new Ext.ux.Desktop
		(
			{
				shortcutConfig:
				{
					style: 'background: url(images/oem_desktop_bg.gif) right top no-repeat #EAF3FF; padding: 5px',
					
					shortcutWidth: 80,
					shortcutHeight: 80,
					
					items: shortcutConfigs
				},
				taskbarConfig:
				{
					// showClock: false,
					
					menuConfig:
					{
						title: _(XB.P.Config.name),
						items: menuItemConfigs
					}
				}
			}
		);
		
		
		desktop.taskbar.addTrayItem
		(
			{
				xtype: 'tbbutton',
				text: _('restart'),
				// minWidth: 100,
				ctCls: 'x-btn-over',
				listeners:
				{
					click: function () {document.location = XB.P.Config.serverUrl;}
				}
			}
		);
		
		
		if (XB.P.Config.debug)
		{
			desktop.taskbar.addTrayItem
			(
				{
					xtype: 'tbbutton',
					text: 'debug mode',
					listeners:
					{
						click: function () {alert('Remember to disable debug mode after deployment!');}
					}
				}
			);
		}
	}
	
	
	return {
		setModuleCollection: setModuleCollection,
		
		// WindowManager: desktop.WindowManager,
		desktop: function ()
		{
			return desktop;
		},
		
		show: function(o)
		{
			var o = o || {};
			
			_self.accModuleName = o.accModuleName;

			
			initStateManager();
			
			Ext.QuickTips.init();
	
			Ext.MessageBox.minWidth = 200;
			
			
			// Fix MessageBox focusing issue
			
			var dlg = Ext.MessageBox.getDialog();
			dlg.previouslyFocusedElement = null;
			dlg.addListener
			(
				'beforeshow',
				function ()
				{
					this.previouslyFocusedElement = document.activeElement;
				},
				dlg
			);
			dlg.addListener
			(
				'hide',
				function ()
				{
					if (this.previouslyFocusedElement) this.previouslyFocusedElement.focus();
				},
				dlg
			);
			
			
			initDesktop();
			
			
			XB.P.UIPath.resolve();
		}
	}
}();


XB.P.Application = Ext.extend
(
	function () {},
	{
		run: function ()
		{
			localize();
	
			
			var bodyTag = Ext.DomQuery.selectNode('body');
			bodyTag.id = 'body';
			
			
			createFileBrowser();
			
			
			XB.P.ModuleManager.init();
		}
	}
);


XB.P.ApplicationModule = function () // Abstract class
{
	var self = this;

	this.permissions = {};

	if (! this.name) throw 'this.name must be specified for a module before calling parent constructor';
		
	this.moduleName = Ext.util.Format.capitalize(this.name);
	this.getName = function()
	{
		return this.name;
	}
	
	
	this.config = function ()
	{
		var collection = XB.P.ModuleManager.getModuleStore().queryBy( function (record, id) {return record.get('name') == self.moduleName;} );
		
		// TRICK: collection.first().data - data property is not actually published according to manual
		return collection.getCount() > 0 ? collection.first().data : {};
	}();
	

	this._ = function(text)
	{
		// Try to localize with local array
		var localizedText = self.MESSAGES[text];

		if (localizedText == null)
		{
			// Try to localize with CMS array
			var localizedText = LC_MESSAGES[text];
		}

		
		return localizedText != null ? localizedText : text;
	}
	
	var _ = this._;

	
	this.createRequestModuleUrl = function(moduleName, actionName, params)
	{
		return XB.Platform.createRequestModuleUrl(moduleName, actionName, params);
	}

	this.createRequestUrl = function(actionName, params)
	{
		return this.createRequestModuleUrl(self.moduleName, actionName, params);
	}

	this.requestModule = function(moduleName, actionName, options)
	{
		XB.Platform.requestModule(moduleName, actionName, options);
	}
	
	this.request = function(actionName, options)
	{
		this.requestModule(self.moduleName, actionName, options);		
	}
	
	this.requestDirect = function (actionName, params)
	{
		XB.Platform.requestModuleDirect(self.moduleName, actionName, params);
	}
	
	this.loadPermissions = function(options)
	{
		options = options || {};
		
		// Ext.Msg.show({title: _('Please wait'), msg: _('preparing application...'), closable: false});
		
		this.requestModule
		(
			XB.P.Config.authModuleName, 'LoadModulePermissionValues',
			{
				params: {name: self.moduleName},
				// callback: function () {Ext.Msg.hide();},
				success: function (response)
				{
					self.permissions = response.permissions;
					
					if (options.success) options.success();
				},
				failure: options.failure || null
			}
		);
	}
	
	this.permission = function (object, permission, value)
	{
		var r = false;
		
		if (self.permissions[object])
		{
			if (self.permissions[object][permission])
			{
				if (self.permissions[object][permission] == value) r = true;
			}
		}
		
		return r;
	}


	// External resources
	
	this.externalResources = new Ext.util.MixedCollection();

	this.loadExternalResources = function (masterCallback)
	{
		// if (! this.useExternalResources) throw 'No external resources defined';
		if (! this.useExternalResources) // no external resources used
		{
			masterCallback(true);
			return;
		}
		
		
		if (this.externalResources.getCount() > 0) throw 'External resources already loaded';
		
		
		var erc = new Ext.util.MixedCollection();
		erc.addAll(this.useExternalResources);
		
		
		var externalResourceLoadedCount = -1;
		
		
		var afterLoad = function (resource, success)
		{
			if (externalResourceLoadedCount > -1) // Not initial load
			{
				if (! success) masterCallback(false); // Fallback after failure
				else
				{
					// Save loaded resource
					
					// var resourceName = externalResourceConfigs[externalResourceLoadedCount].name;
					var resourceAlias = erc.keys[externalResourceLoadedCount];
					
					// externalResources[resourceName] = resource;
					self.externalResources.add(resourceAlias, resource);
				}
			}
			
			
			externalResourceLoadedCount++; // go to next resource config
			
			
			// if (externalResourceLoadedCount < externalResourceConfigs.length)
			if (externalResourceLoadedCount < erc.getCount())
			{
				// Load next resource
				
				// var c = externalResourceConfigs[externalResourceLoadedCount];
				var c = erc.itemAt(externalResourceLoadedCount);
				
				XB.P.ModuleManager.loadResource(c.moduleName, c.type, c.name, c.params, afterLoad);
			}
			else masterCallback(true); // All resources loaded
		}
		
		afterLoad();
	}
	
	this.releaseExternalResources = function ()
	{
		// Release external resources if any
		
		var c = this.externalResources.getCount();
		
		if (c > 0) // loaded resource count
		{
			var erc = new Ext.util.MixedCollection();
			erc.addAll(this.useExternalResources);
			
			for (var i = 0; i < erc.getCount(); i++)
			{
				// XB.P.ModuleManager.releaseResource(externalResourceConfigs[i].moduleName);
				XB.P.ModuleManager.releaseResource(erc.itemAt(i).moduleName);
			}

			this.externalResources.clear();
		
			// externalResourceLoadedCount = -1;
		}
	}
	
	this.getExternalResource = function (alias)
	{
		return this.externalResources.item(alias);
	}


	this.entities = {};
	
	this.loadEntities = function(options)
	{
		options = options || {};
		
		// Ext.Msg.show({title: _('Please wait'), msg: _('preparing application...'), closable: false});
		
		this.request
		(
			'GetEntityList',
			{
				sync: true,
				// params: {name: self.moduleName},
				// callback: function () {Ext.Msg.hide();},
				success: function (response)
				{
					if (! Ext.isEmpty(response.list) && Ext.isArray(response.list) && response.list.length > 0)
					{
						// Init module entities
						
						var convertToBool = function (v)
						{
							return v === true || v === "1"; 
						}

						
						// Create entities, collect sotres

						var fieldStores = [];
						
						Ext.each
						(
							response.list,
							function (item, index, allItems)
							{
								var options =
								{
									create_item_store: convertToBool(item.create_item_store)
								};
					
								// create
								var e = new self.Entity(item.name, item.singular_title, item.plural_title, item.type, options);
								self.entities[item.name] = e;
								
								// collect
								var s = e.createFieldStore()
								fieldStores.push(s);
							}
						);
						
						// Batch load stores
						XB.P.Util.storeBatchLoad
						(
							fieldStores,
							{
								success: function ()
								{
									// Init stores
			
									for (var k in self.entities)
									{
										var e = self.entities[k];
										e.init();
									}
			
									if (options.success) options.success(); // success callback
								},
								failure: options.failure || null // failure callback
							}
						);
					}
					else
					{
						if (options.success) options.success(); // success callback
					}
				},
				failure: options.failure || null // failure callback
			}
		);
	}


	// Field config for a form
	this.getFormFieldConfigByMeta = function (meta)
	{
		var config = null;
		
		
		if (meta.managed === false)
		{
			switch (meta.type)
			{
				case 'string':
				case 'money':
				case 'signed':
				case 'unsigned':
				case 'float_signed':
				case 'float_unsigned':
				case 'double_signed':
				case 'double_unsigned':
				case 'html':
				case 'ext_id':
				case 'strong_ext_id':
					config = 
					{
						anchor: '100%',
						
						// xtype: 'infofield',
						xtype: 'displayfield',
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;
				
				case 'text':
					config = 
					{
						// xtype: 'infofield',
						xtype: 'displayfield',
						
						fieldLabel: _(meta.title),
						name: meta.name,
						
						renderer: function (value)
						{
							return value.replace(/(\r\n|\r|\n)/g, '<br />');
						}
					};
				break;
				
				case 'date':
				case 'date_strong':
				case 'time':
				case 'time_strong':
				case 'datetime':
				case 'datetime_strong':
				case 'created':
				case 'modified':
					config = 
					{
						anchor: '100%',
						
						// xtype: 'infofield',
						xtype: 'displayfield',
						
						fieldLabel: _(meta.title),
						name: meta.name
						/*,
						
						renderer: function (name, value)
						{
							return Date.parseDate(value, "Y-m-d H:i:s");
						}	
						*/
					};
				break;
	
				case 'flag':
				case 'flag_on':
				case 'flag_off':
					config =
					{
						// xtype: 'infofield',
						xtype: 'displayfield',
						
						fieldLabel: _(meta.title),
						name: meta.name,

						renderer: function (name, value)
						{
							if (Ext.type(value) == 'string') value = parseInt(value);
							
							return value ? _('Yes') : _('No');
						}
					}
				break;
			}
		}
		else
		{
			switch (meta.type)
			{
				case 'string':
					config = 
					{
						anchor: '100%',
						
						xtype: 'textfield',
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;
				
				case 'signed':
					config = 
					{
						anchor: '100%',
						
						xtype: 'xbp_integerfield',
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;
				case 'unsigned':
					config = 
					{
						anchor: '100%',
						
						xtype: 'xbp_integerfield',
						
						unsigned: true,
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;
	
				case 'float_signed':
				case 'double_signed':
					config = 
					{
						anchor: '100%',
						
						xtype: 'xbp_floatfield',
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;
				case 'float_unsigned':
				case 'double_unsigned':
					config = 
					{
						anchor: '100%',
						
						xtype: 'xbp_floatfield',
						
						unsigned: true,
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;

				case 'money':
					config = 
					{
						anchor: '100%',
						
						xtype: 'xbp_moneyfield',
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;
				
				case 'time':
				case 'time_strong':
					config = 
					{
						xtype: 'timefield2',
						
						fieldLabel: _(meta.title),
						name: meta.name
					};
				break;

				case 'date':
					config = 
					{
						xtype: 'datefield',
						
						width: 95,

						fieldLabel: _(meta.title),
						name: meta.name,
						
						format: "Y-m-d"
					};
				break;
				
				case 'date_strong':
					config = 
					{
						xtype: 'datefield',
						
						width: 95,

						fieldLabel: _(meta.title),
						name: meta.name,
						
						format: "Y-m-d",
						
						value: new Date().format("Y-m-d")
					};
				break;
				
				case 'datetime':
				case 'datetime_strong':
					config = 
					{
						xtype: 'datefield',
						
						width: 150,
												
						fieldLabel: _(meta.title),
						name: meta.name,
						
						format: "Y-m-d H:i:s",
						
						strict: true
					};
				break;
	
				case 'text':
					config = 
					{
						xtype: 'xbp_textarea',
						
						fieldLabel: _(meta.title),
						name: meta.name,
	
						// width: (meta.width && meta.width != 'auto') ? meta.width : 300,
						width: meta.width == 'auto' ? '100%' : meta.width,
						height: (meta.height && meta.height != 'auto') ? meta.height : 150 // 80
						
						/*
						enableKeyEvents: true,
						listeners:
						{
							keydown: function (field, e)
							{
								var key = Ext.EventObject.getKey();
								if (key === 13)
								{
									e.stopPropagation();
								}
							}
						}
						*/
					};
				break;
				
				case 'html':
					config =
					{
						xtype: 'xbp_rtepopup',
						
						fieldLabel: _(meta.title),
						name: meta.name,
						
						width: meta.width == 'auto' ? '100%' : meta.width,
						height: (meta.height && meta.height != 'auto') ? parseInt(meta.height) : 150 + 29,// 100,
					
						loadingText: _('loading text...'),
						buttonText: _('Edit...'),
						
						storageBaseURL: XB.P.Config.rtePopupStorageBaseUrl ? XB.P.Config.rtePopupStorageBaseUrl : null
					}
				break;
				
				case 'file':
				case 'massstoragefile':
					config =
					{
						// xtype: 'fileuploadfield',
						xtype: 'xbp_dfilefield',
						fieldLabel: _(meta.title),
						name: meta.name,
						buttonCfg:
						{
							text: _('File...')
						},
						baseHref: '?module=' + this.moduleName + '&action=GetFileAttachment',
						
						skipUpdate: true
					};
				break;
				/*
				case 'image':
					config =
					[
						{
							xtype: 'imagefield',
							fieldLabel: _(meta.title),
							name: meta.name,
			 
							// width: 100,
							
							baseUrl: CMS.serverUrl + '?module=' + this.getName() + '&action=GetImageAttachment',
							paramName: 'id'
						},	
						{
							xtype: 'fileuploadfield',
							fieldLabel: _('Load new image'),
							name: meta.name,
							
							buttonCfg:
							{
								text: _('File...')
							}
						}
					];
				break;
				*/			
				case 'flag':
				case 'flag_off':
					config =
					{
						anchor: '100%',
						
						xtype: 'checkbox',
						
						fieldLabel: _(meta.title),
						name: meta.name,
						
    				inputValue: 1
					}
				break;

				case 'flag_on':
					config =
					{
						anchor: '100%',
						
						xtype: 'xcheckbox', // always sends value to server
						
						fieldLabel: _(meta.title),
						name: meta.name,
						
    				inputValue: 1,
    				
    				checked: true,
    				
						submitOffValue: 0,
						submitOnValue: 1
					}
				break;
			}
		}
		
		
		return config;
	}
	
	// Field config for a store
	this.getStoreFieldConfigByMeta = function (meta)
	{
		var config = null;
		
		switch (meta.type)
		{
			case 'string':
			case 'text':
			case 'html':
			case 'date':
			case 'date_strong':
			case 'time':
			case 'time_strong':
			case 'datetime':
			case 'datetime_strong':
			case 'created':
			case 'modified':
				config = {name: meta.name, type: 'string'};
			break;
			
			// case 'image':
			case 'file':
			case 'massstoragefile':
			case 'flag':
			case 'flag_on':
			case 'flag_off':
			case 'pri_id':
			case 'ext_id':
			case 'strong_ext_id':
			case 'signed':
			case 'unsigned':
				config = {name: meta.name, type: 'int'};
			break;

			case 'money':
			case 'float_signed':
			case 'double_signed':
			case 'float_unsigned':
			case 'double_unsigned':
				config = {name: meta.name, type: 'float'};
			break;
		}
		
		
		if (config && meta.sortType) config.sortType = meta.sortType;
		
		
		return config;
	}
	
	// Column config for a grid
	this.getGridColumnConfigByMeta = function (meta)
	{
		var config = null;
		
		switch (meta.type)
		{
			case 'string':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 100,
					sortable: true,
					dataIndex: meta.name,
					
					filter:
					{
						type: 'string'
					}
				};
			break;
				
			case 'text':
			case 'html':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 100,
					sortable: true,
					dataIndex: meta.name
				};
			break;
			
			case 'money':
			case 'signed':
			case 'unsigned':
			case 'float_signed':
			case 'float_unsigned':
			case 'double_signed':
			case 'double_unsigned':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 50,
					sortable: true,
					dataIndex: meta.name
				};
			break;

			case 'pri_id':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 35,
					sortable: true,
					dataIndex: meta.name,
					
					filter:
					{
						type: 'string'
					}
				};
			break;
			
			case 'ext_id':
			case 'strong_ext_id':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 35,
					sortable: true,
					dataIndex: meta.name
				};
			break;
			
			// case 'image':
			case 'file':
			case 'massstoragefile':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 100,
					sortable: true,
					dataIndex: meta.name,
					
					renderer: function (value, metadata, record, rowIndex, colIndex, store)
					{
						return value ? '<a href="?module=' + self.moduleName + '&action=GetFileAttachment&id=' + value + '">' + _('download') + '</a>' : _('no');
					}
				};
			break;

			case 'flag':
			case 'flag_on':
			case 'flag_off':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 35,
					sortable: true,
					dataIndex: meta.name,
					
					renderer: function (value, metadata, record, rowIndex, colIndex, store)
					{
						return value ? _('Yes') : _('No');
					},
					
					filter:
					{
						type: 'list2',
						// dataIndex: meta.name,
						labelField: 'title',
						valueField: 'id',
						options:
						[
							[1, _('Yes')],
							[0, _('No')]
						],
						phpMode: true
						// store: CMS.Components.YesNoComboBoxStore,
						
						// loadOnShow: false
					}
				};
			break;
			
			case 'date':
			case 'date_strong':
			case 'time':
			case 'time_strong':
			case 'datetime':
			case 'datetime_strong':
			case 'created':
			case 'modified':
				config =
				{
					header: _(meta.title),
					width: (meta.column_width && meta.column_width != 'auto') ? parseInt(meta.column_width) : 115,
					sortable: true,
					dataIndex: meta.name
					/*,
					
					renderer: function (value, metadata, record, rowIndex, colIndex, store)
					{
						var r = Date.parseDate(value, 'Y-m-d H:i:s'); 
						return r;
					}
					*/
				};
			break;
		}
		
		return config;
	}
	
	
	/*
		Function options:
			name                          - image field name; default is 'image'
			
		Some underlying component's customizable options (for full list see Components.ImageUploadField in am.<app>.Default.js):
			
			labelWidth:     ?             - regular field label width, inherited from layout
			
			orientation:    'horizontal'  - can be 'vertical'
	
			ifLabel:        _('Image')    - image field label
			
			ipWidth:        120           - image panel width
	
			fpColumnWidth:  1             - file panel column width
			
			paramName:     'id'           - cgi param name for image ID
			
		Notes:
			The default field name for CMS in DB is assumed as <name>_cms_id instead of <name>_mini_id (old style) 
	*/
	this.getImageFieldConfig = function (o)
	{
		o = o || {};
		
		var imageFieldName = 'image';
		if (o.name)
		{
			imageFieldName = o.name;
			delete o.name;
		}
		
		var c =
		{
			xtype: 'xbp_imageuploadfield',
				
			ifName: imageFieldName + '_cms_id',
			ffName: imageFieldName,
			
			baseUrl: XB.P.Config.serverUrl + '?module=' + self.moduleName + '&action=GetFileInline',
		
			ffWidth: 340,
			
			skipUpdate: true
		};
		
		Ext.apply(c, o);

		return c;
	}
	this.getImageFieldConfigLegacy = function (o)
	{
		o = o || {};
		
		o.ifName = 'image_mini_id';
		o.ffName = 'image';
		
		return this.getImageFieldConfig(o);
	}

	this.getImageColumnConfig = function (o)
	{
		o = o || {};
		
		var imageFieldName = 'image';
		if (o.name)
		{
			imageFieldName = o.name;
			delete o.name;
		}
		
		var c =
		{
			header: _('Image'),
			width: 100,
			renderer: function (value, metadata, record, rowIndex, colIndex, store)
			{
				var src = XB.P.Config.serverUrl + '?module=' + self.moduleName + '&action=GetFileInline&id=' + value + '&anticache=' + (new Date()).format('His');
				metadata.attr = 'style="height: 106px; background: url(' + src + ') no-repeat center"';
			},
			dataIndex: imageFieldName + '_cms_id'
		};
		
		Ext.apply(c, o);
		
		return c;
	}
	this.getImageColumnConfigLegacy = function (o)
	{
		o = o || {};
		
		o.dataIndex = 'image_mini_id';
		
		return this.getImageColumnConfig(o);
	}

	
	this.JsonReader = Ext.extend
	(
		Ext.data.JsonReader,
		{
			read : function(response)
			{
					var json = response.responseText;
					var o = eval("("+json+")");
					if(!o) {
							throw {message: "JsonReader.read: Json object not found"};
					}
					
					switch (o.success)
					{
						case true:
							// OK
						break;
						
						case false:
							XB.P.Util.alertErrors(o.errors);
							
							// forces loadexception event to be raised in httpproxy and relayed to store
							// check callback e parameter for exception object
							throw new Error('Success == false');
						break;
						
						default:
							// forces loadexception event to be raised in httpproxy and relayed to store
							// check callback e parameter for exception object
							throw new Error('No SUCCESS flag received');
							// alert('No SUCCESS flag received');
					}
					
					return this.readRecords(o);
			}
		}
	);
	
	this.JsonStore = function(c)
	{
		this.loaded = false;
		
		self.JsonStore.superclass.constructor.call
		(
			this,
			Ext.apply
			(
				c,
				{
					proxy: c.proxy || (!c.data ? new Ext.data.HttpProxy({url: c.url, async: !(c.sync ? true : false)}) : undefined),
					reader: new self.JsonReader(c, c.fields)
				}
			)
		);
		
		this.addListener
		(
			'load',
			function (store, records, options)
			{
				store.loaded = true;
			},
			this
		);
			
	};
	Ext.extend
	(
		this.JsonStore,
		Ext.data.GroupingStore,
		{
			getFullParams: function ()
			{
				var fullParams = {};
				
				// Make fake load() call to intercept real parameters.
				// Call load(), abort the request on beforeload and intercept parameters
				this.on
				(
					'beforeload',
					function (store, options)
					{
						var params = Ext.apply({}, options.params); // make new object
						fullParams = Ext.apply(params, this.baseParams); // add base params

						return false; // abort load
					},
					this,
					{single: true}
				);
				
				this.load(); // no async behaviour is here as the request is not even started
				
				
				return fullParams;
			}
		}
	);


	this.ListStore = function (config)
	{
		Ext.apply
		(
			config,
			{
				url: XB.P.Config.serverUrl,
				
				// action: ?, // should be given
				// paging: ? // may be given (1|0)
				// pageSize: ? // may be given [int]
				// fields: ? // should be given <standard fields config>
				// sync: (true|false) // synchronous mode
				
				root: 'list',
	
				totalProperty: 'total_count'
			}
		);
		
		config.listeners = config.listeners || {};
		Ext.apply
		(
			config.listeners,
			{
				loadexception: function (store, options, response, e)
				{
					if (e)
					{
						// alert('loadexception: thrown exception with message: ' + e.message);
					}
					else
					{
						alert(response.statusText + ': '+ response.responseText);
					}
				}
			}
		);


		// Before parent code

		var baseParams = config.baseParams || {};
		baseParams.module = self.moduleName;
		baseParams.action = config.action;
		/*
		var autoLoad = config.autoLoad || false;

		if (config.paging)
		{
			baseParams.paging = 1;
			
			if (autoLoad)
			{
				var pageSize = config.pageSize || 20;
				autoLoad = {params: {start: 0, limit: pageSize}};
			}
		}
		
		Ext.apply
		(
			config,
			{
				baseParams: baseParams,
				autoLoad: autoLoad
			}
		);
		*/
		if (config.paging)
		{
			baseParams.paging = 1;
			// baseParams.start = 0;
			// baseParams.limit = config.pageSize || 20;
		}
		
		
		if (! config.remoteSort)
		{
			if (config.paging)
			{
				config.remoteSort = true;
			}
			else
			{
				config.remoteSort = false;
			}
		}


		Ext.apply
		(
			config,
			{
				baseParams: baseParams
			}
		);

		
		// Move custom params to xbconfig
		
		var xbconfig = Ext.copyTo({}, config, 'action,paging,pageSize');
		
		delete config.action;
		delete config.paging;
		delete config.pageSize;


		self.ListStore.superclass.constructor.call(this, config);
		
		this.xbconfig = xbconfig;
	}
	
	Ext.extend
	(
		this.ListStore,
		this.JsonStore,
		{
			// remoteSort: true
			
			hasPaging: function ()
			{
				return this.baseParams.paging;
			}			
		}
	);
	
	// deprecated ???
	this.ListFieldMetaStore = function (config)
	{
		Ext.apply
		(
			config,
			{
				fields:
				[
					{name: 'id', type: 'int'},
					{name: 'name', type: 'string'},
					{name: 'type', type: 'string'},
					{name: 'title', type: 'string'},
					{name: 'list', type: 'int'},
					{name: 'push_to_store', type: 'int'},
					{name: 'width', type: 'int'},
					{name: 'height', type: 'int'},
					{name: 'column_width', type: 'int'},
					{name: 'column_expand', type: 'int'},
					{name: 'entity_title', type: 'int'}
				]
			}
		);


		self.ListFieldMetaStore.superclass.constructor.call(this, config);
	}
	
	Ext.extend(this.ListFieldMetaStore, this.ListStore, {});

	// deprecated ???
	this.TreeFieldMetaStore = function (config)
	{
		Ext.apply
		(
			config,
			{
				fields:
				[
					{name: 'id', type: 'int'},
					{name: 'name', type: 'string'},
					{name: 'type', type: 'string'},
					{name: 'title', type: 'string'},
					{name: 'node_text', type: 'int'},
					{name: 'push_to_store', type: 'int'},
					{name: 'width', type: 'int'},
					{name: 'height', type: 'int'}
				]
			}
		);


		self.TreeFieldMetaStore.superclass.constructor.call(this, config);
	}
	
	Ext.extend(this.TreeFieldMetaStore, this.ListStore, {});
	
	
	this.FieldListStore = function (config)
	{
		Ext.apply
		(
			config,
			{
				fields:
				[
					{name: 'name', type: 'string'},
					{name: 'title', type: 'string'},
					{name: 'type', type: 'string'},

					{name: 'entity_title', convert: function(v) { return v === true || v === "1"; }},
					{name: 'push_to_store', convert: function(v) { return v === true || v === "1"; }},
					{name: 'list', convert: function(v) { return v === true || v === "1"; }},
					{name: 'width', type: 'string'},
					{name: 'height', type: 'string'},
					{name: 'column_width', type: 'string'},
					{name: 'column_expand', convert: function(v) { return v === true || v === "1"; }},
					{name: 'form', convert: function(v) { return v === true || v === "1"; }},
					{name: 'managed', convert: function(v) { return v === true || v === "1"; }},
					{name: 'default_sort', convert: function(v) { return v === true || v === "1"; }},
					{name: 'default_sort_order', type: 'string'}
				]
			}
		);


		self.FieldListStore.superclass.constructor.call(this, config);
	}
	
	Ext.extend(this.FieldListStore, this.ListStore, {});

	
	this.entityItemStoreCustomFields = {};
	
	this.Entity = function (name, singularTitle, pluralTitle, type, options)
	{
		var entity = this;
		
		this.name = name;
		this.singularTitle = singularTitle;
		this.singularTitleUCF = Ext.util.Format.ucfirst(this.singularTitle);
		this.pluralTitle = pluralTitle != '' ? pluralTitle : this.singularTitle + 's';
		this.pluralTitleUCF = Ext.util.Format.ucfirst(this.pluralTitle);
		this.type = type;
		
		this.options = options;

		this.fieldStore = null;
		this.itemStore = null;
		this.rootNode = null;
		
		this.titleFieldName = 'id';
		this.sortFieldName = 'id';
		this.sortFieldOrder = 'ASC';
		
		this.reloadOnSave = false;


		this.getTitle = function (variant)
		{
			var title = '';
			
			switch (variant)
			{
				case 's':    title = this.singularTitle;    break;
				case 'sUCF': title = this.singularTitleUCF; break;
				case 'p':    title = this.pluralTitle;      break;
				case 'pUCF': title = this.pluralTitleUCF;   break;
			}
			
			return title;
		}
		
		
		this.loadRootNode = function (callback)
		{
			var node = this;
			
			
			if (node.fireEvent("beforeloadroot", this) === false)
			{
				return;
			}
			
			
			self.request
			(
				'LoadEntityNodeData',
				{
					// params: {id: 1, entity_name: entity.name},
					params: {id: node.id, entity_name: entity.name},
					success: function (response)
					{/*
						var s = entity.fieldStore;
						var sTotal = s.getCount();
						
						var titleFieldName = entity.titleFieldName;
						var fieldNames = [];
						
						for (var i = 0; i < sTotal; i++)
						{
							var meta = s.getAt(i).data;
							
							
							if (meta.entity_title) titleFieldName = meta.name;
							
							
							fieldNames.push(meta.name);
						}
						
						
						var values = response.data;
	
						
						var nodeValues = {};
						
						for (var i = 0; i < fieldNames.length; i++)
						{
							var name = fieldNames[i];
							
							nodeValues[name] = values[name];
						}

						// custom attributes
						nodeValues.name = values['name']; 


						node.setText(values[titleFieldName]); // update node text

						node.attributes.values = nodeValues;
						*/


						node.attributes.values = response.data;

						node.setText(response.data[entity.titleFieldName]); // update node text
						
						var qtipCfg = response.data['qtipCfg'] ? response.data['qtipCfg'] : (response.data['qtip'] ? {text: response.data['qtip']} : null); 
						if (qtipCfg && node.setTooltip)
						{
							var text = qtipCfg['text'] || ''; 
							var title = qtipCfg['title'] || '';
							node.setTooltip(text, title); // update node text
						}


						node.fireEvent("loadroot", this);

							
						callback();
					}
				}
			);
		}
		

		// NEW FUNCTIONALITY !!! BATCH LOAD !!! 
		// MUST BE CALLED BEFORE INIT
		this.createFieldStore = function ()
		{
			this.fieldStore = new self.FieldListStore({action: 'GetEntityFieldList', baseParams: {entity_name: this.name}, sync: true});
			
			return this.fieldStore;
		}
		
		this.init = function ()
		{
			if (! this.fieldStore) throw new 'No field store created for entity "' + this.name + '"';

			/*
			var s = new self.FieldListStore({action: 'GetEntityFieldList', baseParams: {entity_name: this.name}, sync: true});
			s.load
			(
				{
					callback: function (records, options, success)
					{
						if (success)
						{
							// Set entity title field name
							Ext.each
							(
								records,
								function (record)
								{
									if (record.get('entity_title') == 1)
									{
										entity.titleFieldName = record.get('name');
									}
									if (record.get('default_sort') == 1)
									{
										entity.sortFieldName = record.get('name');
										entity.sortFieldOrder = record.get('default_sort_order');
									}
									
									return true; 
								}
							);
						}
						else
						{
							throw new Error('CMS.Module: Could not load field meta store');
						}
					}
				}
			);
			
			this.fieldStore = s;
			*/
			
			var setEntitySpecNames = function (records)
			{
				// Set entity title field name
				Ext.each
				(
					records,
					function (record)
					{
						if (record.get('entity_title') == 1)
						{
							entity.titleFieldName = record.get('name');
						}
						if (record.get('default_sort') == 1)
						{
							entity.sortFieldName = record.get('name');
							entity.sortFieldOrder = record.get('default_sort_order');
						}
						
						return true; 
					}
				);
			}
			
			
			var s = this.fieldStore;
			
			if (! s.loaded)
			{
				s.load // sync store, loaded sync-ly
				(
					{
						callback: function (records, options, success)
						{
							if (success)
							{
								setEntitySpecNames(records);
							}
							else
							{
								throw new Error('CMS.Module: Could not load field meta store');
							}
						}
					}
				);
			}
			else
			{
				setEntitySpecNames(s.getRange());
			}
			
			
			// Create item store
			if (this.options.create_item_store)
			{
				switch (this.type)
				{
					case 'plain':
					case 'plain_timestamped':
						this.itemStore = this.createItemStore();
					break;
				
					case 'celko':
						// Here root node should be created, but
						// currently nodes are not reusable, so it should be re-created for every new tree
						// this.rootNode = this.createRootNode();
					break;
				
					default:
						throw new Error("CMS.Module: Unrecognized entity type '" + this.type + "'");
				}
			}
		}
		
		this.getFieldStore = function ()
		{
			return this.fieldStore;
		}

		this.getItemStore = function ()
		{
			return this.itemStore;
		}

		this.getRootNode = function ()
		{
			return this.rootNode;
		}

		this.getTitleFieldName = function ()
		{
			return this.titleFieldName;
		}
		
		this.getSortFieldName = function ()
		{
			return this.sortFieldName;
		}

		this.getSortFieldOrder = function ()
		{
			return this.sortFieldOrder;
		}

		
		this.createItemStore = function (storeConfig)
		{
			var s = this.getFieldStore();
			var sTotal = s.getCount();
			
			
			var fieldConfigs = [];
			
			// Add dynamic fields
			for (var i = 0; i < sTotal; i++)
			{
				var meta = s.getAt(i).data;
				
				if (meta.push_to_store)
				{
					var config = self.getStoreFieldConfigByMeta(meta);
					
					if (config)
					{
						fieldConfigs.push(config);
					}
					else alert('Entity "' + this.name + '" ::createItemStore(): Unrecognized base field type "' + s.getAt(i).data.type + '"');
				}
			}

			// Add custom fields if any
			if
			(
				! Ext.isEmpty(self.entityItemStoreCustomFields[this.name])
				&& Ext.isArray(self.entityItemStoreCustomFields[this.name])
				&& self.entityItemStoreCustomFields[this.name].length > 0
			)
			{
				for (var i = 0; i < self.entityItemStoreCustomFields[this.name].length; i++)
				{
					var meta = self.entityItemStoreCustomFields[this.name][i];
					
					if (meta.push_to_store)
					{
						var config = self.getStoreFieldConfigByMeta(meta);
						
						if (config)
						{
							fieldConfigs.push(config);
						}
						else alert('Entity "' + this.name + '" ::createItemStore(): Unrecognized custom field type "' + meta.type + '"');
					}
				}
			}
	
	
			// Override base options with config provided if any
			
			var storeBaseConfig =
			{
				// action: 'GetEntityItemDataList',
				action: 'LoadEntityItemDataList',
				baseParams: {entity_name: this.name},
				fields: fieldConfigs,
				sortInfo: {field: this.sortFieldName, direction: this.sortFieldOrder},
				
				reloadOnSave: this.reloadOnSave
			};
			
			
			if (storeConfig)
			{
				if (storeConfig.baseParams)
				{
					storeBaseConfig.baseParams = Ext.apply(storeBaseConfig.baseParams, storeConfig.baseParams);
					
					delete storeConfig.baseParams;
				}
				
				if (storeConfig.fields)
				{
					storeBaseConfig.fields = Ext.apply(storeBaseConfig.fields, storeConfig.fields);
					
					delete storeConfig.fields;
				}
				
				if (storeConfig.sortInfo)
				{
					storeBaseConfig.sortInfo = Ext.apply(storeBaseConfig.sortInfo, storeConfig.sortInfo);
					
					delete storeConfig.sortInfo;
				}

				storeBaseConfig = Ext.apply(storeBaseConfig, storeConfig);
			}
			
			
			return new self.ListStore(storeBaseConfig);
		}
		
		
		this.createItemComboStore = function (storeConfig)
		{
			var applyRemove = function (obj, config, key)
			{
				if (key in config)
				{
					obj[key] = Ext.apply(obj[key], config[key]);
					
					delete config[key];
				}
			}
			
			// set handler which adds "null" item at 0 position on every load
			var setAddNullItemHook = function (base, config, titleFieldName)
			{
				var nv = config.nullValue || '0';
				var nt = config.nullText  || ('- ' + _('[it] not selected') + ' -');
				
				base.listeners = 
				{
					load: (function (store, records, options,  valueFieldName, nullValue, titleFieldName, nullText)
					{
						var tc = {}; tc[valueFieldName] = nullValue; tc[titleFieldName] = nullText;
						var r = new store.recordType(tc);
						
						store.stopLocalEvents = true; // stopprevent inserting the record in master store
						store.insert(0, [r]); // this triggers record insert on master store too
						store.stopLocalEvents = false;
					}).createDelegate(window, ['id', nv, titleFieldName, nt], true)
				}
			}
			
			
			var comboStore = null;
			
			
			storeConfig = storeConfig || {};
			
			if (storeConfig.paging)
			{
				// Implicit loading available (by combobox).
				// Data is split into pages.
				
				var tfn = this.getTitleFieldName();
				
				var s = this.getFieldStore();
				var meta = s.findRecord('name', tfn).data;

				var fieldConfigs = 
				[
					{name: 'id', type: 'int'}, // PRI key field
					self.getStoreFieldConfigByMeta(meta) // title field
				];
				
	
				// Override base options with config provided if any
				
				var storeBaseConfig =
				{
					// action: 'GetEntityItemDataList',
					action: 'LoadEntityItemDataList',
					baseParams: {entity_name: this.name},
					fields: fieldConfigs,
					sortInfo: {field: this.sortFieldName, direction: this.sortFieldOrder}
				};
				

				if (storeConfig.allowNull)
				{
					setAddNullItemHook(storeBaseConfig, storeConfig, tfn);
				}
				
				
				if (storeConfig)
				{
					// individual add ins (overriding throw in params if any)
					
					applyRemove(storeBaseConfig, storeConfig, 'baseParams');
					applyRemove(storeBaseConfig, storeConfig, 'fields');
					applyRemove(storeBaseConfig, storeConfig, 'sortInfo');

					// override the whole rest
					storeBaseConfig = Ext.apply(storeBaseConfig, storeConfig);
				}
				
				
				comboStore = new self.ListStore(storeBaseConfig);
			}
			else if (storeConfig.masterStore)
			{
				// Interactive slave store with copy of master data created and maintained on events.
				// No paging.
				
				var tfn = this.getTitleFieldName();
				var sfn = this.getSortFieldName();
				var sfo = this.getSortFieldOrder();
				
				
				var sortInfo = (sfn == 'id' || sfn == tfn) ? {field: sfn, direction: sfo} : {}
				
				
				var storeBaseConfig =
				{
					sortInfo: sortInfo,
	
					masterStore: storeConfig.masterStore,
					
					lookupFilterFn: function (record, id) {return true} // all records
				};
				
				
				if (storeConfig.allowNull)
				{
					setAddNullItemHook(storeBaseConfig, storeConfig, tfn);
				}
				
				
				comboStore = new Ext.ux.LookupStore(storeBaseConfig);
			}
			else
			{
				// Explicit loading with loadData() required.
				// Data should be passed to loadData() as MixedCollection object.
				
				// Override base options with config provided if any
				
				var tfn = this.getTitleFieldName();
				var sfn = this.getSortFieldName();
				var sfo = this.getSortFieldOrder();
				
				
				var sortInfo = (sfn == 'id' || sfn == tfn) ? {field: sfn, direction: sfo} : {}
				
				var storeBaseConfig =
				{
					fields: ['id', tfn],
					reader: new Ext.ux.MixedCollectionReader({}, ['id', tfn]),
					sortInfo: sortInfo
				};
				
				
				if (storeConfig.allowNull)
				{
					setAddNullItemHook(storeBaseConfig, storeConfig, tfn);
				}

				
				// individual add ins (overriding throw in params if any)
				
				applyRemove(storeBaseConfig, storeConfig, 'fields');
				applyRemove(storeBaseConfig, storeConfig, 'sortInfo');

				// override the whole rest
				storeBaseConfig = Ext.apply(storeBaseConfig, storeConfig);


				comboStore = new Ext.data.Store(storeBaseConfig);
			}
			
			
			return comboStore;
		}


		this.createRootNode = function (configOverride)
		{
			// Loader base config
			var treeLoaderConfig =
			{
				dataUrl: XB.P.Config.serverUrl,
				baseParams: {module: self.moduleName, action: 'LoadEntityNodeDataList', entity_name: this.name},
				listeners:
				{
					loadexception: function (treeLoader, node, response) {alert(response.statusText + ': '+ response.responseText);}
				}
			};
			
			// Override loader config if given
			if (configOverride && configOverride.treeLoaderConfig)
			{
				Ext.apply(treeLoaderConfig, configOverride.treeLoaderConfig);
				
				delete configOverride.treeLoaderConfig;
			}
			

			// Root node base config
			var rootNodeConfig = 
			{
				id: 1,
				text: '',
				expanded: true,
				
				// iconCls: 'pageIconN',
				
				loader: new Ext.tree.TreeLoader(treeLoaderConfig)
			};
			
			// Override loader config if given
			if (configOverride)
			{
				Ext.apply(rootNodeConfig, configOverride);
			}
			
			
			var rootNode = new Ext.tree.AsyncTreeNode(rootNodeConfig);
			
	    rootNode.addEvents('beforeloadroot', 'loadroot');
			rootNode.load = this.loadRootNode;
			
			
			return rootNode;
		}
		
		
		this.getFormConfigs = function ()
		{
			var s = this.getFieldStore();
			var sTotal = s.getCount();
			
			var fieldNames = [];
			var fieldConfigs = [];
		
			for (var i = 0; i < sTotal; i++)
			{
				var meta = s.getAt(i).data;
				
				
				if (! meta.form) continue; // check for 'show in a form'
				
				
				if (meta.type == 'pri_id') continue; // skip PRI key
				
				
				var config = self.getFormFieldConfigByMeta(meta);
				

				if (meta.managed && ! config.skipUpdate) fieldNames.push(meta.name);
				
				
				if (config)
				{
					fieldConfigs.push(config);
				}
				else alert('Entity "' + this.name + '" ::getFormConfigs(): Unrecognized field type "' + meta.type + '"');
			}
			
			
			return {fieldNames: fieldNames, fieldConfigs: fieldConfigs};
		}
		

		this.getColumnConfigs = function ()
		{
			var s = this.getFieldStore();
			var sTotal = s.getCount();
			
			var autoExpandColumnIndex = null;
			var autoExpandColumnName = null;
			
			var columnConfigs = [];
			
			for (var i = 0; i < sTotal; i++)
			{
				var meta = s.getAt(i).data;
	
				
				if (! meta.list) continue;
				
				
				var config = self.getGridColumnConfigByMeta(meta);
				
				if (config)
				{
					columnConfigs.push(config);
				}
				else alert('Entity "' + this.name + '" ::getColumnConfigs(): Unrecognized field type "' + meta.type + '"');
				
				if (meta.column_expand)
				{
					autoExpandColumnIndex = columnConfigs.length - 1;
					autoExpandColumnName = meta.name;
				}
			}
			
			
			return {autoExpandColumnIndex: autoExpandColumnIndex, autoExpandColumnName: autoExpandColumnName, columnConfigs: columnConfigs};
		}
		
		
		this.getFormFieldConfig = function (name, overrides, metaOverrides)
		{
			var config = null;


			var s = entity.getFieldStore();
			var r = s.findRecord('name', name);
			
			
			if (! r) throw 'No field named "' + name + '" found within entity "' + entity.name + '"';
			

			// Apply meta overrides if any
			
			var meta = Ext.apply({}, r.data); // make copy
			if (metaOverrides)
			{
				Ext.apply(meta, metaOverrides);
			}
			
			
			// Make config
			
			var config = self.getFormFieldConfigByMeta(meta);

			
			// Apply config overrides if any
			
			if (overrides)
			{
				Ext.apply(config, overrides);
			}
			
			
			return config;
		}
		
		this.getColumnConfig = function (name, overrides)
		{
			var config = null;


			var s = entity.getFieldStore();
			var r = s.findRecord('name', name);
			
			
			if (! r) throw 'No field named "' + name + '" found within entity "' + entity.name + '"';
			
			
			var config = self.getGridColumnConfigByMeta(r.data);

			if (! config) throw 'Entity "' + this.name + '" ::getColumnConfig(): Unrecognized field type "' + meta.type + '"';

			
			if (overrides)
			{
				Ext.apply(config, overrides);
			}
			
			
			return config;
		}
	}
	
	
	this.EntityFieldFormPopup = function()
	{
		var that = 
		{
			popup: null,
			formPanel: null,
			record: null,
			store: null
		}
		
		var fieldNames = ['name', 'type', 'title', 'push_to_store', 'width', 'height', 'list', 'column_width', 'column_expand', 'entity_title'];

		var onShowPopup = function()
		{
			if (that.record) that.formPanel.getForm().loadRecord(that.record);

			that.formPanel.items.first().focus(false, 500);
		}

		var onHidePopup = function()
		{
			that.formPanel.baseParams.entity_name = '';
			that.formPanel.baseParams.id = 0;
			
			that.formPanel.getForm().reset();
			
			that.record = null;
			that.store = null;
		}
		
		var onSaveClick = function()
		{
			that.formPanel.getForm().submit
			(
				{
					params: {action: 'SaveEntityField'},
					success: function(form, action)
					{
						if (that.record)
						{
							var values = form.getValues();
							

							that.record.beginEdit();
							for (var i = 0; i < fieldNames.length; i++)
							{
								var name = fieldNames[i];
								
								that.record.set(name, values[name]);
							}

							that.record.endEdit();
							
							
							that.record.commit();
						}
						else
						{
							that.store.reload();
						}

						that.popup.hide();
					},
					failure: function(form, action)
					{
						XB.P.Util.alertErrors(action.result.errors);
					}
				}
			);
		}
		
		var onCancelClick = function ()
		{
			that.popup.hide();
		}

		that.popup = new Ext.Window
		(
			{
				renderTo: Ext.getBody(),

				width: 560,
				// height: 490,
				resizable: false,

				layout: 'fit',

				modal: true,
				closeAction: 'hide',

				title: '',
				
				items:
				[
					{
						xtype: 'form',
						
						url: XB.P.Config.serverUrl,
						method: 'POST',
						baseParams: {module: self.moduleName, id: 0},
						
						autoHeight: true,
						frame: true,
	
						labelWidth: 85, // label settings here cascade unless overridden
						defaults: {width: 360},
						defaultType: 'textfield',
						
						items:
						[
							{
								fieldLabel: _('Name'),
								name: 'name'
							},
							{
								xtype: 'combo',
								
								fieldLabel: _('Type'),
								hiddenName: 'type',
							
								store: new Ext.data.SimpleStore
								(
									{
										fields: ['type'],
										data:
										[
											['string'],
											['text'],
											['html'],
											['flag']
										]
									}
								),
								editable: false,
								displayField: 'type',
								valueField: 'type',
				
								mode: 'local',
								triggerAction: 'all'
							},							
							{
								fieldLabel: _('Title'),
								name: 'title'
							},
							{
								xtype: 'checkbox',
								
								fieldLabel: _('Push to store'),
								name: 'push_to_store',
								
								checked: true,
								
								inputValue: 1
							},
							{
								fieldLabel: _('Width'),
								name: 'width'
							},
							{
								fieldLabel: _('Height'),
								name: 'height'
							},
							{
								xtype: 'checkbox',
								
								fieldLabel: _('Show in a list'),
								name: 'list',
								
								inputValue: 1
							},
							{
								fieldLabel: _('Column width'),
								name: 'column_width'
							},
							{
								xtype: 'checkbox',
								
								fieldLabel: _('Expand column'),
								name: 'column_expand',
								
								inputValue: 1
							},
							{
								xtype: 'checkbox',
								
								fieldLabel: _('Entity title'),
								name: 'entity_title',
								
								inputValue: 1
							}
						],
						buttons:
						[
							{
								text: _('Save'),
								minWidth: 100,
 								cls: 'x-btn-text-icon',
								icon: XB.P.Config.silkUrl + 'disk.gif',

								handler: onSaveClick
							},
							{
								text: _('Cancel'),
								minWidth: 100,
 								cls: 'x-btn-text-icon',
								icon: XB.P.Config.silkUrl + 'cross.gif',

								handler: onCancelClick
							}
						]
					}
				],
				listeners:
				{
					show: onShowPopup,
					hide: onHidePopup
				},
				keys:
				[
					{
						key: Ext.EventObject.ENTER,
						handler: onSaveClick
					}
				]
			}
		);

		that.formPanel = that.popup.findByType('form')[0];
		
		
		this.show = function (entityName, title, record, store)
		{
			that.formPanel.baseParams.entity_name = entityName;
			that.formPanel.baseParams.id = record ? record.get('id') : 0;
	
			that.record = record;
			that.store = store;
			that.popup.setTitle(title);
			that.popup.show();
		}
	}
	
	
	this.PopupFormWindow = function (config)
	{
		// config.formBaseParams = config.formBaseParams || {};
		config.formParams = config.formParams || {};
		
		// Inject module param
		// config.formBaseParams.module = self.moduleName;
		config.formParams.module = self.moduleName;


		// Call constructor
		self.PopupFormWindow.superclass.constructor.call(this, config);
	}
	
	Ext.extend
	(
		this.PopupFormWindow,
		XB.P.Components.PopupFormWindow,
		// CMS.Components.StorePopup,
		{}
	);
	
	
	this.StorePopup = function (config)
	{
		// config.formBaseParams = config.formBaseParams || {};
		config.formParams = config.formParams || {};
		
		// Inject module param
		// config.formBaseParams.module = self.moduleName;
		config.formParams.module = self.moduleName;


		// Call constructor
		self.StorePopup.superclass.constructor.call(this, config);
	}
	
	Ext.extend
	(
		this.StorePopup,
		// CMS.Components.PopupFormWindow,
		XB.P.Components.StorePopup,
		{}
	);


	// (optional) onCustomTitle:  custom title handler
	//														function(recordTitle, mode)
	//															recordTitle - value of record's title field
	//															mode - (add|edit)
	//														must return string of title
	// (optional) useCustomTitle: turns off auto titles if no 'onCustomTitle' specified
	//														"true" - no auto title applied,
	//														otherwise - applied if applicable
	
	this.EntityPopup = function (config)
	{
		if (! config.entityName) throw 'entityName not set';
			
		var entity = self.entities[config.entityName];
			
		config.formParams = config.formParams || {};
		
		// Inject action & antity_name params
		config.formParams.action = 'SaveEntityItemData';
		config.formParams.entity_name = config.entityName;

		if (! config.store)
		{
			config.store = entity.getItemStore();
			
			if (! config.store) throw '"' + config.entityName + '" popup store not set';
		}

		
		// Add field configs (predefined and custom)

		var fieldNames = [];
		var fieldConfigs = [];
		
		var fc = entity.getFormConfigs();
		
		config.fieldNames = fc.fieldNames;
		config.fieldConfigs = fc.fieldConfigs;
		
		if (config.customFieldConfigs)
		{
			var unshiftConfigs = [];
			var pushConfigs = [];
			
			var addNames = function (c)
			{
				if (c.items) // c is container
				{
					if (Ext.isArray(c.items))
					{
						for (var i = 0; i < c.items.length; i++)
						{
							addNames(c.items[i]);
						}
					}
					else
					{
						addNames(c.items);
					}
				}
				else if ((c.name || c.hiddenName) && ! c.skipUpdate)
				{
					var name = c.name ? c.name : c.hiddenName;
					
					config.fieldNames.push(name);
				}
			}
			
			for (var k in config.customFieldConfigs)
			{
				var c = config.customFieldConfigs[k];
				
				
				if (k == '+')
				{
					if (Ext.isArray(c))
					{
						for (var i = 0; i < c.length; i++)
						{
							pushConfigs.push(c[i]);
						}
					}
					else
					{
						pushConfigs.push(c);
					}
				}
				else if (k == '-')
				{
					if (Ext.isArray(c))
					{
						for (var i = 0; i < c.length; i++)
						{
							unshiftConfigs.push(c[i]);
						}
					}
					else
					{
						unshiftConfigs.push(c);
					}
				}
				else
				{
					var index = parseInt(k);

					if (Ext.isArray(c))
					{
						for (var i = 0; i < c.length; i++)
						{
							config.fieldConfigs.splice(index, 0, c[i]);
							
							addNames(c[i]);
							/*
							if (c[i].name && ! c[i].skipUpdate)
							{
								config.fieldNames.push(c[i].name);
							}
							*/
							
							index++;
						}
					}
					else
					{
						config.fieldConfigs.splice(index, 0, c);
						
						addNames(c);
						/*
						if (c.name && ! c.skipUpdate)
						{
							config.fieldNames.push(c.name);
						}
						*/
					}
				}
			}
			
			
			if (unshiftConfigs.length > 0)
			{
				var index = 0;
				
				for (var i = 0; i < unshiftConfigs.length; i++)
				{
					var c = unshiftConfigs[i];
					
					config.fieldConfigs.splice(index, 0, c);
					
					addNames(c);
					/*
					if (c.name && ! c.skipUpdate)
					{
						config.fieldNames.push(c.name);
					}
					*/
					
					index++;
				}
			}
			
			if (pushConfigs.length > 0)
			{
				for (var i = 0; i < pushConfigs.length; i++)
				{
					var c = pushConfigs[i];
					
					config.fieldConfigs.push(c);
					
					addNames(c);
					/*
					if (c.name && ! c.skipUpdate)
					{
						config.fieldNames.push(c.name);
					}
					*/
				}
			}
		}
		

		// Apply formFileUpload if any file field found
		
		var hasFileFields = function (c)
		{
			var r = false;
			
			
			if (Ext.isArray(c))
			{
				for (var i = 0; i < c.length; i++)
				{
					r = hasFileFields(c[i]);
					
					if (r) break;
				}
			}
			else
			{
				if (c.items) // c is container
				{
					if (Ext.isArray(c.items))
					{
						for (var i = 0; i < c.items.length; i++)
						{
							r = hasFileFields(c.items[i]);

							if (r) break;
						}
					}
					else
					{
						r = hasFileFields(c.items);
					}
				}
				// else if ((c.name || c.hiddenName) && c.xtype && ['massstoragefile', 'imageuploadfield'].indexOf(c.xtype) != -1)
				else if (c.xtype && ['xbp_dfilefield', 'xbp_imageuploadfield'].indexOf(c.xtype) != -1)
				{
					r = true;
				}
			}
			
			
			return r;
		}
			
		config.formFileUpload = hasFileFields(config.fieldConfigs);


		// Call constructor
		self.EntityPopup.superclass.constructor.call(this, config);
	}
	
	Ext.extend
	(
		this.EntityPopup,
		// CMS.Components.PopupFormWindow,
		this.StorePopup,
		{
			show: function (record)
			{
				self.EntityPopup.superclass.show.apply(this, arguments);


				var entity = self.entities[this.entityName];
				
				
				if (this.onCustomTitle)
				{
					var mode = 'add';
					var recordTitle = '';
					
					if (record)
					{
						mode = 'edit';
						recordTitle = record.get(entity.getTitleFieldName());
					}
					
					this.setTitle(this.onCustomTitle(recordTitle, mode));
				}
				else if (! this.useCustomTitle)
				{
					var title = '';
					
					if (record)
					{
						title = _(entity.getTitle('sUCF')) + ' "' + record.get(entity.getTitleFieldName()) + '"';
					}
					else
					{
						title = _('New' + ' ' + entity.getTitle('s'));
					}
					
					this.setTitle(title);
				}
			}
		}
	);
	
	
	this.EntityGrid = function (config)
	{
		var grid = this;
		
		
		if (! config.entityName) throw 'entityName not set';
		
		var entity = self.entities[config.entityName];
		

		// set title
		
		if (config.title === undefined)
		{
			config.title = _(entity.getTitle('pUCF'));
		}
		
		
		// set store
		
		if (! config.store)
		{
			config.store = entity.getItemStore();
			
			if (! config.store) throw '"' + config.entityName + '" grid store not set';
		}
		
		
		// set columns

		var cc = entity.getColumnConfigs();
		
		var autoExpandColumnIndex = cc.autoExpandColumnIndex;
		var autoExpandColumnName = cc.autoExpandColumnName;
		var columnConfigs = cc.columnConfigs;

		if (config.customColumnConfigs)
		{
			XB.P.Util.applyCustomConfigs(columnConfigs, config.customColumnConfigs);
			
			
			// set expandable column index
			if (autoExpandColumnName != null)
			{
				for (var i = 0; i < columnConfigs.length; i++)
				{
					if (columnConfigs[i].dataIndex == autoExpandColumnName)
					{
						autoExpandColumnIndex = i;
						break;
					}					
				}
			}
		}
		
		config.columns = columnConfigs;
		config.autoExpandColumn = config.autoExpandColumn ? config.autoExpandColumn : autoExpandColumnIndex;


		var focus = function (cb)
		{
			var view = grid.getView();
						
			// source from GridView::focusCell()
			
			var focusEl = view.focusEl;
			
			if (Ext.isGecko) {
					focusEl.focus();
					
					if (cb) cb();
			} else {
				(function ()
				{
					focusEl.focus.call(this);
					
					if (cb) cb();
				}).defer(1, focusEl);
			}
		}
		
		var selectFirstAndFocus = function (cb)
		{
			var s = grid.getStore();
			var sm = grid.getSelectionModel();

			var records = sm.getSelections();
			if (records.length == 0) // no rows selected
			{
				if (s.getCount() > 0) // grid has rows
				{
					(function ()
					{
						sm.selectFirstRow();
					
						focus(cb);
					}).defer(300);
				}
			}
			else
			{
				focus(cb);
			}
		}
		
		var onActivate = function (g)
		{
			selectFirstAndFocus(
				/*function ()
			{
				if (g.activatenext)
				{
					if (g.activate) activate();
				}
				
			}*/
			);
		}
		
		config.store.on
		(
			'load',
			function (store)
			{
				 // skip focusing if popup is shown or other element is focused when the store is loaded
				if (! grid.getEl() || ! grid.getEl().contains(document.activeElement)) return;

				selectFirstAndFocus();
			}
		);
		
		/**
			Once select first row and focus it on next store load
		*/
		this.activateOnLoad = function ()
		{
			this.store.on
			(
				'load',
				function (store)
				{
					if (! grid.getEl()) return;
					
					selectFirstAndFocus();
				},
				this,
				{single: true}
			);
		}

		
		var onDeleteAction = function (records)
		{
			var onDelete = grid.onDelete || null;
			
			if (onDelete)
			{
				var entity = self.entities[grid.entityName];
				var titleFieldName = entity.getTitleFieldName();
				
				
				var titles = [];
				Ext.each(records, function (record, index, allItems)
				{
					var recordTitle = record.get(titleFieldName);
					
					titles.push('"' + recordTitle + '"');
				});
				
				
				var entityTitle = '';
				if (titles.length > 1)
				{
					entityTitle = entity.getTitle('p');
				}
				else
				{
					entityTitle = entity.getTitle('s');
				}
					
				
				var message = _('Delete') + ' ' + _(entityTitle) + ' ' + titles.join(', ') + '. <br/>' + _('Are you sure?');
				
				
				Ext.MessageBox.confirm
				(
					_('Deleting'), // _('Deleting selected'),
					message,
					function (buttonId) {if (buttonId == 'yes') onDelete(records);}
				);
			}
		}
		
		var onEditAction = function (record)
		{
			if (grid.onEdit)
			{
				grid.onEdit(record);
			}
		}
		
		var onCopyAction = function (record)
		{
			if (grid.onCopy)
			{
				grid.onCopy(record);
			}
		}
		
		var onExportAction = function (records)
		{
			/*
			var exportMode = 'data';
			var sendMode = 'file';
			
			var entityRecord = XB.P.ModuleManager.getModuleStore().findRecord('name', grid.entityName);
			var entityId = entityRecord.get('id');
			var entityIds = [entityId];
			
			var recordIds = [];
			if (records.length > 0)
			{
				Ext.each
				(
					records,
					function (record, index, allItems)
					{
						recordIds.push(record.get('id'));
					}
				);
			}
			
			var url = XB.P.Config.serverUrl + '?module=Modules&action=ExportModuleEntity&export_mode=' + exportMode + '&send_mode=' + sendMode + '&ids=' + entityIds.join(',');
			if (recordIds.length > 0)
			{
				url += '&record_ids=' + recordIds.join(',');
			}
			
			window.location.href = url;
			
			*/
			
			// var entityRecord = XB.P.ModuleManager.getModuleStore().findRecord('name', grid.entityName);
			// var entityId = entityRecord.get('id');
			
			
			var recordIds = [];
			if (records.length > 0)
			{
				Ext.each
				(
					records,
					function (record, index, allItems)
					{
						recordIds.push(record.get('id'));
					}
				);
			}
			
			
			var params = {};
			
			// params.entity_id = entityId;
			params.entity_name = grid.entityName;
			
			if (recordIds.length > 0)
			{
				params.record_ids = recordIds.join(',');
			}
			
			
			self.requestDirect('ExportEntityItemData', params);
		}
		
		
		// set listeners
		
		var onRowDblClick = function(grid, rowIndex, e)
		{
			if (self.permission(grid.entityName, 'edit', 1))
			{
				var record = grid.getStore().getAt(rowIndex);
				
				/*
				if (grid.onEdit)
				{
					grid.onEdit(record);
				}
				*/
				onEditAction(record);
			}
		};
		
		var onSelectionChange = function ()
		{
			var tt = this.getTopToolbar();
			var btnEdit = tt.tbBtnEdit;
			var btnDelete = tt.tbBtnDelete;
			

			var ss = this.getSelectionModel().getSelections();

			if (ss.length == 1)
			{
				if (btnEdit) btnEdit.enable();
				if (btnDelete) btnDelete.enable();
			}
			else if (ss.length > 1)
			{
				if (btnEdit) btnEdit.disable();
				if (btnDelete) btnDelete.enable();
			}
			else
			{
				if (btnEdit) btnEdit.disable();
				if (btnDelete) btnDelete.disable();
			}
		}
		
		var defaultListeners = 
		{
			rowdblclick: onRowDblClick,
			selectionchange: onSelectionChange,
			activate: onActivate
		};
		
		
		config.listeners = config.listeners || {};
		
		Ext.applyIf(config.listeners, defaultListeners);
		
		
		// set paging or refreshable
		
		if (config.store.baseParams.paging)
		{
			config.bbar = new XB.P.Components.PagingToolbar
			(
				{
					store: config.store
				}
			);
		}
		else
		{
			config.refreshable = true;
		}
		
		
		var onDeleteDefault = function(records) // called in scope of grid
		{
			var ids = [];
			Ext.each(records, function (record, index, allItems)
			{
				ids.push(record.get('id'));
			});
			
			var grid = this;
			
			self.request
			(
				'DeleteEntityItemData', 
				{
					// params: {entity_name: 'item', id: record.get('id')},
					params: {entity_name: grid.entityName, ids: ids.join(',')},
					success: function ()
					{
						if (grid.refreshable)
						{
							// non-paged store
							var s = grid.getStore();
							Ext.each(records, function (record, index, allItems)
							{
								s.remove(record);
							});

							// s.removeRecords(records);
							
							
							if (grid.afterDelete)
							{
								grid.afterDelete();
							}
						}
						else
						{
							// paged store
							grid.getStore().reload
							(
								{
									callback: function ()
									{
										if (grid.afterDelete)
										{
											grid.afterDelete();
										}
									}
								}
							);
						}
					}
				}
			);
		}
		
		var onDelete = config.onDelete || onDeleteDefault;
		config.onDelete = onDelete.createDelegate(this);
		
		
		// custom icons
		
		// var icons = config.icons || {};
		var iconNames = config.iconNames || {};
		
		// set toolbar

		var toolbarItemConfigs = [];
		
		if (config.customToolbarConfig)
		{
			toolbarItemConfigs = config.customToolbarConfig;
			
			delete config.customToolbarConfig;
		}
		else
		{
			// Params: <action name>, <show triple dots>, <handler>, <custom text>, <custom icon name>, <custom config>
			var addToolbarItem = function (aname, etc, h, ctext, cicon, cc)
			{
				var c = aname;
				
				if (c !== '-')
				{
					var text = _( XB.Util.JS.ucfirst(ctext || aname) + (etc ? '...' : '') );

					c = 
					{
						text: text,
						iconName: iconNames[aname] || (cicon || aname),
		
						handler: h
					};
				}
	
				toolbarItemConfigs.push(Ext.apply(c, cc || {}));
			}
			
			
			if (self.permission(config.entityName, 'add', 1))
			{
				var h = function () { onEditAction(null); };
				
				addToolbarItem('add', true, h, 'create');
			}
			
			if (self.permission(config.entityName, 'edit', 1) || self.permission(config.entityName, 'delete', 1))
			{
				if (toolbarItemConfigs.length > 0) addToolbarItem('-');
				
				
				if (self.permission(config.entityName, 'edit', 1))
				{
					var h = function () { onEditAction(grid.getSelectionModel().getSelections()[0]); };
			
					addToolbarItem('edit', true, h, null, null, { disabled: true, ref: 'tbBtnEdit' });
				}
				
				if (self.permission(config.entityName, 'delete', 1))
				{
					var h = function () { onDeleteAction(grid.getSelectionModel().getSelections()); };
			
					addToolbarItem('delete', false, h, null, null, { disabled: true, ref: 'tbBtnDelete' });
				}
			}
			
			if (self.permission(config.entityName, 'export', 1))
			{
				if (toolbarItemConfigs.length > 0) addToolbarItem('-');

			
				var h = function () { onExportAction(grid.getSelectionModel().getSelections()); };
		
				addToolbarItem('export', true, h, null, 'arrow_down', { ref: 'tbBtnExport' });
			}
		}
		
		
		if (config.customToolbarItemConfigs)
		{
			XB.P.Util.applyCustomConfigs(toolbarItemConfigs, config.customToolbarItemConfigs);
		}
		
		// config.tbar = new Ext.Toolbar(toolbarItemConfigs);
		config.tbar = new XB.P.Components.Toolbar(toolbarItemConfigs);


		// set context menus
		
		var contextMenuItemConfigs = [];
		
		// Params: <action name>, <show triple dots>, <handler>, <is default action>, <custom text>, <custom icon name>
		var addMenuItem = function (aname, etc, h, def, ctext, cicon)
		{
			var c = aname;
			
			if (c !== '-')
			{
				var text = _( XB.Util.JS.ucfirst(ctext || aname) + (etc ? '...' : '') );
				if (def) text = '<b>' + text + '</b>';
				
				c = 
				{
					text: text,
					iconName: iconNames[aname] || (cicon || aname),
	
					handler: h
				};
			}
			
			contextMenuItemConfigs.push(c);
		}
		
		if (self.permission(config.entityName, 'edit', 1))
		{
			var h = function () { onEditAction(this.parentMenu.contextRecord); };
			
			addMenuItem('edit', true, h, true);

			addMenuItem('-');
		}

		if (self.permission(config.entityName, 'add', 1))
		{
			var h = function () { onEditAction(null); };
			
			addMenuItem('add', true, h, false, 'create');
			
			if (config.onCopy)
			{
				var h = function () { onCopyAction(this.parentMenu.contextRecord); };
				
				addMenuItem('copy', true, h, false, null, 'page_copy');
			}
		}
		
		if (self.permission(config.entityName, 'delete', 1))
		{
			var h = function () { onDeleteAction([this.parentMenu.contextRecord]); };
			
			addMenuItem('delete', false, h, false);
		}
		
		// apply custom context menu item configs if any
		
		if (config.customContextMenuItemConfigs)
		{
			XB.P.Util.applyCustomConfigs(contextMenuItemConfigs, config.customContextMenuItemConfigs);
		}
		

		// set contextMenuConfig if any items present
		
		if (contextMenuItemConfigs.length > 0)
		{
			config.contextMenuConfig = contextMenuItemConfigs;
		}

		
		// Do the same with group context menu (when more than one record selected)
		
		var groupContextMenuItemConfigs = [];
		
		if (self.permission(config.entityName, 'delete', 1))
		{
			groupContextMenuItemConfigs.push
			(
				{
					text: _('Delete selected'),
					iconName: iconNames['delete'] || 'delete',
					
					handler: function ()
					{
						var records = this.parentMenu.grid.getSelectionModel().getSelections();

						onDeleteAction(records);
					}
				}
			);
		}
		
		// apply custom if any
		if (config.customGroupContextMenuItemConfigs)
		{
			XB.P.Util.applyCustomConfigs(groupContextMenuItemConfigs, config.customGroupContextMenuItemConfigs);
		}
		
		if (groupContextMenuItemConfigs.length > 0)
		{
			config.groupContextMenuConfig = groupContextMenuItemConfigs;
		}
		

		// Context toolbox
		
		if (false && config.expUseToolbox)
		{
			Ext.apply	
			(
				this,
				{
					_hideToolbox: function (e)
					{
						var ctb = this.contextToolbox;
						
						if (! e.within(ctb.getEl(), true))
						{
							ctb.hide();
						}
					},
					hideToolboxTimeout: null,
					hideToolbox: function (e)
					{
						if (! this.hideToolboxTimeout) this.hideToolboxTimeout = this._hideToolbox.defer(150, this, [e]);
					},
					cancelHideToolbox: function ()
					{
						if (this.hideToolboxTimeout)
						{
							 clearTimeout(this.hideToolboxTimeout);
							 
							 this.hideToolboxTimeout = null;
						}
					},
					
					showToolbox: function (row, rowIndex, e)
					{
						this.cancelHideToolbox();
						
						var record = this.getStore().getAt(rowIndex);
						var ctb = this.contextToolbox;
						
						ctb.contextRecord = record;
						
						var rowEl = Ext.get(row);
								
						var mode = this.toolboxShowMode || -1;
						switch (mode)
						{
							case 1:
								ctb.showAt(e.getXY());
							break;
							
							case 2:
								var yDelta = (ctb.getHeight() - rowEl.getHeight()) / 2;
								ctb.showAt([rowEl.getRight() - ctb.getWidth(), rowEl.getTop() - yDelta]);
							break;
							
							case 3:
								ctb.showAt([rowEl.getRight() - ctb.width, rowEl.getTop() - 3]); // align to right
							break;
							
							case 4:
								ctb.showAt([rowEl.getLeft(), rowEl.getTop() - 3]); // align to left
							break;
	
							case 5:
								var xy = e.getXY();
								
								var yDelta = rowEl.getHeight() / 2;
	
								ctb.showAt([xy[0], rowEl.getTop() + yDelta]);
							break;
							
							case 6:
								var xy = e.getXY();
								
								var yDelta = rowEl.getHeight() * 2/3;
	
								ctb.showAt([xy[0], rowEl.getTop() + yDelta]);
							break;
							
							case 7:
								var xy = e.getXY();
								var ctbEl = ctb.getEl();
								
								var prevRowIndex = arguments.callee.prevRowIndex;
								
								if (! ctb.isVisible() || xy[0] > ctbEl.getRight() || xy[0] < ctbEl.getLeft() || prevRowIndex != rowIndex)
								{
									var yDelta = rowEl.getHeight() * 2/3;
		
									ctb.showAt([xy[0] - ctb.getWidth()/2, rowEl.getTop() + yDelta]);
									
									var tn = self.entities[this.entityName].getTitleFieldName();
									ctb.setTitle(record.get(tn));
									
									
									arguments.callee.prevRowIndex = rowIndex;
								}
							break;
							
							case 8:
							default:
								var xy = e.getXY();
								var ctbEl = ctb.getEl();
								
								var prevRowIndex = arguments.callee.prevRowIndex;
								
								if (! ctb.isVisible() || xy[0] > ctbEl.getRight() || xy[0] + 16 < ctbEl.getLeft() || prevRowIndex != rowIndex)
								{
									var yDelta = rowEl.getHeight() * 3/4;
		
									ctb.showAt([xy[0] + 16, rowEl.getTop() + yDelta]);
									
									var tn = self.entities[this.entityName].getTitleFieldName();
									ctb.setTitle(String.format('{1} [ID:{0}]', record.get('id'), record.get(tn)));
									
									
									arguments.callee.prevRowIndex = rowIndex;
								}
							break;
						}
						
					}
					
				}
			);
			
			Ext.apply	
			(
				config.listeners,
				{
					mousemove: function (e, target)
					{
						console.log('mousemove');
						
						var row = this.getView().findRow(target);
						var rowIndex = row ? row.rowIndex : false;

						if (rowIndex !== false) {
							this.showToolbox(row, rowIndex, e);
						}
						else {
							this.hideToolbox(e);							
						}
					},
					mouseout: function (e, target)
					{
						console.log('mouseout');
						
						this.hideToolbox(e);						
					},
					click: function (e, target)
					{
						this.contextToolbox.hide();
					}
				}
			);
			
			//  Create toolbox
			
			var cv = // config vars
			{
				panel: {hpad: 5, bgc: 'rgba(225,225,225,.9)'},
				btn: {w: 26, h: 26, mrgn: 10}
			}
			
			// ext panel
			var cpanel =
			{
				cls: 'xb-toolbox',
				
				renderTo: Ext.getBody(),
				
				floating: true,

				shadow:   false,
				unstyled: true,
				/*
				style: 'box-shadow: rgba(204, 216, 231, 0.66) 0px 2px 2px 0px',
				bodyStyle: 'background-color: ' + cv.panel.bgc,
				*/
				style: 'box-shadow: rgba(204, 216, 231, 0.66) 0px 2px 3px 3px',
				bodyStyle: 'background-color: rgba(245, 245, 245, .9);',
				
				padding: '0 ' + cv.panel.hpad + ' 0 ' + cv.panel.hpad,
				
				items: [],
				
				grid: grid,
				contextRecord: null // to be set for a row dynamically
			};
			
			var cbutton =
			{
				// margins: '0 ' + btcfg_mrgn + ' 0 0', // right margin
				// margins: '0 0 0 ' + btcfg_mrgn, // left margin
				xtype: 'button',
				width:  cv.btn.w,
				height: cv.btn.h,
				style: 'float: left; margin-left: ' + cv.btn.mrgn
			};
			
			var addButton = function (aname)
			{
				cpanel.items.push
				(
					Ext.apply
					(
						{},
						cbutton,
						{
							icon: XB.P.Skin.getIconUrl(aname),
							handler: (function (aname)
							{
								return function(btn, e)
								{
									var tb = this.ownerCt;
									var record = tb.contextRecord;
									var grid = tb.grid;
			
									tb.hide();
									
									grid.doAction(aname, record);
								};
							})(aname)
						}
					)
				);
			}

			if (self.permission(config.entityName, 'edit', 1))   addButton('edit');
			if (self.permission(config.entityName, 'delete', 1)) addButton('delete');

			
			cpanel.items.push({
				ref: 'tpanel', xtype:'panel',
				unstyled: true,
				height: 26,
				padding: '3 0 3 10',
				bodyStyle: 'line-height: 20px; color: rgb(194,103,21); font-family: arial, tahoma, helvetica, sans-serif; font-size: 11px',
				html: '&nbsp;'
			});
			
			grid.contextToolbox = new (Ext.extend(Ext.Panel, {
					onRender: function(ct, position)
					{
						this.__proto__.__proto__.onRender.apply(this, arguments);
						
						var c = this.getEl();
						
						this.relayEvents(c, ['mouseover']);
					},
					showAt: function (xy)
					{
						if (! this.isVisible()) this.show();
						this.setPagePosition(xy[0],xy[1]);
					},
					setTitle: function (title)
					{
						this.tpanel.update(title);
						this.syncShadow();
					},
					
					// TODO: create toolbox manager class for this (see Ext.menu.MenuMgr)
					
					onDocMouseDown: function (e, target)
					{
						// debugger;
						
						if (!e.getTarget(".xb-toolbox"))
						{
							this.hide();
						}
					},
   
					onShow: function(){
						console.log('CTB onshow');
						this.__proto__.__proto__.onShow.apply(this, arguments);

						Ext.getDoc().on("mousedown", this.onDocMouseDown, this);
					},

					onHide: function(){
						console.log('CTB onhide');
						Ext.getDoc().un("mousedown", this.onDocMouseDown, this);
						
						this.__proto__.__proto__.onHide.apply(this, arguments);
					},
					
					listeners:
					{
						mouseover: function ()
						{
							console.log('CTB mouseover');
							this.grid.cancelHideToolbox();
						}
					}
			}))(cpanel);
		}
			
			
			
		// Bound shortcut keys
		
		var keyc = function (c)
		{
			var mods = c.mods ? c.mods.toLowerCase() : '', has = function (s) {return mods.indexOf(s) !== -1;};
				
			return Ext.apply(c, {
				alt:   has('a'),
				shift: has('s'),
				ctrl:  has('c'),
				
				stopEvent: true
			});
		}
		
		config.keys =
		[
			keyc({ key: 'nN', mods: 's',
				fn: function ()
				{
					onEditAction(null);
				}
			}),
			keyc({ key: Ext.EventObject.ENTER,
				fn: function ()
				{
					var records = grid.getSelectionModel().getSelections();

					if (records.length == 1)
					{
						var record = records[0];
						
						onEditAction(record);
					}
				},
			}),
			keyc({ key: Ext.EventObject.DELETE,
				fn: function ()
				{
					var records = grid.getSelectionModel().getSelections();
					
					if (records.length > 0)
					{
						onDeleteAction(records);
					}
				}
			})
		];
		
		
		// Call constructor
		self.EntityGrid.superclass.constructor.call(this, config);
		
		
		this.relayEvents(this.getSelectionModel(), ['selectionchange']);


		this.doAction = function (name)
		{
			// skip first argument
			var args = [];
			for (var i = 1; i < arguments.length; i++)
			{
				args.push(arguments[i]);
			}
			

			switch (name)
			{
				case 'add':
					if (self.permission(this.entityName, 'add', 1))
					{
						onEditAction.apply(null);
					}
				break;
				
				case 'edit':
					if (self.permission(this.entityName, 'edit', 1))
					{
						onEditAction.apply(null, args);
					}
				break;
				
				case 'copy':
					if (self.permission(this.entityName, 'add', 1))
					{
						onCopyAction.apply(null);
					}
				break;
				
				case 'delete':
					if (self.permission(this.entityName, 'delete', 1))
					{
						onDeleteAction.apply(null, args);
					}
				break;
			}
		}
	}
	
	Ext.reg('xb_grid',
		Ext.extend
		(
			this.EntityGrid,
			XB.P.Components.GridPanel,
			{
				activate: function ()
				{
					this.fireEvent("activate", this);
				},
				
				onRender: function (ct, position)
				{
					self.EntityGrid.superclass.onRender.apply(this, arguments);
					
					if (this.showToolbox)
					{
						var c = this.getGridEl();
						
						this.relayEvents(c, ['mousemove']);
					}
				}
			}
		)
	);
	
		
	this.MainWindow = Ext.extend
	(
		Ext.Window,
		{
			renderTo: 'desktop',

			width: 970,
			height: 510,

			resizable: true,
			maximizable: true,
			minimizable: true,
			constrain: true,
			// shadowOffset: 4,
			
			modal: false,

			layout: 'fit',
			
			uiPath: '',
			
			// iconCls: self.iconCls,
			// title: this.title,

			listeners:
			{
				beforerender: function (w)
				{
					var hSpace = 50, vSpace = 50
					
					var s = XB.P.Desktop.desktop().shortcutPanel.getSize();
					
					s.width -= hSpace;
					s.height -= vSpace;
					
					w.setSize(s);
				},
				render: function(w)
				{
					if (Ext.isEmpty(w.title, true)) w.setTitle(self.title);
					if (Ext.isEmpty(w.iconCls, true)) w.setIconClass(self.iconCls);
				},
				beforeclose: function (w)
				{
					if (!	w.closeConfirmed)
					{
						Ext.MessageBox.confirm
						(
							_('Close') + ' "' + _(self.config.title) + '"',
							_('Are you sure?'),
							function (buttonId) {if (buttonId == 'yes') { w.closeConfirmed = true; w.close(); }}
						);
					}
					
					return !!w.closeConfirmed;
				},
				close: function(w)
				{
					XB.P.ModuleManager.terminateInteractive(self.getName());
				},

				activate: function (w)
				{// console.log('onactivate MainWindow; set UI path to "' + w.uiPath +'"');
					XB.P.UIPath.set(w.uiPath);
					
					var ii = w.items;
					if (ii.getCount() > 0)
					{
						var i = ii.first();
						// debugger
						// console.log('mainwindow activate i.getXType() == ', i.getXType());
						if (i.getXType() != 'tabpanel'
							&& ! i.activeTab // hack: detect tabpanel descendants without explicit xtype
							&& i.activate) i.activate();
					}
				},
				hide: function ()
				{// console.log('onhide MainWindow; clear UI');
					XB.P.UIPath.clear();
				},
				afterRender: function (w)
				{// console.log('onafterrender MainWindow; replace UI path "' +w.uiPath+'" with "'+self.moduleName+'"');
					// w.uiPath = XB.P.UIPath.parser.push(w.uiPath, 'module', self.moduleName);
					w.uiPath = XB.P.UIPath.parser.replace(w.uiPath, 0, 'module', self.moduleName);
				}
			},

			resolveUIPath: function (path, callback)
			{
				// console.log('resolving UI path "'+path+'"');
				
				
				var resolve = function (component, path, callback)
				{
					if (path.length == 0 || component.items.length == 0)
					{
						callback();
					}
					else
					{
						if (component.items.length == 1)
						{
							component = component.items.first();
							
							resolve(component, path, callback);
						}
						else // component.items.length > 1
						{
							var testValue = path.shift();

							var component = component.items.find( function (item, key) { return (item.title && item.title == testValue); } );
										
							if (component)
							{
								// activate depending on type
								// nextC.activate
								/*
								switch(component.getXType())
								{
									case 'tabpanel':
										component
									break;
								}
								*/
								// component.activate();
								/*
								if (component.ownerCt && component.ownerCt.getXType() == 'tabpanel')
								{
									component.ownerCt.activate(component);
								}
								*/
								if (component.ownerCt && component.ownerCt.activate) // e.g. tabpanel
								{
									component.ownerCt.activate(component);
								}
								
								// console.log('resolving: activate component');
								
								resolve(component, path, callback);
							}
						}
						
					}
				}
				
				resolve(this, path, callback);
			},
			
			getComponentPathPosition: function (component)
			{
				// go up throught component tree to the parent (this window)

				var getPosition = function (root, component, position)
				{
					if (position === undefined) // initial
					{
						position = false;
					}
					
					if (component === root)
					{
						return position;
					}
					else if (! component.ownerCt)
					{
						return false;
					}
					else
					{
						var parent = component.ownerCt;
						
						if (parent.items.length > 1)
						{
							position = (position === false) ? 0 : position++;
						}
						
						return getPosition(root, parent, position);
					}
				}
				
				return getPosition(this, component);
			}
		}
	);
}

XB.P.ApplicationModule.prototype.onLaunch = function(launchParams, launchCallback) // callback MUST be launched
{
	var self = this;
	
	
	// Load permissions

	XB.P.ModuleManager.updateLaunchProgress(_('Loading permissions...'));

	self.loadPermissions
	(
		{
			success: function ()
			{
				try
				{
					// Load entity list and init entities

					XB.P.ModuleManager.updateLaunchProgress(_('Loading data model...'));

					self.loadEntities
					(
						{
							success: function ()
							{
								if (launchCallback) launchCallback(self);
							},
							failure: function ()
							{
								XB.P.ModuleManager.terminate(self.getName());
							}
						}
					);
				}
				catch (e)
				{
					XB.P.ModuleManager.terminate(self.getName());
				}
			},
			failure: function ()
			{
				XB.P.ModuleManager.terminate(self.getName());
			}
		}
	);
}

XB.P.ApplicationModule.prototype.onTerminate = function ()
{
}
	
XB.P.ApplicationModule.prototype.getResource = function(type, name, params)
{
	var resource = null;

	/*		
	switch (type)
	{
		case ?:
			switch (name)
			{
				case ?:
					resource = <get resource>;
				break;
			}
		break;
	}
	*/
	
	return resource;	
}


XB.P.UIPath = new (function ()
{
	this.resolving = false;
	
	/*
	this.add = function (type, name)
	{
		// debugger;
		var hash = window.location.hash;
		var path = hash.replace(/^#/, '');
		var parts = path.length > 0 ? path.split('/') : [];
		
		switch (type)
		{
			case 'module':
				parts.push(name);
			break;

			case 'tab':
				parts.push(name);
			break;
		}

		path = parts.join('/');
		
		window.location.hash = path;
		
		return path;
	}
	*/
	this.parser =
	{
		push: function (path, type, name)
		{
			// debugger;
			var parts = path.length > 0 ? path.split('/') : [];
			
			switch (type)
			{
				case 'module':
					parts.push(name);
				break;
	
				case 'tab':
					parts.push(name);
				break;
			}
	
			path = parts.join('/');
			
			return path;
		},

		replace: function (path, pos, type, name)
		{
			var part = '';			
			switch (type)
			{
				case 'module':
					part = name;
				break;
	
				case 'tab':
					part = name;
				break;
			}
	

			var parts = path.length > 0 ? path.split('/') : [];

			if (parts.length > 0)
			{
				parts = parts.slice(0, pos);
			}
			parts.push(part);
			
			path = parts.join('/');
			
			return path;
		},
		/*
		shift: function (path)
		{
			// debugger;
			var parts = path.length > 0 ? path.split('/') : [];
			
			if (! parts.length) throw 'path must contain at least one element';
			
			var first = parts.shift();
			
			path = parts.length > 0 ? parts.join('/') : '';
			
			return [first, path];
		},
		*/	
		parse: function (path)
		{
			return path.length > 0 ? path.split('/') : [];
		}
	};
	/*
	this.build = function (path, type, name)
	{
		// debugger;
		var parts = path.length > 0 ? path.split('/') : [];
		
		switch (type)
		{
			case 'module':
				parts.push(name);
			break;

			case 'tab':
				parts.push(name);
			break;
		}

		path = parts.join('/');
		
		return path;
	}
	*/
	this.set = function (path)
	{
		if (! this.resolving)
		{
			window.location.hash = path;
		}
	}
	
	this.clear = function ()
	{
		window.location.hash = '';
	}
	
	this.resolve = function ()
	{
		var hash = window.location.hash;
		if (hash)
		{
			var path = hash.replace( /^#/, '' );
			if (path)
			{
				path = decodeURIComponent(path); // TODO: verify
				
				this.resolving = true;
				
				var path = this.parser.parse(path);
				var moduleName = path.shift();
				
				var self = this;
				var onInteractiveReady = function (module, mainWindow)
				{
					mainWindow.resolveUIPath(path, function ()
					{
						self.resolving = false;
					});
				}
				XB.P.ModuleManager.launchInteractive(Ext.util.Format.capitalize(moduleName), {}, onInteractiveReady);
			}
			
		}
	}
});

