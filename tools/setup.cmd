@echo off

echo Setup tools?
pause

call j ckeditor       d:\Dev\www\shared\ckeditor\3.6.6.1_7696\dev
call j extjs          d:\Dev\www\shared\extjs\3.3.1\dev
rem call j fckeditor      d:\Dev\www\shared\fckeditor\2.6.3_19836\dev
call j kcaptcha       d:\Dev\www\shared\kcaptcha\1.2.6\dev
call j kcfinder       d:\Dev\www\shared\kcfinder\2.21\dev
call j loadscript    "d:\Dev\www\shared\loadscript\1.6 modified 1\dev"
call j PHPExcel       d:\Dev\www\shared\PHPExcel\1.7.4\dev
call j PHPMailer      d:\Dev\www\shared\PHPMailer\5.1\dev
call j Services_JSON  d:\Dev\www\shared\Services_JSON\1.3\dev
call j smarty         d:\Dev\www\shared\smarty\3.0.7\dev
call j TCPDF          d:\Dev\www\shared\TCPDF\6.2.11\dev 
call j zeroclipboard  d:\Dev\www\shared\zeroclipboard\1.0.7\dev

pause