CKEDITOR.editorConfig = function (config)
{
	// config.toolbar = 'xbcms';
	
	// contentsLanguage
	config.defaultLanguage = 'en';

  config.toolbar_xbcms =
	[
		[
			'Save','-','-',
			'Print','-','-',
			// 'SpellCheck','-',
			'SelectAll','-','Cut','Copy','Paste','PasteText','PasteFromWord','-',
			'Undo','Redo','-','-',
			'Find','Replace'
		],
		[
			'Anchor', 'Link','Unlink','-',
			'Image','Table','Flash','HorizontalRule','Smiley','SpecialChar','PageBreak','-','-',
			'Blockquote','CreateDiv','-','-',
			'ShowBlocks','-',
			'Source','-',
			'About'
		],
		'/',
		[
			'RemoveFormat','-','-',
			'Styles','Format','Font','FontSize'
		],
		[
			'Templates'
		],
		[
			'Bold','Italic','Underline','Strike','-',
			'Subscript','Superscript','-','-',
			'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','-',
			'NumberedList','BulletedList','-',
			'Outdent','Indent','-','-',
			'BGColor','TextColor','Maximize'
		]
	];
};


