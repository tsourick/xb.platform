CKEDITOR.addTemplates
(
	'default',
	{
		// The name of sub folder which hold the shortcut preview images of the templates.
		// imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),
	
		// The templates definitions.
		templates :
		[
			{
				title: 'Университет (одна подпрограмма)',
				description: 'Таблица с описанием единственной подпрограммы университета.',
				html:
				'<table border="0" cellspacing="0" cellpadding="0" class="special">' +
					'<col width="190"/>' +
					'<col width="640"/>' +
					'<tbody>' +
						'<tr>' +
							'<td><strong>Описание программы:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Направления обучения:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Специальности:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Продолжительность обучения:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Вступительные требования:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Срок подачи заявки:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Начало обучения:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Структура курса:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Проживание:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Стоимость:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Практика и трудоустройство:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Документы для поступления:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Дополнительная информация:</strong></td>' +
							'<td></td>' +
						'</tr>' +
					'</tbody>' +
				'</table>'
			},
			{
				title: 'Университет (несколько подпрограмм)',
				description: 'Таблица с описанием одной из нескольких подпрограмм университета.',
				html:
				'<table border="0" cellspacing="0" cellpadding="0" class="special">' +
					'<col width="164"/>' +
					'<col width="666"/>' +
					'<tbody>' +
						'<tr>' +
							'<td><strong>Описание программы:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Направления обучения:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Специальности:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Продолжительность обучения:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Вступительные требования:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Срок подачи заявки:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Начало обучения:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Структура курса:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Проживание:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Стоимость:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Практика и трудоустройство:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Документы для поступления:</strong></td>' +
							'<td></td>' +
						'</tr>' +
						'<tr>' +
							'<td><strong>Дополнительная информация:</strong></td>' +
							'<td></td>' +
						'</tr>' +
					'</tbody>' +
				'</table>'
			}
		]
	}
);

