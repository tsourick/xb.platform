// *** MixedCollectionReader ***
Ext.ux.MixedCollectionReader = function(meta, recordType){
    meta = meta || {};
    Ext.ux.MixedCollectionReader.superclass.constructor.call(this, meta, recordType || meta.fields);
};

Ext.extend(Ext.ux.MixedCollectionReader, Ext.data.DataReader, {
    // private function a store will implement
    onMetaChange : function(meta, recordType, o){

    },

    simpleAccess: function(obj, subsc) {
    	return obj[subsc];
    },


    readRecords : function(o){
			var collection = o;
			var Record = this.recordType;
			
			var records = [];
			
			collection.each
			(
				function (item, index, length)
				{
					records.push(item.copy());
				}
			);
			
	    return {
	        records : records
	    };
    }
});

