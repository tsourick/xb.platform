Ext.override(Ext.form.DateField, {
		strict: false,
		
    safeParse : function(value, format) {
        if (/[gGhH]/.test(format.replace(/(\\.)/g, ''))) {
            // if parse format contains hour information, no DST adjustment is necessary
            return Date.parseDate(value, format, this.strict);
        } else {
            // set time to 12 noon, then clear the time
            var parsedDate = Date.parseDate(value + ' ' + this.initTime, format + ' ' + this.initTimeFormat, this.strict);

            if (parsedDate) {
                return parsedDate.clearTime();
            }
        }
    }
});
