Ext.override
(
	Ext.form.FieldSet,
	{
		add: function ()
		{
			var c = Ext.form.FieldSet.superclass.add.apply(this, arguments);
			
			if (c)
			{
				this.initFields(c);
			}
		},
		
		initFields: function ()
		{
			if (! this.rendered) return;
			if (! this.items) return;
			
			var fp = this.findParentByType('form');
			
			if (! fp) return;
			
			if (fp)
			{
				var f = fp.getForm();
				
				var fn = function(c)
				{
					if (c.isFormField && c.xtype != 'checkboxgroup' && c.xtype != 'radiogroup')
					{
						f.add(c);
					}
					
					if (c.items)
					{
						c.items.each(fn);
					}
				}
				
				this.items.each(fn);
			}
		}
	}
);

