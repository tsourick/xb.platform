Ext.override(Ext.form.ComboBox, {
    beforeBlur: function()
    {
    	if (this.editable !== false) this.assertValue();
    }
});
