FCKConfig.ToolbarSets["xbcms"] =
[
//	['Source','DocProps','-','Save','NewPage','Preview','-','Templates'],
//	['Cut','Copy','Paste','PasteText','PasteWord','-','Print','SpellCheck'],
// 	['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField'],
//	['Image','Flash','Table','Rule','Smiley','SpecialChar','PageBreak'],

	['Source','-','Preview'],
	['Cut','Copy','Paste','PasteText','PasteWord','-','Print'],
	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	['Image','Flash','Table','Rule','SpecialChar','PageBreak'],
	'/',
	['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
	['OrderedList','UnorderedList','-','Outdent','Indent','Blockquote','CreateDiv'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
 	['Link','Unlink','Anchor'],
	'/',
	['Style','FontFormat','FontName','FontSize'],
	['TextColor','BGColor'],
	['FitWindow','ShowBlocks','-','About']		// No comma for the last row.
];

FCKConfig.AutoDetectLanguage = false;
FCKConfig.DefaultLanguage		= 'ru' ;
FCKConfig.ContentLangDirection	= 'ltr' ;

