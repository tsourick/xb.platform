Ext.ux.FileBrowser = Ext.extend
(
	Ext.Window,
	{
		title: 'FileBrowser',
		url: '',
		baseParams: {},
		
		rootDirectoryName: 'root',
		
		imagePath: '',
		
		GetDirectoryListActionName: 'GetDirectoryList',
		GetFileListActionName: 'GetFileList',
		UploadFileActionName: 'UploadFile',
		
		directoryPanel: null,
		selectedNode: null,
		filePanel: null,
		fileView: null,
		fileStore: null,
		
		uploadFormPanel: null,

		initComponent: function()
		{
			var selectionModel = new Ext.tree.DefaultSelectionModel
			(
				{
					listeners:
					{
						selectionchange: function(selectionModel, newNode)
						{
							var sm = selectionModel;
							var smTree = sm.tree;
							

							var scheduledBlurSelection = function ()
							{
								if (this.stopBlurSelectionEvent) this.stopBlurSelectionEvent = false;
								else
								{
									this.tree.fireEvent('blurselection');
								}
								
								this.scheduledBlurSelection = false;
							}
	
							
							var node = sm.getSelectedNode();
	
							if (node)
							{
								// record selected
								
								if (sm.scheduledBlurSelection)
								{
									sm.scheduledBlurSelection = false;
									
									sm.stopBlurSelectionEvent = true;
								}
								
								smTree.fireEvent('setselection', node);
							}
							else
							{
								// nothing selected
	
								sm.scheduledBlurSelection = true;
								
								scheduledBlurSelection.defer(600, sm);
							}
						}
					}
				}
			);
			
			
			var rootNode = new Ext.tree.AsyncTreeNode
			(
				{
					id: '/',
					// directoryName: this.rootDirectoryName,
					directoryName: 'root',
					text: '/',
					expanded: true,
					
					loader: new Ext.tree.TreeLoader
					(
						{
							dataUrl: this.url,
							// hack; dirty clonning
							baseParams: Ext.apply(Ext.util.JSON.decode(Ext.util.JSON.encode(this.baseParams)), {action: this.GetDirectoryListActionName}),
							listeners:
							{
								loadexception: function (treeLoader, node, response) {alert(response.statusText + ': '+ response.responseText);}
							}
						}
					)
				}
			);

			var rootContextMenuGroupConfigs = [];
			var contextMenuGroupConfigs = [];
			
			
			this.directoryPanel = new Ext.tree.TreePanel
			(
				{
					region: 'west',
					width: 300,
					split: true,
					
					// xtype: 'treepanel',

					title: 'Каталоги',
					
					autoScroll: true,
	
					tools: [{id: 'refresh', qtip: 'Обновить', handler: function(event, toolEl, panel) { panel.root.reload(); }}],
					
					root: rootNode,
					rootVisible: true,
	
					selModel: selectionModel,
	
					rootContextMenu: new Ext.menu.Menu({defaults: {listeners: {click: function() {this.parentMenu.hide();} } }, items: rootContextMenuGroupConfigs}),
					contextMenu: new Ext.menu.Menu({defaults: {listeners: {click: function() {this.parentMenu.hide();} } }, items: contextMenuGroupConfigs}),
	
					listeners:
					{
						/*
						click: function (node, e)
						{
							alert('node path by directoryName: ' + node.getPath('directoryName'));
						},
						*/
						contextmenu: function (node, e)
						{
							if (node.id == 1)
							{
								var c = node.getOwnerTree().rootContextMenu;
							}
							else
							{
								var c = node.getOwnerTree().contextMenu;
							}
							
							
							c.contextNode = node;
							c.showAt(e.getXY());
						},
						setselection: this.setItemView,
						blurselection: this.resetItemView,
						
						scope: this
					}
				}
			);
			

			this.fileStore = new Ext.data.JsonStore
			(
				{
					url: this.url,
					// hack; dirty clonning
					baseParams: Ext.apply(Ext.util.JSON.decode(Ext.util.JSON.encode(this.baseParams)), {action: this.GetFileListActionName}),
					root: 'list',
					fields:
					[
						'name',
						'url',
						'path',
						{name:'size', type: 'float'}
					]
				}
			);
			
			var tpl = new Ext.XTemplate
			(
				'<tpl for=".">',
					'<div class="thumb-wrap" id="{name}">',
					'<div class="thumb"><img src="{url}" title="{name}"></div>',
					'<span class="x-editable">{name}</span></div>',
				'</tpl>',
				'<div class="x-clear"></div>'
			);
			
			this.filePanel = new Ext.Panel
			(
				{
					region: 'center',
					
					title: 'Файлы',

					tools: [{id: 'refresh', qtip: 'Обновить', handler: function(event, toolEl, panel) { this.fileStore.reload(); }, scope: this}],

					layout: 'border',
			
					hideMode: 'visibility',
					hidden: true,
				
					items:
					[
						{
							region: 'center',
							
							id: 'images-view',
							
							xtype: 'dataview',
							
							store: this.fileStore,
							tpl: tpl,
							// autoHeight: true,
							multiSelect: true,
							overClass: 'x-view-over',
							itemSelector: 'div.thumb-wrap',
							selectedClass: 'x-view-selected',
							emptyText: 'Пусто',
							loadingText: 'Загрузка',
							
							listeners:
							{
								click: this.onFileClick,
								scope: this
							}
						},
						{
							region: 'south',
							
							height: 40,

							
							xtype: 'form',
							
							url: this.url,
							method: 'POST',
							// hack; dirty clonning
							baseParams: Ext.apply(Ext.util.JSON.decode(Ext.util.JSON.encode(this.baseParams)), {action: this.UploadFileActionName}),
							
							fileUpload: true,
							
							
							frame: true,
							
							layout: 'column',
							
							items:
							[
								{
									columnWidth: 1,

									xtype: 'panel',
									
									layout: 'form',
									
									items:
									{
										anchor: '-10',

										xtype: 'fileuploadfield',
										
										labelWidth: 70,

										name: 'file',
										fieldLabel: 'Загрузить файл',
										buttonCfg:
										{
											text: 'Обзор...'
										}
									}
								},
								{
									width: 105,

									xtype: 'panel',
									
									items:
									{
										xtype: 'button',
										
										text: 'Загрузить',
										minWidth: 100,
										cls: 'x-btn-text-icon',
										icon: this.imagePath + 'page_go.gif',

										listeners:
										{
											click: this.onSubmit,
											scope: this
										}
									}
								}
							]
							
						}
					]
				}
			);
			
			
			this.fileView = this.filePanel.findByType('dataview')[0];
			this.uploadFormPanel = this.filePanel.findByType('form')[0];


			Ext.apply
			(
				this,
				{
					layout: 'border',
					
					items:
					[
						this.directoryPanel,
						this.filePanel
					]
				}
			);
	
			
			Ext.ux.FileBrowser.superclass.initComponent.apply(this, arguments);
			
			
			this.addEvents('fileclick');
			
			
			this.on('hide', this.onHide, this);
		},
		
		onHide: function (fileBrowser)
		{
			this.uploadFormPanel.getForm().reset();
		},
		
    onFileClick: function(dataView, index, htmlNode, e)
		{
			var r = dataView.getRecord(htmlNode);
			
			this.fireEvent("fileclick", this, r);
		},
		
		onSubmit: function ()
		{
			var dir = this.selectedNode.id;

			this.uploadFormPanel.getForm().submit
			(
				{
					params: {dir: dir},
					waitTitle: 'Загрузка файла',
					waitMsg: 'Подождите...',
					
					success: function(form, action)
					{
						this.fileStore.reload();
					},
					failure: function(form, action)
					{
						alertErrors(action.result.errors);
					},
					scope: this
				}
			);
		},

		selectPath: function (path)
		{
			var re = new RegExp('/([^/]*)$');

			var res = re.exec(path);
			var fileName = res[1];
			
			path = path.replace(re, '');
			path = '/root' + path;
			
			
			var curPath = null;
			
			if (this.selectedNode)
			{
				curPath = this.selectedNode.getPath('directoryName');
			}

			
			if (path !== curPath)
			{
				// Queue file selection
				
				this.fileStore.on
				(
					'load',
					(
						function (fileName)
						{
							var rec = this.fileStore.findRecord('name', fileName);
							this.fileView.select(rec);
						}
					).createDelegate(this, [fileName]),
					null,
					{
						single: true
					}
				);
				
				
				// Select dir
				
				this.directoryPanel.selectPath(path, 'directoryName');
			}
		},

		setItemView: function(node)
		{
			//var itemStore = getItemStore();
			//var itemGrid = getItemGrid();
	
			this.selectedNode = node;
	
			//itemStore.baseParams.folder_id = groupNode.id;
			//itemStore.load();
			
			this.fileStore.load({params: {dir: node.id}});
			
			//itemGrid.show();
			
			this.filePanel.show();
		},
		
		resetItemView: function()
		{
			// var itemStore = getItemStore();
			// var itemGrid = getItemGrid();
	
			// itemGrid.hide();
	
			// itemStore.baseParams.folder_id = 0;
			// itemStore.removeAll();
			
			// selectedGroupNode = null;

			this.filePanel.hide();
			
			this.fileStore.removeAll();
			
			this.selectedNode = null;
		}
		
			/*,
	 
			onRender: function()
			{
				this.store.load();
	 
				Application.PersonnelGrid.superclass.onRender.apply(this, arguments);
			}
			*/
	}
);

