
Ext.ux.grid.filter.List2Filter = Ext.extend
(
	Ext.ux.grid.filter.ListFilter,
	{
		valueField:  'id',
			
		init: function(){
			this.menu.add('<span class="loading-indicator">' + this.loadingText + '</span>');
			
			if(this.store){
				if(this.loadOnShow)
					this.menu.on('show', this.onMenuLoad, this);
				else
				{
					// Grab existing records
					var records = this.store.getRange();
					if (records.length > 0) this.onLoad(this.store, records);
				}
				
			} else if(this.options){
				var options = [];
				for(var i=0, len=this.options.length; i<len; i++){
					var value = this.options[i];
					switch(Ext.type(value)){
						case 'array':  options.push(value); break;
						case 'object': options.push([value.id, value[this.labelField]]); break;
						case 'string': options.push([value, value]); break;
					}
				}
				
				this.store = new Ext.data.Store({
					reader: new Ext.data.ArrayReader({id: 0}, ['id', this.labelField])
				});
				this.options = options;
				
				this.menu.on('show', this.onMenuLoad, this);
			}
			this.store.on('load', this.onLoad, this);
			
			this.bindShowAdapter();
		},
		
		onLoad: function(store, records)
		{
			var visible = this.menu.isVisible();
			this.menu.hide(false);
			
			this.menu.removeAll();
			
			var gid = this.single ? Ext.id() : null;
			for(var i=0, len=records.length; i<len; i++)
			{
				var record = records[i];
				
				var item;

				var itemText = record.get(this.labelField);
				if (itemText.indexOf('-') === 0) // group title
				{
					item = new Ext.menu.Item({
						text: itemText.substring(1),
						activatable: false,
						hideOnClick: false
					});
				}
				else // regular item
				{
					var itemId = record.get(this.valueField);
					
					item = new Ext.menu.CheckItem({
						text:    itemText, 
						group:   gid, 
						checked: this.value.indexOf(itemId) > -1,
						hideOnClick: false});
					
					item.itemId = itemId;
					item.on('checkchange', this.checkChange, this);
				}
							
				this.menu.add(item);
			}
			
			this.setActive(this.isActivatable());
			this.loaded = true;
			
			if(visible)
				this.menu.show(); //Adaptor will re-invoke with previous arguments
		}
	}
);

