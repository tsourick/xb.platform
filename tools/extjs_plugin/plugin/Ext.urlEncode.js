
// skip null values from resulting url
Ext.urlEncode = function(o, pre)
{
	var empty,
			buf = [],
			e = encodeURIComponent;

	Ext.iterate
	(
		o,
		function(key, item)
		{
			// [[ patch
			if (item === null) return;
			// ]]
			
			
			empty = Ext.isEmpty(item);
			Ext.each
			(
				empty ? key : item,
				function(val)
				{
					buf.push
					(
						'&',
						e(key),
						'=',
						(
							!Ext.isEmpty(val) && (val != key || !empty)
						)
						?
						(
							Ext.isDate(val) ? Ext.encode(val).replace(/"/g, '') : e(val)
						)
						: ''
					);
				}
			);
		}
	);
	
	if (!pre)
	{
		buf.shift();
		pre = '';
	}
	
	return pre + buf.join('');
}
