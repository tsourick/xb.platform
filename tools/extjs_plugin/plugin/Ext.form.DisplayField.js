Ext.override
(
	Ext.form.DisplayField,
	{
		renderer: null,
		
		
    getRawValue : function(){
    	return this.getValue();
    },

    setRawValue : function(value){
   		var displayValue = '';
   		
    	if (this.rendered)
    	{
       	displayValue = value;
       	
    		if (this.renderer)
    		{
    			displayValue = this.renderer(displayValue);
    		}
        
        displayValue = Ext.isEmpty(displayValue) ? '' : displayValue;
        
        
    		if (this.htmlEncode)
        {
          displayValue = Ext.util.Format.htmlEncode(displayValue);
        }


        this.el.update(displayValue);
      }
      
      
      return displayValue;
    },

    
    getValue: function()
    {
    	return this.value;
    },
    
    setValue: function(value)
    {
      this.value = value;
       

      this.setRawValue(value);
      
      
      return this;
    }
		
	}
);
