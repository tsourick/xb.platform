
// Menu item tooltip support
// text and title supported only:
//   tooltip: {title: '<title>', text: '<text>'} OR tooltip: '<text>'
Ext.override(Ext.menu.Menu,
{
	afterRender: function()
	{
		Ext.menu.Menu.superclass.afterRender.apply(this, arguments);
		
		var menu = this;
		
		this.tip = new Ext.ToolTip({
			target: this.getEl().getAttribute('id'),
			renderTo: document.body,
			delegate: '.x-menu-item',
			
			title: 'dummy title', // makes title to be rendered
			
			// trackMouse: true,
			
			listeners: {
				beforeshow: function updateTip(tip) {
					// var mi = menu.findById(tip.triggerElement.id);
					var mi = menu.activeItem;
					
					if (!mi || !mi.initialConfig.tooltip)
						return false;
					
					var tt = mi.initialConfig.tooltip;
					var text = Ext.isString(tt) ? tt : tt.text;
					var title = !Ext.isString(tt) ? tt.title : '';

					tip.header.dom.firstChild.innerHTML = title;
					tip.body.dom.innerHTML = text;
					
					/*
					var c = menu.activeItem.initialConfig; 
					if ( c  && c.tooltip)
							tip.update(c.tooltip);
					else
							return false ;					
					*/
				}
			}
		});
	}
});
