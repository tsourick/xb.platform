// Original function patched to apply type convert according to field spec
Ext.data.Record.prototype.set = function(name, value)
{
	// [Convert patch] start
	var f = this.fields.get(name);
	value = f.convert(value);
	// [Convert patch] end
	if(String(this.data[name]) == String(value))
	{
		return;
	}
	this.dirty = true;
	if(!this.modified)
	{
		this.modified = {};
	}
	if(typeof this.modified[name] == 'undefined')
	{
		this.modified[name] = this.data[name];
	}
	this.data[name] = value;
	if(!this.editing && this.store)
	{
		this.store.afterEdit(this);
	}
}


Ext.override(Ext.tree.TreeNode, {
	removeChild : function(node){
			while(node.firstChild){
				node.removeChild(node.firstChild);
			}
		
			this.ownerTree.getSelectionModel().unselect(node);
			Ext.tree.TreeNode.superclass.removeChild.apply(this, arguments);
			// if it's been rendered remove dom node
			if(this.childrenRendered){
					node.ui.remove();
			}
			if(this.childNodes.length < 1){
					this.collapse(false, false);
			}else{
					this.ui.updateExpandIcon();
			}
			if(!this.firstChild && !this.isHiddenRoot()) {
					this.childrenRendered = false;
			}
			return node;
	}
});


// Fix checkbox margins for IE (probably for Safari too)
Ext.override(Ext.form.Checkbox, {
	getResizeEl : function(){
		return this.wrap;
	}
});


// This not only makes destroy(), but also show(), hide(), disable() and enable() act on the label too.

Ext.override(Ext.layout.FormLayout, {
	renderItem : function(c, position, target){
		if(c && !c.rendered && c.isFormField && c.inputType != 'hidden'){
			var args = [
				   c.id, c.fieldLabel,
				   c.labelStyle||this.labelStyle||'',
				   this.elementStyle||'',
				   typeof c.labelSeparator == 'undefined' ? this.labelSeparator : c.labelSeparator,
				   (c.itemCls||this.container.itemCls||'') + (c.hideLabel ? ' x-hide-label' : ''),
				   c.clearCls || 'x-form-clear-left' 
			];
			if(typeof position == 'number'){
				position = target.dom.childNodes[position] || null;
			}
			if(position){
				c.itemCt = this.fieldTpl.insertBefore(position, args, true);
			}else{
				c.itemCt = this.fieldTpl.append(target, args, true);
			}
			c.actionMode = 'itemCt';
			c.render('x-form-el-'+c.id);
			c.container = c.itemCt;
			c.actionMode = 'container';
		}else {
			Ext.layout.FormLayout.superclass.renderItem.apply(this, arguments);
		}
	}
});
Ext.override(Ext.form.TriggerField, {
	actionMode: 'wrap',
	onShow: Ext.form.TriggerField.superclass.onShow,
	onHide: Ext.form.TriggerField.superclass.onHide
});
Ext.override(Ext.form.Checkbox, {
	actionMode: 'wrap',
	getActionEl: Ext.form.Checkbox.superclass.getActionEl
});
Ext.override(Ext.form.HtmlEditor, {
	actionMode: 'wrap'
});


// Fix for Gecko's error "Permission denied to access property 'dom' from non-chrome context" 
Ext.lib.Event.resolveTextNode = Ext.isGecko ? function(node){
	if(!node){
		return;
	}
	var s = HTMLElement.prototype.toString.call(node);
	if(s == '[xpconnect wrapped native prototype]' || s == '[object XULElement]'){
		return;
	}
	return node.nodeType == 3 ? node.parentNode : node;
} : function(node){
	return node && node.nodeType == 3 ? node.parentNode : node;
};

// Checkbox/Radio cumulative bugfix
// From here: http://www.extjs.com/forum/showthread.php?t=44603 
Ext.override(Ext.form.Checkbox, {
	onRender: function(ct, position){
		Ext.form.Checkbox.superclass.onRender.call(this, ct, position);
		if(this.inputValue !== undefined){
			this.el.dom.value = this.inputValue;
		}
		//this.el.addClass('x-hidden');
		this.innerWrap = this.el.wrap({
			//tabIndex: this.tabIndex,
			cls: this.baseCls+'-wrap-inner'
		});
		this.wrap = this.innerWrap.wrap({cls: this.baseCls+'-wrap'});
		this.imageEl = this.innerWrap.createChild({
			tag: 'img',
			src: Ext.BLANK_IMAGE_URL,
			cls: this.baseCls
		});
		if(this.boxLabel){
			this.labelEl = this.innerWrap.createChild({
				tag: 'label',
				htmlFor: this.el.id,
				cls: 'x-form-cb-label',
				html: this.boxLabel
			});
		}
		//this.imageEl = this.innerWrap.createChild({
			//tag: 'img',
			//src: Ext.BLANK_IMAGE_URL,
			//cls: this.baseCls
		//}, this.el);
		if(this.checked){
			this.setValue(true);
		}else{
			this.checked = this.el.dom.checked;
		}
		this.originalValue = this.checked;
	},
	afterRender: function(){
		Ext.form.Checkbox.superclass.afterRender.call(this);
		//this.wrap[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
		this.imageEl[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
	},
	initCheckEvents: function(){
		//this.innerWrap.removeAllListeners();
		this.innerWrap.addClassOnOver(this.overCls);
		this.innerWrap.addClassOnClick(this.mouseDownCls);
		this.innerWrap.on('click', this.onClick, this);
		//this.innerWrap.on('keyup', this.onKeyUp, this);
	},
	onFocus: function(e) {
		Ext.form.Checkbox.superclass.onFocus.call(this, e);
		//this.el.addClass(this.focusCls);
		this.innerWrap.addClass(this.focusCls);
	},
	onBlur: function(e) {
		Ext.form.Checkbox.superclass.onBlur.call(this, e);
		//this.el.removeClass(this.focusCls);
		this.innerWrap.removeClass(this.focusCls);
	},
	onClick: function(e){
		if (e.getTarget().htmlFor != this.el.dom.id) {
			if (e.getTarget() !== this.el.dom) {
				this.el.focus();
			}
			if (!this.disabled && !this.readOnly) {
				this.toggleValue();
			}
		}
		//e.stopEvent();
	},
	onEnable: Ext.form.Checkbox.superclass.onEnable,
	onDisable: Ext.form.Checkbox.superclass.onDisable,
	onKeyUp: undefined,
	setValue: function(v) {
		var checked = this.checked;
		this.checked = (v === true || v === 'true' || v == '1' || String(v).toLowerCase() == 'on');
		if(this.rendered){
			this.el.dom.checked = this.checked;
			this.el.dom.defaultChecked = this.checked;
			//this.wrap[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
			this.imageEl[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
		}
		if(checked != this.checked){
			this.fireEvent("check", this, this.checked);
			if(this.handler){
				this.handler.call(this.scope || this, this, this.checked);
			}
		}
	},
	getResizeEl: function() {
		//if(!this.resizeEl){
			//this.resizeEl = Ext.isSafari ? this.wrap : (this.wrap.up('.x-form-element', 5) || this.wrap);
		//}
		//return this.resizeEl;
		return this.wrap;
	}
});
Ext.override(Ext.form.Radio, {
	checkedCls: 'x-form-radio-checked'
});

// Checkbox/Radio cumulative bugfix (validation part)
// From here: http://www.extjs.com/forum/showthread.php?t=44603 
Ext.override(Ext.form.Field, {
	markInvalid : function(msg){
		if(!this.rendered || this.preventMark){
			return;
		}
		var markEl = this.markEl || this.el;
		markEl.addClass(this.invalidClass);
		msg = msg || this.invalidText;
		switch(this.msgTarget){
			case 'qtip':
				markEl.dom.qtip = msg;
				markEl.dom.qclass = 'x-form-invalid-tip';
				if(Ext.QuickTips){
					Ext.QuickTips.enable();
				}
				break;
			case 'title':
				markEl.dom.title = msg;
				break;
			case 'under':
				if(!this.errorEl){
					var elp = this.getErrorCt();
					if(!elp){
						markEl.dom.title = msg;
						break;
					}
					this.errorEl = elp.createChild({cls:'x-form-invalid-msg'});
					this.errorEl.setWidth(elp.getWidth(true)-20);
				}
				this.errorEl.update(msg);
				Ext.form.Field.msgFx[this.msgFx].show(this.errorEl, this);
				break;
			case 'side':
				if(!this.errorIcon){
					var elp = this.getErrorCt();
					if(!elp){
						markEl.dom.title = msg;
						break;
					}
					this.errorIcon = elp.createChild({cls:'x-form-invalid-icon'});
				}
				this.alignErrorIcon();
				this.errorIcon.dom.qtip = msg;
				this.errorIcon.dom.qclass = 'x-form-invalid-tip';
				this.errorIcon.show();
				this.on('resize', this.alignErrorIcon, this);
				break;
			default:
				var t = Ext.getDom(this.msgTarget);
				t.innerHTML = msg;
				t.style.display = this.msgDisplay;
				break;
		}
		this.fireEvent('invalid', this, msg);
	},
	clearInvalid : function(){
		if(!this.rendered || this.preventMark){
			return;
		}
		var markEl = this.markEl || this.el;
		markEl.removeClass(this.invalidClass);
		switch(this.msgTarget){
			case 'qtip':
				markEl.dom.qtip = '';
				break;
			case 'title':
				markEl.dom.title = '';
				break;
			case 'under':
				if(this.errorEl){
					Ext.form.Field.msgFx[this.msgFx].hide(this.errorEl, this);
				}else{
					markEl.dom.title = '';
				}
				break;
			case 'side':
				if(this.errorIcon){
					this.errorIcon.dom.qtip = '';
					this.errorIcon.hide();
					this.un('resize', this.alignErrorIcon, this);
				}else{
					markEl.dom.title = '';
				}
				break;
			default:
				var t = Ext.getDom(this.msgTarget);
				t.innerHTML = '';
				t.style.display = 'none';
				break;
		}
		this.fireEvent('valid', this);
	},
	alignErrorIcon : function(){
		this.errorIcon.alignTo(this.markEl || this.el, 'tl-tr', [2, 0]);
	}
});
Ext.override(Ext.form.Checkbox, {
	onRender: function(ct, position){
		Ext.form.Checkbox.superclass.onRender.call(this, ct, position);
		if(this.inputValue !== undefined){
			this.el.dom.value = this.inputValue;
		}
		this.el.removeClass(this.baseCls);
		//this.el.addClass('x-hidden');
		this.innerWrap = this.el.wrap({
			//tabIndex: this.tabIndex,
			cls: this.baseCls+'-wrap-inner'
		});
		this.wrap = this.innerWrap.wrap({cls: this.baseCls+'-wrap'});
		this.imageEl = this.innerWrap.createChild({
			tag: 'img',
			src: Ext.BLANK_IMAGE_URL,
			cls: this.baseCls
		});
		if(this.boxLabel){
			this.labelEl = this.innerWrap.createChild({
				tag: 'label',
				htmlFor: this.el.id,
				cls: 'x-form-cb-label',
				html: this.boxLabel
			});
		}
		//this.imageEl = this.innerWrap.createChild({
			//tag: 'img',
			//src: Ext.BLANK_IMAGE_URL,
			//cls: this.baseCls
		//}, this.el);
		if(this.checked){
			this.setValue(true);
		}else{
			this.checked = this.el.dom.checked;
		}
		this.originalValue = this.checked;
		this.markEl = this.innerWrap;
	},
	afterRender: function(){
		Ext.form.Checkbox.superclass.afterRender.call(this);
		//this.wrap[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
		this.imageEl[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
	},
	initCheckEvents: function(){
		//this.innerWrap.removeAllListeners();
		this.innerWrap.addClassOnOver(this.overCls);
		this.innerWrap.addClassOnClick(this.mouseDownCls);
		this.innerWrap.on('click', this.onClick, this);
		//this.innerWrap.on('keyup', this.onKeyUp, this);
		if(this.validationEvent !== false){
			this.el.on(this.validationEvent, this.validate, this, {buffer: this.validationDelay});
		}
	},
	onFocus: function(e) {
		Ext.form.Checkbox.superclass.onFocus.call(this, e);
		//this.el.addClass(this.focusCls);
		this.innerWrap.addClass(this.focusCls);
	},
	onBlur: function(e) {
		Ext.form.Checkbox.superclass.onBlur.call(this, e);
		//this.el.removeClass(this.focusCls);
		this.innerWrap.removeClass(this.focusCls);
	},
	onClick: function(e){
		if (e.getTarget().htmlFor != this.el.dom.id) {
			if (e.getTarget() != this.el.dom) {
				this.el.focus();
			}
			if (!this.disabled && !this.readOnly) {
				this.toggleValue();
			}
		}
		//e.stopEvent();
	},
	onEnable: Ext.form.Checkbox.superclass.onEnable,
	onDisable: Ext.form.Checkbox.superclass.onDisable,
	onKeyUp: undefined,
	setValue: function(v) {
		var checked = this.checked;
		this.checked = (v === true || v === 'true' || v == '1' || String(v).toLowerCase() == 'on');
		if(this.rendered){
			this.el.dom.checked = this.checked;
			this.el.dom.defaultChecked = this.checked;
			//this.wrap[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
			this.imageEl[this.checked ? 'addClass' : 'removeClass'](this.checkedCls);
		}
		if(checked != this.checked){
			this.fireEvent("check", this, this.checked);
			if(this.handler){
				this.handler.call(this.scope || this, this, this.checked);
			}
		}
	},
	getResizeEl: function() {
		//if(!this.resizeEl){
			//this.resizeEl = Ext.isSafari ? this.wrap : (this.wrap.up('.x-form-element', 5) || this.wrap);
		//}
		//return this.resizeEl;
		return this.wrap;
	},
	markInvalid: Ext.form.Checkbox.superclass.markInvalid,
	clearInvalid: Ext.form.Checkbox.superclass.clearInvalid,
	validationEvent: 'click',
	validateOnBlur: false,
	validateValue: function(value){
		if(this.vtype){
			var vt = Ext.form.VTypes;
			if(!vt[this.vtype](value, this)){
				this.markInvalid(this.vtypeText || vt[this.vtype +'Text']);
				return false;
			}
		}
		if(typeof this.validator == "function"){
			var msg = this.validator(value);
			if(msg !== true){
				this.markInvalid(msg);
				return false;
			}
		}
		return true;
	}
});
Ext.override(Ext.form.Radio, {
	checkedCls: 'x-form-radio-checked',
	markInvalid: Ext.form.Radio.superclass.markInvalid,
	clearInvalid: Ext.form.Radio.superclass.clearInvalid
});


// totalLength updating
Ext.override(Ext.data.Store, {
    add : function(records){
        records = [].concat(records);
        if(records.length < 1){
            return;
        }
        for(var i = 0, len = records.length; i < len; i++){
            records[i].join(this);
        }
        var index = this.data.length;
        this.data.addAll(records);
        if(this.snapshot){
            this.snapshot.addAll(records);
        }
        this.totalLength += records.length; // here it is
        this.fireEvent('add', this, records, index);
    },
    remove : function(record){
        var index = this.data.indexOf(record);
        this.data.removeAt(index);
        if(this.pruneModifiedRecords){
            this.modified.remove(record);
        }
        if(this.snapshot){
            this.snapshot.remove(record);
        }
        this.totalLength--; // here it is
        this.fireEvent("remove", this, record, index);
/*
        if(Ext.isArray(record)){
            Ext.each(record, function(r){
                this.remove(r);
            }, this);
        }
        var index = this.data.indexOf(record);
        if(index > -1){
            record.join(null);
            this.data.removeAt(index);
            if(this.pruneModifiedRecords){
                this.modified.remove(record);
            }
            if(this.snapshot){
                this.snapshot.remove(record);
            }
            this.totalLength--; // here it is
            this.fireEvent('remove', this, record, index);
        }
				*/
    },
    removeAll : function(){
        this.data.clear();
        if(this.snapshot){
            this.snapshot.clear();
        }
        if(this.pruneModifiedRecords){
            this.modified = [];
        }
        this.totalLength = 0; // here it is
        this.fireEvent("clear", this);

/*
        var items = [];
        this.each(function(rec){
            items.push(rec);
        });
        this.clearData();
        if(this.snapshot){
            this.snapshot.clear();
        }
        if(this.pruneModifiedRecords){
            this.modified = [];
        }
        this.totalLength = 0; // here it is
        this.fireEvent('clear', this, items);
*/
    }
});


Ext.override(Ext.menu.Menu, {
    // See http://extjs.com/forum/showthread.php?t=33475&page=2
    showAt : function(xy, parentMenu, /* private: */_e) {
        this.parentMenu = parentMenu;
        if (!this.el) {
            this.render();
        }
        if (_e !== false) {
            this.fireEvent("beforeshow", this);
            xy = this.el.adjustForConstraints(xy);
        }
        this.el.setXY(xy);

        // Start of extra logic to what is in Ext source code...
        // See http://www.extjs.com/deploy/ext/docs/output/Menu.jss.html
        // get max height from body height minus y cordinate from this.el
        var maxHeight = Ext.getBody().getHeight() - xy[1];
        if (this.el.getHeight() > maxHeight) {
            // set element with max height and apply vertical scrollbar
            this.el.setHeight(maxHeight);
            this.el.applyStyles('overflow-y: auto;');
        }
        // .. end of extra logic to what is in Ext source code

        this.el.show();
        this.hidden = false;
        this.focus();
        this.fireEvent("show", this);
    }
});  



Ext.override
(
	Ext.Editor,
	{
		initComponent: function()
		{
			Ext.Editor.superclass.initComponent.call(this);
			this.addEvents(
					"beforestartedit",
					"startedit",
					"beforecomplete",
					"complete",
					"canceledit",
					"specialkey"
			);
			
			this.field.editor = this;
		}
	}
);


Ext.override
(
	Ext.grid.ColumnModel,
	{
    getCellEditor : function(colIndex, rowIndex){
// OVERRIDES START
			var ed = this.config[colIndex].editor
			ed.grid = this.grid;
			
			return ed;
// OVERRIDES END

			// return this.config[colIndex].editor;
    }
	}
);


Ext.override
(
	Ext.grid.GridPanel,
	{
    initComponent : function(){
        Ext.grid.GridPanel.superclass.initComponent.call(this);

        // override any provided value since it isn't valid
        // and is causing too many bug reports ;)
        this.autoScroll = false;
        this.autoWidth = false;

        if(Ext.isArray(this.columns)){
            this.colModel = new Ext.grid.ColumnModel(this.columns);
            
// OVERRIDES START
            this.colModel.grid = this;
// OVERRIDES END
            
            
            delete this.columns;
        }

        // check and correct shorthanded configs
        if(this.ds){
            this.store = this.ds;
            delete this.ds;
        }
        if(this.cm){
            this.colModel = this.cm;
            delete this.cm;
        }
        if(this.sm){
            this.selModel = this.sm;
            delete this.sm;
        }
        this.store = Ext.StoreMgr.lookup(this.store);

        this.addEvents(
            // raw events
            "click",
            "dblclick",
            "contextmenu",
            "mousedown",
            "mouseup",
            "mouseover",
            "mouseout",
            "keypress",
            "keydown",

            // custom events
            "cellmousedown",
            "rowmousedown",
            "headermousedown",
            "cellclick",
            "celldblclick",
            "rowclick",
            "rowdblclick",
            "headerclick",
            "headerdblclick",
            "rowcontextmenu",
            "cellcontextmenu",
            "headercontextmenu",
            "bodyscroll",
            "columnresize",
            "columnmove",
            "sortchange"
        );
    }
  }
);
