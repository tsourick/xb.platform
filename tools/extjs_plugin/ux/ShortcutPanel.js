Ext.ux.Shortcut = Ext.extend
(
	Ext.Panel,
	{
		// id: 'desktopShortcutText',
		x: 10,
		y: 10,
		width: 80,
		height: 80,
		
		imageSrc: '',
		
		text: '',
		
		callback: Ext.emptyFn,
		
		initComponent: function()
		{
			// Before parent code

			/*
			Ext.apply
			(
				this,
				{
					baseCls: 'applicationShortcut',
					
					html: '<div class="shortcutBody"><div><img src="' + this.imageSrc + '" /></div><div>' + this.text  + '</div></div>'
				}
			);
			*/
			Ext.apply
			(
				this,
				{
					baseCls: 'applicationShortcut',
					
					html: '<div class="shortcutBody"><div><img src="' + this.imageSrc + '" /></div><div><!-- this.text cut will be here --></div></div>'
				}
			);

			// Call parent (required)
			Ext.ux.Shortcut.superclass.initComponent.apply(this, arguments);

			// After parent code
			// e.g. install event handlers on rendered component
		},
		
		onRender: function(ct, position)
		{
			// Before parent code

			// Call parent (required)
			Ext.ux.Shortcut.superclass.onRender.apply(this, arguments);
			
			// After parent code
			this.el.addClassOnOver('applicationShortcutOver');
			this.el.on('dblclick', this.callback);
			this.el.unselectable();
		},
		
		afterRender: function()
		{
			Ext.ux.Shortcut.superclass.afterRender.apply(this);
			
			var bodyEl = this.el.child('div.shortcutBody');
			bodyEl.setHeight(this.height - 2);
			

			// Cut overflowing words
			
			var bodyWidth = bodyEl.getWidth(true);
			var textEl = bodyEl.child('div:last');

			var words = this.text.split(' ');
			
			Ext.each
			(
				words,
				function (word, i)
				{
					var size = Ext.util.TextMetrics.measure(textEl, word); // we don't fix width here and disregard calculation of the height
					
					if (size.width > bodyWidth)
					{
						_word = word;
						do
						{
							_word = _word.substr(0, _word.length - 1); // pop last char

							var size = Ext.util.TextMetrics.measure(textEl, _word); // we don't fix width here and disregard calculation of the height
						}
						while (size.width > bodyWidth);
						
						// pop two chars before the last one to get space for '...' and one for last char
						word = _word.substr(0, _word.length - 3) + '...' + word.substr(word.length - 1, 1); 
						
						words[i] = word;
					}
				}
			);
			
			this.displayText = words.join(' ');
			
			
			textEl.update(this.displayText);
			
			
			// Add tooltip (quicktip) if necessary
			
			if (this.text != this.displayText)
			{
				this.toolTip = new Ext.ToolTip
				(
					{
						target: bodyEl,
						title: this.text,

						showDelay: 600
					}
				);				
			}
		}
	}
);

Ext.reg('shortcut', Ext.ux.Shortcut);


Ext.layout.ShortcutLayout = Ext.extend(Ext.layout.AbsoluteLayout, {
    onLayout : function(ct, target){
        Ext.layout.ShortcutLayout.superclass.onLayout.call(this, ct, target);

				var hostWidth = ct.getInnerWidth(), hostHeight = ct.getInnerHeight();
				var cs = ct.items.items, csNumber = cs.length;
				
				if (this.growOrientation == 'v')
				{
					var perColNum = parseInt((hostHeight + this.spacing) / (this.shortcutHeight + this.spacing));

					var colPos = 1;
					var colIndex = 0;
					for (var i = 0; i < csNumber; i++)
					{
						if (colPos > perColNum)
						{
							colPos = 1;
							colIndex++;
						}
						
						var c = cs[i];
						c.setPosition(colIndex * (this.shortcutWidth + this.spacing), (colPos - 1) * (this.shortcutHeight + this.spacing));
						
						colPos++;
					}
				}
    }
});
Ext.Container.LAYOUTS['shortcut'] = Ext.layout.ShortcutLayout;


Ext.ux.ShortcutPanel = Ext.extend
(
	Ext.Panel,
	{
		// Disabled
		// items: [],
		
		shortcutWidth: 80,
		shortcutHeight: 80,
		spacing: 5,
		growOrientation: 'v',

		initComponent: function()
		{
			// Before parent code

			Ext.apply
			(
				this,
				{
					defaultType: 'shortcut',
					layout: 'shortcut',
					layoutConfig: 
					{
						shortcutWidth: this.shortcutWidth,
						shortcutHeight: this.shortcutHeight,
						growOrientation: this.growOrientation,
						spacing: this.spacing
					}
				}
			);

			var cs = this.items, c, i, l = cs.length;
			for (i = 0; i < l; i++)
			{
				c = cs[i];
				c.width = this.shortcutWidth;
				c.height = this.shortcutHeight;
			}

			// Call parent (required)
			Ext.ux.ShortcutPanel.superclass.initComponent.apply(this, arguments);

			
			// After parent code
			// e.g. install event handlers on rendered component
		}
	}
);

Ext.reg('shortcutpanel', Ext.ux.ShortcutPanel);



