
/**
*
* Config:
*  menuConfig: {title: <string>, items: <menu item configs>}
*  shortcutConfig: {items: <shortcut item configs>, ...any shortcut panel configs except items}
*  taskbarConfig: {showClock: <true|false>}
*
*
*/
Ext.ux.Desktop = function (config)
{
	// Create shortcut panel
	
	var shortcutPanelConfigDefaults =
	{
		border: false,

		bodyStyle: 'background-color: transparent',

		id: 'desktop',
		
		items: config.shortcutConfig.items
	};
	delete config.shortcutConfig.items; // skip items
	var shortcutPanelConfig = Ext.apply(shortcutPanelConfigDefaults, config.shortcutConfig); // apply the rest of properties if any

	this.shortcutPanel = new Ext.ux.ShortcutPanel(shortcutPanelConfig);


	// Create taskbar
	
	this.taskbar = new Ext.ux.Desktop.Taskbar(config.taskbarConfig);
	

	// Arrange interface elements
	
	var centerPanel = new Ext.Panel
	(
		{
			region: 'center',
			layout: 'fit',
			
			items: this.shortcutPanel,
			
			bbar: this.taskbar
		}
	);
			

	this.viewport = new Ext.Viewport
	(
		{
			id: 'viewport',
			
			layout: 'border',
			items:
			[
				centerPanel
			]
		}
	);
	
	
	this.taskbar.initTaskbar();
	

	var desktop = this;
	this.WindowManager = function()
	{
		var windows = new Ext.util.MixedCollection();

		
		desktop.shortcutPanel.addListener
		(
			'resize',
			function (v)
			{
				
				// var vs = v.getSize();
				// var vs = desktop.shortcutPanel.getSize();
				
				windows.each
				(
					function (wo, index, length)
					{
						// var wSpace = 50, hSpace = 50;
						var w = wo.w;
						
						
						w.el.shadowOffset = w.el.shadow.offset; // quick bug fix
						w.onWindowResize();
						/*
						if (w.maximized)
						{
								w.fitContainer();
						}
						w.doConstrain();
						*/
						

						var vs = w.container.getSize();
						var ws = w.getSize();
						
						var newS = {};
						var newW = null, newH = null;
						if (ws.width > vs.width)
						{
							newW = vs.width;
						}
						if (ws.height > vs.height)
						{
							newH = vs.height;
						}
						
						if (newW || newH)
						{
							if (newW) newS.width = newW;
							if (newH) newS.height = newH;

							w.setSize(newS);
							// console.log('size set');
							
							w.center();
						}

					}
				);
				
			}
		);
		
		
		var onWindowActivate = function (w)
		{
			var wId = w.getId();

			if (windows.containsKey(wId))
			{
				var o = windows.get(wId);
				
				// o.w.show();

				desktop.taskbar.onActivate(o.tb);
				// o.tb.toggle(true);
			}
		}
		
		var onWindowDeactivate = function (w)
		{
			var wId = w.getId();

			if (windows.containsKey(wId))
			{
				var o = windows.get(wId);
				
				// o.w.show();

				desktop.taskbar.onDeactivate(o.tb);
				// o.tb.toggle(true);
			}
		}

		var onTaskbarActivate = function (w)
		{
			var wId = w.getId();

			if (windows.containsKey(wId))
			{
				var o = windows.get(wId);
				
				o.w.show();

				// desktop.taskbar.onActivate(o.tb);
				// o.tb.toggle(true);
			}
		}
		
		
		return {
			register: function (w)
			{
				var tb = desktop.taskbar.addTask(w.title, w.iconCls, onTaskbarActivate.createCallback(w));
				
				w.setAnimateTarget(tb.getEl());

				w.on('activate', onWindowActivate);
				w.on('hide', onWindowDeactivate);

				
				if (w.minimizable)
				{
					w.on
					(
						'minimize',
						function(w)
						{
							w.hide();
						}
					);
				}

				
				var o = 
				{
					w: w,
					tb: tb
				};
				
				windows.add(w.getId(), o);
			},
			
			unregister: function (w)
			{
				var wId = w.getId();
				
				if (windows.containsKey(wId))
				{
					var o = windows.get(wId);
					
					// o.tb.destroy();
					desktop.taskbar.removeTask(o.tb);

					windows.removeKey(wId);
				}
			},

			activate: function (w)
			{
				var wId = w.getId();

				if (windows.containsKey(wId))
				{
					var o = windows.get(wId);
					
					o.w.show();

					desktop.taskbar.onActivate(o.tb);
					// o.tb.toggle(true);
				}
			}
		}			
	}();
}


Ext.ux.Desktop.Taskbar = function (config)
{
	var configDefaults =
	{
		showClock: true
	};
	var config = Ext.apply(configDefaults, config);
	
	
	this.menu = new Ext.menu.Menu
	(
		{
			items: config.menuConfig.items
		}
	);


	var toolbarButtonConfigs = 
	[
		' ',
		// new Ext.Toolbar.ClickableButton
		new Ext.Toolbar.Button
		(
			{
				text: config.menuConfig.title,
				minWidth: 100,
				ctCls: 'x-btn-over',
				
				menu: this.menu
			}
		),
		'-',
		'->'
	];
	
	if (config.showClock)
	{
		toolbarButtonConfigs.push
		(
			'-',
			{
				id: 'clock',
				text: '',
				minWidth: 40,
				disabled: true
			},
			'-'
		);
	}

	toolbarButtonConfigs.push(' ');
	
	
	// this.toolbar = new Ext.Toolbar(toolbarButtonConfigs);
	delete config.menuConfig;
	config.items = toolbarButtonConfigs;
	
	
	// Ext.apply(this.config);
	
	
	Ext.ux.Desktop.Taskbar.superclass.constructor.call(this, config);
}

Ext.extend
(
	Ext.ux.Desktop.Taskbar,
	Ext.Toolbar,
	{
		initTaskbar: function ()
		{
			if (this.showClock)
			{
				var clock = this.items.get('clock');
				
				var tc = new Ext.ux.TimeCounter
				(
					{
						interval: 1000,
						format: 'H:i',
						callback: function (tc, formattedTime, date)
						{
							clock.setText(formattedTime);
						}
					}
				);
				tc.start();
			}
		},
		
		addTask: function (text, iconCls, activateCallback)
		{
			var bb = this.insertButton
			(
				3,
				new Ext.Toolbar.Button(
				{
					text: text,
					minWidth: 100,
					ctCls: 'x-btn-over taskbarButton',
					
					iconCls: iconCls,
					
					enableToggle: true,
					toggleGroup: 'taskbuttons',
					allowDepress: false,
					
					listeners:
					{
						// click: CMS.Desktop.WinMan.activate.createCallback(w)
						click: activateCallback
					}
				})
			);
			
			this.doLayout();
			
			return bb;
		},
		
		removeTask: function (button)
		{
			button.destroy();
		},
		
		addTrayItem: function (config)
		{
			return this.insertButton(4, config);
		},
		
		onActivate: function (button)
		{
			button.toggle(true);			
		},
		
		onDeactivate: function (button)
		{
			button.toggle(false);			
		}
	}	
);
