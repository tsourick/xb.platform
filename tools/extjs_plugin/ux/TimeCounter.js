/**
* Config:
*   interval: <seconds>
*   format: <time format>
*   callback: function ()
*
*/
Ext.ux.TimeCounter = function (config)
{
	var dt = new Ext.util.DelayedTask();
	
	this.interval = config.interval;
	this.format = config.format;
	this.callback = config.callback;
	

	this.start = function ()
	{
		dt.delay
		(
			config.interval,
			function ()
			{
				var date = new Date();
				var formattedTime = date.format(this.format);
				
				this.callback(this, formattedTime, date);
				
	
				dt.delay(this.interval);
			},
			this
		);
	}
	
	this.stop = function ()
	{
		dt.cancel();
	}
}
