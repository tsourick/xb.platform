/****************************************************
* FCKEditor Extension
*****************************************************/

// Patch 1 (for IE)

var FCKPatch1_IsDirty = function ()
{
	if (this.EditMode==1)
	{
		return (this.StartupValue!=this.EditingArea.Textarea.value);
	}
	else
	{
		if (!this.EditorDocument)
		{
			return false;
		}

		var bodyHTML = this.EditorDocument.body ? this.EditorDocument.body.innerHTML : null;
		return (this.StartupValue!=bodyHTML);
	}
}


var oFCKeditorOptions = {
    BasePath: '',
    Config: {
        BaseHref: window.location,
        SkinPath: '',
        ProcessHTMLEntities: true,
        ProcessNumericEntities: false
    },
    ToolbarSet: 'Default'
};

// Override with predefined options
if (oFCKeditorOptionsBase) Ext.apply(oFCKeditorOptions, oFCKeditorOptionsBase);

Ext.form.FCKeditor = function(config){
    this.config = config;
    Ext.form.FCKeditor.superclass.constructor.call(this, config);
    this.FCKid=0;
    this.MyisLoaded=false;
    this.MyValue='';
		this.on({beforedestroy: this.destroyInstance});
};

Ext.extend(Ext.form.FCKeditor, Ext.form.TextArea,  {
			onRender: function(ct, position){
        if(!this.el){
            this.defaultAutoCreate = {
                tag: "textarea",
                style:"width:100px;height:60px;",
                autocomplete: "off"
            };
        }
        Ext.form.TextArea.superclass.onRender.call(this, ct, position);
        //Hide textarea to stop flashing up before FCKEditor renders.
        this.hideMode = "visibility"; // set hideMode to visibility, to retain height.
        this.hidden = true; // hide textarea
        
        if(this.grow){
            this.textSizeEl = Ext.DomHelper.append(document.body, {
                tag: "pre", cls: "x-form-grow-sizer"
            });
            if(this.preventScrollbars){
                this.el.setStyle("overflow", "hidden");
            }
            this.el.setHeight(this.growMin);
        }
        if (this.FCKid==0) this.FCKid=get_FCKeditor_id_value()
        // setTimeout("loadFCKeditor('"+this.id+"',"+ this.config.height +");",100); //Change this.name to this.id    
        loadFCKeditor(this.id, this.config.height); //Change this.name to this.id    
    },
    setValue: function(value){
        this.MyValue=value;
        if (this.FCKid==0) this.FCKid=get_FCKeditor_id_value();
        // only after FCKeditor_OnComplete
        if (this.MyisLoaded){
          FCKeditorSetValue(this.FCKid,this.id,value); //Change this.name to this.id
        } else {
          //alert('MyisLoaded = false');
        }
        Ext.form.TextArea.superclass.setValue.apply(this,[value]);
    },

    getValue: function(){
        if (this.MyisLoaded){
            value=FCKeditorGetValue(this.id); //Change this.name to this.id
            Ext.form.TextArea.superclass.setValue.apply(this,[value]);
			if (Ext.form.TextArea.superclass.getValue(this))
	            return Ext.form.TextArea.superclass.getValue(this);
			else
				return value;
        }else{
            return this.MyValue;
        }
    },

    getRawValue: function(){
        if (this.MyisLoaded){
            value=FCKeditorGetValue(this.id); //Change this.name to this.id
            Ext.form.TextArea.superclass.setRawValue.apply(this,[value]);
            return Ext.form.TextArea.superclass.getRawValue(this);
        }else{
            return this.MyValue;
        }
	  },
		destroyInstance: function(){		
			if (FCKeditorAPI.Instances[this.name]) {
				delete FCKeditorAPI.Instances[this.name];
			}
		}
});
Ext.reg('fckeditor', Ext.form.FCKeditor);


function loadFCKeditor(element, height){
    oFCKeditor = new FCKeditor( element );
    oFCKeditor.BasePath      = oFCKeditorOptions.BasePath;
    oFCKeditor.ToolbarSet    = oFCKeditorOptions.ToolbarSet;
    oFCKeditor.Config        = oFCKeditorOptions.Config;
    oFCKeditor.Height = height;
    oFCKeditor.ReplaceTextarea();
}
function FCKeditor_OnComplete(editorInstance){

    var fckComponent = Ext.getCmp(editorInstance.Name);
		fckComponent.MyisLoaded=true;
		fckComponent.fireEvent('complete');

    // Removed OnStatusChange element, does not appear to be need, cause permission error in IE
    // })
}
var FCKeditor_value=new Array();
function FCKeditorSetValue(id,name,value){
    if ((id!=undefined)&&(name!=undefined)){
        if (value!=undefined) FCKeditor_value[id]=value;
        else if (FCKeditor_value[id]==undefined) FCKeditor_value[id]='';
        var oEditor = FCKeditorAPI.GetInstance(name);
        // some trouble in Opera 9.50
        if(oEditor!=undefined) oEditor.SetData(FCKeditor_value[id]);
    }
}
function FCKeditorGetValue(name){
    if ((id!=undefined)&&(name!=undefined)){
        data='';
        var oEditor = FCKeditorAPI.GetInstance(name);
				oEditor.IsDirty = FCKPatch1_IsDirty;
       // some trouble in Opera 9.50:
        //
        // message: Statement on line 36: Cannot convert undefined or null to Object
        // oEditor.GetData();
        //
        if(oEditor!=undefined)
				{
					data=oEditor.GetData(false);
				}
        return data;
    }
}

var FCKeditor_id_value;
function get_FCKeditor_id_value(){
    if (!FCKeditor_id_value){
        FCKeditor_id_value=0;
    }
    FCKeditor_id_value=FCKeditor_id_value+1;
    return FCKeditor_id_value;
}
