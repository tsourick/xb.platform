/*
Ext.Toolbar.ClickableButton = Ext.extend
(
	Ext.Toolbar.Button,
	{
		click : function()
		{
			var e = Ext.EventObject;
			
			e.button = this;
			
			if(!this.disabled){
					if(this.enableToggle && (this.allowDepress !== false || !this.pressed)){
							this.toggle();
					}
					if(this.menu && !this.menu.isVisible() && !this.ignoreNextClick){
							this.showMenu();
					}
					this.fireEvent("click", this, e);
					if(this.handler){
							this.handler.call(this.scope || this, this, e);
					}
			}
		}
	}
);
*/


Ext.util.Format.ucfirst = function (s)
{
	return !s ? s : s.charAt(0).toUpperCase() + s.substr(1);
}

// New function to provide the record field bulk update
Ext.data.Record.prototype.update = function(data)
{
	var record = this;
	
	record.beginEdit();
	
	record.fields.each
	(
		function(field)
		{
			if (typeof(data[field.name]) !== 'undefined')
			{
				record.set(field.name, data[field.name]);
			}
		}
	);
	
	record.endEdit();
	record.commit();
}


Ext.override(Ext.data.Store, {
	findRecord: function(prop, value)
	{
		var record = null;
		
		if(this.getCount() > 0)
		{
			this.each(function(r)
			{
				if(r.data[prop] == value)
				{
					record = r;
					return false;
				}
			});
		}
		
		return record;
	},
	
	findRecordIndex: function(prop, value)
	{
		var r = this.findRecord(prop, value);
		return this.indexOf(r);
	},
	
	// @param record  record OR index
	getPrevNextRecord: function (record, prev)
	{
		var r;
		// debugger;
		var index = Ext.isNumber(record) ? record : this.indexOf(record);
		if (index != -1)
		{
			if (prev) r = this.getAt(--index); else r = this.getAt(++index);
		}
		
		return r;
	},
	getPrevRecord: function (r) { return this.getPrevNextRecord(r, true); },
	getNextRecord: function (r) { return this.getPrevNextRecord(r); }
});


Ext.override
(
	Ext.form.BasicForm,
	{
    findFieldByName: function(name)
		{
			var field = null;
			
			this.items.each
			(
				function(f)
				{
					if(f.isFormField && (f.getName() == name))
					{
						field = f;
						return false;
					}
				}
			);
			
			return field;
    }
	}
);


Ext.override
(
	Ext.Window,
	{
		show: function(animateTarget, cb, scope){
				if(!this.rendered){
						this.render(Ext.getBody());
				}
				if(this.hidden === false){
						this.toFront();
						return;
				}
				if(this.fireEvent("beforeshow", this) === false){
						return;
				}
				if(cb){
						this.on('show', cb, scope, {single:true});
				}
				this.hidden = false;
				
				var thisAnimateTarget = this.animateTarget;
				
				if(animateTarget !== undefined){
						this.setAnimateTarget(animateTarget);
				}
				this.beforeShow();
				if(this.animateTarget){
						this.animShow();
				}else{
						this.afterShow();
				}
				if(animateTarget !== undefined){
					this.setAnimateTarget(thisAnimateTarget);
				}
		}
	}
);


// Toolbar which reacts to store's add/remove record
Ext.ux.SensitivePagingToolbar = Ext.extend
(
	Ext.PagingToolbar,
	{
    unbind : function(store){
        store = Ext.StoreMgr.lookup(store);
        store.un("beforeload", this.beforeLoad, this);
        store.un("load", this.onLoad, this);
        store.un("loadexception", this.onLoadError, this);
				
				store.un('datachanged', this.onChange, this);
				store.un('add', this.onChange, this);
				store.un('remove', this.onChange, this);
				store.un('clear', this.onClear, this);
				
        this.store = undefined;
    },

    bind : function(store){
        store = Ext.StoreMgr.lookup(store);
        store.on("beforeload", this.beforeLoad, this);
        store.on("load", this.onLoad, this);
        store.on("loadexception", this.onLoadError, this);

				store.on('datachanged', this.onChange, this);
				store.on('add', this.onChange, this);
				store.on('remove', this.onChange, this);
				store.on('clear', this.onClear, this);

        this.store = store;
    },
		
    onChange : function(){
        if(this.rendered){
            var d = this.getPageData(), ap = d.activePage, ps = d.pages;
						this.afterTextEl.el.innerHTML = String.format(this.afterPageText, d.pages);
						this.field.dom.value = ap;
            this.first.setDisabled(ap == 1);
            this.prev.setDisabled(ap == 1);
            this.next.setDisabled(ap == ps);
            this.last.setDisabled(ap == ps);
						this.loading.enable();
            this.updateInfo();
        }
        this.fireEvent('change', this, d);
    },
    onClear : function(){
        this.cursor = 0;
        this.onChange();
    }
	}
);


Ext.ux.SensitiveComboBox = Ext.extend
(
	Ext.form.ComboBox,
	{
    initComponent : function ()
    {
    	Ext.ux.SensitiveComboBox.superclass.initComponent.call(this);
    	
			this.addEvents
			(
				'clear'
			);
		},
        
    // private
    bindStore: function(store, initial)
    {
        if(this.store && !initial){
            this.store.un('beforeload', this.onBeforeLoad, this);
            this.store.un('load', this.onLoad, this);
            this.store.un('loadexception', this.collapse, this);

            this.store.un('clear', this.onStoreClear, this);
            // this.store.un('datachanged', this.onStoreDataChanged, this);
            this.store.un('remove', this.onStoreRemove, this);
            this.store.un('update', this.onStoreUpdate, this);

            if(!store){
                this.store = null;
                if(this.view){
                    this.view.setStore(null);
                }
            }
        }
        if(store){
            this.store = Ext.StoreMgr.lookup(store);

            this.store.on('beforeload', this.onBeforeLoad, this);
            this.store.on('load', this.onLoad, this);
            this.store.on('loadexception', this.collapse, this);

            this.store.on('clear', this.onStoreClear, this);
            // this.store.on('datachanged', this.onStoreDataChanged, this);
            this.store.on('remove', this.onStoreRemove, this);
            this.store.on('update', this.onStoreUpdate, this);

            if(this.view){
                this.view.setStore(store);
            }
        }
    },
    
		onStoreClear: function ()
		{
			this.clearValue();
			
			this.fireEvent('clear', this);
		},
		
		onStoreDataChanged: function ()
		{
		},
		
		onStoreRemove: function (store, record, index)
		{
			if (record.data[this.valueField] == this.getValue())
			{
				this.clearValue();

				this.fireEvent('clear', this);
			}
		},
		
		onStoreUpdate: function (store, record, operation)
		{
			if (operation == Ext.data.Record.COMMIT)
			{
				var v = this.getValue();
				
				if (record.data[this.valueField] == v)
				{
					this.setValue(v); // Re-render selected value
				}
			}
		}		
	}
);

Ext.reg('sensitivecombo', Ext.ux.SensitiveComboBox);


Ext.ux.StatefulGridPanel = Ext.extend
(
	Ext.grid.GridPanel,
	{
		stateful: true,

    initComponent : function()
    {
        Ext.ux.StatefulGridPanel.superclass.initComponent.call(this);
        
        this.stateEvents.push('resize');
    },
    
		getState : function()
		{
			var o = Ext.ux.StatefulGridPanel.superclass.getState.call(this);
			
			o.size = this.getSize();
			
			return o;
		},
		
    applyState : function(state)
    {
			Ext.ux.StatefulGridPanel.superclass.applyState.call(this, state);
			
			if (state.size) this.setSize(state.size);
    }
	}
);



//  http://www.extjs.com/forum/showthread.php?t=10857&highlight=synchronous

/*
Ext.lib.Ajax.request = function(method, uri, cb, data, options) {
    if(options){
        var hs = options.headers;
        if(hs){
            for(var h in hs){
                if(hs.hasOwnProperty(h)){
                    this.initHeader(h, hs[h], false);
                }
            }
        }
        if(options.xmlData){
            this.initHeader('Content-Type', 'text/xml', false);
            method = 'POST';
            data = options.xmlData;
        }else if(options.jsonData){
            this.initHeader('Content-Type', 'text/javascript', false);
            method = 'POST';
            data = typeof options.jsonData == 'object' ? Ext.encode(options.jsonData) : options.jsonData;
        }
        if (options.async == false) {
            return this.syncRequest(method, uri, cb, data);
        }
    }
    return this.asyncRequest(method, uri, cb, data);
};


Ext.lib.Ajax.syncRequest = function(method, uri, callback, postData)
{
    var o = this.getConnectionObject();

    if (!o) {
        return null;
    }
    else {
        o.conn.open(method, uri, false);

        if (this.useDefaultXhrHeader) {
            if (!this.defaultHeaders['X-Requested-With']) {
                this.initHeader('X-Requested-With', this.defaultXhrHeader, true);
            }
        }

        if(postData && this.useDefaultHeader){
            this.initHeader('Content-Type', this.defaultPostHeader);
        }

        if (this.hasDefaultHeaders || this.hasHeaders) {
            this.setHeader(o);
        }

        o.conn.send(postData || null);
        this.handleTransactionResponse(o, callback);
        return o;
    }
};

*/

/**
* Sync store
*/

Ext.data.JsonStore2 = function(c){
    Ext.data.JsonStore2.superclass.constructor.call(this, Ext.apply(c, {
        proxy: c.proxy || (!c.data ? new Ext.data.HttpProxy({url: c.url, async: !(c.sync ? true : false)}) : undefined)
    }));
};
Ext.extend(Ext.data.JsonStore2, Ext.data.JsonStore);


/**
* LookupStore
*/

// TODO: implement one way data maintainig (now it is dual: inserting record in any store also triggers insert in the other)
Ext.ux.LookupStore = function (config)
{
	// Check config
	
	if (! config.masterStore) throw 'masterStore not defined';
	if (config.fields && ! Ext.isArray(config.fields)) throw 'config\'s fields is not an array';

	
	// Get master store and fields
	
	this.masterStore = config.masterStore;

	var masterFields = [];
	this.masterStore.fields.eachKey(function (key, item) {masterFields.push(key);});
	

	// Set up fields
	var fields = config.fields ? config.fields : masterFields;
	

	// Save some options
	
	var lookupFilterFn = null, lookupFilter = null;
	if (config.lookupFilterFn)
	{
		lookupFilterFn = config.lookupFilterFn;
		delete config.lookupFilterFn;
	}
	if (config.lookupFilter)
	{
		lookupFilter = config.lookupFilter;
		delete config.lookupFilter;
	}
	
	
	// Call constructor
	Ext.ux.LookupStore.superclass.constructor.call
	(
		this,
		Ext.apply
		(
			config,
			{
				fields: fields,
				reader: new Ext.ux.MixedCollectionReader({}, fields)
			}
		)
	);
	

	// Add listeners

	this.masterStore.on
	(
		{
			// 'beforeload': this.onMasterBeforeLoad,
			'load': this.onMasterLoad,

			'datachanged': this.onMasterDataChanged,
			'add': this.onMasterAdd,
			'update': this.onMasterUpdate,
			'remove': this.onMasterRemove,
			'clear': this.onMasterClear,
			
			scope: this
		}
	);

	this.on
	(
		{
			// 'beforeload': this.onBeforeLoad,
			'load': this.onLoad,

			'datachanged': this.onDataChanged,
			'add': this.onAdd,
			'update': this.onUpdate,
			'remove': this.onRemove,
			
			'beforeclear': this.onBeforeClear, // custom event
			
			scope: this
		}
	);
	
	this.stopMasterEvents = false;
	this.stopLocalEvents = false;
	
	
	if (lookupFilterFn)
	{
		this.lookupFilterFn = lookupFilterFn;
	}
	else if (lookupFilter)
	{
		this.lookupFilter(lookupFilter.field, lookupFilter.value);
	}
	else
	{
		this.lookupFilterFn = function (record, id) {return false};
	}
};

Ext.extend
(
	Ext.ux.LookupStore,
	Ext.data.Store,
	{
		// override
    removeAll: function()
    {
			this.fireEvent("beforeclear", this);
			
			Ext.ux.LookupStore.superclass.removeAll.call(this);
    },
    

		// private
    createLookupFilterFn: function(field, value)
    {
			return function(record, id)
			{
				return record.get(field) == value;
			};
    },
    
		lookupFilter: function (field, value)
		{
			var fn = this.createLookupFilterFn(field, value);
			this.lookupFilterBy(fn);
		},
		
		lookupFilterBy: function (fn)
		{
			this.setLookupFilterFn(fn);
			
			this.lookupReload();
		},
		
		setLookupFilterFn: function (fn)
		{
			this.lookupFilterFn = fn;
		},
		
		lookupReload: function () // reload from master store
		{
			// alert('lookupReload (from master store)');
			
			var thisCollection = this.masterStore.queryBy(this.lookupFilterFn);
			
			this.loadData(thisCollection);
		},
		
		load: function (options)
		{
			return this.masterStore.load(options);
		},
		
		
		onMasterBeforeLoad: function ()
		{
			// alert('onMasterBeforeLoad');
		},
		onMasterLoad: function ()
		{
			// alert('onMasterLoad');
			
			this.lookupReload();
		},
		onMasterDataChanged: function ()
		{
			// alert('onMasterDataChanged');
		},
		onMasterAdd: function (masterStore, records, index)
		{
			if (! this.stopMasterEvents)
			{
				// alert('onMasterAdd');
				
				var properRecords = [];
				
				for (var i = 0; i < records.length; i++)
				{
					var r = records[i];
					var fit = this.lookupFilterFn(r, r.id);
					if (fit)
					{
						properRecords.push(r.copy());
					}
				}
				
				if (properRecords.length > 0)
				{
					this.stopLocalEvents = true; // sync this store manually
					this.add(properRecords);
					this.stopLocalEvents = false;
				}
			}
		},
		onMasterUpdate: function (masterStore, record, operation)
		{
			if (! this.stopMasterEvents)
			{
				// alert('onMasterUpdate');
				
				var thisRecord = this.getById(record.id);
				
				if (thisRecord)
				{
					this.stopLocalEvents = true; // sync this store manually
					switch (operation)
					{
						case Ext.data.Record.EDIT: // triggered upon endEdit
							thisRecord.update(record.data);
						break;
						
						case Ext.data.Record.REJECT:
							thisRecord.reject();
						break;
						
						case Ext.data.Record.COMMIT:
							thisRecord.commit();
						break;
					}
					this.stopLocalEvents = false;
				}
			}
		},
		onMasterRemove: function (masterStore, record, index)
		{
			if (! this.stopMasterEvents)
			{
				// alert('onMasterRemove');
	
				var thisRecord = this.getById(record.id);
				
				if (thisRecord)
				{
					this.stopLocalEvents = true; // sync this store manually
					this.remove(thisRecord);
					this.stopLocalEvents = false;
				}
			}
		},
		onMasterClear: function ()
		{
			if (! this.stopMasterEvents)
			{
				// alert('onMasterClear');
	
				this.stopLocalEvents = true; // sync this store manually
				this.removeAll();
				this.stopLocalEvents = false;
			}
		},

		
		onBeforeLoad: function ()
		{
			// alert('onBeforeLoad');
		},
		onLoad: function ()
		{
			// alert('onLoad');
		},
		onDataChanged: function ()
		{
			// alert('onDataChanged');
		},
		/*
		// override
    add : function(records)
    {
    	this.suspendEvents(); // skip "add" event

			Ext.ux.LookupStore.superclass.add.call(this, records);

    	this.resumeEvents();
    	
			this.masterStore.add(records);
    	
			records = [].concat(records);
			if(records.length < 1){
					return;
			}
			for(var i = 0, len = records.length; i < len; i++){
					records[i].join(this);
			}
			var index = this.data.length;
			this.data.addAll(records);
			if(this.snapshot){
					this.snapshot.addAll(records);
			}
			this.fireEvent("add", this, records, index);
    },
    */
		onAdd: function (thisStore, records, index)
		{
			if (! this.stopLocalEvents)
			{
				// alert('onAdd');
				
				this.stopMasterEvents = true; // sync masterStore manually
				this.masterStore.add(records);
				this.stopMasterEvents = false;
			}
		},
		onUpdate: function (thisStore, record, operation)
		{
			if (! this.stopLocalEvents)
			{
				// alert('onUpdate');
				
				var masterRecord = this.masterStore.getById(record.id);
				
				this.stopMasterEvents = true; // sync masterStore manually
				switch (operation)
				{
					case Ext.data.Record.EDIT: // triggered upon endEdit
						masterRecord.update(record.data);
					break;
					
					case Ext.data.Record.REJECT:
						masterRecord.reject();
					break;
					
					case Ext.data.Record.COMMIT:
						masterRecord.commit();
					break;
				}
				this.stopMasterEvents = false;
			}
		},
		onRemove: function (thisStore, record, index)
		{
			if (! this.stopLocalEvents)
			{
				// alert('onRemove');
				
				var masterRecord = this.masterStore.getById(record.id);
				
				this.stopMasterEvents = true; // sync masterStore manually
				this.masterStore.remove(masterRecord);
				this.stopMasterEvents = false;
			}
		},
		onBeforeClear: function ()
		{
			if (! this.stopLocalEvents)
			{
				// alert('onBeforeClear');
					
				this.stopMasterEvents = true; // sync masterStore manually
				// Delete every record from masterStore which exists in this store
				this.each
				(
					function (record)
					{
						var masterRecord = this.masterStore.getById(record.id);
						
						this.masterStore.remove(masterRecord);
						
						return true;
					},
					this
				);
				this.stopMasterEvents = false;
			}
		}
	}
);


Ext.override
(
	Ext.form.ComboBox,
	{
		// Prevent onKeyUp binding if disableKeyTrigger is true
		// Method is almost the same as original, the change is at the end of the code.
    initEvents : function()
    {
			Ext.form.ComboBox.superclass.initEvents.call(this);

			this.keyNav = new Ext.KeyNav(this.el, {
					"up" : function(e){
							this.inKeyMode = true;
							this.selectPrev();
					},

					"down" : function(e){
							if(!this.isExpanded()){
									this.onTriggerClick();
							}else{
									this.inKeyMode = true;
									this.selectNext();
							}
					},

					"enter" : function(e){
							this.onViewClick();
							this.delayedCheck = true;
							this.unsetDelayCheck.defer(10, this);
					},

					"esc" : function(e){
							this.collapse();
					},

					"tab" : function(e){
							this.onViewClick(false);
							return true;
					},

					scope : this,

					doRelay : function(foo, bar, hname){
							if(hname == 'down' || this.scope.isExpanded()){
								 return Ext.KeyNav.prototype.doRelay.apply(this, arguments);
							}
							return true;
					},

					forceKeyDown : true
			});
			this.queryDelay = Math.max(this.queryDelay || 10,
							this.mode == 'local' ? 10 : 250);
			this.dqTask = new Ext.util.DelayedTask(this.initQuery, this);
			if(this.typeAhead){
					this.taTask = new Ext.util.DelayedTask(this.onTypeAhead, this);
			}
			if(this.editable !== false && this.disableKeyTrigger !== true){ // <-- here is the change
					this.el.on("keyup", this.onKeyUp, this);
			}
			if(this.forceSelection){
					this.on('blur', this.doForce, this);
			}
    }
	}
);


Ext.override
(
	Ext.grid.GridView,
	{
    focusRow: function(row)
    {
    	if (! this.disableFocusRow)
    	{
    		this.focusCell(row, 0, false);
    	}
    }
  }
);


Ext.override
(
	Ext.grid.CheckboxSelectionModel,
	{
    initEvents : function(){
    	
    		this.grid.addEvents("userrowselect","userrowdeselect","userrowselectall","userrowdeselectall");
    	
    	
    		// from Ext.grid.RowSelectionModel

				if (! this.checkOnly)
				{
					
					if(!this.grid.enableDragDrop && !this.grid.enableDrag){
							this.grid.on("rowmousedown", this.handleMouseDown, this);
					}else{ // allow click to work like normal
							this.grid.on("rowclick", function(grid, rowIndex, e) {
									if(e.button === 0 && !e.shiftKey && !e.ctrlKey) {
											this.selectRow(rowIndex, false);
											grid.view.focusRow(rowIndex);
									}
							}, this);
					}
	
					this.rowNav = new Ext.KeyNav(this.grid.getGridEl(), {
							"up" : function(e){
									if(!e.shiftKey){
											this.selectPrevious(e.shiftKey);
									}else if(this.last !== false && this.lastActive !== false){
											var last = this.last;
											this.selectRange(this.last,  this.lastActive-1);
											this.grid.getView().focusRow(this.lastActive);
											if(last !== false){
													this.last = last;
											}
									}else{
											this.selectFirstRow();
									}
							},
							"down" : function(e){
									if(!e.shiftKey){
											this.selectNext(e.shiftKey);
									}else if(this.last !== false && this.lastActive !== false){
											var last = this.last;
											this.selectRange(this.last,  this.lastActive+1);
											this.grid.getView().focusRow(this.lastActive);
											if(last !== false){
													this.last = last;
											}
									}else{
											this.selectFirstRow();
									}
							},
							scope: this
					});
					
				}

        var view = this.grid.view;
        view.on("refresh", this.onRefresh, this);
        view.on("rowupdated", this.onRowUpdated, this);
        view.on("rowremoved", this.onRemove, this);
        

        // from Ext.grid.CheckboxSelectionModel
        
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
            Ext.fly(view.innerHd).on('mousedown', this.onHdMouseDown, this);

        }, this);
        
    },
    /*
    onRefresh : function(){
    		if (this.skipOnRefresh) return;
    	
        var ds = this.grid.store, index;
        var s = this.getSelections();
        this.clearSelections(true);
        for(var i = 0, len = s.length; i < len; i++){
            var r = s[i];
            if((index = ds.indexOfId(r.id)) != -1){
                this.selectRow(index, true);
            }
        }
        if(s.length != this.selections.getCount()){
            this.fireEvent("selectionchange", this);
        }
    },
    */
    onMouseDown : function(e, t){
        if(e.button === 0 && t.className == 'x-grid3-row-checker'){ // Only fire if left-click
            e.stopEvent();
            var row = e.getTarget('.x-grid3-row');
            if(row){
                var index = row.rowIndex;
                if(this.isSelected(index)){
                    this.deselectRow(index);

                    var r = this.grid.getStore().getAt(index);
                    this.grid.fireEvent("userrowdeselect", this, index, r);
                }else{
                    this.selectRow(index, true);
                    
                    var r = this.grid.getStore().getAt(index);
                    this.grid.fireEvent("userrowselect", this, index, r);
                }
            }
        }
    },
    
    onHdMouseDown : function(e, t){
        if(t.className == 'x-grid3-hd-checker'){
            e.stopEvent();
            var hd = Ext.fly(t.parentNode);
            var isChecked = hd.hasClass('x-grid3-hd-checker-on');
            if(isChecked){
                hd.removeClass('x-grid3-hd-checker-on');
                this.clearSelections();
                
                this.grid.fireEvent("userrowdeselectall", this);
            }else{
                hd.addClass('x-grid3-hd-checker-on');
                this.selectAll();
                
                this.grid.fireEvent("userrowselectall", this);
            }
        }
    }
    
		/*
    onMouseDown : function(e, t){
			if(e.button === 0 && t.className == 'x-grid3-row-checker'){ // Only fire if left-click
					e.stopEvent();
					var row = e.getTarget('.x-grid3-row');
					if(row){
							var index = row.rowIndex;
							if(this.isSelected(index)){
									if (this.checkOnly) this.unlock();
									this.deselectRow(index);
									if (this.checkOnly) this.lock();
							}else{
									if (this.checkOnly) this.unlock();
									this.selectRow(index, true);
									if (this.checkOnly) this.lock();
							}
					}
			}
    },
    
    onHdMouseDown : function(e, t){
			if(t.className == 'x-grid3-hd-checker'){
					e.stopEvent();
					var hd = Ext.fly(t.parentNode);
					var isChecked = hd.hasClass('x-grid3-hd-checker-on');
					if(isChecked){
							hd.removeClass('x-grid3-hd-checker-on');

							if (this.checkOnly) this.unlock();
							this.clearSelections();
							if (this.checkOnly) this.lock();
					}else{
							hd.addClass('x-grid3-hd-checker-on');
							this.selectAll();
					}
			}
    }
    */
    
	}
);



Ext.override
(
	Ext.grid.GroupingView,
	{
		getRecordGroupEl: function (recordIndex)
		{
			var row = this.getRow(recordIndex);
			var groupEl = this.findGroup(row).dom; // HTMLElement 
			
			return groupEl;
		},
		
		updateRecordGroupTitle: function (recordIndex, html)
		{
			var groupEl = this.getRecordGroupEl(recordIndex);

			this.updateGroupTitle(groupEl, html);
		},
		
		updateGroupTitle: function (groupEl, html)
		{
			var titleEl = Ext.get(groupEl).child('div.x-grid-group-hd div');
			titleEl.update(html);
		},
		
		getRecordGroups: function ()
		{
			var result = [];
			
			var s = this.grid.store;
			
			var index = 0;
			
			var g, gs = this.getGroups();
			for(var i = 0, len = gs.length; i < len; i++){
					var groupEl = gs[i];
					var rows = groupEl.childNodes[1].childNodes;
					
					var records = [];
					for(var j = 0, jlen = rows.length; j < jlen; j++){
							
							// var index = r.length;
							records.push(s.getAt(index));
							index++;							
					}
					
					result.push({groupEl: groupEl, records: records});
			}

			return result;
		}
	}
);



// events rowOver/rowOut added
//Ext.ux.GridView = Ext.extend
//(

Ext.override(
	Ext.grid.GridView,
	{
		/*
		afterRenderUI: function() {
        Ext.ux.GridView.superclass.afterRenderUI.call(this);
        
        if (grid.trackMouseOver) {
            this.mainBody.on({
                scope    : this,
                mouseenter: this.onMouseEnter,
                mouseleave: this.onMouseLeave
            });
        }
		},
		*/
		onRowOver: function(e, target) {
        var row = this.findRow(target);
        var rowIndex = row ? row.rowIndex : false;
				
				if (rowIndex !== false) {
						this.addRowClass(rowIndex, this.rowOverCls);
	

						this.fireEvent('rowover', this, row, rowIndex, e);
				}			
		},
		onRowOut: function(e, target) {
				var row = this.findRowIndex(target);
				
				if (row !== false && !e.within(this.getRow(row), true)) {
						this.removeRowClass(row, this.rowOverCls);

						this.fireEvent('rowout', this, row, null, e);
				}
		}	
	}
);
