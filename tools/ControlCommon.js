String.prototype.repeat = function (length)
{
	return new Array(parseInt(length) + 1).join(this);
};

/*
var alertErrors = function(errors)
{
	var msg = '';
	
	for (var errorType in errors)
	{
		msg += errorType + ': ' + errors[errorType] + "\n";
	}

	alert(msg);
}


var convertDateTimeFromSQLToDMY = function(value)
{
	if (value)
	{
		return Date.parseDate(value, "Y-m-d h:i:s").format("d.m.Y H:i");
	}
	
	return '';
}


function roundNumber(num, dec)
{
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
*/
