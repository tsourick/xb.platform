<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityCelkoBase extends ApplicationEntity
{
	public function __construct(ApplicationDataModel $dm, $name, $singularTitle, $pluralTitle, $amFields = array(), $options = array())
	{
		parent::__construct($dm, $name, $singularTitle, $pluralTitle, $amFields, $options);
		

		$this->dmEntity = new DMCelkoEntity($dm->DMDataModel(), $name);
		

		$this->registerPrimaryFields();
	}
	

	/**
	*
	*
	* @param string $pre_condition
	*/

	public function setPreCondition($pre_condition)
	{
		$this->dmEntity->setPreCondition($pre_condition);
	}

	
	/**
	*
	*
	* @param array $node_pk
	* @param int $max_depth
	* @param string|NULL $pre_condition
	*
	* @return array
	*/

	public function getChildItemDataListAll($node_pk, $max_depth = 1, $pre_condition = NULL)
	{
		return $this->dmEntity->getChildItemDataListAll($node_pk, $max_depth, $pre_condition);
	}
	
	/**
	*
	*
	* @param array $node_pk
	* @param int $min_level
	* @param string|NULL $pre_condition
	*
	* @return array
	*/

	public function getParentItemDataListAll($node_pk, $min_level = 1, $pre_condition = NULL)
	{
		return $this->dmEntity->getParentItemDataListAll($node_pk, $min_level, $pre_condition);
	}
	
	/**
	*
	*
	* @param array $node_pk
	* @param int|NULL $level
	* @param string|NULL $pre_condition
	*
	* @return array
	*/

	public function getParentItemDataAll($node_pk, $level = NULL, $pre_condition = NULL)
	{
		return $this->dmEntity->getParentItemDataAll($node_pk, $level, $pre_condition);
	}
	
	/**
	*
	*
	* @param int $node_id
	* @param int $sibling_node_id
	* @param bool $as_child
	*/

	public function moveNodeAfter($node_id, $sibling_node_id, $as_child)
	{
		$this->dmEntity->moveNodeAfter($node_id, $sibling_node_id, $as_child);
	}
}

?>
