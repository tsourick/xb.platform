<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class CMSHelperException extends Exception
{
}


class CMSHelper
{
	/**
	*
	*
	* @param string $language
	*/

	static public function setlanguage($language)
	{
		switch ($language)
		{
			case 'en':
				setlocaleall('en', 'US', 'UTF-8');
			break;
			case 'ru':
				setlocaleall('ru', 'RU', 'UTF-8');
			break;
		}
	}
	
	/**
	*
	*
	* @param string $params
	*
	* @return array
	*/

	static public function extractParams($params)
	{
		return explode_params($params); // from lib.tool.general.php
		/*
		$array = array();
		
		$pairs = explode(';', $params);
		foreach ($pairs as $pair)
		{
			list($n, $v) = explode('=', $pair);
			$array[$n] = $v;
		}
		
		return $array;
		*/
	}
	
	static public function parseTemplate($template, $values = array())
	{
		$result = '';
		
/*
		$c = Application::getObject()->getConfig();
		$application_url = $c->get('application/url');
		$application_name = $c->get('application/name');

		
		$vars = array();
		$vars['{$application.url}'] = $application_url;
		$vars['{$application.name}'] = $application_name;
		$vars['{$datetime}'] = date('d.m.Y H:i:s');
		$vars['{$date}'] = date('d.m.Y');
		$vars['{$time}'] = date('H:i:s');
		foreach ($values as $n => $v)
		{
			if (is_array($v))
			{
				foreach ($v as $n2 => $v2)
				{
					$vars['{$' . $n . '.' . $n2 . '}'] = $v2;
				}
			}
			else
			{
				$vars['{$' . $n . '}'] = $v;
			}
		}

		$result = str_replace(array_keys($vars), array_values($vars), $template);
*/

		$c = Application::getObject()->getConfig();
		$application_url = $c->get('application/url');
		$application_name = $c->get('application/name');
		
		$values = array_merge
		(
			array
			(
				'application' => array
				(
					'url' => $application_url,
					'name' => $application_name
				),
				'datetime' => date('d.m.Y H:i:s'),
				'date' => date('d.m.Y'),
				'time' => date('H:i:s')
			),
			$values
		);
		
		$result = parseTemplate($template, $values);
		
		return $result;
	}
}

?>
