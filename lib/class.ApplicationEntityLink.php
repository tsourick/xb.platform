<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityLinkException extends ApplicationDataModelException
{
}


require_once('class.ApplicationEntityLinkBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.ApplicationEntityLink.php_5_3.php');
}
else
{
	include('class.ApplicationEntityLink.php_5_2.php');
}

?>
