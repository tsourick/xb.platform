<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationBase.php');


abstract class ApplicationControlBase extends ApplicationBase
{
	/**
	*
	*
	* @param int $target_module_id
	* @param int $module_event_id
	*
	* @return bool
	*/
	public function subscribeModuleForEvent($target_module_id, $module_event_id)
	{
		return $this->invokeModuleMethod('Modules', 'subscribeModuleForEvent', array($target_module_id, $module_event_id));
		/*
		$dbc = DBConnectionManager::getObject()->getDefaultConnection();

		$q = "
			INSERT IGNORE INTO site_module_event_subscription (module_event_id, module_id)
			VALUES ($module_event_id, $target_module_id)
		";
		$dbc->exec($q);
		
		return true;
		*/
	}

	/**
	*
	*
	* @param int $target_module_id
	* @param int $module_event_id
	*
	* @return bool
	*/
	public function unsubscribeModuleFromEvent($target_module_id, $module_event_id)
	{
		return $this->invokeModuleMethod('Modules', 'unsubscribeModuleFromEvent', array($target_module_id, $module_event_id));
		/*
		$dbc = DBConnectionManager::getObject()->getDefaultConnection();

		$q = "
			DELETE FROM site_module_event_subscription WHERE module_event_id = $module_event_id AND module_id = $target_module_id
		";
		$dbc->exec($q);
		
		return true;
		*/
	}
}
?>
