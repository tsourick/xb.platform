<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityPlain extends ApplicationEntityPlainBase
{
	static public function createFromConfig(ApplicationDataModel $dm, $config, $__class__ = __CLASS__)
	{
		return parent :: createFromConfig($dm, $config, $__class__);
	}
}

?>
