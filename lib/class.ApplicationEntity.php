<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationEntityField.php');
require_once('class.ApplicationEntityLinkEndPoint.php');

class ApplicationEntityException extends ApplicationDataModelException
{
}


require_once('class.ApplicationEntityBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.ApplicationEntity.php_5_3.php');
}
else
{
	include('class.ApplicationEntity.php_5_2.php');
}
?>
