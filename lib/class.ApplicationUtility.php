<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2014
*
* Application utility base class
*
* @package platform
*/

/**
* Class
*/

class ApplicationUtility implements MVCInterface
{
	protected $application;
	
	
	public function __construct(Application $application)
	{
		$this->application = $application;
	}
	
	
	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array())
	{
		return $this->application->invokeModuleAction($module_name, $action_name, $params, $options);
	}
	
	public function invokeModuleMethod($module_name, $method_name, $params = array())
	{
		return $this->application->invokeModuleMethod($module_name, $method_name, $params);
	}
	
	
	public function moduleEntity($module_name, $entity_name)
	{
		return $this->application->moduleEntity($module_name, $entity_name);
	}

	public function moduleLink($module_name, $link_name)
	{
		return $this->application->moduleLink($module_name, $link_name);
	}
}

?>
