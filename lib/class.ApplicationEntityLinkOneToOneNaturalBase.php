<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityLinkOneToOneNaturalBase extends ApplicationEntityLink
{
	public function __construct(ApplicationDataModel $dm, $name, ApplicationEntity $from_entity, $from_required, ApplicationEntity $to_entity, $to_required, $custom_field_map = NULL)
	{
		parent::__construct($dm, $name, $from_entity, $from_required, $to_entity, $to_required, $custom_field_map);
		
		
		$this->type = '1:n natural';
		
		
		$this->dmLink = new DMOneToOneNaturalLink($dm->DMDataModel(), $name, $from_entity->DMEntity(), $from_required, $to_entity->DMEntity(), $to_required, $custom_field_map);
	}
}

?>
