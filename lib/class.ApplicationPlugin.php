<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationPluginException extends Exception
{
}

// abstract class ApplicationModule implements ModuleInvocationInterface
abstract class ApplicationPlugin implements MVCModuleInterface
{
	protected $application = NULL;
	
	protected $pluginName = 'undefined';


	/**
	*
	*
	* @return array
	*/

	public function getPermissionList()
	{
		return array();
	}

	/**
	*
	*
	*/

	public function __construct()
	{
		$this->application = Application::getObject(); // for config retrieving purposes, for example
	}
	
	/**
	*
	*
	*/

	public function init()
	{
	}
	
	/**
	* Returns plugin name
	*
	* @return string plugin name
	*/

	public function getName()
	{
		return $this->pluginName;
	}

	/**
	* Calls module action handler through application's MVC controller
	*
	* @param string $module_name module name
	* @param string $action_name action name
	* @param array $params module action parameters
	*
	* @return mixed action result
	*/

	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array())
	{
		return $this->application->invokeModuleAction($module_name, $action_name, $params, $options);
	}

	/**
	* Calls module method through application's MVC controller
	*
	* @param string $module_name module name
	* @param string $method_name method name
	* @param array $params module method parameters
	*
	* @return mixed method result
	*/

	public function invokeModuleMethod($module_name, $method_name, $params = array())
	{
		return $this->application->invokeModuleMethod($module_name, $method_name, $params);
	}
	
	/**
	* Default dispatcher for plugin actions
	*
	* Calls callActionHandler().
	*
	* @param string $name action name
	* @param array $params action parameters
	*
	* @return mixed action result
	*/

	public function defaultDispatcher($name, $params = array(), $options = array())
	{
		return $this->callActionHandler($name, $params, $options);
	}
	
	/**
	* Directly calls action handler method
	*
	* @param string $name action name
	* @param array $params action parameters
	*
	* @return mixed action result
	*/

	public function callActionHandler($name, $params = array(), $options = array())
	{
		$handler_name = 'on' . ucfirst($name);

		$callable_name = '';
		if (! is_callable(array($this, $handler_name), false, $callable_name)) throw new ApplicationPluginException("Could not find callable handler for action '$name'");
		
		return call_user_func(array($this, $handler_name), $params);
	}
	
	/**
	* Plugin action dispatcher
	*
	* Calls defaultDispatcher(). May be overridden in derived classes for custom processing.
	*
	* @param string $name action name
	* @param array $params action parameters
	*
	* @return mixed action result
	*/

	public function dispatchAction($name, $params = array(), $options = array())
	{
		$result = NULL;
		
		switch ($name)
		{
			default:
				$result = $this->defaultDispatcher($name, $params, $options);
		}
		
		return $result;
	}
}

?>
