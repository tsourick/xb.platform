<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationDataModel.php');


class ApplicationDataModelBuilderException extends ApplicationException
{
}

class ApplicationDataModelBuilder
{
	private $application = NULL;
	
	private $amDataModel = NULL;
	

	public function __construct(Application $application, ApplicationDataModel $amDataModel)
	{
		$this->application = $application;
		
		
		$this->amDataModel = $amDataModel;
	}

	public function AMDataModel()
	{
		return $this->amDataModel;
	}
	
	
	private function makeModuleEntityName($module_name, $entity_name)
	{
		return ApplicationDataModel::makeModuleEntityName($module_name, $entity_name);
	}
	

	// Add to model and add table to DB
	public function createEntity($type, $name, $singular_title, $plural_title, $options)
	{
		$amEntity = $this->amDataModel->createEntity(array_merge(compact('type', 'name', 'singular_title', 'plural_title'), $options));
		
		$qset = $amEntity->DMEntity()->ORMTable()->SQLDDLGetCreateQuerySet();
		
		
		$dbc = $this->amDataModel->getDBConnection();
		
		
		$dbc->exec($qset);
		
		
		return $amEntity;
	}
	
	// Update within model and DB (only name could be updated)
	public function updateEntity($entity_name, $new_name, $singular_title, $plural_title, $options)
	{
		$amEntity = $this->amDataModel->entity($entity_name);
		

		$amEntity->setSingularTitle($singular_title);
		$amEntity->setPluralTitle($plural_title);

		$amEntity->setOptions($options);


		if (strcmp($entity_name, $new_name) != 0)
		{
			$old_table_name = $amEntity->DMEntity()->ORMTable()->getName();

			
			$amEntity->setName($new_name);
				
		
			$dbc = $this->amDataModel->getDBConnection();
			
	
			$qset = $amEntity->DMEntity()->ORMTable()->SQLDDLGetRenameQuerySet($old_table_name);

			$dbc->exec($qset);
		}
	}
	
	// Remove from model and remove table to DB
	public function deleteEntity($name)
	{
		$dbc = $this->amDataModel->getDBConnection();
		
		
		$qset = $this->amDataModel->entity($name)->DMEntity()->ORMTable()->SQLDDLGetDropQuerySet();

		$dbc->exec($qset);
		
		
		$this->amDataModel->deleteEntity($name);
	}

	
	// Module facilities
	
	public function createModuleEntity($module_name, $type, $name, $singular_title, $plural_title, $options)
	{
		$this->application->dataModel($module_name);

		
		$name = $this->makeModuleEntityName($module_name, $name);
		
		return $this->createEntity($type, $name, $singular_title, $plural_title, $options);
	}

	public function updateModuleEntity($module_name, $entity_name, $new_name, $singular_title, $plural_title, $options)
	{
		// $this->application->loadModule($module_name);
		$this->application->dataModel($module_name);


		$entity_name = $this->makeModuleEntityName($module_name, $entity_name);
		$new_name = $this->makeModuleEntityName($module_name, $new_name);
		
		$this->updateEntity($entity_name, $new_name, $singular_title, $plural_title, $options);
	}

	public function deleteModuleEntity($module_name, $name)
	{
		// $this->application->loadModule($module_name);
		$this->application->dataModel($module_name);


		$name = $this->makeModuleEntityName($module_name, $name);
		
		return $this->deleteEntity($name);
	}
	

	// Add to model and add field to DB table
	public function createEntityField($entity_name, $type, $name, $title, $options)
	{
		$amField = $this->amDataModel->entity($entity_name)->createField(array_merge(compact('type', 'name', 'title'), $options));

		
		$qset = $amField->DMField()->ORMField()->SQLDDLGetAddQuerySet();
		
		
		$dbc = $this->amDataModel->getDBConnection();
		

		$dbc->exec($qset);
	}

	// Update in model and update field in DB table
	public function updateEntityField($entity_name, $type, $old_name, $new_name, $title, $options = array())
	{
		$amField = $this->amDataModel->entity($entity_name)->field($old_name);
		
		
		$amField->setType($type);
		$amField->setName($new_name);
		$amField->setTitle($title);
		
		$amField->setOptions($options);
		// $amField->setEntityTitle($entity_title);
		// $amField->setPushToStore($push_to_store);
				
		
		if (strcmp($old_name, $new_name) != 0)
		{
			$qset = $amField->DMField()->ORMField()->SQLDDLGetChangeQuerySet($old_name);
		}
		else
		{
			$qset = $amField->DMField()->ORMField()->SQLDDLGetModifyQuerySet();
		}
		
		
		$dbc = $this->amDataModel->getDBConnection();
		

		$dbc->exec($qset);
	}

	// Remove from model and remove field to DB table
	public function deleteEntityField($entity_name, $name)
	{
		$dbc = $this->amDataModel->getDBConnection();
		
		
		$qset = $this->amDataModel->entity($entity_name)->field($name)->DMField()->ORMField()->SQLDDLGetDropQuerySet();

		$dbc->exec($qset);
		
		
		$this->amDataModel->entity($entity_name)->deleteField($name);
	}
	
	
	// Module facilities
	
	public function createModuleEntityField($module_name, $entity_name, $type, $name, $title, $options = array())
	{
		// $this->application->loadModule($module_name);
		$this->application->dataModel($module_name);


		$entity_name = $this->makeModuleEntityName($module_name, $entity_name);
	
		$this->createEntityField($entity_name, $type, $name, $title, $options);
	}
	
	public function updateModuleEntityField($module_name, $entity_name, $type, $old_name, $new_name, $title, $options)
	{
		// $this->application->loadModule($module_name);
		$this->application->dataModel($module_name);


		$entity_name = $this->makeModuleEntityName($module_name, $entity_name);

		$this->updateEntityField($entity_name, $type, $old_name, $new_name, $title, $options);
	}
	
	public function deleteModuleEntityField($module_name, $entity_name, $name)
	{
		// $this->application->loadModule($module_name);
		$this->application->dataModel($module_name);


		$entity_name = $this->makeModuleEntityName($module_name, $entity_name);

		$this->deleteEntityField($entity_name, $name);
	}
	
	
	public function createLink($type, $name, $from_module_entity_name, $from_module_entity_required, $to_module_entity_name, $to_module_entity_required)
	{
		$dbc = $this->amDataModel->getDBConnection();


		$amLink = $this->amDataModel->createLink
		(
			array
			(
				'type' => $type,
				'name' => $name,
				'from' => $from_module_entity_name,
				'from_required' => $from_module_entity_required,
				'to' => $to_module_entity_name,
				'to_required' => $to_module_entity_required
			)
		);

		
		switch ($amLink->getType())
		{
			case '1:1 natural':
				if (! $from_module_entity_required && ! $to_module_entity_required)
				{
					// through link table
					
					$qset = $amLink->DMLink()->ORMTable()->SQLDDLGetCreateQuerySet();
					
					$dbc->exec($qset);
				}
				else
				{
					// through original tables' primary keys
					// nothing to do - all fields are always exist
				}
			break;
			
			case '1:n':
				$dmFields = $amLink->DMLink()->toFields();
				
				foreach ($dmFields as $dmField)
				{
					$qset = $dmField->ORMField()->SQLDDLGetAddQuerySet();
					
					
					$dbc->exec($qset);
				}
			break;
			
			case 'n:n':
				$qset = $amLink->DMLink()->ORMTable()->SQLDDLGetCreateQuerySet();
				
				$dbc->exec($qset);
			break;
		}


		return $amLink;
	}

	public function deleteLink($name)
	{
		$dbc = $this->amDataModel->getDBConnection();


		$amLink = $this->amDataModel->link($name);

		
		switch ($amLink->getType())
		{
			case '1:1 natural':
				if (! $amLink->fromRequired && ! $amLink->toRequired)
				{
					// through link table
					
					$qset = $amLink->DMLink()->ORMTable()->SQLDDLGetDropQuerySet();
					
					$dbc->exec($qset);
				}
				else
				{
					// through original tables' primary keys
					// nothing to do - all fields are always exist
				}
			break;
			
			case '1:n':
				$dmFields = $amLink->DMLink()->toFields();
				
				foreach ($dmFields as $dmField)
				{
					$qset = $dmField->ORMField()->SQLDDLGetDropQuerySet();
					
					
					$dbc->exec($qset);
				}
			break;
			
			case 'n:n':
				$qset = $amLink->DMLink()->ORMTable()->SQLDDLGetDropQuerySet();
				
				$dbc->exec($qset);
			break;
		}


		$amLink->destruct();
	}
	

	// Module facilities
	
	public function createModuleLink($module_name, $type, $name, $from_module_name, $from_module_entity_name, $from_module_entity_required, $to_module_name, $to_module_entity_name, $to_module_entity_required)
	{
		// $this->application->loadModule($from_module_name);
		// $this->application->loadModule($to_module_name);
		$this->application->dataModel($from_module_name);
		$this->application->dataModel($to_module_name);


		$name = $this->makeModuleEntityName($module_name, $name);
		$from_module_entity_name = $this->makeModuleEntityName($from_module_name, $from_module_entity_name);
		$to_module_entity_name = $this->makeModuleEntityName($to_module_name, $to_module_entity_name);
		
		return $this->createLink($type, $name, $from_module_entity_name, $from_module_entity_required, $to_module_entity_name, $to_module_entity_required);
	}
	
	public function deleteModuleLink($module_name, $from_module_name, $to_module_name, $name)
	{
		// $this->application->loadModule($from_module_name);
		// $this->application->loadModule($to_module_name);
		$this->application->dataModel($from_module_name);
		$this->application->dataModel($to_module_name);

		
		$name = $this->makeModuleEntityName($module_name, $name);

		$this->deleteLink($name);
	}
}
?>