<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Application module base class
*
* @package platform
*/

/**
* Class
*/

abstract class ApplicationModuleBase implements MVCModuleInterface, MVCInterface
{
	protected $application = NULL;

	protected $moduleName = 'undefined';

	protected $debug_log_disabled = true;
	protected $debug_log_blocks = array('info', 'info::init');
	// protected $debug_log_blocks = array('info');

	
	/**
	* Constructor
	*/

	public function __construct()
	{
		$this->application = Application::getObject(); // for config retrieving purposes, for example
	}
	
	/**
	* Method is called after module is loaded and registered as loaded
	*
	* May be overridden in derived classes to implement initialization which can not be placed in constructor.
	*/

	public function init()
	{
	}
	

	/**
	* Returns name of the module
	*
	* @return string name of the module
	*/

	public function getName()
	{
		return $this->moduleName;
	}

	
	/**
	* Returns application's default DB connection name
	*
	* @return string connection name
	*/

	public function getDefaultDBConnectionName()
	{
		return $this->application->getDefaultDBConnectionName();
	}
	
	/**
	* Returns application's default DB connection object
	*
	* @return DBConnection connection object
	*/

	public function getDefaultDBConnection()
	{
		return $this->application->getDefaultDBConnection();
	}


	/**
	* Returns table name global prefix, as set in configuration file
	*
	* Used for old modules without datamodel and for custom tables.
	*
	* @return string
	*/

	public function getDBTablePrefix()
	{
		return $this->application->getDBTablePrefix();
	}


	/**
	* Returns full table name prefix for a module
	*
	* @param string $name this module name is assumed if null
	*
	* @return string
	*/

	protected function getTablePrefix($name = NULL)
	{
		$global_prefix = $this->getDBTablePrefix();

		$prefix = $global_prefix . 'am_' . strtolower($name ?: $this->moduleName) . '_';
		
		return $prefix;
	}
	
	/**
	* Returns table field prefix for a field (the link field is implied)
	*
	* @param string $name this module name is assumed if null
	*
	* @return string
	*/

	protected function getFieldPrefix($name = NULL)
	{
		$prefix = 'am_' . ucfirst(strtolower($name ?: $this->moduleName)) . '_';
		
		return $prefix;
	}

	
	
	/**
	* Returns application object
	*
	* @return ApplicationBase
	*/

	public function application()
	{
		return $this->application;
	}
	
	
	/**
	* Returns module base dir path
	*
	* @return string path to module's directory
	*/

	public function getBaseDirPath()
	{
		$module_name = $this->getName();
		
		return $this->application->getModuleBaseDirPath($module_name);
	}

	/**
	* Returns module dir path
	*
	* @return string path to module's directory
	*/

	public function getDirPath()
	{
		$module_name = $this->getName();
		
		return $this->application->getModuleDirPath($module_name);
	}


	
	/**
	* Default dispatcher for module actions
	*
	* Calls callActionHandler().
	*
	* @param string $name action name
	* @param array $params action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function defaultDispatcher($name, $params = array(), $options = array())
	{
		return $this->callActionHandler($name, $params, $options);
	}
	
	/**
	* Directly calls action handler method
	*
	* @param string $name action name
	* @param array $params action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function callActionHandler($name, $params = array(), $options = array())
	{
		$handler_name = 'on' . ucfirst($name);

		$callable_name = '';
		if (! is_callable(array($this, $handler_name), false, $callable_name)) throw new ApplicationModuleException("Could not find callable handler for action '$name' in module '" . $this->getName() . "'");
		
		return call_user_func(array($this, $handler_name), $params);
	}
	
	/**
	* Module action dispatcher
	*
	* Calls defaultDispatcher(). May be overridden in derived classes for custom processing.
	*
	* @param string $name action name
	* @param array $params action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function dispatchAction($name, $params = array(), $options = array())
	{
		$result = NULL;


		$result = $this->defaultDispatcher($name, $params, $options);
		

		return $result;
	}
	

	protected function debugLog($message, $block)
	{
		if (! $this->debug_log_disabled)
		{
			if (in_array($block, $this->debug_log_blocks))
			{
				dump('modules.' . $this->getName() . ': ' . $message, $block);
			}
		}
	}
}

?>
