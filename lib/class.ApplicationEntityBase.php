<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

abstract class ApplicationEntityBase
{
	protected $dm = NULL; // Data model
	protected $dbc = NULL; // DB connection in use

	protected $name;
	protected $singularTitle;
	protected $pluralTitle;
	
	protected $dmEntity;

	protected $amFields = array();
	protected $amPrimaryFields = array();
	// protected $amLinkFields = array();

	protected $amLinkEndPoints = array();
	
	
	// Extensible options (modifying options here don't forget to modify the module entity table structure as well)
	static public $defaultOptions = array
	(
		'create_item_store' => true 
	);
	
	private $options;
	
	/**
	*
		Config structure (DMEntity config)
		
		array
		(
			'name' => '<entity name>',
			'fields' => array
			(
				array('name' => '<field name>', 'type' => '<entity field type name>')
			)
		)
	*
	*
	*
	*
	* @param ApplicationDataModel $dm
	* @param string $name
	* @param string $singularTitle
	* @param string $pluralTitle
	* @param array $amFields
	* @param array $options
	*/
	public function __construct(ApplicationDataModel $dm, $name, $singularTitle, $pluralTitle, $amFields = array(), $options = array())
	{
		$this->dm = $dm;
		
		$this->dbc = $dm->getDBConnection();

		$this->name = $name;
		$this->singularTitle = $singularTitle;
		$this->pluralTitle = $pluralTitle;
		
		$this->amFields = $amFields;
		
		
		// Set options (override default values) if any
		$this->setOptions($options);


		$dm->addEntity($this);
	}
	
	// THIS IS NOT A LANGUAGE DESTRUCTOR
	/**
	*
	*
	*/

	public function destruct()
	{
		// $this->dm->DMDataModel()->deleteEntity($this->dmEntity);
		$this->dmEntity->destruct();
		
		
		$this->dm->removeEntity($this);
	}
	
	
	/**
	*
	*
	* @return string
	*/

	public function getName()
	{
		return $this->name;
	}

	/**
	*
	*
	* @return
	*/

	public function getPreparedName()
	{
		return $this->dmEntity->getPreparedName();
	}


	// deprecated	
	/**
	*
	*
	* @return string
	*/

	public function getTitle()
	{
		return $this->getSingularTitle();
	}
	
	/**
	*
	*
	* @return string
	*/

	public function getSingularTitle()
	{
		return $this->singularTitle;
	}

	/**
	*
	*
	* @return string
	*/

	public function getPluralTitle()
	{
		return $this->pluralTitle;
	}

	/**
	*
	*
	* @param string $name
	*
	* @return mixed
	*/

	public function getOption($name)
	{
		return $this->options[$name];
	}
	
	/**
	*
	*
	* @return array
	*/

	public function getOptions()
	{
		return $this->options;
	}
	
	
	/**
	*
	*
	* @param string $name
	*/

	public function setName($name)
	{
		if (strcmp($this->name, $name) != 0)
		{
			$this->dmEntity->setName($name);
			
			
			$old_name = $this->name;

			$this->name = $name;
			
			
			$this->dm->remapEntity($this, $old_name);
			
			
			foreach ($this->amLinkEndPoints as $amLinkEndPoint)
			{
				$amLinkEndPoint->link()->remapEntity($this, $old_name);
			}
		}
	}


	/**
	* @deprecated
	*
	* @param string $title
	*/

	public function setTitle($title)
	{
		$this->setSingularTitle($title);
	}

	/**
	*
	*
	* @param string $title
	*/

	public function setSingularTitle($title)
	{
		$this->singularTitle = $title;
	}
	
	/**
	*
	*
	* @param string $title
	*/

	public function setPluralTitle($title)
	{
		$this->pluralTitle = $title;
	}

	/**
	*
	*
	* @param string $name
	* @param mixed $value
	*/

	public function setOption($name, $value)
	{
		$this->options[$name] = $value;
	}

	/**
	*
	*
	* @param array $options
	*/

	public function setOptions($options)
	{
		if (! empty($options))
		{
			foreach (self::$defaultOptions as $n => $defaultValue)
			{
				$this->options[$n] = isset($options[$n]) ? $options[$n] : $defaultValue;
			}
		}
	}
	
	
	/**
	*
	*
	* @param string $name
	*
	* @return ApplicationField
	*/

	public function field($name)
	{
		return $this->amFields[$name];
	}

	/**
	*
	*
	* @return array
	*/

	public function fields()
	{
		return $this->amFields;
	}
	
	/**
	*
	*
	* @return array
	*/

	public function linkEndPoints()
	{
		return $this->amLinkEndPoints;
	}

	
	/**
	*
	*
	* @param ApplicationEntityField $amField 
	* @param string $old_name
	*/

	public function remapField(ApplicationEntityField $amField, $old_name)
	{
		if ($amField->entity() !== $this) throw new ApplicationEntityException("Could not remap field '" . $amField->getName()  . "' from name '" . $old_name . "' because field belongs to another entity.");

		if (! array_key_exists($old_name, $this->amFields)) throw new ApplicationEntityException("Could not remap field '" . $amField->getName()  . "' from name '" . $old_name . "' because old name was not found within entity.");

		if (array_key_exists($old_name, $this->amPrimaryFields)) throw new ApplicationEntityException("Could not remap primary field '" . $amField->getName()  . "' from name '" . $old_name . "' because primary field remapping is currently forbidden.");

			
		array_replace_key($this->amFields, $old_name, $amField->getName());
	}

	
	/**
	*
	*
	* @return ApplicationDataModel
	*/

	public function dataModel()
	{
		return $this->dm;
	}	


	/**
	*
	*
	* @return
	*/
	
	public function getDBConnection()
	{
		return $this->dbc;
	}
	

	/**
	*
	*
	* @return
	*/

	public function getLastQuery()
	{
		return $this->dbc->getLastQuery();
	}	


	/**
	*
	*
	* @return DMEntity
	*/

	public function DMEntity()
	{
		return $this->dmEntity;
	}


	/**
	*
	*
	*/

	protected function registerPrimaryFields()
	{
		$dmFields = $this->dmEntity->getPrimaryFields();
		
		foreach ($dmFields as $dmField)
		{
			$config = array('type' => 'pri_id', 'title' => 'ID', 'entity_title' => false, 'push_to_store' => true);
			
			$amField = ApplicationEntityField::createFromDMField($this, $dmField, $config);
			
			$this->amPrimaryFields[$amField->getName()] = $amField;
		}
	}
	
	/**
	*
	*
	* @return array
	*/

	public function getPrimaryFields()
	{
		return $this->amPrimaryFields;
	}


	/**
	*
	*
	*/

	protected function registerSpecialFields()
	{
		$dmFields = $this->dmEntity->fields();
		
		foreach ($dmFields as $dmField)
		{
			switch ($dmField->getType())
			{
				case 'created':
					$config = array('type' => 'created', 'title' => 'Created', 'push_to_store' => false, 'list' => false, 'form' => false, 'managed' => false);
					
					$amField = ApplicationEntityField::createFromDMField($this, $dmField, $config);
				break;
				
				case 'modified':
					$config = array('type' => 'modified', 'title' => 'Modified', 'push_to_store' => false, 'list' => false, 'form' => false, 'managed' => false);
					
					$amField = ApplicationEntityField::createFromDMField($this, $dmField, $config);
				break;
			}
			
			
			// NOT necessary - field is added within ApplicationEntityField constructor
			// $this->amFields[$amField->getName()] = $amField;
		}
	}


	/**
	*
	*
	* @param ApplicationEntity $from_entity
	* @param bool $from_required
	* @param array $dmFieldPairs
	*/

	protected function registerLinkToFields(ApplicationEntity $from_entity, $from_required, $dmFieldPairs)
	{
		foreach ($dmFieldPairs as $dmFieldPair)
		{
			$dmFromField = $dmFieldPair[0];
			$dmToField = $dmFieldPair[1];
			
			$config = array('type' => 'ext_id', 'title' => $from_entity->getTitle());
			
			$amField = ApplicationEntityField::createFromDMField($this, $dmToField, $config);
			
			// $this->amLinkFields[$amField->getName()] = $amField;
		}
	}

	/*
	static public function createFromConfig(ApplicationDataModel $dm, $config)
	{
		$amEntity = new static($dm, $config['name'], $config['title']);
		

		if (isset($config['fields']))
		{
			foreach ($config['fields'] as $field_config)
			{
				$amEntity->createField($field_config);
			}
		}
		
		
		return $amEntity;
	}
	*/
	/**
	*
	*
	* @param array $config
	*
	* @return ApplicationEntityField
	*/

	public function createField($config)
	{
		$amField = ApplicationEntityField::createFromConfig($this, $config);
		
		return $amField;
	}
	
	/**
	*
	*
	* @param string $name
	*/

	public function deleteField($name)
	{
		if (! isset($this->amFields[$name])) throw new ApplicationEntityException("Could not delete field '" . $name . "' from entity '" . $this->dmEntity->getName() . "' from module '" . $this->dmEntity->applicationModule()->getName() . "'. Field not found in the entity.");

		$amField = $this->removeField($this->amFields[$name]);
		
		unset($amField);
	}

	
	/**
	*
	*
	* @param ApplicationEntityField $amField
	*/

	public function addField(ApplicationEntityField $amField)
	{
		$this->amFields[$amField->getName()] = $amField;
	}
	
	/**
	*
	*
	* @param ApplicationEntityField $amField
	*
	* @return ApplicationEntityField
	*/

	public function removeField(ApplicationEntityField $amField)
	{
		if ($amField->entity() !== $this) throw new ApplicationEntityException("Could not remove field '" . $amField->getName() . "' from entity '" . $this->dmEntity->getName() . "' from module '" . $this->dmEntity->applicationModule()->getName() . "' because field belongs to another entity.");
		
		
		$name = $amField->getName();
		
		
		if (! isset($this->amFields[$name])) throw new ApplicationEntityException("Could not remove field '" . $amField->getName() . "' from entity '" . $this->dmEntity->getName() . "' from module '" . $this->dmEntity->applicationModule()->getName() . "'. Field belongs to this entity but was not added.");

			
		$amField = $this->amFields[$name];
		
		unset($this->amFields[$name]);
		if (isset($this->amPrimaryFields[$name])) unset($this->amPrimaryFields[$name]);
		
		return $amField;
	}


	/**
	*
	*
	* @param ApplicationEntityLinkEndPoint $amLinkEndPoint
	*/

	public function addLinkEndPoint(ApplicationEntityLinkEndPoint $amLinkEndPoint)
	{
		$this->amLinkEndPoints[] = $amLinkEndPoint;
	}

	/**
	*
	*
	* @param ApplicationEntityLinkEndPoint $amLinkEndPoint
	*/

	public function removeLinkEndPoint(ApplicationEntityLinkEndPoint $amLinkEndPoint)
	{
		foreach ($this->amLinkEndPoints as $k => $p)
		{
			if ($p == $amLinkEndPoint) unset($this->amLinkEndPoints[$k]);
		}
	}

	
	/**
	*
	*
	* @param array $data
	*
	* @return array item PK
	*/

	public function insertItemData($data)
	{
		return $this->dmEntity->insertItemData($data);
	}
	
	/**
	*
	*
	*/

	public function insertFromCGIItemData()
	{
		$values = array();


		// Collect field data from CGI
		foreach ($this->amFields as $amField)
		{
			if (in_array($amField, $this->amPrimaryFields)) continue;
			
			$values[$amField->getName()] = $amField->getCGIValue();
		}
		

		$this->insertItemData($values);
	}
	
	/**
	*
	*
	* @param string|array|NULL $condition
	*/

	public function deleteItemData($condition = NULL)
	{
		$this->dmEntity->deleteItemData($condition);
	}
	
	/**
	*
	*
	*/

	public function deleteFromCGIItemData()
	{
		$condition = array();
		foreach ($this->amPrimaryFields as $amField)
		{
			$value = $amField->getCGIValue();
			
			if (is_null($value)) throw new ApplicationEntityException("'" . $amField->getName() . "' CGI value not given for deleteFromCGIItemData()");
			
			$condition[] = $amField->DMField()->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($value);
		}
			
		
		$this->deleteItemData(implode(' AND ', $condition));
	}
	
	
	/**
	*
	*
	* @param string|array|NULL $condition
	* @param string $group
	*
	* @return int
	*/

	public function getItemCount($condition = NULL, $group = '')
	{
		return $this->dmEntity->getItemCount($condition, $group);
	}

	/**
	*
	*
	* @param string|array $fields
	* @param string|array|NULL $condition
	*
	* @return array
	*/

	public function getItemData($fields, $condition = NULL)
	{
		return $this->dmEntity->getItemData($fields, $condition);
	}

	/**
	*
	*
	* @param string|array|NULL $condition
	*
	* @return array
	*/

	public function getItemDataAll($condition = NULL)
	{
		return $this->dmEntity->getItemDataAll($condition);
	}

	/**
	*
	*
	* @param string $select
	* @param string|array|NULL $condition
	*
	* @return array
	*/

	public function getItemDataCustom($select, $condition = NULL)
	{
		return $this->dmEntity->getItemDataCustom($select, $condition);
	}

	/**
	*
	*
	* @param string $field_name
	* @param string|array|NULL $condition
	*
	* @return string
	*/

	public function getItemValue($field_name, $condition = NULL)
	{
		return $this->dmEntity->getItemValue($field_name, $condition);
	}

	/**
	*
	*
	* @param string|array $fields
	* @param string|array|NULL $condition
	* @param string $group
	* @param string $order
	* @param array|int|NULL $limits
	* @param string $row_key_field
	* @param string $row_value_field
	*
	* @return array
	*/

	public function getItemDataList($fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmEntity->getItemDataList($fields, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param string|array|NULL $condition
	* @param string $group
	* @param string $order
	* @param array|int|NULL $limits
	* @param string $row_key_field
	* @param string $row_value_field
	*
	* @return array
	*/

	public function getItemDataListAll($condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmEntity->getItemDataListAll($condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param string $select
	* @param string|array|NULL $condition
	* @param string $group
	* @param string $order
	* @param array|int|NULL $limits
	* @param string $row_key_field
	* @param string $row_value_field
	*
	* @return array
	*/

	public function getItemDataListCustom($select, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmEntity->getItemDataListCustom($select, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	
	/**
	*
	*
	* @param string|array|NULL $condition
	*
	* @return array
	*/

	public function getFromCGIByPKItemDataAll($condition = NULL)
	{
		if (is_null($condition))
		{
			$condition = array();
		}
		
		foreach ($this->amPrimaryFields as $amField)
		{
			$cgi_value = get_cgi_value($amField->getName(), $amField->getInputType(), $amField->getInputDefault());
			
			$condition[$amField->getName()] = $cgi_value;
		}
	
		return $this->getItemDataAll($condition);
	}
	
	
	/**
	*
	*
	* @param string|array $set
	* @param string|array|NULL $condition
	* @param string $order
	* @param string $limit
	*
	* @return int PDO:exec result
	*/

	public function updateItemData($set, $condition = NULL, $order = '', $limit = '')
	{
		return $this->dmEntity->updateItemData($set, $condition, $order, $limit);
	}
	
	
	/**
	*
	*
	* @param string $foreign_entity_name
	* @param array $local_pk
	* @param array $foreign_pk
	*/

	public function createItemLink($foreign_entity_name, $local_pk, $foreign_pk)
	{
		$this->dmEntity->createItemLink($foreign_entity_name, $local_pk, $foreign_pk);
	}

	/**
	*
	*
	* @param string $foreign_entity_name
	* @param array $local_pk
	* @param array $foreign_pk
	*/

	public function dropItemLink($foreign_entity_name, $local_pk, $foreign_pk)
	{
		$this->dmEntity->dropItemLink($foreign_entity_name, $local_pk, $foreign_pk);
	}

	/**
	*
	*
	* @param string $foreign_entity_name
	* @param array $local_pk
	*/

	public function dropAllItemLinks($foreign_entity_name, $local_pk)
	{
		$this->dmEntity->dropAllItemLinks($foreign_entity_name, $local_pk);
	}
}
?>
