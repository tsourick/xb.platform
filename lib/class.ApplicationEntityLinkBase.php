<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

abstract class ApplicationEntityLinkBase
{
	protected $dm = NULL; // Data model

	protected $name; // Link name

	protected $type;
	
	protected $dmLink = NULL;


	public $fromEntity = NULL;
	public $fromRequired = false;
	
	public $toEntity = NULL;
	public $toRequired = false;
	
	
	/**
	*
	*
	* @param ApplicationDataModel $dm
	* @param string $name
	* @param ApplicationEntity $from_entity
	* @param bool $from_required
	* @param ApplicationEntity $to_entity
	* @param bool $to_required
	* @param array|NULL $custom_field_map
	*/

	public function __construct(ApplicationDataModel $dm, $name, ApplicationEntity $from_entity, $from_required, ApplicationEntity $to_entity, $to_required, $custom_field_map = NULL)
	{
		$this->dm = $dm;
		
		// $this->name = $config['name'];
		$this->name = $name;
		
		
		// $from_entity = $dm->entity($config['from']);
		// $to_entity = $dm->entity($config['to']);
		
		
		// Register link in entities through endpoints

		$from_entity->addLinkEndPoint(new ApplicationEntityLinkEndPoint($this, true, false));
		$to_entity->addLinkEndPoint(new ApplicationEntityLinkEndPoint($this, false, true));
		
		
		// Finalize construction

		$this->fromEntity = $from_entity;
		$this->fromRequired = $from_required;
		
		$this->toEntity = $to_entity;
		$this->toRequired = $to_required;


		$dm->addLink($this);
	}
	
	// THIS IS NOT A LANGUAGE DESTRUCTOR
	/**
	*
	*
	*/

	public function destruct()
	{
		$this->dm->removeLink($this);
	}


	/**
	*
	*
	* @return string
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @return string
	*/

	public function getType()
	{
		return $this->type;
	}
	
	/**
	*
	*
	* @return ApplicationDataModel
	*/

	public function dataModel()
	{
		return $this->dm;
	}	

	/**
	*
	*
	* @return ApplicationEntityLink
	*/

	public function DMLink()
	{
		return $this->dmLink;
	}


	/**
	*
		Config structure
		
		array
		(
			'name' => '<link name>',
			'from' => '<parent entity name>',
			'from_required' => (true|false),
			'to' => '<child entity name>',
			'to_required' => (true|false),
			'custom_field_map' => array
			(
				['from' => array('<from field name>' => '<custom_field_name>'),]
				['to' => array('<from field name>' => '<custom_field_name>')]
			)
			
		)
	*/
	/*
	static public function createFromConfig(ApplicationDataModel $dm, $config)
	{
		$custom_field_map = isset($config['custom_field_map']) ? $config['custom_field_map'] : NULL;

		$from_entity = $dm->entity($config['from']);
		$to_entity = $dm->entity($config['to']);


		$amLink = new static($dm, $config['name'], $from_entity, $config['from_required'], $to_entity, $config['to_required'], $custom_field_map);
		
		
		return $amLink;
	}
	*/
	
	
	/**
	*
	*
	* @param ApplicationEntity $amEntity
	* @param string $old_name
	*/

	public function remapEntity(ApplicationEntity $amEntity, $old_name)
	{
		if ($this->fromEntity != $amEntity && $this->toEntity != $amEntity) throw new ApplicationEntityLinkException("Could not remap entity '" . $amEntity->getName()  . "' from name '" . $old_name . "' because entity is not used in this link.");

		
		if (strcmp($old_name, $amEntity->getName()) != 0)
		{
			$this->dmLink->remapEntity($amEntity->DMEntity(), $old_name);
		}
	}
	
	
	/**
	*
	*
	* @param array $from_pk
	* @param array $to_pk
	*/

	public function createItemLink($from_pk, $to_pk)
	{
		$this->dmLink->createItemLink($from_pk, $to_pk);
	}
	
	/**
	*
	*
	* @param array $from_pk
	* @param array $to_pk
	*/

	public function dropItemLink($from_pk, $to_pk)
	{
		$this->dmLink->dropItemLink($from_pk, $to_pk);
	}
}

?>
