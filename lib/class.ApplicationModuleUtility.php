<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2014
*
* Application module utility base class
*
* @package platform
*/

/**
* Class
*/

class ApplicationModuleUtility implements MVCInterface
{
	private $module;
	
	
	public function __construct(ApplicationModule $module)
	{
		$this->module = $module;
	}
	
	
	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array())
	{
		return $this->module->invokeModuleAction($module_name, $action_name, $params, $options);
	}
	
	public function invokeModuleMethod($module_name, $method_name, $params = array())
	{
		return $this->module->invokeModuleMethod($module_name, $method_name, $params);
	}
	
	
	public function moduleEntity($module_name, $entity_name)
	{
		return $this->module->moduleEntity($module_name, $entity_name);
	}
	
	public function entity($name)
	{
		return $this->module->entity($name);
	}
}

?>
