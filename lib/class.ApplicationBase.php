<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Application base class (singleton)
*
* @package platform
*/

// Framework classes

// require_once('class.DBConnectionManager.php');
// require_once('class.TParserFactory.php');
// require_once('class.Lang.php');
Framework::useClass('DBConnectionManager');
Framework::useClass('TParserFactory');
Framework::useClass('Lang');

/**
* Class exception
*/

class ApplicationException extends Exception
{
}


// Application classes

// require_once('class.ApplicationConfig.php');
// require_once('class.ApplicationSession.php');
// require_once('class.ApplicationMVCController.php');
// require_once('class.ApplicationModule.php');
// require_once('class.ApplicationDataModel.php');
// require_once('class.CMSHelper.php');
Platform::useClass('ApplicationConfig');
Platform::useClass('ApplicationSession');
Platform::useClass('ApplicationMVCController');
Platform::useClass('ApplicationModule');
Platform::useClass('ApplicationDataModel');
Platform::useClass('CMSHelper');



/**
* Application singleton
*/

abstract class ApplicationBase implements MVCInterface
{
	private static $instance = NULL;
	
	protected $name = '';
	
	protected $config = NULL;
	protected $tparser = NULL;
	
	protected $defaultDBConnectionName = 'dbc1';
	protected $defaultDBConnection = NULL;
	
	protected $mvcc = NULL;

	protected $dataModel = NULL;
	
	private $temporaryFiles = array();
	private $temporaryDirs = array();

	
	/**
	* Returns Application object
	*
	* @return ApplicationBase object
	*/

	public static function getObject()
	{
		$object = NULL;
		
		if (! is_null(self::$instance))
		{
			$object = self::$instance;
		}
		
		return $object;
	}
	
	/**
	* Prevents clonning
	*
	* @access private
	*/

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	/**
	* Constructor
	*/

	public function __construct()
	{
		if (! is_null(self::$instance)) throw new ApplicationException(__CLASS__ . ' singleton already created');
		
		self::$instance = $this;
	}
	
	/**
	* Destructor
	*
	* Cleans up temporary files and directories, created with createTemporaryFile() createTemporaryDir()
	*
	* @see createTemporaryFile(), createTemporaryDir()
	*/

	public function __destruct()
	{
		// Cleanup temporary files and dirs if any

		foreach ($this->temporaryFiles as $path)
		{
			if (file_exists($path)) fs_delete($path);
		}
	}

	/**
	* Returns application template parser
	*
	* @return object template parser
	*/

	public function getTParser()
	{
		return $this->tparser;
	}

	/**
	* Returns application config object
	*
	* @return object config object
	*/

	public function getConfig()
	{
		return $this->config;
	}

	/**
	* Returns application session object
	*
	* @return object session object
	*/

	public function getSession()
	{
		$as = ApplicationSession::getObject();
		return $as->getSession();
	}

	/**
	* Returns application config object
	*
	* @deprecated
	*/

	public function getSystemConfig()
	{
		return $this->getConfig();
	}


	// for old modules without datamodel
	
	private $_DBTablePrefix = NULL; // cache
	
	/**
	* Returns table name global prefix, as set in configuration file
	*
	* Used for old modules without datamodel and for custom tables.
	*
	* @return string
	*/

	public function getDBTablePrefix()
	{
		if (is_null($this->_DBTablePrefix))
		{
			$this->_DBTablePrefix = $this->config->get($this->defaultDBConnectionName . '/table_prefix');
		}
		
		return $this->_DBTablePrefix;
	}

	
	/**
	* Loads module through application's controller
	*
	* @param string $module_name
	*
	* @return ApplicationModule module object
	*/

	public function loadModule($module_name)
	{
		return $this->mvcc->loadModule($module_name);
	}
	
	/**
	* Calls module action handler through application's MVC controller
	*
	* @param string $module_name module name
	* @param string $action_name action name
	* @param array $params module action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array())
	{
		return $this->mvcc->invokeModuleAction($module_name, $action_name, $params, $options);
	}

	/**
	* Calls module method through application's MVC controller
	*
	* @param string $module_name module name
	* @param string $method_name method name
	* @param array $params module method parameters
	*
	* @return mixed method result
	*/

	public function invokeModuleMethod($module_name, $method_name, $params = array())
	{
		return $this->mvcc->invokeModuleMethod($module_name, $method_name, $params);
	}
	
	
	/**
	* Fires module event
	*
	* Module event handler is called first, then all subscribed modules are triggered for the event if any.
	*
	* @param string $module_name module name
	* @param string $event_name event name
	* @param array $event_params event parameters
	*/

	public function fireModuleEvent($module_name, $event_name, $event_params)
	{
		$module_table_name       = $this->getDBTablePrefix() . 'module';
		$event_table_name        = $this->getDBTablePrefix() . 'module_event';
		$subscription_table_name = $this->getDBTablePrefix() . 'module_event_subscription';
		
		
		$dbc = $this->getDefaultDBConnection();

		$module = $dbc->getRowAll($module_table_name, array('name' => $module_name));
		$event = $dbc->getRowAll($event_table_name, array('module_id' => $module['id'], 'name' => $event_name));
		
		if (! empty($event))
		{
			// Fire event on the module itself
			$this->invokeModuleMethod($module_name, 'onModuleEvent', array($module_name, $event_name, $event_params));
			
			
			// Notify subscribed modules for the event
			$subscribed_module_ids = $dbc->getRows('module_id', $subscription_table_name, array('module_event_id' => $event['id']), '', '', NULL, '', 'module_id');
			
			foreach ($subscribed_module_ids as $subscribed_module_id)
			{
				$subscribed_module = $dbc->getRow('name', $module_table_name, array('id' => $subscribed_module_id));
				
				$this->invokeModuleMethod($subscribed_module['name'], 'onModuleEvent', array($module['name'], $event_name, $event_params));
			}
		}
	}


	/**
	* Returns default DB connection name
	*
	* @return string name of default DB connection
	*/

	public function getDefaultDBConnectionName()
	{
		return $this->defaultDBConnectionName;
	}
	
	/**
	* Returns default DB connection object
	*
	* @return DBConnection default DB connection object
	*/

	public function getDefaultDBConnection()
	{
		return $this->defaultDBConnection;
	}



	/**
	* Returns path to directory where config file resides
	*
	* Default path is the path of current file.
	* For customizing the path this method should be overridden in derived classes. 
	*
	* @return string config file dir path
	*/

	protected function makeConfigFileDirPath()
	{
		return dirpath(dirname(__FILE__));
	}


	/**
	* Returns path to directory which should be used as default template directory.
	*
	* Default path is "<the path of current file>templates/".
	* Method is called when no default dir is set in config explicitly.
	*
	* @return string default dir path to default template dir
	*/

	protected function makeDefaultDefaultTemplateDirPath()
	{
		return dirpath(dirname(realpath(__FILE__))) . 'templates/';
	}

	// used if dir not set explicitly in config
	/**
	* Returns path to directory which should be used as module directory.
	*
	* Default path is "<the path of current file>modules/".
	* Method is called when no dir is set in config explicitly.
	*
	* @return string default dir path to module dir
	*/

	protected function makeDefaultModuleDirPath()
	{
		return dirpath(dirname(realpath(__FILE__))) . 'modules/';
	}

	
	/**
	* Returns module base dir path
	*
	* @param string $module_name module name
	*
	* @return string path to module's base directory
	*/

	public function getModuleBaseDirPath($module_name)
	{
		$c = $this->getConfig();
		
		return align_path(dirpath($c->get('application/module_base_dir')) . $module_name . '/');
	}


	/**
	* Returns module dir path
	*
	* @param string $module_name module name
	*
	* @return string path to module's directory
	*/

	public function getModuleDirPath($module_name)
	{
		$c = $this->getConfig();
		
		return align_path(dirpath($c->get('application/module_dir')) . $module_name . '/');
	}
	
	
	/**
	* Includes file (with require()) relative to the application install dir
	*
	* @param string $path path to the file relative to the application/install_dir
	* @param bool $once when false require() call issued (default); require_once() call issued otherwise
	*/
	
	static public function useFile($path, $once = false)
	{
		$application = self::getObject();
	
		
		$c = $application->getConfig();
		
		if ($once)
		{
			require_once(align_path(dirpath($c->get('application/install_dir')) . $path));
		}
		else
		{
			require(align_path(dirpath($c->get('application/install_dir')) . $path));
		}
	}
	

	/**
	* Returns temp dir base
	*/
	public function getTempDir()
	{
		$c = $this->getConfig();
		$tmp_dir_path = dirpath($c->get('application/temporary_dir'));
		
		if (! file_exists($tmp_dir_path)) throw new ApplicationException("Temporary base dir '$tmp_dir_path' not exists");

		return $tmp_dir_path;
	}
	
	
	/**
	* Generates path for new temporary file
	*
	* @return string path to temporary file
	*/

	public function getTemporaryFilepath($length = 16)
	{
		// $c = $this->getConfig();
		// $tmp_dir_path = dirpath($c->get('application/temporary_dir'));
		$tmp_dir_path = $this->getTempDir();
		
		$file_name = str_rand($length);
		$tmp_file_path = $tmp_dir_path . $file_name;
		
		return $tmp_file_path;
	}

	/**
	* Generates path for new temporary directory
	*
	* @return string path to temporary directory
	*/

	public function getTemporaryDirpath($length = 16)
	{
		return dirpath($this->getTemporaryFilepath($length));
	}
	
	/**
	* Creates temporary file
	*
	* @param bool $cleanup whether to cleanup the file on script shutdown, default is true
	*
	* @return string path to newly created file
	*/

	public function createTemporaryFile($cleanup = true, $length = 16)
	{
		$path = $this->getTemporaryFilepath($length);
		
		touch($path);
		
		if ($cleanup)
		{
			$this->temporaryFiles[] = $path;
		}
		
		return $path;
	}
	
	/**
	* Creates temporary directory
	*
	* @param bool $cleanup whether to cleanup the directroy on script shutdown, default is true
	*
	* @return string path to newly created dir
	*/

	public function createTemporaryDir($cleanup = true, $length = 16)
	{
		$path = $this->getTemporaryDirpath($length);
		
		mkdir($path);
		
		if ($cleanup)
		{
			$this->temporaryFiles[] = $path;
		}
		
		return $path;
	}


	// Locker facility
	
	private $locker = NULL;
	
	public function getLocker()
	{
		if (is_null($this->locker))
		{
			Framework::useClass('Locker');
	
			$c = $this->getConfig();
			$dir_path = dirpath($c->get('application/temporary_dir'));
	
			$this->locker = new Locker($dir_path);
		}
		
		
		return $this->locker;
	}
	
	/**
	* @param string $name if NULL 'application' if used
	*/
	public function locked($name = NULL)
	{
		$locker = $this->getLocker();


		$lock_name = is_null($name) ? 'application' : $name;
		
		return $locker->locked($lock_name);
	}

	/**
	* @param string $name if NULL 'application' if used
	* @param int $timeout if not set taken from config; if not set in config falls back to Locker's default (max execution time)
	*/
	public function lock($name = NULL, $timeout = NULL)
	{
		$locker = $this->getLocker();


		$c = $this->getConfig();
		
		$lock_name = is_null($name) ? 'application' : $name;
		$lock_timeout = is_null($timeout) ? $c->get('application/lock_timeout', false) : $timeout; // if not set in config falls back to Locker's default (max_execution_time)
		
		return $locker->lock($lock_name, $lock_timeout);
	}
	
	/**
	* @param string $name if NULL 'application' if used
	*/
	public function unlock($name = NULL)
	{
		$locker = $this->getLocker();


		$lock_name = is_null($name) ? 'application' : $name;
		
		return $locker->unlock($lock_name);
	}
	
	
	/**
	* Replace "{$application.url}" placeholder with the URL of application inside <img>'s SRC  and <a>'s href
	*
	* @param string $html HTML string
	*
	* @return string result after substitution
	*/
	
	private function _resolveHTMLURLs($html)
	{
		$base_url = $this->getConfig()->get('application/url');

		$replacer = create_function
		(
			'$matches',
			'
				$matches[2] = str_replace(\'{$application.url}\', \'' . $base_url . '\', $matches[2]);
				
				return $matches[1] . $matches[2] . $matches[3];
			'
		);

		$re = '/(<img\\s.*?src=[\'"])([^\'"]+)([\'"])/';
		$html = preg_replace_callback($re, $replacer, $html);
		
		$re = '/(<a\\s.*?href=[\'"])([^\'"]+)([\'"])/';
		$html = preg_replace_callback($re, $replacer, $html);
		
		return $html;
	}

	/**
	* Resolves standard "{$application.url}" placeholder within text
	*
	* Three modes available:
	*  1: $data as string
	*  2: $data as array and $names
	*  3: $data as array of rows and $names
	*
	* @param (string|array)  $data    Value or array of values or multidim array of rows to resolve text in.
	* @param array           $names   Key names to resolve in array. Required in modes 2, 3.
	*
	* @return mixed  
	*/
	
	public function resolveHTMLURLs($data, $names = array())
	{
		if (! is_array($data)) // single string mode
		{
			$data = $this->_resolveHTMLURLs($data);
		}
		else
		{
			$av = array_values($data);
			if (isset($av[0]) && is_array($av[0])) // multidim array
			{
				foreach ($data as & $row)
				{
					foreach ($names as $name)
					{
						if (isset($row[$name])) $row[$name] = $this->_resolveHTMLURLs($row[$name]);
					}
				}
			}
			else // flat arrray (1 dim)
			{
				foreach ($names as $name)
				{
					if (isset($data[$name])) $data[$name] = $this->_resolveHTMLURLs($data[$name]);
				}
			}
		}
		
		return $data;
	}


	/**
	* Send email from application
	*/
	
	public function sendNotification($email_to, $name_to, $subject, $body, $mime_type = 'text/plain', $email_from = NULL, $name_from = NULL)
	{
		$c = $this->getConfig();
		
		$email_from = is_null($email_from) ? $c->get('application/email') : $email_from;
		$name_from = is_null($name_from) ? $c->get('application/name') : $name_from;
		
		include_once('class.SimpleMail.php');

		$mail = new SimpleMail($email_from, $email_to, $subject, $body, $mime_type, 'utf-8', 'utf-8', $name_from, $name_to);
		$mail->send();
	}


	/**
	* Creates application config object and sets up critical values
	*
	* Called by run().
	*
	* @access private
	*/

	protected function setupConfig()
	{
		$config_file_dir = $this->makeConfigFileDirPath();
		
		
		$c = new ApplicationConfig($config_file_dir); // This is a singleton
		
		
		// Map application section
		
		$application_section_name = $this->name . '_application';
		
		$application_section_data = $c->get($application_section_name);
		$c->set('application', $application_section_data);
		
		
		if (is_null($c->get('application', false))) throw new ApplicationException('application config section not found');
		
		
		// Check application name

		if (is_null($c->get('application/name', false))) throw new ApplicationException('application name is not set');


		// Set up dir to default parser

		$tparser_list = $c->get('tparsers/list');
		if (is_null($tparser_list)) throw new ApplicationException('no TParsers defined');
		
		
		$default_tparser = $c->get('application/default_tparser', false);
		if (is_null($default_tparser))
		{
			list($default_tparser) = explode(',', $tparser_list);
			$c->set('application/default_tparser', $default_tparser);
		}
		else
		{
			if (! in_array($default_tparser, explode(',', $tparser_list))) throw new ApplicationException("'$default_tparser' set as default tparser but not found in the list");
		}


		// Set up default dir to templates
		
		$default_template_dir = $c->get('application/default_template_dir', false);
		if (is_null($default_template_dir))
		{
			$default_template_dir = $this->makeDefaultDefaultTemplateDirPath();
		}
		
		$c->set('application/default_template_dir', dirpath($default_template_dir));


		// Check theming
		
		$theme = $c->get('application/theme', false);
		if (! empty($theme))
		{
			$theme_fallback = $c->get('application/theme_fallback', false);
			
			if ($theme_fallback)
			{
				$default_theme = $c->get('application/default_theme', false);
				
				if (empty($default_theme)) throw new ApplicationException("Theming fallback is on, but no default theme specified.");
			}
		}


		// Set up base dir to modules
		
		if (is_null($c->get('application/module_base_dir'))) throw new ApplicationException('module base dir is not set');
		
		$c->set('application/module_base_dir', dirpath($c->get('application/module_base_dir')));


		// Set up dir to modules
		
		if (is_null($c->get('application/module_dir', false)))
		{
			$module_dir = $this->makeDefaultModuleDirPath();
		}
		else
		{
			$module_dir = $c->get('application/module_dir');
		}
		
		$c->set('application/module_dir', dirpath($module_dir));


		// Set lang default language config
		
		if (! is_null($c->get('application/language')))
		{
			$c->set('lang/default_language', $c->get('application/language'));
		}
	


		$this->config = $c;
	}
	
	/**
	* Sets up locale values
	*
	* Called by run().
	*
	* @access private
	*/
	
	final protected function setupLocale()
	{
		/*
		$locales = array
		(
			'WIN' => array('ru' => 'Russian_Russia.65001', 'en' => 'English_United States.65001'),
			'NIX' => array('ru' => 'ru_RU.UTF-8', 'en' => 'en_EN.UTF-8')
		);
		*/

		$c = $this->config;
		
		$app = $c->get('application');
		$language = isset($app['language']) ? $app['language'] : NULL;
		$default_locale = isset($app['default_locale']) ? $app['default_locale'] : NULL;
		
		if (! is_null($default_locale))
		{
			if (preg_match('/(\w{2})_(\w{2})\.(.*)/', $default_locale, $matches))
			{
				$language = $matches[1];
				// $country = $matches[2];
				// $charset = $matches[3];
			}
		}
		
		/*
		$php_os = strtoupper(substr(PHP_OS, 0, 3));
		$locale = $locales[$php_os][$language];

		setlocale(LC_ALL, $locale);
		*/
		CMSHelper::setlanguage($language);
	}
	
	/**
	* Sets up framework config
	*
	* Called by run().
	*
	* @access private
	*/

	final protected function setupFramework()
	{
		Framework::setConfig($this->config->get()); // tparser_smarty and services_json are required
	}
	/*
	final protected function setupLang()
	{
		$this->lang = new Lang();
	}
	*/

	/**
	* Sets up application default DB connection
	*
	* Called by run().
	*
	* @access private
	*/
	
	final protected function setupDB()
	{
		$dbcm = new DBConnectionManager();
		
		$dbc = $dbcm->getDefaultConnection();
		$dbc->exec('SET NAMES "utf8";');
		$dbc->setFetchMode(DBConnection::FETCH_ASSOC);

		
		$this->defaultDBConnection = $dbc;
	}
	
	/**
	* Sets up application session singleton
	*
	* Called by run().
	*
	* @access private
	*/

	protected function setupSession()
	{
		$session_id = $this->name . '_session_id';
		$table_name = $this->getDBTablePrefix() . 'session';
		
		new ApplicationSession($session_id, $this->defaultDBConnection, $table_name);
	}

	/**
	* Invokes module action from within template
	*
	* Template parser handler.
	*
	* @param array $params parameters
	* @param object $tparser template parser
	*
	* @return mixed action result
	*/

	public function tparserInvoke($params, &$tparser)
	{
		$module = $params['module'];
		unset($params['module']);

		$action = $params['action'];
		unset($params['action']);


		return $this->invokeModuleAction($module, $action, $params);
	}

	/**
	* Populates module settings into template variables
	*
	* @param array $params parameters
	* @param object $tparser template parser
	*
	* @return mixed action result
	*/

	public function tparserModuleSettings($params, &$tparser)
	{
		$module = isset($params['module']) ? $params['module'] : NULL;
		$names = $params['names'];
		
		if (! $module)
		{
			$module = $tparser->getTemplateVars('module');
			if ($module && isset($module['name']))
			{
				$module = $module['name'];
			}
		}
		
		
		if ($module)
		{
			$names = is_array($names) ? $names : explode(',', $names);
			
			$settings = $this->invokeModuleMethod($module, 'getSettings', array($names));
	
			$module = $tparser->getTemplateVars('module');
			$module['settings'] = $settings;
			$tparser->assign('module', $module);
		}
		else
		{
			trigger_error('Template error: "module" not given in non-module template');
		}
		
		return '';
	}

	/**
	* Sets up application template parser
	*
	* Called by run().
	*
	* @access private
	*/

	protected function setupTParser()
	{
		$a =  $this->config->get('application');
		
		$tparser = $a['default_tparser'];
		$template_dir = $a['default_template_dir'];
		$theme = array_all_not_empty($a, 'theme') ? $a['theme'] : '';
		$default_theme = (! empty($theme) && array_all_not_empty($a, 'theme_fallback')) ? $a['default_theme'] : '';
		
		// Create tparser
		
		$this->tparser = TParserFactory::createTParser($tparser, $template_dir, $theme, $default_theme);

		$this->tparser->register_function('invoke', array($this, 'tparserInvoke'));
		$this->tparser->register_function('settings', array($this, 'tparserModuleSettings'));
		
		
		$this->tparser->set(array('application' => $this->getConfig()->get('application')));

		
		$this->tparser->debug($this->config->get('application/debug'));
	}
	
	/**
	* Sets up application MVC controller
	*
	* Called by run().
	*
	* @access private
	*/

	protected function setupMVCController()
	{
		// Create controller
		
		// $this->mvcc = new MVCController1($this->tparser, $this->config->get('application/module_dir')); // actually singleton, but i am not sure whether it is necessary
		$this->mvcc = new ApplicationMVCController($this->tparser, $this->config->get('application/module_dir'));
	}
	
	/**
	* Sets up application data model object
	*
	* Called by run().
	*
	* @access private
	*/

	protected function setupDataModel()
	{
		$connection_config = $this->config->get($this->defaultDBConnectionName);
		
		
		$model_name = $connection_config['dbname'];
		$model_options = array('table_name_prefix' => $connection_config['table_prefix']);


		$dbc = $this->getDefaultDBConnection();
		
		
		$this->dataModel = new ApplicationDataModel($dbc, $model_name, $model_options);
	}
	

	private $resolvedDataModelModules = array();
	
	/**
	* Loads data model config for specified module and for linked modules if any
	*
	* @param string $module_name module name
	*
	* @return array array of module configs which are just resolved and should be imported right now
	*
	* @access private
	*/

	protected function resolveModuleDataModel($module_name)
	{
		$resolved_module_configs = array();
		

		if (! in_array($module_name, $this->resolvedDataModelModules))
		{
			$data_model_config = $this->invokeModuleMethod($module_name, 'getDataModelConfig');

			if (isset($data_model_config['links']))
			{
				$external_module_names = array();
				
				foreach ($data_model_config['links'] as $link_config)
				{
					if ($link_config['from_module'] != $module_name)
					{
						$external_module_names[] = $link_config['from_module'];
					}
					
					if ($link_config['to_module'] != $module_name)
					{
						$external_module_names[] = $link_config['to_module'];
					}
				}
				
				
				if (! empty($external_module_names))
				{
					foreach ($external_module_names as $external_module_name)
					{
						$configs = $this->resolveModuleDataModel($external_module_name);
						
						$resolved_module_configs = array_merge($resolved_module_configs, $configs);
					}
				}
			}

			$resolved_module_configs[$module_name] = $data_model_config;
		}
		

		return $resolved_module_configs;
	}
	
	/**
	* Returns application data model ensuring that data model for specified module is initialized
	*
	* If $model_name is not empty, loads data model for that module (if not already loaded) into application's
	* model resolving links to external modules and loading those model's as well.
	* If $module_name is empty, just returns data model as is.
	*
	* @param string $module_name module name
	*
	* @return ApplicationDataModel application data model object
	*/

	public function dataModel($module_name = '')
	{
		if (is_null($this->dataModel))
		{
			$this->setupDataModel();
		}
		
		
		if (! empty($module_name))
		{
			$resolved_configs = $this->resolveModuleDataModel($module_name);

			if (! empty($resolved_configs))
			{
				// import entities
				foreach ($resolved_configs as $resolved_module_name => $its_config)
				{
					if (isset($its_config['links'])) unset($its_config['links']);

					$this->dataModel->importFromModuleConfig($resolved_module_name, $its_config);
				}

				// import links
				foreach ($resolved_configs as $resolved_module_name => $its_config)
				{
					if (isset($its_config['entities'])) unset($its_config['entities']);
					
					$this->dataModel->importFromModuleConfig($resolved_module_name, $its_config);
				}

				$this->resolvedDataModelModules = array_merge($this->resolvedDataModelModules, array_keys($resolved_configs));
			}
		}
		
		
		return $this->dataModel;
	}
	
	
	/**
	* Returns entity object for specified module from application's data model
	*
	* @param string $module_name module name
	* @param string $entity_name entity name
	*
	* @return object entity object
	*/

	public function moduleEntity($module_name, $entity_name)
	{
		$dm = $this->dataModel($module_name);
		return $dm->entity(ApplicationDataModel::makeModuleEntityName($module_name, $entity_name));
	}
	
	
	/**
	* Returns link object for specified module from application's data model
	*
	* @param string $module_name module name
	* @param string $link_name link name
	*
	* @return object link object
	*/

	public function moduleLink($module_name, $link_name)
	{
		$dm = $this->dataModel($module_name);
		return $dm->link(ApplicationDataModel::makeModuleLinkName($module_name, $link_name));
	}
	
	
	/**
	* Returns localized text string from specified module or from Default module if not found in specified one
	*
	* @param string $text text for translation
	* @param ApplicationModule $module module object 
	*
	* @return string localized text string
	*/

	public function t($text, ApplicationModule $module)
	{
		// Call module's lang
		
		$lang = $module->getLang();
		
		$found = false;
		$text = $lang->t($text, $found);
		
		
		if (! $found && $module->getName() != 'Default')
		{
			// Call default (default module's) lang
			
			$lang = $this->invokeModuleMethod('Default', 'getLang');

			$text = $lang->t($text);
		}


		return $text;
	}

	
	public function beforeRun()
	{
	}
	
	public function afterRun()
	{
	}
	
	/**
	* Executes application
	*
	* Application is set up and MVC is executed.
	*/

	public function run()
	{
		try
		{
			// Setup
			
			$this->setupConfig();
			
			$this->setupLocale();
			
			$this->setupFramework();
	
			// $this->setupLang();
			
			$this->setupDB();
	
			$this->setupSession();
	
			$this->setupTParser();
	
			$this->setupMVCController();
	
			// $this->setupDataModel();
			
	
			$this->beforeRun();

			// Run controller
			$this->mvcc->run();

			$this->afterRun();
		}
		catch (Exception $e)
		{
			$this->_internalServerError($e);
		}
	}
	
	
	/**
	* Logs given exception object data, generates HTTP server error (code 500) and exits script 
	*
	* @param Exception $e exception object
	*/

	public function _internalServerError($e, $send500 = true)
	{
		$class_name = get_class($e);
		$message      = $e->getMessage();
		$file_path    = $e->getFile();
		$line_number  = $e->getLine();
		$trace        = $e->getTraceAsString();
		$trace = '  ' . str_replace("\n", "\n  ", $trace); // indent

		$url = isset($_SERVER) ? xbf_http_get_url() : NULL;
		$ip = isset($_SERVER) ? xbf_http_get_ip() : NULL;

		
		$log = "EXCEPTION \"{$class_name}\"\n\nSAYS: {$message}";

		if ($url)
		$log .= "\n\n" . "TRIGGERED by: (url) - {$url}";
		if ($ip)
		$log .= "; (possible ip) - {$ip}";
	
		$log .= "\n\n" . <<<LOG
THROWN in: {$file_path} on line {$line_number}
	
TRACE:
{$trace}

LOG;
	
		logger_log($log);
	

		if ($send500)
		{
			$content = 'Internal server error' . str_repeat(' ', 60);
			xbf_http_send_response_new($content, 'text/plain', '', 500, 'Internal server error'); // exits after this
		}
		
		return $log; // no log returned if send500 is true
	}
	
	
	/**
	* Implements authorization method.
	*
	* Calls authorization module to get permissions in given module for current user.
	* Must be overwritten in derived classes to provide module of authorization.
	*
	* @param int $module_id module id
	*
	* @return array array of module permission values for current user
	*/
	
	public function getModulePermissionValues($module_id)
	{
		abstract_method(__CLASS__, __METHOD__);
	}
	/*
	{
		return $this->invokeModuleMethod('Users', 'getModulePermissionValues', array($module_id));		
	}
	*/
}
?>
