<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityFieldException extends ApplicationDataModelException
{
}


class ApplicationEntityField
{
	private $amEntity; // host DMEntity instance

	private $name; // string
	/**
	* @var string field type
	*
	* @see ApplicationDataModel::$fieldTypeConfigs
	*/
	private $type; // string
	private $title; // string

	private $dmField; // DMField is created or taken within createFromXXX() and stored here
	
	private $inputType;
	private $inputDefault;


	// Extensible options (modifying options here don't forget to modify the module entity field table structure as well)
	static public $defaultOptions = array
	(
		'entity_title' => false, 
		'push_to_store' => true,
		'list' => true,
		'width' => 'auto',
		'height' => 'auto',
		'column_width' => 'auto',
		'column_expand' => false,
		'form' => true,
		'managed' => true,
		'default_sort' => false,
		'default_sort_order' => 'ASC'
	);
	
	private $options;

	
	// Private constructor; Instead use static constructors from outside
	// private function __construct(ApplicationEntity $amEntity, $name, $type, $title, $entity_title = false, $push_to_store = true)
	/**
	* Private constructor
	*
	*
	* @param ApplicationEntity $amEntity
	* @param string $name
	* @param string $type
	* @param string $title
	* @param array $options
	*/

	private function __construct(ApplicationEntity $amEntity, $name, $type, $title, $options = array())
	{
		$this->amEntity = $amEntity;

		$this->name = $name;
		$this->type = $type;
		$this->title = $title;

		$am_field_type_config = ApplicationDataModel::$fieldTypeConfigs[$type];
		
		$this->inputType = $am_field_type_config['input_type'];
		$this->inputDefault = $am_field_type_config['input_default'];

		// Init options
		$this->options = self::$defaultOptions;

		// Update options (override default values) if any
		$this->setOptions($options);
		
		
		$amEntity->addField($this);
	}
	
	/**
	* Returns field name
	*
	* @return string field name
	*/

	public function getName()
	{
		return $this->name;
	}

	/**
	* Returns field type
	*
	* @return string field type
	*/

	public function getType()
	{
		return $this->type;
	}

	/**
	* Returns field title
	*
	* @return string field title
	*/

	public function getTitle()
	{
		return $this->title;
	}

	/**
	* Returns field CGI input type
	*
	* Input type is used for get_cgi_value() call.
	*
	* @return string CGI input type
	*/

	public function getInputType()
	{
		return $this->inputType;
	}

	/**
	* Returns field CGI input default value
	*
	* Input default value is used for get_cgi_value() call.
	*
	* @return mixed CGI input default value
	*/

	public function getInputDefault()
	{
		return $this->inputDefault;
	}

	
	/**
	* Returns option value
	*
	* @param string $name option name
	*
	* @return mixed option value
	*/

	public function getOption($name)
	{
		return $this->options[$name];
	}
	
	/**
	* Returns option values
	*
	* @return array option values
	*/

	public function getOptions()
	{
		return $this->options;
	}

	
	/**
	* Returns entity_title option value
	*
	* @return bool
	*
	* @deprecated
	*/

	public function isEntityTitle()
	{
		return $this->getOption('entity_title');
	}
	
	/**
	* Returns push_to_store option value
	*
	* @return bool
	*
	* @deprecated
	*/

	public function isPushToStore()
	{
		return $this->getOption('push_to_store');
	}


	/**
	* Changes field name
	*
	* @param string $name field new name
	*/

	public function setName($name)
	{
		if (strcmp($this->name, $name) != 0)
		{
			$this->dmField->setName($name);
			
			
			$old_name = $this->name;

			$this->name = $name;
			
			$this->amEntity->remapField($this, $old_name); // Update field name upstream
		}
	}
	
	/**
	* Chages field type
	*
	* @param string $type field new type
	*/

	public function setType($type)
	{
		if (strcmp($this->type, $type) != 0)
		{
			$am_field_type_config = ApplicationDataModel::$fieldTypeConfigs[$type];
	
			
			$dm_type = $am_field_type_config['dmType'];
			
			$this->dmField->setType($dm_type);
	
	
			$this->inputType = $am_field_type_config['input_type'];
			$this->inputDefault = $am_field_type_config['input_default'];
	
			
			$this->type = $type;
		}
	}
	
	/**
	* Changes field title
	*
	* @param string $title field new title
	*/

	public function setTitle($title)
	{
		$this->title = $title;
	}


	/**
	* Changes option value
	*
	* @param string $name option name
	* @param mixed $value option value
	*/

	public function setOption($name, $value)
	{
		$this->options[$name] = $value;
	}

	/**
	* Changes option values
	*
	* @param array $options option new values
	*/

	public function setOptions($options)
	{
		if (! empty($options))
		{
			/*
			foreach (self::$defaultOptions as $n => $defaultValue)
			{
				$this->options[$n] = isset($options[$n]) ? $options[$n] : $defaultValue;
			}
			*/
			foreach ($options as $k => $v)
			{
				// if (! isset(self::$defaultOptions[$k])) throw new ApplicationEntityFieldException("Unknown field option '{$k}' tried to be set on field '{$this->name}'");
				if (! isset(self::$defaultOptions[$k])) continue;
				
				$this->options[$k] = $v;
			}
		}
	}
	

	/**
	* Changes 'entity_title' option value
	*
	* @param bool $entity_title
	*
	* @deprecated
	*/

	public function setEntityTitle($entity_title)
	{
		$this->setOption('entity_title', $entity_title);
	}
	

	/**
	* Changes 'push_to_store' option value
	*
	* @param bool $push_to_store
	*
	* @deprecated
	*/

	public function setPushToStore($push_to_store)
	{
		$this->setOption('push_to_store', $push_to_store);
	}


	/**
	* Returns parent ApplicationEntity object
	*
	* @return ApplicationEntity
	*/

	public function entity()
	{
		return $this->amEntity;
	}
	
	
	/**
	* Returns underlying DMEntity object
	*
	* @return DMField
	*/

	public function DMField()
	{
		return $this->dmField;
	}


	/**
	* Creates class object (factory method)
	*
	* @param ApplicationEntity $amEntity parent ApplicationEntity object
	* @param array $config field config
	*
	* @return ApplicationEntityField class object
	*/

	static public function createFromConfig(ApplicationEntity $amEntity, $config)
	{
		// Create new AMField

		// Use default values if not given in config
		// $entity_title = isset($config['entity_title']) ? $config['entity_title'] : false;
		// $push_to_store = isset($config['push_to_store']) ? $config['push_to_store'] : true;

		// $amField = new self($amEntity, $config['name'], $config['type'], $config['title'], $entity_title, $push_to_store);
		$amField = new self($amEntity, $config['name'], $config['type'], $config['title'], $config);


		// Manually create new DMField for underlayer
		
		$dmEntity = $amEntity->DMEntity();
		
		$am_field_type_config = ApplicationDataModel::$fieldTypeConfigs[$config['type']];
		$dm_type = $am_field_type_config['dmType'];
		
		$dmFieldConfig = DMDataModel::$fieldTypeConfigs[$dm_type];
		$dmFieldConfig['name'] = $config['name'];
		$dmFieldConfig['type'] = $dm_type;
		$dmField = DMField::createFromConfig($dmEntity, $dmFieldConfig);
		
		$amField->dmField = $dmField;
		
		
		return $amField;
	}
	

	/**
	* Creates class object (factory method)
	*
	* @param ApplicationEntity $amEntity parent ApplicationEntity object
	* @param DMField $dmField source object
	* @param array $config field config
	*
	* @return ApplicationEntityField class object
	*/

	static public function createFromDMField(ApplicationEntity $amEntity, $dmField, $config)
	{
		// Use default values if not given in config
		// $entity_title = isset($config['entity_title']) ? $config['entity_title'] : false;
		// $push_to_store = isset($config['push_to_store']) ? $config['push_to_store'] : true;


		// Create new AMField

		$name = $dmField->getName();

		// $amField = new self($amEntity, $name, $config['type'], $config['title'], $entity_title, $push_to_store);
		$amField = new self($amEntity, $name, $config['type'], $config['title'], $config);

		
		// Store DMField for underlayer
		$amField->dmField = $dmField;
		
		
		return $amField;
	}


	/**
	* Returns value given by CGI for the field
	*
	* Calls get_cgi_value() using $this->inputType and $this->inputDefault.
	*
	* @return mixed field value
	*/

	public function getCGIValue()
	{
		$value = NULL;
		
		
		if (is_null($this->inputType)) throw new ApplicationEntityFieldException("Field '{$this->name}' has no input type");
			
			
		switch ($this->inputType)
		{
			case 'date':
				$value = get_cgi_date($this->name, $this->inputDefault);
			break;
			
			case 'date_string':
				$value = get_cgi_date_string($this->name, $this->inputDefault);
			break;
			
			case 'time':
				$value = get_cgi_time($this->name, $this->inputDefault);
			break;
			
			case 'time_string':
				$value = get_cgi_time_string($this->name, $this->inputDefault);
			break;
			
			case 'datetime':
				$value = get_cgi_datetime($this->name, $this->inputDefault);
			break;
			
			case 'datetime_string':
				$value = get_cgi_datetime_string($this->name, $this->inputDefault);
			break;
			
			case 'file':
				$value = get_cgi_file($this->name);
			break;
			
			default:
				$value = get_cgi_value($this->name, $this->inputType, $this->inputDefault);
		}
		

		return $value;
	}
}


?>
