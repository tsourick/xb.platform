<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Application MVC controller class
*
* @package classes
*/

// Framework classes

// require_once('class.MVCController.php');
Framework::useClass('MVCController');


/**
* Class
*/

class ApplicationMVCController extends MVCController
{
	protected $defaultModuleName = 'Default';
	protected $defaultActionName = 'Default';
	

	/**
	* Returns path to module base class file
	*
	* May be overridden in derived classes to change default location.
	*
	* @param string $name
	*
	* @return string
	*/

	protected function makeModuleBaseFilePath($name)
	{
		$application = Application::getObject();
		$c = $application->getConfig();
		
		
		// Load module application base class
		
		$application_id = $c->get('application/id');
		

		$dir_path = $application->getModuleBaseDirPath($name);
		$file_name = 'am.' . $application_id . '.' . $name . '.php';
		
		return $dir_path . $file_name;
	}	

	
	/**
	* Returns path to module class file
	*
	* May be overridden in derived classes to change default location.
	*
	* @param string $name
	*
	* @return string
	*/

	protected function makeModuleFilePath($name)
	{
		return $this->moduleDir . 'am.' . $name . '.php';
	}	
	
	/**
	* Returns module class normalized name
	*
	* May be overridden in derived classes to change default normalization.
	*
	* @param string $name
	*
	* @return string
	*/

	protected function makeModuleClassName($name)
	{
		return 'AM' . $name;
	}	


	protected function _loadModule($name)
	{
		$application = Application::getObject();
		$c = $application->getConfig();
		
		$application_id = $c->get('application/id');

		
		// Load module application base class
		
		/*
		$file_name = 'am.' . $application_id . '.' . $name . '.php';
		$dir_path = $application->getModuleBaseDirPath($name);
		
		$module_path = $dir_path . $file_name;
		*/	
		$module_path = $this->makeModuleBaseFilePath($name);

		if (! is_readable($module_path)) throw new MVCControllerException("Could not read application module '$name' base at '$module_path'");


		include_once($module_path);

		
		// Make module final class or load custom one if any
		
		$default_class_name = $this->makeModuleClassName($name);

		$base_class_name = $default_class_name . ucfirst(strtolower($application_id)) . 'Base';
		
		
		$module_path = $this->makeModuleFilePath($name);

		if (file_exists($module_path))
		{
			if (! is_readable($module_path)) throw new MVCControllerException("Could not read application module '$name' at '$module_path'");


			// Make intermediate class
			
			$class_code = 'class _' . $default_class_name . ' extends ' . $base_class_name . ' {}';
			eval($class_code);
			

			// Include custom final class
			include_once($module_path);
		}
		else
		{
			// Make final class
			
			$class_code = 'class ' . $default_class_name . ' extends ' . $base_class_name . ' {}';
			eval($class_code);
		}


		// Instantiate module class
		
		if (! class_exists($default_class_name)) throw new MVCControllerException('Application module class "' . $default_class_name . '" not found');

		$module = new $default_class_name($this);
		
		
		return $module;
	}
	
	
	/**
	* Encodes query to be used in URL as param value
	*
	* @param array $query name/value pairs
	*
	* @return string URL-ready encoded value
	*/
	
	static public function encodeQuery($query)
	{
		$r = '';
		

		if (is_array($query))
		{
			if (! empty($query))
			{
				$s = cgi_array_to_query($query, '&');
			
				$r = rawurlencode(base64_encode(gzdeflate($s, 9)));
			}
		}
		else
		{
			// not implemented
			// $r = $query;
			not_implemented();
		}
		
		
		return $r;
	}
	
	/**
	* Decodes query param encoded by encodeQuery()
	*
	* @param string $value encoded value
	*/
	
	static public function decodeQuery($value)
	{
		return gzinflate(base64_decode($value));
	}
	
	
	/**
	* Encodes query to be used in URL as param
	*
	* @param array $query name/value pairs
	* @param string $param_name URL query param name to use; default is '_r'
	*
	* @return string URL-ready name/value pair with encoded value
	*/
	
	static public function compactQuery($query, $param_name = '_r')
	{
		/*
		$r = '';
		
		
		if (is_array($query))
		{
			if (! empty($query))
			{
				$s = cgi_array_to_query($query, '&');
			
				$r = $param_name . '=' . rawurlencode(base64_encode(gzdeflate($s, 9)));
			}
		}
		else
		{
			// not implemented
			$r = $query;
		}
		
		
		return $r;
		*/
		$r = '';
		
		
		if (is_array($query) && ! empty($query))
		{
			$r = $param_name . '=' . self::encodeQuery($query);
		}
		else
		{
			// not implemented
			$r = $query;
		}
		
		
		return $r;
	}	
	
	/**
	* Decodes query encoded in specified param if any and injects it into CGI values
	*
	* NOTE: Original param is removed from CGI values.
	*
	* @param string $param_name URL query param name to use; default is '_r'
	*
	* @return bool returns false if no specified param exists
	*/
	
	static public function extractQuery($param_name = '_r')
	{
		$r = false;
		
		
		// Decode and reinject query parameters if any encoded
		
		$query = get_cgi_value($param_name);
		
		if (! is_null($query))
		{
			clear_cgi_value($param_name);
			
			
			// $s = gzinflate(base64_decode($_r));
			$s = self::decodeQuery($query);
			
			// dump($s);
			
			parse_str($s, $vars);
			
			// dump($vars);
			
			foreach ($vars as $name => $value)
			{
				set_cgi_value($name, $value);
			}

			
			$r = true;
		}
		
		
		return $r;
	}
	
	
	/**
	* Executes controller after request decoding 
	*
	* Calls parent method after decoding request string if applicable
	*/

	public function run()
	{
		// $this->extractQuery();
		self::extractQuery();
		
		parent::run();
	}
}
?>
