<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @version 1.0
* @package modules
*/


/**
* Class exception
*/

class ApplicationModuleACLException extends Exception
{
}


/**
* Application module ACL manipulation
*/

class ApplicationModuleACL
{
	const ENCRYPT_KEY         = 'fXEH3knjiKT4';
	const ENCRYPT_ALGORITHM   = MCRYPT_RIJNDAEL_256;
	const ENCRYPT_MODE        = MCRYPT_MODE_CBC;
	const ENCRYPT_HASH_EXTRA  = 'XB.CMS';

	private $module = NULL; // ApplicationModule
	private $application = NULL; // Application
	private $dataModel = NULL; // ApplicationDataModel
	private $db = NULL; // ORMDatabase
	
	private $amGroupEntity = NULL; // ApplicationEntity
	private $amUserEntity = NULL; // ApplicationEntity
	private $amGroupToUserLink = NULL; // ApplicationLink
	private $amPermissionsEntity = NULL; // ApplicationEntity
	
	private $permissionCache = array(); // for all protected modules
	
	
	public function __construct($module, $amGroupEntity, $amUserEntity, $amGroupToUserLink, $amPermissionsEntity)
	{
		$this->module = $module;
		$this->application = $this->module->application();
		$this->dataModel = $this->module->dataModel();
		$this->db = $this->dataModel->DMDataModel()->ORMDatabase();
		
		$this->amGroupEntity = $amGroupEntity;
		$this->amUserEntity = $amUserEntity;
		$this->amGroupToUserLink = $amGroupToUserLink;
		$this->amPermissionsEntity = $amPermissionsEntity;
	}
	
	
	private function getModuleList()
	{
		return $this->module->aclGetModuleList();
	}
	
	private function getModulePermissionList($name)
	{
		return $this->module->aclGetModulePermissionList($name);
	}


	private function getGroupLinkFieldName()
	{
		$from_field_pairs = $this->amGroupToUserLink->DMLink()->fromFieldPairs();
		$ormFromField = $from_field_pairs[0][1];

		return $ormFromField->getName();
	}
	
	private function getUserLinkFieldName()
	{
		$to_field_pairs = $this->amGroupToUserLink->DMLink()->toFieldPairs();
		$ormToField = $to_field_pairs[0][1];

		return $ormToField->getName();
	}
	

	public function resetPermissionCache()
	{
		$this->permissionCache = array();
	}


	private function setItemLinks($amEntity, $amLinkedEntity, $item_id, $linked_ids)
	{
		$item_pk = SQL::pk($item_id);

		if (! is_array($linked_ids))
		{
			$linked_ids = ! empty($linked_ids) ? explode(',', $linked_ids) : '';
		}


		// Update links
		
		// Drop existing links
		$amEntity->dropAllItemLinks($amLinkedEntity->getName(), $item_pk);

		// Set new links if any
		if (! empty($linked_ids))
		{
			foreach ($linked_ids as $linked_id)
			{
				$amEntity->createItemLink($amLinkedEntity->getName(), $item_pk, SQL::pk($linked_id));
			}
		}
	}
	
	public function setGroupUsers($group_id, $user_ids)
	{
		$this->setItemLinks($this->amGroupEntity, $this->amUserEntity, $group_id, $user_ids);
	}

	public function setUserGroups($user_id, $group_ids)
	{
		$this->setItemLinks($this->amUserEntity, $this->amGroupEntity, $user_id, $group_ids);
	}

	
	static public function hashPassword($pwd, $salt)
	{
		$pwd = strval($pwd) . self::ENCRYPT_HASH_EXTRA;
		$salt = strval($salt) . self::ENCRYPT_HASH_EXTRA;
		// dump("pwd: $pwd; salt: $salt");

		$pwd_md5 = md5($pwd);		
		$salt_sha1 = sha1($salt);		
		// dump("pwd_md5: $pwd_md5; salt_sha1: $salt_sha1");

		$xored = $pwd_md5 ^ $salt_sha1;
		// dump("xored: $xored");
		
		$hexed = bin2hex($xored);
		// dump("hexed: $hexed");
		
		$sha1 = sha1($hexed);
		// dump("hexed sha1: $sha1");
		
		$md5 = md5($hexed);
		// dump("hexed md5: $md5");

		$hash =  $md5 . $sha1;
		// dump("hash: $hash");
		
		return $hash;
	}
	
	static private function encryptPermissions($array)
	{
		$key = self::ENCRYPT_KEY;
		$key = str_pad($key, 16, "\0"); // key must be of supported length, e.g. 16
		$alg = self::ENCRYPT_ALGORITHM;
		$mode = self::ENCRYPT_MODE;

		// Make init vector, now empty
		$iv_size = mcrypt_get_iv_size($alg, $mode);
		$iv = str_repeat("\0", $iv_size);
	
		// Encrypt
		$string = serialize($array);
		$string = gzdeflate($string, 9);
		$string = bin2hex($string);
		
		return bin2hex(mcrypt_encrypt($alg, $key, $string, $mode, $iv));
	}

	static private function decryptPermissions($string)
	{
		$key = self::ENCRYPT_KEY;
		$key = str_pad($key, 16, "\0"); // key must be of supported length, e.g. 16
		$alg = self::ENCRYPT_ALGORITHM;
		$mode = self::ENCRYPT_MODE;
		
		// Make init vector, now empty
		$iv_size = mcrypt_get_iv_size($alg, $mode);
		$iv = str_repeat("\0", $iv_size);
		
		// Decrypt
		$string = pack("H*", $string);
		$string = trim(mcrypt_decrypt($alg, $key, $string, $mode, $iv));
		$string = pack("H*", $string);
		$string = gzinflate($string);

		return unserialize($string);
	}


	public function getUserGroupIds($user_id)
	{
		$dm = $this->dataModel;
		$db = $this->db;
		$amGroupEntity = $this->amGroupEntity;
		$amGroupToUserLink = $this->amGroupToUserLink;

		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();
		
		
		// Get table names
		
		$_group_table_name = $amGroupEntity->DMEntity()->ORMTable()->getPreparedName();
		$_link_table_name = $amGroupToUserLink->DMLink()->ORMTable()->getPreparedName();
		

		$ids = $db->getRows
		(
			'g.id',
			"{$_group_table_name} g INNER JOIN {$_link_table_name} gtu ON g.id = gtu.{$group_link_field_name}",
			array("gtu.{$user_link_field_name}" => $user_id),
			'',
			'',
			NULL,
			'',
			'id'
		);
		

		return $ids;
	}


	public function getModulePermissionValues($module_id, $user_id)
	{
		$permissions = array();


		$amGroupToUserLink = $this->amGroupToUserLink;
		$amPermissionsEntity = $this->amPermissionsEntity;

		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();

		
		if
		(
			array_key_exists($module_id, $this->permissionCache)
			&& is_array($this->permissionCache[$module_id])
			&& array_key_exists($user_id, $this->permissionCache[$module_id])
		)
		{
			$permissions = $this->permissionCache[$module_id][$user_id];
		}
		else
		{
			/*
			// Get permissions ONLY FOR SINGLE GROUP
			
			$row = $amGroupToUserLink->DMLink()->ORMTable()->getRowData($group_link_field_name, array($user_link_field_name => $user_id));
	
			if (! empty($row))
			{
				$group_id = $row[$group_link_field_name];
				
				$row = $amPermissionsEntity->getItemData('permissions', array($group_link_field_name => $group_id, 'module_id' => $module_id));
				
				if (! empty($row))
				{
					$permissions_raw = $row['permissions'];
					$permissions = ! empty($permissions_raw) ? self::decryptPermissions($permissions_raw) : array();
				}
			}
			*/
			// Get permission values for every group the user in.
			// Merge all permission values, so as value is 1 (on) if at least one group contains 1 for that permission.
			// Groups are unordered (simplified system), permission values are 0 nad not 0 (usually 1).
			
			$link_rows = $amGroupToUserLink->DMLink()->ORMTable()->getRowDataList($group_link_field_name, array($user_link_field_name => $user_id));
	
			if (! empty($link_rows))
			{
				foreach ($link_rows as $link_row)
				{
					$group_id = $link_row[$group_link_field_name];
					
					$pv_row = $amPermissionsEntity->getItemData('permissions', array($group_link_field_name => $group_id, 'module_id' => $module_id));
					
					if (! empty($pv_row))
					{
						$permissions_raw = $pv_row['permissions'];
						$_permissions = ! empty($permissions_raw) ? self::decryptPermissions($permissions_raw) : array();

						// Merge if any
						if (! empty($_permissions))
						{
							foreach ($_permissions as $object_id => $values)
							{
								foreach ($values as $name => $value)
								{
									if
									(
										! isset($permissions[$object_id]) // module/group has no object
										|| ! isset($permissions[$object_id][$name]) // object has no permission
										|| ! $permissions[$object_id][$name] // permission is not true
									)
									{
										$permissions[$object_id][$name] = $value;
									}
								}
							}
						}
					}
				}
			}
			
			
			$this->permissionCache[$module_id][$user_id] = $permissions;
		}
		

		return $permissions;
	}


	private function getPermissionValuesFromCGI()
	{
		$group_id = get_cgi_value('id', 'i');
		$module_ids = get_cgi_value('module_ids', 'ai', array());

		
		// Collect permission values and prepare for insertion
		
		// $module_list = $this->application->invokeModuleMethod('Modules', 'getModuleList');
		$module_list = $this->getModuleList();
		
		
		$permissions = array();
		foreach ($module_list as $module_item) // per module
		{
			if (! in_array($module_item['id'], $module_ids)) continue;
				
				
			// $module_permission_list = $this->application->invokeModuleMethod($module_item['name'], $this->modulePermissionMethodName);
			$module_permission_list = $this->getModulePermissionList($module_item['name']);

			foreach ($module_permission_list as $p_object_id => $p_object) // per permission subject
			{
				foreach (array_keys($p_object['permissions']) as $p_name) // per permission
				{
					$cgi_name = '_p_' . $module_item['name'] . '_' . $p_object_id . '_' . $p_name;
					$cgi_value = get_cgi_value($cgi_name);
					
					if (! is_null($cgi_value))
					{
						$permissions[$module_item['id']][$p_object_id][$p_name] = $cgi_value;
					}
				}
			}
		}
		
		
		return array($group_id, $permissions);
	}
	
	private function updateGroupModulePermissions($group_id, $module_id, $permission_data)
	{
		$dm = $this->dataModel;
		$amPermissionsEntity = $this->amPermissionsEntity;
		
		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();


		$permission_data = ! empty($permission_data) ? self::encryptPermissions($permission_data) : '';
		
		$values = array($group_id, $module_id, $permission_data);
			

		$in_transaction = $dm->inTransaction();

		if (! $in_transaction) $dm->beginTransaction();
		
		try
		{
			if (! empty($group_id))
			{
				// Replace permissions
		
				$amPermissionsEntity->deleteItemData(array($group_link_field_name => $group_id, 'module_id' => $module_id));
		
				
				if (! empty($values))
				{
					$amPermissionsEntity->DMEntity()->ORMTable()->insert($values, array($group_link_field_name, 'module_id', 'permissions'));
				}
			}

	
			if (! $in_transaction) $dm->commit();
		}
		catch (Exception $e)
		{
			if (! $in_transaction) $dm->rollBack();
			

			throw $e;
		}
	}
	
	private function updateGroupPermissions($group_id, $permissions)
	{
		$dm = $this->dataModel;


		$in_transaction = $dm->inTransaction();

		if (! $in_transaction) $dm->beginTransaction();
		
		try
		{
			foreach ($permissions as $module_id => $permission_data)
			{
				$this->updateGroupModulePermissions($group_id, $module_id, $permission_data);
			}
			
			
			if (! $in_transaction) $dm->commit();
		}
		catch (Exception $e)
		{
			if (! $in_transaction) $dm->rollBack();
			
			
			throw $e;
		}
	}
	
	public function saveGroupPermissionsFromCGI()
	{
		list($group_id, $permissions) = $this->getPermissionValuesFromCGI();
		
		$this->updateGroupPermissions($group_id, $permissions);
	}
	
	
	public function getGroupAvailableUserList($group_id)
	{
		$result = array();
	

		$dm = $this->dataModel;
		$db = $this->db;
		$amUserEntity = $this->amUserEntity;
		$amGroupToUserLink = $this->amGroupToUserLink;
		
		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();

		$_user_table_name = $amUserEntity->DMEntity()->ORMTable()->getPreparedName();
		$_link_table_name = $amGroupToUserLink->DMLink()->ORMTable()->getPreparedName();


		$rows = $db->getRows
		(
			'u.id, u.login',
			"{$_user_table_name} u LEFT JOIN (SELECT * FROM {$_link_table_name} WHERE {$group_link_field_name} = {$group_id}) gtu ON u.id = gtu.{$user_link_field_name}",
			"gtu.{$user_link_field_name} IS NULL"
		);
			
		$result = array('list' => $rows);
		

		return $result;
	}

	public function getUserAvailableGroupList($user_id)
	{
		$result = array();
	

		$dm = $this->dataModel;
		$db = $this->db;
		$amGroupEntity = $this->amGroupEntity;
		$amGroupToUserLink = $this->amGroupToUserLink;
		
		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();

		$_group_table_name = $amGroupEntity->DMEntity()->ORMTable()->getPreparedName();
		$_link_table_name = $amGroupToUserLink->DMLink()->ORMTable()->getPreparedName();


		$rows = $db->getRows
		(
			'g.id, g.name',
			"{$_group_table_name} g LEFT JOIN
			(
				SELECT * FROM {$_link_table_name} WHERE {$user_link_field_name} = {$user_id}
			) gtu ON g.id = gtu.{$group_link_field_name}",
			"gtu.{$group_link_field_name} IS NULL"
		);
			
		$result = array('list' => $rows);


		return $result;
	}

	public function getGroupSelectedUserList($group_id)
	{
		$result = array();
	

		$dm = $this->dataModel;
		$db = $this->db;
		$amUserEntity = $this->amUserEntity;
		$amGroupToUserLink = $this->amGroupToUserLink;
		
		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();

		$_user_table_name = $amUserEntity->DMEntity()->ORMTable()->getPreparedName();
		$_link_table_name = $amGroupToUserLink->DMLink()->ORMTable()->getPreparedName();

		
		$rows = $db->getRows
		(
			'u.id, u.login',
			"{$_user_table_name} u INNER JOIN {$_link_table_name} gtu ON u.id = gtu.{$user_link_field_name}",
			array("gtu.{$group_link_field_name}" => $group_id)
		);
			
		$result = array('list' => $rows);
		

		return $result;
	}
	
	public function getUserSelectedGroupList($user_id)
	{
		$result = array();

		
		$dm = $this->dataModel;
		$db = $this->db;
		$amGroupEntity = $this->amGroupEntity;
		$amGroupToUserLink = $this->amGroupToUserLink;
		
		$group_link_field_name = $this->getGroupLinkFieldName();
		$user_link_field_name = $this->getUserLinkFieldName();
		
		$_group_table_name = $amGroupEntity->DMEntity()->ORMTable()->getPreparedName();
		$_link_table_name = $amGroupToUserLink->DMLink()->ORMTable()->getPreparedName();
		

		$rows = $db->getRows
		(
			'g.id, g.name',
			"{$_group_table_name} g INNER JOIN {$_link_table_name} gtu ON g.id = gtu.{$group_link_field_name}",
			array("gtu.{$user_link_field_name}" => $user_id)
		);
			
		$result = array('list' => $rows);
		

		return $result;
	}


	public function getGroupPermissionValuesList($group_id)
	{
		$result = array();
		
		
		$amPermissionsEntity = $this->amPermissionsEntity;
		$group_link_field_name = $this->getGroupLinkFieldName();
		
		
		// $module_list = $this->application->invokeModuleMethod('Modules', 'getModuleList');
		$module_list = $this->getModuleList();

		
		$permission_list = array();
		
		foreach ($module_list as $module_item)
		{
			//$module_permission_list = $this->application->invokeModuleMethod($module_item['name'], $this->modulePermissionMethodName);
			$module_permission_list = $this->getModulePermissionList($module_item['name']);
			
			if (! empty($group_id))
			{
				// Collect permission values for existing group
				
				
				$row = $amPermissionsEntity->getItemData('permissions', array($group_link_field_name => $group_id, 'module_id' => $module_item['id']));
				
				if (! empty($row))
				{
					$permissions_raw = $row['permissions'];
					$permissions = ! empty($permissions_raw) ? self::decryptPermissions($permissions_raw) : array();
					// wdebug($permissions);
					foreach ($module_permission_list as $p_object_id => $p_object)
					{
						if (isset($permissions[$p_object_id]))
						{
							$_p_object = $permissions[$p_object_id];
							
							foreach (array_keys($p_object['permissions']) as $p_name)
							{
								if (isset($_p_object[$p_name]))
								{
									$module_permission_list[$p_object_id]['permissions'][$p_name]['value'] = $permissions[$p_object_id][$p_name];
								}
							}
						}
					}
				}
			}

			$permission_list[$module_item['id']] = array('name' => $module_item['name'], 'permission_list' => $module_permission_list);
		}				

		$result = array('list' => $permission_list);

			
		return $result;
	}


	public function getGroupModulePermissionValuesList($group_id, $module_id)
	{
		$result = array();
		
		
		$amPermissionsEntity = $this->amPermissionsEntity;
		$group_link_field_name = $this->getGroupLinkFieldName();
		
		
		$module_item = $this->application->invokeModuleMethod('Modules', 'getModuleItem', array($module_id));

		
		// $module_permission_list = $this->application->invokeModuleMethod($module_item['name'], $this->modulePermissionMethodName);
		$module_permission_list = $this->getModulePermissionList($module_item['name']);
		

		// Collect permission values for existing group
		
		// Get permission values if any
		$row = $amPermissionsEntity->getItemData('permissions', array($group_link_field_name => $group_id, 'module_id' => $module_item['id']));
		
		if (! empty($row))
		{
			// Decrypt permission values
			
			$permission_values_raw = $row['permissions'];
			$permission_values = ! empty($permission_values_raw) ? self::decryptPermissions($permission_values_raw) : array();

			// Apply permission values to permission objects
			foreach ($module_permission_list as $p_object_id => $p_object) // cycle objects
			{
				if (isset($permission_values[$p_object_id])) // if there are any values for the object
				{
					$_p_object = $permission_values[$p_object_id];
					
					foreach (array_keys($p_object['permissions']) as $p_name) // cycle object permissions
					{
						
						if (isset($_p_object[$p_name]))
						{
							$module_permission_list[$p_object_id]['permissions'][$p_name]['value'] = $permission_values[$p_object_id][$p_name];
						}
					}
				}
			}
		}


		$result = array('list' => $module_permission_list);

			
		return $result;
	}
	
	
	public function getAllPermissionList()
	{
		$result = array();


		// $module_list = $this->application->invokeModuleMethod('Modules', 'getModuleDataListAll', array(NULL, '', 'name'));
		$module_list = $this->getModuleList();
		
		$permission_list = array();
		foreach ($module_list as $module_item)
		{
			// $module_permission_list = $this->application->invokeModuleMethod($module_item['name'], $this->modulePermissionMethodName);
			$module_permission_list = $this->getModulePermissionList($module_item['name']);

			$permission_list[$module_item['id']] = array('name' => $module_item['name'], 'title' => $module_item['title'], 'permission_list' => $module_permission_list);
		}				

		$result = array('list' => $permission_list);

		
		return $result;
	}
}
?>
