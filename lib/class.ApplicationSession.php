<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationSession
{
	private static $instance;
	
	private $name;
	private $dbConnection = NULL;
	private $tableName = 'session';
	
	private $session = NULL;
	
	
	/**
	* Returns ApplicationSession object
	*
	* @return ApplicationSession object
	*/

	public static function getObject()
	{
		$object = NULL;
		
		if (! is_null(self::$instance))
		{
			$object = self::$instance;
		}
		
		return $object;
	}

	/**
	* Prevents clonning
	*
	* @access private
	*/

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	/**
	*
	*
	* @param string $name
	* @param DBConnection $db_connection
	* @param string|NULL $table_name
	*/

	public function __construct($name, $db_connection, $table_name = NULL)
	{
		if (! is_null(self::$instance)) throw new Exception(__CLASS__ . ' singleton already created');


		if (is_null($db_connection)) throw new Exception("Connection not provided");

		$this->name = $name;
		$this->dbConnection = $db_connection;
		if (! is_null($table_name)) $this->tableName = $table_name;


		self::$instance = $this;
	}
	
	/**
	*
	*
	* @return object
	*/

	public function getSession()
	{
		if (is_null($this->session))
		{
			include_once('class.SessionDB.php');

			
			SessionDB::setName($this->name);
			SessionDB::setTableName($this->tableName);
			$this->session = SessionDB::start($this->dbConnection);
		}
		
		
		return $this->session;
	}
}

?>
