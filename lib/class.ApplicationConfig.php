<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Application config class (singleton)
*
* @package platform
*/

/**
* Class exception
*/

class ApplicationConfigException extends Exception
{
}

/**
* Application configuration class
*/

class ApplicationConfig
{
	private static $instance;

	private $config;
	

	/**
	* Returns ApplicationConfig object
	*
	* @return ApplicationConfig
	*/

	public static function getObject()
	{
		$object = NULL;
		
		if (! is_null(self::$instance))
		{
			$object = self::$instance;
		}
		
		return $object;
	}

	/**
	* Prevents clonning
	*
	* @access private
	*/

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	/**
	* Constructor
	*
	* Searches for config file with '.config' at the beginning of the name and (from high to low priority):
	* - server name at the end
	* - server IPv4 at the end
	* - nothing at the end, default '.config'
	*
	* First file found is parsed and saved in the object.
	*
	* @param string $config_file_dir directory to search config file in
	*/

	public function __construct($config_file_dir)
 	{
		if (! is_null(self::$instance)) throw new ApplicationConfigException(__CLASS__ . ' singleton already created');


		$dir = dirpath(realpath($config_file_dir));
		
		if (! is_readable($dir)) throw new ApplicationConfigException("'{$dir}' is not readable");
		
		
		// Select config file per server name/IP
		
		$path_base = $dir . '.config';
		
		$path = isset($_SERVER['SERVER_NAME']) ? ($path_base . '.' . $_SERVER['SERVER_NAME']) : NULL; // try per-server file if run under server
		
		if (is_file($path))
		{
			if (! is_readable($path)) throw new ApplicationConfigException("'Config file {$path}' is not readable");			
		}
		else
		{
			$path = isset($_SERVER['SERVER_ADDR']) ? ($path_base . '.' . $_SERVER['SERVER_ADDR']) : NULL; // try per-server-IP file if run under server
			
			if (is_file($path))
			{
				if (! is_readable($path)) throw new ApplicationConfigException("'Config file {$path}' is not readable");			
			}
			else
			{
				$path = $path_base; // use default file
				
				if (! is_file($path)) throw new ApplicationConfigException("'Config file '{$path}' not found");
	
				if (! is_readable($path)) throw new ApplicationConfigException("'Config file '{$path}' is not readable");			
			}
		}


		// Parse config file
		
		include_once('class.IniConfig.php');


		$this->config = new IniConfig($path);
		
		
		self::$instance = $this;
	}
	
	/**
	* Clears static singleton object
	*/

	public function destroy()
	{
		// unset(self::$instance);
		
		self::$instance = NULL;
	}

	/**
	* Returns path of the config file which is actually used
	*
	* @return string
	*/

	public function getActiveConfigFilePath()
	{
		return $this->config->getConfigFilePath();
	}

	
	/**
	* Returns value or section by path from config or the whole config as array
	*
	* Path to value is '<section name>/<value name>'.
	* Path to section is '<section name>'.
	*
	* @param string|NULL $path path to value or section or NULL to get the whole config 
	* @param bool $throw if true, an exception is thrown if nothing found
	*
	* @return mixed config value or section, the whole config as array, NULL if nothing found
	*/

	public function get($path = NULL, $throw = true)
	{
		return $this->config->get($path, $throw);
	}
	
	/**
	* Sets value or section by path
	*
	* Path to value is '<section name>/<value name>'.
	* Path to section is '<section name>'.
	*
	* @param string $path path to value or section
	* @param mixed $value value or array of values for section
	*/

	public function set($path, $value)
	{
		$this->config->set($path, $value);
	}
}

?>
