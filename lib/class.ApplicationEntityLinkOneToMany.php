<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationEntityLink.php');


require_once('class.ApplicationEntityLinkOneToManyBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.ApplicationEntityLinkOneToMany.php_5_3.php');
}
else
{
	include('class.ApplicationEntityLinkOneToMany.php_5_2.php');
}

?>
