<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

abstract class ApplicationEntity extends ApplicationEntityBase
{
	static public function createFromConfig(ApplicationDataModel $dm, $config, $__class__ = __CLASS__)
	{
		$amEntity = new $__class__($dm, $config['name'], $config['singular_title'], $config['plural_title'], array(), $config);
		

		if (isset($config['fields']))
		{
			foreach ($config['fields'] as $field_config)
			{
				$amEntity->createField($field_config);
			}
		}
		
		
		return $amEntity;
	}
}
?>
