<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityPlainBase extends ApplicationEntity
{
	public function __construct(ApplicationDataModel $dm, $name, $singularTitle, $pluralTitle, $amFields = array(), $options = array())
	{
		parent::__construct($dm, $name, $singularTitle, $pluralTitle, $amFields, $options);
		

		$this->dmEntity = new DMPlainEntity($dm->DMDataModel(), $name);
		
		
		$this->registerPrimaryFields();
	}
}

?>
