<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityLinkOneToManyBase extends ApplicationEntityLink
{
	public function __construct(ApplicationDataModel $dm, $name, ApplicationEntity $from_entity, $from_required, ApplicationEntity $to_entity, $to_required, $custom_field_map = NULL)
	{
		parent::__construct($dm, $name, $from_entity, $from_required, $to_entity, $to_required, $custom_field_map);
		
		
		$this->type = '1:n';


		$this->dmLink = new DMOneToManyLink($dm->DMDataModel(), $name, $from_entity->DMEntity(), $from_required, $to_entity->DMEntity(), $to_required, $custom_field_map);
		
		
		// $to_entity->registerLinkToFields($from_entity, $from_required, $this->dmLink->fieldPairs());
	}
	

	/**
	*
	*
	* @param array $parent_pk_values
	* @param string|array $fields
	* @param string|array|NULL $condition
	* @param string $group
	* @param string $order
	* @param array|int|NULL $limits
	* @param string $row_key_field
	* @param string $row_value_field
	*
	* @return array
	*/

	public function getChildItemDataList($parent_pk_values, $fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getChildItemDataList($parent_pk_values, $fields, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	
	/**
	*
	*
	* @param array $parent_pk_values
	* @param string|array|NULL $condition
	* @param string $group
	* @param string $order
	* @param array|int|NULL $limits
	* @param string $row_key_field
	* @param string $row_value_field
	*
	* @return array
	*/

	public function getChildItemDataListAll($parent_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getChildItemDataListAll($parent_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
}

?>
