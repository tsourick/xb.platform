<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationEntityLink.php');


require_once('class.ApplicationEntityLinkManyToManyBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.ApplicationEntityLinkManyToMany.php_5_3.php');
}
else
{
	include('class.ApplicationEntityLinkManyToMany.php_5_2.php');
}

?>
