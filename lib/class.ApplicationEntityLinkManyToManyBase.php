<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationEntityLinkManyToManyBase extends ApplicationEntityLink
{
	public function __construct(ApplicationDataModel $dm, $name, ApplicationEntity $from_entity, $from_required, ApplicationEntity $to_entity, $to_required, $custom_field_map = NULL)
	{
		parent::__construct($dm, $name, $from_entity, $from_required, $to_entity, $to_required, $custom_field_map);
		
		
		$this->type = 'n:n';


		$this->dmLink = new DMManyToManyLink($dm->DMDataModel(), $name, $from_entity->DMEntity(), $from_required, $to_entity->DMEntity(), $to_required, $custom_field_map);
	}
	

	public function getPreparedName()
	{
		return $this->dmLink->getPreparedName();
	}
	

	public function getLeftItemDataListAll($right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getLeftItemDataListAll($right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param array $left_pk_values
	* @param string|array|NULL $condition
	* @param string $group
	* @param string $order
	* @param array|int|NULL $limits
	* @param string $row_key_field
	* @param string $row_value_field
	*
	* @return array
	*/

	public function getRightItemDataListAll($left_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getRightItemDataListAll($left_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* @see DMManyToManyLinkBase::getLeftItemDataListRaw()
	*/
	public function getLeftItemDataListRaw($fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getLeftItemDataListRaw($fields, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* @see DMManyToManyLinkBase::getRightItemDataListRaw()
	*/
	public function getRightItemDataListRaw($fields, $left_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getRightItemDataListRaw($fields, $left_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	
	/**
	* @see DMManyToManyLinkBase::getLeftUnlinkedItemDataListRaw()
	*/
	public function getLeftUnlinkedItemDataListRaw($fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getLeftUnlinkedItemDataListRaw($fields, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* @see DMManyToManyLinkBase::getRightUnlinkedItemDataListRaw()
	*/
	public function getRightUnlinkedItemDataListRaw($fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dmLink->getRightUnlinkedItemDataListRaw($fields, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	
	public function dropAllFromItemLinks($to_pk)
	{
		$this->dmLink->dropAllFromItemLinks($to_pk);
	}

	public function dropAllToItemLinks($from_pk)
	{
		$this->dmLink->dropAllToItemLinks($from_pk);
	}

	public function createItemLink($from_pk, $to_pk)
	{
		$this->dmLink->createItemLink($from_pk, $to_pk);
	}	
	
	/**
	* Create several links at once
	*
	* @param mixed $from_pk_values array of PKs or single PK
	* @param mixed $to_pk_values array of PKs or single PK
	*/
	
	public function createItemLinks($from_pk_values, $to_pk_values)
	{
		$this->dmLink->createItemLinks($from_pk_values, $to_pk_values);
	}
}

?>
