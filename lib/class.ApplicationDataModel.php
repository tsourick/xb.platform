<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

// require_once('class.DM.php');
Framework::useClass('DM');


class ApplicationDataModelException extends ApplicationException
{
}


// require_once('class.ApplicationEntityPlain.php');
// require_once('class.ApplicationEntityPlainTimestamped.php');
// require_once('class.ApplicationEntityCelko.php');
// require_once('class.ApplicationEntityLinkOneToOneNatural.php');
// require_once('class.ApplicationEntityLinkOneToMany.php');
// require_once('class.ApplicationEntityLinkManyToMany.php');
Platform::useClass('ApplicationEntityPlain');
Platform::useClass('ApplicationEntityPlainTimestamped');
Platform::useClass('ApplicationEntityCelko');
Platform::useClass('ApplicationEntityLinkOneToOneNatural');
Platform::useClass('ApplicationEntityLinkOneToMany');
Platform::useClass('ApplicationEntityLinkManyToMany');


class ApplicationDataModel
{
	// private $application = NULL;
	
	private $dmDataModel = NULL;
	
	private $amEntities = array(); // Datamodel entities
	private $amLinks = array(); // Datamodel links
	
	static public $fieldTypeConfigs = array
	(
		'pri_id'             => array('dmType' => 'pri_id',           'input_type' => 'i',                'input_default' => NULL  ),
		                                                                                                                           
		'ext_id'             => array('dmType' => 'ext_id',           'input_type' => 'i',                'input_default' => NULL  ),
		'strong_ext_id'      => array('dmType' => 'strong_ext_id',    'input_type' => 'i',                'input_default' => NULL  ),
		                                                                                                                           
		'string'             => array('dmType' => 'string',           'input_type' => 's',                'input_default' => NULL  ),
		'text'               => array('dmType' => 'text',             'input_type' => 's',                'input_default' => NULL  ),
		'html'               => array('dmType' => 'text',             'input_type' => 's',                'input_default' => NULL  ),
                                                                                                      
		'flag'               => array('dmType' => 'flag',             'input_type' => 'b',                'input_default' => 0     ),
		'flag_off'           => array('dmType' => 'flag_off',         'input_type' => 'b',                'input_default' => 0     ),
		'flag_on'            => array('dmType' => 'flag_on',          'input_type' => 'b',                'input_default' => 1     ),

		'date'               => array('dmType' => 'date',             'input_type' => 'date_string',      'input_default' => NULL  ),
		'date_strong'        => array('dmType' => 'date_strong',      'input_type' => 'date_string',      'input_default' => NULL  ),
		'time'               => array('dmType' => 'time',             'input_type' => 's', 'input_default' => NULL),
		'datetime'           => array('dmType' => 'datetime', 'input_type' => 'datetime_string', 'input_default' => NULL),
		'datetime_strong'    => array('dmType' => 'datetime_strong',  'input_type' => 'datetime_string',  'input_default' => NULL  ),
		                                                                                                                           
		'created'            => array('dmType' => 'created',          'input_type' => NULL,               'input_default' => NULL  ),
		'modified'           => array('dmType' => 'modified',         'input_type' => NULL,               'input_default' => NULL  ),
                                                                                                                               
		'massstoragefile'    => array('dmType' => 'ext_id',           'input_type' => 'file',             'input_default' => NULL  ),
		'money'              => array('dmType' => 'money',            'input_type' => 'f',                'input_default' => '0.00'),
                                                                                                      
		'signed'             => array('dmType' => 'signed',           'input_type' => 'i',                'input_default' => '0'   ),
		'unsigned'           => array('dmType' => 'unsigned',         'input_type' => 'i',                'input_default' => '0'   ),
                                                                                                                               
		'float_signed'       => array('dmType' => 'double_signed',    'input_type' => 'f',                'input_default' => '0'   ),
		'float_unsigned'     => array('dmType' => 'double_unsigned',  'input_type' => 'f',                'input_default' => '0'   ),
                                                                                                                               
		'double_signed'      => array('dmType' => 'double_signed',    'input_type' => 'f',                'input_default' => '0'   ),
		'double_unsigned'    => array('dmType' => 'double_unsigned',  'input_type' => 'f',                'input_default' => '0'   )
	);
	
	
	/**
	*
	*
	* @param DBConnection $dbc
	* @param string $model_name
	* @param array $model_options
	*/

	public function __construct(DBConnection $dbc, $model_name, $model_options = array())
	{
		/*
		$c = $application->getConfig();
		
		$connection_name = $application->getDefaultDBConnectionName();
		
		// $table_prefix = $c->get($application->getDefaultDBConnectionName() . '/table_prefix') . 'am_' . $this->applicationModule->getName() . '_';
		$model_name = $c->get($connection_name . '/dbname');
		$table_prefix = $c->get($connection_name . '/table_prefix');

		$model_options = array('table_name_prefix' => $table_prefix);
		

		$dbc = $application->getDefaultDBConnection();
		*/
		
		$this->dmDataModel = new DMDataModel($dbc, $model_name, $model_options);


		// $this->application = $application;
	}

	/**
	*
	*
	* @return object
	*/

	public function DMDataModel()
	{
		return $this->dmDataModel;
	}
	/*

	public function applicationModule()
	{
		return $this->applicationModule;
	}
	*/
	/**
	*
	*
	* @return DBConnection
	*/

	public function getDBConnection()
	{
		return $this->dmDataModel->getDBConnection();
	}
	
	/**
	* $config structure:

	array
	(
		'entities' => array
		(
			array
			(
				'type' => '(plain|celko)',

				'name' => '<entity name>',
				'fields' => array
				(
					array('name' => '<field name>', 'type' => '<field type>')
				)
			),
			
			...
		),
		'links' => array
		(
			array
			(
				'type' => '(1:1 natural|1:n|n:n)',
				'from' => '<entity name>',
				'from_required' => (true|false),
				'to' => '<entity name>',
				'to_required' => (true|false)
			)
		)
	);
	*
	*
	* @param DBConnection $dbc
	* @param array $config
	*
	* @return ApplicationDataModel
	*/
	
	static public function createFromConfig(DBConnection $dbc, $config)
	{
		$model_name = $config['model_name'];
		$model_options = isset($config['model_options']) ? $config['model_options'] : array();
		
		$amDataModel = new self($dbc, $model_name, $model_options);
		
		
		foreach ($config['entities'] as $entity_config)
		{
			$amDataModel->createEntity($entity_config);
		}
		
		
		if (isset($config['links']))
		{
			foreach ($config['links'] as $link_config)
			{
				$amDataModel->createLink($link_config);
			}
		}

		
		return $amDataModel;
	}
	
	/**
	* Config structure:
	* @see DMEntity->__construct()
	*
	* @param array $config
	*
	* @return ApplicationEntity
	*/
	public function createEntity($config)
	{
		switch ($config['type'])
		{
			case 'plain':
				$amEntity = ApplicationEntityPlain::createFromConfig($this, $config);
			break;

			case 'plain_timestamped':
				$amEntity = ApplicationEntityPlainTimestamped::createFromConfig($this, $config);
			break;

			case 'celko':
				$amEntity = ApplicationEntityCelko::createFromConfig($this, $config);
			break;
		}
		
		// $this->dmEntities[$dmEntity->getName()] = $dmEntity;
		
		return $amEntity;
	}
	
	/**
	*
	*
	* @param string $name
	*/

	public function deleteEntity($name)
	{
		if (! isset($this->amEntities[$name])) throw new ApplicationDataModelException("Could not delete entity '" . $name . "' from module '" . $this->applicationModule->getName() . "'. Entity not found in the model.");

		
		$amEntity = $this->amEntities[$name];
		
		// $amEntity = $this->removeEntity($this->amEntities[$name]);
		$amEntity->destruct(); // this->removeEntity() is called from within entity
		
		
		unset($amEntity);
	}
	
	/**
	*
		Config structure
		
		array
		(
		'type' => '(1:n|n:n|1:1 natural)',
			'name' => '<link name>',
			'from' => '<parent entity name>',
			'from_required' => (true|false),
			'to' => '<child entity name>',
			'to_required' => (true|false),
			'custom_field_map' => array
			(
				['from' => array('<from field name>' => '<custom_field_name>'),]
				['to' => array('<from field name>' => '<custom_field_name>')]
			)
			
		)
	*	
	* @param array $config
	*
	* @return ApplicationEntityLink
	*/
	public function createLink($config)
	{
		// wdebug('2');
		// global $aaa;
		// if ($aaa) throw new Exception(wprintr($config, true));
		switch ($config['type'])
		{
			case '1:1 natural':
				$amLink = ApplicationEntityLinkOneToOneNatural::createFromConfig($this, $config);
			break;

			case '1:n':
				$amLink = ApplicationEntityLinkOneToMany::createFromConfig($this, $config);
			break;

			case 'n:n':
				$amLink = ApplicationEntityLinkManyToMany::createFromConfig($this, $config);
			break;
		}
		
		// $this->dmLinks[$dmLink->getName()] = $dmLink;
		
		return $amLink;
	}
	
	/**
	*
	*
	* @param string $name
	*/

	public function deleteLink($name)
	{
		if (! isset($this->amLinks[$name])) throw new ApplicationDataModelException("Could not delete link '" . $name . "' from module '" . $this->applicationModule->getName() . "'. Link not found in the model.");

		
		$amLink = $this->amLinks[$name];
		
		
		$amLink->destruct(); // this->removeLink() is called from within link
		
		
		unset($amLink);
	}

	
	// called from entity
	/**
	*
	*
	* @param ApplicationEntity $amEntity
	*/

	public function addEntity(ApplicationEntity $amEntity)
	{
		if ($amEntity->dataModel() !== $this) throw new ApplicationDataModelException("Could not add entity '" . $amEntity->getName() . "' to module '" . $this->applicationModule->getName() . "' because entity belongs to another model.");
		
		
		$this->amEntities[$amEntity->getName()] = $amEntity;
	}
	
	// called from entity
	/**
	*
	*
	* @param ApplicationEntity $amEntity
	*/

	public function removeEntity(ApplicationEntity $amEntity)
	{
		if ($amEntity->dataModel() !== $this) throw new ApplicationDataModelException("Could not remove entity '" . $amEntity->getName() . "' from module '" . $this->applicationModule->getName() . "' because entity belongs to another model.");
		
		
		$name = $amEntity->getName();
		
		
		if (! isset($this->amEntities[$name])) throw new ApplicationDataModelException("Could not remove entity '" . $amEntity->getName() . "' from module '" . $this->applicationModule->getName() . "'. Entity belongs to this model but was not added.");

			
		unset($this->amEntities[$name]);
	}

	// called from link
	/**
	*
	*
	* @param ApplicationEntityLink $amLink
	*/

	public function addLink(ApplicationEntityLink $amLink)
	{
		if ($amLink->dataModel() !== $this) throw new ApplicationDataModelException("Could not add link '" . $amLink->getName() . "' to module '" . $this->applicationModule->getName() . "' because link belongs to another model.");
		
		
		$this->amLinks[$amLink->getName()] = $amLink;
	}

	// called from link
	/**
	*
	*
	* @param ApplicationEntityLink $amLink
	*/

	public function removeLink(ApplicationEntityLink $amLink)
	{
		if ($amLink->dataModel() !== $this) throw new ApplicationDataModelException("Could not remove link '" . $amLink->getName() . "' from module '" . $this->applicationModule->getName() . "' because link belongs to another model.");
		
		
		$name = $amLink->getName();
		
		
		if (! isset($this->amLinks[$name])) throw new ApplicationDataModelException("Could not remove link '" . $amLink->getName() . "' from module '" . $this->applicationModule->getName() . "'. Link belongs to this model but was not added.");

			
		unset($this->amLinks[$name]);
	}

	
	/**
	*
	*
	* @param string $name
	*
	* @return ApplicationEntity
	*/

	public function entity($name)
	{
		if (! isset($this->amEntities[$name])) throw new ApplicationDataModelException("'$name' entity not exists in data model.");
			
		return $this->amEntities[$name];
	}

	/**
	*
	*
	* @param string $name
	*
	* @return ApplicationEntityLink
	*/

	public function link($name)
	{
		if (! isset($this->amLinks[$name])) throw new ApplicationDataModelException("'$name' link not exists in data model.");
		
		return $this->amLinks[$name];
	}
	
	
	/**
	*
	*
	* @param ApplicationEntity $amEntity 
	* @param string $old_name
	*/

	public function remapEntity(ApplicationEntity $amEntity, $old_name)
	{
		if ($amEntity->dataModel() !== $this) throw new ApplicationDataModelException("Could not remap entity '" . $amEntity->getName()  . "' from name '" . $old_name . "' because entity belongs to another model.");

		if (! array_key_exists($old_name, $this->amEntities)) throw new ApplicationDataModelException("Could not remap entity '" . $amEntity->getName()  . "' from name '" . $old_name . "' because old name was not found within model.");

			
		array_replace_key($this->amEntities, $old_name, $amEntity->getName());
	}
	

	// Module facilities
	
	/**
	*
	*
	* @param string $module_name
	* @param string $entity_name
	*
	* @return string
	*/

	static public function makeModuleEntityName($module_name, $entity_name)
	{
		return 'am_' . $module_name . '_' . $entity_name;
	}

	/**
	*
	*
	* @param string $module_name
	* @param string $entity_name
	*
	* @return string
	*/

	static public function makeModuleLinkName($module_name, $link_name)
	{
		return 'am_' . $module_name . '_' . $link_name;
	}

	
	/**
	*
	*
	* @param string $module_name
	* @param array $config
	*
	* @return ApplicationEntity
	*/

	public function createModuleEntity($module_name, $config)
	{
		$config['name'] = self::makeModuleEntityName($module_name, $config['name']);

		return $this->createEntity($config);
	}
	
	/**
	*
	*
	* @param string $module_name
	* @param array $config
	*
	* @return ApplicationEntityLink
	*/

	public function createModuleLink($module_name, $config)
	{
		$from_module_name = isset($config['from_module']) ? $config['from_module'] : $module_name;
		$to_module_name = isset($config['to_module']) ? $config['to_module'] : $module_name;
		
		$config['name'] = self::makeModuleEntityName($module_name, $config['name']);
		$config['from'] = self::makeModuleEntityName($from_module_name, $config['from']);
		$config['to'] = self::makeModuleEntityName($to_module_name, $config['to']);
		
		return $this->createLink($config);
	}

	/**
	*
	*
	* @param string $module_name
	* @param array $config
	*/

	public function importFromModuleConfig($module_name, $config)
	{
		if (isset($config['entities']))
		{
			foreach ($config['entities'] as $entity_config)
			{
				$this->createModuleEntity($module_name, $entity_config);
			}
		}		
		
		if (isset($config['links']))
		{
			foreach ($config['links'] as $link_config)
			{
				$this->createModuleLink($module_name, $link_config);
			}
		}
	}
	
	
	/**
	*
	*
	*/

	public function beginTransaction()
	{
		return $this->dmDataModel->beginTransaction();
	}

	/**
	*
	*
	*/

	public function commit()
	{
		return $this->dmDataModel->commit();
	}

	/**
	*
	*
	*/

	public function rollBack()
	{
		return $this->dmDataModel->rollBack();
	}

	/**
	*
	*
	* @return bool
	*/

	public function inTransaction()
	{
		return $this->dmDataModel->inTransaction();
	}
	

	/**
	*
	* see DBConnection
	* @param string|array
	*
	* @return PDOStatement
	*/

	public function query($qset)
	{
		return $this->dmDataModel->query($qset);
	}

	/**
	*
	*
	* @param string|array $qset
	*
	* @return int|false
	*/

	public function exec($qset)
	{
		return $this->dmDataModel->exec($qset);
	}
}

?>