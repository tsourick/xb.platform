<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationEntity.php');
require_once('class.ApplicationEntityField.php');


require_once('class.ApplicationEntityPlainBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.ApplicationEntityPlain.php_5_3.php');
}
else
{
	include('class.ApplicationEntityPlain.php_5_2.php');
}

?>
