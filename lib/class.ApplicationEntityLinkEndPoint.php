<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

require_once('class.ApplicationEntityLink.php');


class ApplicationEntityLinkEndPoint
{
	private $amLink = NULL;
	
	private $toFrom = false;
	private $isTo = false;

	
	public function __construct(ApplicationEntityLink $amLink, $isFrom, $isTo)
	{
		$this->amLink = $amLink;
		
		$this->isFrom = $isFrom;
		$this->isTo = $isTo;
	}
	
	
	public function link()
	{
		return $this->amLink;
	}
	
	public function isFrom()
	{
		return $this->isFrom;
	}

	public function isTo()
	{
		return $this->isTo;
	}
}

?>
