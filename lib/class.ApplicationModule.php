<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Application module base class
*
* @package platform
*/

// Framework classes


// Platform classes

// require_once('class.ApplicationModuleTParser.php');
Platform::useClass('ApplicationModuleTParser');


/**
* Class exception
*/

class ApplicationModuleException extends Exception
{
}

/**
* Class exception shortcut
*/

class AMException extends ApplicationModuleException
{
}


// Application classes

// require_once('class.ApplicationModuleBase.php');
Platform::useClass('ApplicationModuleBase');


/**
* Class
*/

abstract class ApplicationModule extends ApplicationModuleBase
{
	// protected $application = NULL;
	
	// protected $moduleName = 'undefined';
	protected $config = NULL; // module config object

	protected $lang = NULL;

	protected $_final = false;
	
	protected $permissions = NULL;
	

	protected $handlers = array();
	
	
	/**
	* Constructor
	*
	* Loads module config options from modules DB table, creates Lang object, registers default handlers, loads data model config.
	*/

	public function __construct()
	{
		parent::__construct();
		// $this->application = Application::getObject(); // for config retrieving purposes, for example
		
		
		$table_name_prefix = $this->getDBTablePrefix();
		
			
		// $dbc = DBConnectionManager::getObject()->getDefaultConnection();
		$dbc = $this->getDefaultDBConnection();
		
		$row = $dbc->getRowAll($table_name_prefix . 'module', array('name' => $this->moduleName));

		$this->config = $row;
		
		
		$this->lang = new Lang(align_path($this->getBaseDirPath() . 'lang/')); // language is already set at framework config 'lang' in application run()
		
		$lang_dir = align_path($this->getDirPath() . 'lang/');
		if (file_exists($lang_dir))
		{
			$this->lang->addDir($lang_dir);
		}
		
		
		// Add handlers
		
		$handlers = array
		(
			'GetJSBase'                 => array('responseType' => 'js'),
			// 'GetJSLangBase'             => array('responseType' => 'js'),
			'GetJSLang'                 => array('responseType' => 'js'),
			'GetJSApplicationBase'      => array('responseType' => 'js'),
			'GetJS'                     => array('responseType' => 'js'),

			'GetShortcutImage'          => array('responseType' => 'icon'),
			'GetIconImage'              => array('responseType' => 'icon'),
			

			'GetEntityList'             => array('responseType' => 'json'),

			'GetEntityFieldList'        => array('responseType' => 'json'),
			'SaveEntityField'           => array('responseType' => 'json'),
			'DeleteEntityField'         => array('responseType' => 'json'),

			// @deprecated
			// 'GetEntityItemDataList'     => array('responseType' => 'json'), 
			'LoadEntityItemDataList'    => array('responseType' => 'json'),
			'SaveEntityItemData'        => array('responseType' => $this->config['files_enabled'] ? 'json_iframe' : 'json'),
			'DeleteEntityItemData'      => array('responseType' => 'json'),
			
			'LoadEntityItemData'        => array('responseType' => 'json'),
						
			'LoadEntityNodeData'        => array('responseType' => 'json'),
			'LoadEntityNodeDataList'    => array('responseType' => 'http'),
			'MoveEntityNodeAfter'       => array('responseType' => 'json'),

			'GetFile'                   => array('responseType' => NULL),
			'GetFileInline'             => array('responseType' => NULL),
			'GetFileAttachment'         => array('responseType' => NULL),
			
			'ExportEntityItemData'      => array('responseType' => NULL)
		);
		
		$this->handlers = array_merge($this->handlers, $handlers);
		
		
		$this->loadDataModelConfig();
		//$this->initDataModel();
		
		// data model related handlers here
	}
	
	/*
	public function init()
	{
	}
	*/
	
	protected $dataModelConfig = NULL;
	
	/**
	* Loads data model config
	*/

	protected function loadDataModelConfig()
	{
		if (is_null($this->dataModelConfig))
		{
			$this->dataModelConfig = $this->invokeModuleMethod('Modules', 'loadModuleDataModelConfig', array($this->moduleName));
		}
	}
	
	
	/**
	* Returns data model config
	*
	* @return array data model config
	*/

	public function getDataModelConfig()
	{
		return $this->dataModelConfig;
	}
	
	
	/**
	* Returns entity config
	*
	* @return array entity config
	*/

	public function getEntityConfig($entity_name)
	{
		return $this->dataModelConfig['entities'][$entity_name];
	}
	
	
	/**
	* Returns application data model, ensuring the model data model is imported
	*
	* @return object data model object
	*/

	public function dataModel()
	{
		return $this->application->dataModel($this->moduleName);
	}
	
	/**
	* (wrapper) Returns entity object for specified module from data model
	*
	* @param string $module_name module name
	* @param string $entity_name entity name
	*
	* @return object entity object
	*
	* @see ApplicationBase::moduleEntity()
	*/

	public function moduleEntity($module_name, $entity_name)
	{
		return $this->application->moduleEntity($module_name, $entity_name);
	}
	
	/**
	* Returns entity object for this module from data model
	*
	* @param string $name entity name
	*
	* @return object entity object
	*/

	public function entity($name)
	{
		return $this->moduleEntity($this->moduleName, $name);
	}

	/**
	* Returns entity link object for specified module from data model
	*
	* @param string $module_name module name
	* @param string $entity_name link name
	*
	* @return object link object
	*/

	public function moduleLink($module_name, $link_name)
	{
		/*
		$dm = $this->application->dataModel($module_name);
		return $dm->link(ApplicationDataModel::makeModuleLinkName($module_name, $link_name));
		*/
		return $this->application->moduleLink($module_name, $link_name);
	}

	/**
	* Returns entity link object for this module from data model
	*
	* @param string $name link name
	*
	* @return object link object
	*/

	public function link($name)
	{
		return $this->moduleLink($this->moduleName, $name);
	}


	/**
	* Returns full name of a table which represents given entity of the module
	*
	* @param string $name entity name
	*
	* @return string table full name
	*/
	
	public function makeEntityTableName($entity_name)
	{
		$dm = $this->dataModel();
		
		$full_entity_name = $dm->makeModuleEntityName($this->moduleName, $entity_name);

		return $dm->DMDataModel()->makeORMTableName(strtolower($full_entity_name));
	}
	/*
	public function application()
	{
		return $this->application;
	}
	
	public function getName()
	{
		return $this->moduleName;
	}
	*/
	/**
	* Returns language object
	*
	* @return object language object
	*/

	public function getLang()
	{
		return $this->lang;
	}
	/*
	public function getDirPath()
	{
		$module_name = $this->getName();
		
		return $this->application->getModuleDirPath($module_name);
	}
	*/	
	/**
	* Returns localized text string
	*
	* @param string $text text string
	*
	* @return string localized text string
	*/

	public function t($text)
	{
		return $this->application->t($text, $this);
	}
	

	/**
	* Returns module settings
	*/

	public function getSettings($names)
	{
		return $this->invokeModuleMethod('Modules', 'getModuleSettings', array($this->moduleName, $names));
	}

	/**
	* Returns module setting
	*/

	protected function getSetting($name)
	{
		return $this->invokeModuleMethod('Modules', 'getModuleSetting', array($this->moduleName, $name));
	}

	/**
	* Returns module setting
	*/

	protected function getSettingValue($name)
	{
		return $this->invokeModuleMethod('Modules', 'getModuleSettingValue', array($this->moduleName, $name));
	}

	/**
	* Sets module setting value
	*/

	protected function setSetting($name, $value)
	{
		$this->setSettingValue($name, $value);
	}

	/**
	* Sets module setting value
	*/

	protected function setSettingValue($name, $value)
	{
		$this->invokeModuleMethod('Modules', 'setModuleSetting', array($this->moduleName, $name, $value));
	}

	
	/**
	* Returns module default permission list
	*
	* Default permissions are permissions for entities.
	*
	* @return array permission list
	*/

	public function getPermissionList()
	{
		$list = array();
		

		if (isset($this->dataModelConfig['entities']))
		{
			$common_permissions = array
			(
				'load' => array
				(
					'title' => 'Load',
					'options' => array
					(
						array('text' => 'No', 'value' => 0),
						array('text' => 'Yes', 'value' => 1)
					)
				),
				'operate' => array
				(
					'title' => 'Operate',
					'options' => array
					(
						array('text' => 'No', 'value' => 0),
						array('text' => 'Yes', 'value' => 1)
					)
				),
				'add' => array
				(
					'title' => 'Add',
					'options' => array
					(
						array('text' => 'No', 'value' => 0),
						array('text' => 'Yes', 'value' => 1)
					)
				),
				'edit' => array
				(
					'title' => 'Edit',
					'options' => array
					(
						array('text' => 'No', 'value' => 0),
						array('text' => 'Yes', 'value' => 1)
					)
				),
				'delete' => array
				(
					'title' => 'Delete',
					'options' => array
					(
						array('text' => 'No', 'value' => 0),
						array('text' => 'Yes', 'value' => 1)
					)
				),
				'export' => array
				(
					'title' => 'Export',
					'options' => array
					(
						array('text' => 'No', 'value' => 0),
						array('text' => 'Yes', 'value' => 1)
					)
				)
			);

			
			foreach ($this->dataModelConfig['entities'] as $entity_config)
			{
				// Add common permissions
				$permissions = $common_permissions;

				// Add entity type related permissions
				switch ($entity_config['type'])
				{
					case 'celko':
						/*
						$permissions['load'] = array
						(
							'title' => 'Load',
							'options' => array
							(
								array('text' => 'No', 'value' => 0),
								array('text' => 'Yes', 'value' => 1)
							)
						);
						*/
						
						$permissions['move'] = array
						(
							'title' => 'Move',
							'options' => array
							(
								array('text' => 'No', 'value' => 0),
								array('text' => 'Yes', 'value' => 1)
							)
						);
					break;
					
					default:
					break;
				}
				
				
				$list[$entity_config['name']] = array
				(
					'title' => $entity_config['singular_title'],
					'permissions' => $permissions
				);
			}
		}
		
		
		return $list;
	}

	/**
	* Returns module default permission list for 'site' application
	*
	* Default list is empty. Method should be overridden in derived classes.
	*
	* @return array permission list
	*/

	public function getSitePermissionList()
	{
		return array();
	}

	/*
	public function getDefaultDBConnectionName()
	{
		return $this->application->getDefaultDBConnectionName();
	}
	
	public function getDefaultDBConnection()
	{
		return $this->application->getDefaultDBConnection();
	}


	// for old modules without datamodel and for custom tables
	public function getDBTablePrefix()
	{
		return $this->application->getDBTablePrefix();
	}
	*/

	private $tparser = NULL;
	
	
	/**
	* Returns module template parser object
	*
	* @return object template parser object
	*/

	public function TParser()
	{
		if (is_null($this->tparser))
		{
			$this->tparser = new ApplicationModuleTParser($this);
		}
		
		return $this->tparser;
	}	

	protected function parseTemplate($template_name, $data = array(), $page_language = NULL)
	{
		if (is_null($page_language))
		{
			$page_language = $this->invokeModuleMethod('Pages', 'getPageLocaleLanguage');
		}
		

		if ($page_language == 'en') $template_name .= '_en';
		else $template_name .= '_ru';

		
		$tparser = $this->TParser();
		
		$tparser->set($data);
		return $tparser->parseTemplate($template_name);
	}
	
	
	/**
	* Returns permission values for current user
	*
	* Uses application authorization method getModulePermissionValues().
	*
	* @return array array of module permission values for current user
	*/

	protected function getPermissionValues()
	{
		if (is_null($this->permissions))
		{
			/*
			$table_name_prefix = $this->getDBTablePrefix();

			$dbc = $this->getDefaultDBConnection();
			
			$row = $dbc->getRow('id', $table_name_prefix . 'module', array('name' => $this->getName()));

			$this->permissions = $this->application->getModulePermissionValues($row['id']);
			*/
			$this->permissions = $this->application->getModulePermissionValues($this->config['id']);
		}

		
		return $this->permissions;
	}
	
	
	protected function hasObjectPermission($object_name, $permission_name, $permission_value = 1)
	{
		$r = true;
		
		
		$pv = $this->getPermissionValues();

		if
		(
			! isset($pv[$object_name])
			|| ! isset($pv[$object_name][$permission_name])
			|| $pv[$object_name][$permission_name] != $permission_value
		)
		$r = false;
		
		
		return $r;
	}
	
	protected function hasEntityPermission($entity_name, $permission_name, $permission_value = 1)
	{
		return $this->hasObjectPermission($entity_name, $permission_name, $permission_value);
	}
	
	/**
	* Throws AMException if specified permission value is not set
	*
	* @param string $object_name name of an object under access control
	* @param string $permission_name permission name
	* @param mixed $permission_value value to ensure, default is 1
	*/

	protected function ensureObjectPermission($object_name, $permission_name, $permission_value = 1)
	{
		/*
		$pv = $this->getPermissionValues();

		if
		(
			! isset($pv[$object_name])
			|| ! isset($pv[$object_name][$permission_name])
			|| $pv[$object_name][$permission_name] != $permission_value
		)
		throw new AMException("Permission '{$permission_name}' not set to '{$permission_value}' for object '{$object_name}'");
		*/
		if (! $this->hasObjectPermission($object_name, $permission_name, $permission_value))
			throw new AMException("Permission '{$permission_name}' not set to '{$permission_value}' for object '{$object_name}'");
	}
	
	/**
	* Throws AMException if specified permission value is not set
	*
	* @param string $entity_name entity name
	* @param string $permission_name permission name
	* @param mixed $permission_value value to ensure, default is 1
	*/
	
	protected function ensureEntityPermission($entity_name, $permission_name, $permission_value = 1)
	{
		$this->ensureObjectPermission($entity_name, $permission_name, $permission_value);
	}
	

	
	protected function getUserPermissionValues($user_id)
	{
		return $this->application->getModulePermissionValues($this->config['id'], $user_id);
	}

	protected function hasUserObjectPermission($user_id, $object_name, $permission_name, $permission_value = 1)
	{
		$r = true;
		
		
		$pv = $this->getUserPermissionValues($user_id);

		if
		(
			! isset($pv[$object_name])
			|| ! isset($pv[$object_name][$permission_name])
			|| $pv[$object_name][$permission_name] != $permission_value
		)
		$r = false;
		
		
		return $r;
	}

	protected function ensureUserObjectPermission($user_id, $object_name, $permission_name, $permission_value = 1)
	{
		if (! $this->hasUserObjectPermission($user_id, $object_name, $permission_name, $permission_value))
			throw new AMException("Permission '{$permission_name}' not set to '{$permission_value}' for object '{$object_name}' for user '{$user_id}'");
	}

	
	
	/**
	* Calls module action handler through application's MVC controller
	*
	* @param string $module_name module name
	* @param string $action_name action name
	* @param array $params module action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array())
	{
		return $this->application->invokeModuleAction($module_name, $action_name, $params, $options);
	}

	/**
	* Calls module method through application's MVC controller
	*
	* @param string $module_name module name
	* @param string $method_name method name
	* @param array $params module method parameters
	*
	* @return mixed method result
	*/

	public function invokeModuleMethod($module_name, $method_name, $params = array())
	{
		return $this->application->invokeModuleMethod($module_name, $method_name, $params);
	}
	

	/**
	* Subscribes this module for specified event
	*
	* @param int $module_event_id module event id
	*
	* @return bool true
	*/

	public function subscribeForEvent($module_event_id)
	{
		return $this->application->subscribeModuleForEvent($this->config['id'], $module_event_id);
	}
	
	/**
	* Unsubscribes this module from specified event
	*
	* @param int $module_event_id module event id
	*
	* @return bool true
	*/

	public function unsubscribeFromEvent($module_event_id)
	{
		return $this->application->unsubscribeModuleFromEvent($this->config['id'], $module_event_id);
	}

	/**
	* Fires the module event
	*
	* @see ApplicationBase::fireModuleEvent()
	*
	* @param string $event_name event name
	* @param array $event_params event parameters
	*/

	public function fireEvent($event_name, $event_params = NULL)
	{
		$this->application->fireModuleEvent($this->moduleName, $event_name, $event_params);
	}
	
	/**
	* Fires module event
	*
	* @see ApplicationBase::fireModuleEvent()
	*
	* @param string $module_name module name
	* @param string $event_name event name
	* @param array $event_params event parameters
	*/

	public function fireModuleEvent($module_name, $event_name, $event_params)
	{
		$this->application->fireModuleEvent($module_name, $event_name, $event_params);
	}

	
	/**
	* Registers custom action handlers
	*
	* @param string|array $action_name action name or array of handlers' info:
	* <pre>
	* array
	* (
	*   '<action name>'[ => array('responseType' => '(json|json_iframe|http|css|js|icon|NULL)')],
	*   ...
	* )
	* </pre>
	* If action name is specified as array key, responseType should be provided.
	* If action name is specified as array value, responseType is set to json by default.
	* @param string $type type of handler, 'json' by default
	*/
	
	protected function registerActionHandlers($action_name, $type = 'json')
	{
		// Convert to array
		
		if (! is_array($action_name))
		{
			$handlers = array
			(
				$action_name => array('responseType' => $type)
			);
		}
		else
		{
			$handlers = $action_name;
		}
		
		
		// Check and convert to info array

		foreach ($handlers as $action_name => $config)
		{
			if (! is_string($action_name))
			{
				$index = $action_name;
				$action_name = $config;
				$config = NULL;
			}
			
			
			if (! method_exists($this, 'on' . $action_name)) throw new AMException("Action '{$action_name}' handler is registered, but method 'on{$action_name}' not found in module {$this->moduleName}");
			
			
			if (is_null($config))
			{
				$handlers[$action_name] = array('responseType' => 'json');
				unset($handlers[$index]);
			}
		}
		
		
		// Set
		
		if (! isset($this->handlers))
		{
			$this->handlers = array();
		}
		
		
		$this->handlers = array_merge($this->handlers, $handlers);
	}
	

	/**
	* Safely registers entity custom hooks
	*
	* @param array $hooks array of entity hooks:
	* <pre>
  * array
  * (
  *   '<entity name>' => array
  *   (
  *     '<hook name>' => <hook reference>,
  *     ...
  *   )
  * )
  * </pre>
  * Hook name should match '(before|after)(update|insert|save|loadlist)'.
  * Hook reference may be reference itself or string. The string is assumed to be a method name of the module instance.
  *
  * If specified entity does not exist in data model, the hook is skipped from registration.
  * But all the hook names and hook handlers are checked regardless of entity existance!
	*/
	
	protected function registerEntityHooks($hooks)
	{                           
		if (! is_null($this->dataModelConfig))
		{
			$ec =& $this->dataModelConfig['entities'];
			

			foreach ($hooks as $entity_name => $hook_list)
			{
				foreach ($hook_list as $hook_name => $hook_reference)
				{
					// Check hook name
					if (! preg_match('/^(before|after)(insert|update|save|save2|delete|loadlist|loadnodelist)$/', $hook_name)) throw new Exception("Invalid hook name '{$hook_name}' for entity '{$entity_name}'");
					
					
					if (is_string($hook_reference))
					{
						$hook_reference = array($this, $hook_reference);
					}
					
					
					// Check handler
					if (! is_callable($hook_reference)) throw new Exception("Invalid '{$hook_name}' hook handler for entity '{$entity_name}'");
					

					// Register hook if entity configured
					if (isset($ec[$entity_name]))
					{
						$ec[$entity_name]['handlers'][$hook_name] = $hook_reference;
					}
				}
			}
		}
	}
	
	/**
	* Safely registers entity image fields
	*
	* @param array $fields array of entity image fields:
	* <pre>
  * array
  * (
  *   '<entity name>' => array
  *   (
  *     '<image name>' => array
  *     (
  *       array(<image field config>),
  *       ...
  *     )
  *   )
  * )
  * </pre>
  * 
	*/
	
	protected function registerEntityImageFields($fields)
	{
		if (! is_null($this->dataModelConfig))
		{
			$ec =& $this->dataModelConfig['entities'];
			

			foreach ($fields as $entity_name => $field_list)
			{
				if (isset($ec[$entity_name]))
				{
					$ec[$entity_name]['image_fields'] = $field_list;
				}
			}
		}
	}
	
	/*
	public function defaultDispatcher($name, $params = array())
	{
		return $this->callActionHandler($name, $params);
	}
	
	public function callActionHandler($name, $params = array())
	{
		$handler_name = 'on' . ucfirst($name);

		$callable_name = '';
		if (! is_callable(array($this, $handler_name), false, $callable_name)) throw new ApplicationModuleException("Could not find callable handler for action '$name' in module '" . $this->getName() . "'");
		
		return call_user_func(array($this, $handler_name), $params);
	}
	*/

	/**
	* Module action dispatcher
	*
	* @param string $name action name
	* @param array $params action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function dispatchAction($name, $params = array(), $options = array())
	{
		$result = NULL;

		
		$o = array
		(
			'send' => array_get_value($options, 'send', true)
		);
		
		
		if (isset($this->handlers) && isset($this->handlers[$name]))
		{
			$hInfo = $this->handlers[$name];

			switch ($hInfo['responseType'])
			{
				case 'json':
				case 'json_iframe':
					try
					{
						$result = $this->callActionHandler($name, $params, $options);

						if (is_array($result)) $result['success'] = true; else $result = array('success' => true);
					}
					catch (DBConnectionException $e)
					{
						$result = array('success' => false, 'errors' => array('DB exception' => $e->getMessage()));
					}
					catch (AMException $e)
					{
						$result = array('success' => false, 'errors' => array('Module "' . $this->getName() . '" exception' => $e->getMessage()));
					}
					catch (Exception $e)
					{
						$result = array('success' => false, 'errors' => array('Exception' => $e->getMessage()));
					}
					
					
					if ($o['send'])
					{
						include_once('class.JSON.php');
						
						$content = JSON::encode($result);
						
						xbf_http_content_length(strlen($content)); // Enable pipelining (if request is GET)

						if ($hInfo['responseType'] == 'json_iframe')
						{
							xbf_http_send_response($content, 'html', 'utf-8', true);
						}
						else
						{
							xbf_http_send_response($content, 'json', '', true);
						}
					}
				break;

				case 'http':
					$result = $this->callActionHandler($name);
					
					if (! is_array($result))
					{
						$result = array('text' => $result);
					}
					
					if (! isset($result['content_type'])) $result['content_type'] = 'text/plain';
					if (! isset($result['charset'])) $result['charset'] = 'utf-8';
					if (! isset($result['code'])) $result['code'] = 200;
					if (! isset($result['message'])) $result['message'] = 'OK';
		
					xbf_http_send_response_new($result['text'], $result['content_type'], $result['charset'], $result['code'], $result['message']);
				break;
				
				case 'css':
				case 'js':
					if (DEBUG)
					{
						// Skip debug nocache headers
						
						xbf_http_remove_header('Pragma');
						xbf_http_remove_header('Expires');
						xbf_http_remove_header('Last-Modified');
						xbf_http_remove_header('Cache-Control');
					}
					

					// Check whether modified is forced (for debugging or deployment reasons)
					
					$c = $this->application->getConfig();
					
					
					$debug = $c->get('application/debug');
					
					if ($hInfo['responseType'] == 'css')
					{
						$debug_force_modified = $c->get('application/debug_force_css_modified');
					}
					else
					{
						$debug_force_modified = $c->get('application/debug_force_js_modified');
					}

					$force_modified = ($debug && $debug_force_modified) ? true : false;


					// Call action handler with if_modified_since unix-timestamp param or NULL
					
					$if_modified_since = $force_modified ? NULL : xbf_http_if_modified_since();

					$result = $this->callActionHandler($name, compact('if_modified_since'));

					
					if ($result['modified'])
					{
						// Force browser if-modified-since check on every request
						xbf_http_set_header('Cache-Control: no-cache');
						
						xbf_http_last_modified($result['last_modified']);
						
						
						xbf_http_content_length(strlen($result['content'])); // Enable pipelining (if request is GET)
						
						
						if ($hInfo['responseType'] == 'css')
						{
							$mime_type = 'text/css';
						}
						else
						{
							$mime_type = 'text/javascript';
						}

						xbf_http_send_response($result['content'], $mime_type, 'utf-8');
					}
					else
					{
						xbf_http_not_modified();
					}
				break;
				
				case 'icon':
					if (DEBUG)
					{
						// Skip debug nocache headers
						
						xbf_http_remove_header('Pragma');
						xbf_http_remove_header('Expires');
						xbf_http_remove_header('Last-Modified');
						xbf_http_remove_header('Cache-Control');
					}
					

					// Check whether modified is forced (for debugging or deployment reasons)
					
					$c = $this->application->getConfig();
					
					
					$debug = $c->get('application/debug');
					$debug_force_modified = $c->get('application/debug_force_icon_modified');

					$force_modified = ($debug && $debug_force_modified) ? true : false;


					// Call action handler with if_modified_since unix-timestamp param or NULL
					
					$if_modified_since = $force_modified ? NULL : xbf_http_if_modified_since();

					$result = $this->callActionHandler($name, compact('if_modified_since'));

					
					if ($result['modified'])
					{
						// Force browser if-modified-since check on every request
						xbf_http_set_header('Cache-Control: no-cache');
						
						xbf_http_last_modified($result['last_modified']);
						
						xbf_http_send_file('icon', 'image/gif', $result['content'], NULL, 'inline');
					}
					else
					{
						xbf_http_not_modified();
					}
				break;
				
				default:
					$result = $this->defaultDispatcher($name, $params, $options);
			}
		}
		else
		{
			$result = $this->defaultDispatcher($name, $params, $options);
		}
		
		return $result;
	}
	
	
	// Common handlers
	
	/**
	* Reads file, returns data for last-modified HTTP header and content itself if modified
	*
	* @param string $file_path path to file
	* @param string $if_modified_since if-modified-since header value
	*
	* @return array array('modified', 'last_modified', 'content')
	*/

	private function readFileIfModifiedSince($file_path, $if_modified_since)
	{
		$modified = true;
		$last_modified = NULL;
		$content = '';
		
		
		$last_modified = @filemtime($file_path);

		
		// Check for if-modified-since request
		if ($last_modified !== false && ! is_null($if_modified_since))
		{
			$modified = $last_modified > $if_modified_since;
		}

		// Prepare content if modified or new (never sent before)
		if ($modified)
		{
			$content = file_get_contents($file_path);
		}
		
		
		return compact('modified', 'last_modified', 'content');
	}

	
	/**
	* Returns base JS source file data
	*
	* @param array $params
	*
	* @return array array('modified', 'last_modified', 'content')
	*/

	public function onGetJSBase($params)
	{
		$c = $this->application->getConfig();

		// $application_id = $c->get('application/id');
		$application_debug = $c->get('application/debug');
		$module_name = $this->getName();
		// $module_dir = align_path(dirpath($c->get('application/module_dir')) . $module_name . '/');
		// $module_dir = $this->getDirPath();
		$module_dir = $this->getBaseDirPath();

		
		// Read control base file
		
		if ($application_debug)
		{
			// use plain file
			$file_name =  $module_name . 'ControlBase.js';
		}
		else
		{
			// use compressed file
			$file_name = $module_name . 'ControlBase.compressed.js';
		}
		
		$file_path = $module_dir . $file_name;
		
		$file_data = $this->readFileIfModifiedSince($file_path, $params['if_modified_since']);


		return $file_data;
	}
	
	/**
	* Returns base JS lang source file data
	*
	* @param array $params
	*
	* @return array array('modified', 'last_modified', 'content')
	*/
	/*
	public function onGetJSLangBase($params)
	{
		$modified = true;
		$last_modified = mktime(); // always modified though no cache available yet for this file
		$content = '';


		$messages = $this->lang->export();

		if (! empty($messages))
		{
			$js_messages = array();
			foreach ($messages as $k => $text)
			{
				$k = escape_javascript_string($k);
				$text = escape_javascript_string($text);
				
				$js_messages[] = "'{$k}': '{$text}'";
			}
			$js_messages = implode(", ", $js_messages);
			
	
			$ucf_module_name = $this->getName();
			
			$content = "
			
				Ext.override
				(
					XB.ApplicationModules.{$ucf_module_name}ControlBase,
					{
						MESSAGES: {{$js_messages}}
					}
				);
				
			";
		}

		
		return compact('modified', 'last_modified', 'content');
	}
	*/

	/**
	* Returns application base JS file data
	*
	* @param array params
	*
	* @return array array('modified', 'last_modified', 'content')
	*/

	public function onGetJSApplicationBase($params)
	{
		$c = $this->application->getConfig();

		$application_id = $c->get('application/id');
		$application_debug = $c->get('application/debug');
		$module_name = $this->getName();
		// $module_dir = align_path(dirpath($c->get('application/module_dir')) . $module_name . '/');
		// $module_dir = $this->getDirPath();
		$module_base_dir = $this->getBaseDirPath();
		$module_dir = $this->getDirPath();

		
		// Read main file
		
		if ($application_debug)
		{
			// use plain file
			$base_file_name =  'am.' . $application_id . '.' . $module_name . '.js';
			$file_name =  'am.' . $module_name . '.js';
		}
		else
		{
			// use compressed file
			$base_file_name = 'am.' . $application_id . '.' . $module_name . '.compressed.js';
			$file_name = 'am.' . $module_name . '.compressed.js';
		}
		
		$base_file_path = $module_base_dir . $base_file_name;
		
		$base_file_data = $this->readFileIfModifiedSince($base_file_path, $params['if_modified_since']);
		

		// if ($base_file_data['modified'] && ! $this->_final)
		if (! $this->_final)
		{
			// Autogenerate appropriate class
			
			$default_class_name = $module_name;
			$application_name = ucfirst(strtolower($application_id));
			
			$base_class_name = $default_class_name . $application_name . 'Base';
			
			
			$file_path = $module_dir . $file_name;
			
			if (file_exists($file_path))
			{
				// Intermediate class
				$autogenerated_class_source = "
// Intermediate class (autogenerated)
XB.ApplicationModules._{$default_class_name} = function()
{
	// Call parent constructor
	XB.ApplicationModules._{$default_class_name}.superclass.constructor.call(this);
}
			
Ext.extend(XB.ApplicationModules._{$default_class_name}, XB.ApplicationModules.{$base_class_name}, {});			
				";
			}
			else
			{
				// Final class
				$autogenerated_class_source = "
// Final class (autogenerated)
XB.ApplicationModules.{$default_class_name} = function()
{
	// Call parent constructor
	XB.ApplicationModules.{$default_class_name}.superclass.constructor.call(this);
}
			
Ext.extend(XB.ApplicationModules.{$default_class_name}, XB.ApplicationModules.{$base_class_name}, {});			
				";
			}
		
		
			// Append intermediate class
		
			$base_file_data['content'] .= $autogenerated_class_source;
		}


		return $base_file_data;
	}
	
	
	/**
	* Returns JS lang source file data
	*
	* @param array $params
	*
	* @return array array('modified', 'last_modified', 'content')
	*/

	public function onGetJSLang($params)
	{
		// if ($this->_final)
		// {
		// 	$modified = false;
		// 	$last_modified = 0; // never modified
		// 	$content = '';
		// }
		// else
		// {
			$modified = true;
			// $last_modified = mktime(); // always modified though no cache available yet for this file
			$last_modified = time(); // always modified though no cache available yet for this file
			$content = '';
	
	
			$messages = $this->lang->export();
	
			if (! empty($messages))
			{
				$js_messages = array();
				foreach ($messages as $k => $text)
				{
					$k = escape_javascript_string($k);
					$text = escape_javascript_string($text);
					
					$js_messages[] = "'{$k}': '{$text}'";
				}
				$js_messages = implode(", ", $js_messages);
				
		
				$ucf_module_name = $this->getName();
				/*
				$c = $this->application->getConfig();
				$application_id = $c->get('application/id');
				$application_name = ucfirst(strtolower($application_id));
				*/
				// $class_name = $ucf_module_name . $application_name . 'Base';
				$class_name = $ucf_module_name . 'ControlBase';
				
				
				$content = "
				if (! XB.ApplicationModules.{$class_name}) alert('No XB.ApplicationModules.{$class_name} found to apply language');
				else
				{
					Ext.override
					(
						XB.ApplicationModules.{$class_name},
						{
							MESSAGES: {{$js_messages}}
						}
					);
				}
				";
			}
		// }

		
		return compact('modified', 'last_modified', 'content');
	}


	/**
	* Returns JS file data
	*
	* @param array params
	*
	* @return array array('modified', 'last_modified', 'content')
	*/

	public function onGetJS($params)
	{
		if ($this->_final)
		{
			$modified = false;
			$last_modified = 0; // never modified
			$content = '';
			
			$file_data = compact('modified', 'last_modified', 'content');
		}
		else
		{
			$c = $this->application->getConfig();
	
			// $application_id = $c->get('application/id');
			$application_debug = $c->get('application/debug');
			$module_name = $this->getName();
			// $module_dir = align_path(dirpath($c->get('application/module_dir')) . $module_name . '/');
			$module_dir = $this->getDirPath();
	
			
			// Read main file
			
			if ($application_debug)
			{
				// use plain file
				// $file_name =  'am.' . $application_id . '.' . $module_name . '.js';
				$file_name =  'am.' . $module_name . '.js';
			}
			else
			{
				// use compressed file
				// $file_name = 'am.' . $application_id . '.' . $module_name . '.compressed.js';
				$file_name = 'am.' . $module_name . '.compressed.js';
			}
			
			$file_path = $module_dir . $file_name;
			
			if (! file_exists($file_path))
			{
				$modified = true;
				// $last_modified = mktime(); // always modified
				$last_modified = time(); // always modified
				$content = '// nothing here';
				
				$file_data = compact('modified', 'last_modified', 'content');
			}
			else
			{
				$file_data = $this->readFileIfModifiedSince($file_path, $params['if_modified_since']);
			}
		}


		return $file_data;
	}


	/**
	* Returns shotcut image data
	*
	* @param array $params
	*
	* @return array array('modified', 'last_modified', 'content');
	*/

	public function onGetShortcutImage($params)
	{
		return $this->getIconImage(true, $params['if_modified_since']);
	}

	/**
	* Returns icon image data
	*
	* @param array $params
	*
	* @return array array('modified', 'last_modified', 'content');
	*/

	public function onGetIconImage($params)
	{
		return $this->getIconImage(false, $params['if_modified_since']);
	}

	/**
	* Returns shotcut or icon image data
	*
	* @param bool $shortcut returns shortcut image data if true, icon image data otherwise
	* @param string $if_modified_since if-modified-since header value
	*
	* @return array array('modified', 'last_modified', 'content');
	*/

	private function getIconImage($shortcut, $if_modified_since)
	{
		$modified = true;
		$last_modified = NULL;
		$content = '';


		$icon_filename = $shortcut ? 'shortcut.gif' : 'icon.gif';
		
		$module_dir = $this->getBaseDirPath();
		$icon_path = $module_dir . $icon_filename;

		$last_modified = filemtime($icon_path);
		
		
		// Check for if-modified-since request
		if (! is_null($if_modified_since))
		{
			$modified = $last_modified > $if_modified_since;
		}

		// Prepare content if modified or new (never sent before)
		if ($modified)
		{
			$content = file_get_contents($icon_path);
		}
		

		return compact('modified', 'last_modified', 'content');
	}


	/**
	* Module event dispatcher
	*
	* Should be overridden in derived classes for custom processing.
	*
	* @param string $module_name module name
	* @param string $event_name event name
	* @param array $params event parameters
	*
	* @return bool true
	*/

	public function onModuleEvent($module_name, $event_name, $params = array())
	{
		return true;
	}
	
	
	/**
	* Returns all record data list or one page of record data list and its overall count from database
	*
	* Method is used for interface grid-like components.
	* Supports particular record request, sorting and paging controlled by CGI parameters.
	*
	* If $pri_key_name specified, 'ids' CGI value is taken as array of integers and if not empty - respective records are retrieved.
	*
	* If 'sort' CGI value specified, the list is sorted by that field name in order specified by 'dir' CGI value or 'ASC' by default.
	*
	* If 'paging' CGI value is true then:
	* - 'start' and 'limit' CGI values are applied as LIMITS if both are specified
	* - overall record count is returned in result as 'total_count'
	* - one page of list is returned in result as 'list'
	* otherwise:
	* - all record list is returned in result as 'list'
	*
	* @param string $storage "FROM" part of SQL query as for DBConnection::select() 
	* @param string|array|NULL $condition condition for SQL query as for DBConnection::select()
	* @param string $order default "ORDER" part of SQL query as for DBConnection::select(), applied if no 'sort' CGI value specified
	* @param string|NULL $pri_key_name primary key field name to query records by PK
	*
	* @return array array('list'[, 'total_count'])
	*
	* @see DBConnection::select()
	*/

	protected function getGridItemPlainList($storage, $condition = NULL, $order = '', $pri_key_name = NULL)
	{
		$result = array();
		
		
		$dbc = $this->getDefaultDBConnection();
		
		
		// Normalize condition
		
		if (is_null($condition))
		{
			$condition = array();
		}
		
		if (! is_array($condition))
		{
			$condition = array($condition);
		}
		

		// Particular records request

		if (! is_null($pri_key_name))
		{
			$ids = get_cgi_value('ids', 'ai');
			
			if (! is_null($ids))
			{
				$condition[] = SQL::in($pri_key_name, $ids);
			}
		}

		
		// Sorting if requested
		
		$sfield = get_cgi_value('sort');
		
		if (! is_null($sfield))
		{
			$sorder = get_cgi_value('dir');
			if (! in_array($sorder, array('ASC', 'DESC'))) $sorder = 'ASC';
			
			$order = '`' . SQL::escape($sfield) . '` ' . $sorder;
		}
		

		$paging = get_cgi_value('paging', 'b');
		
		if ($paging)
		{
			$start = get_cgi_value('start', 'i');
			$limit = get_cgi_value('limit', 'i');
			
			$limits = array();
			if (! is_null($start) && ! is_null($limit)) $limits = array($start, $limit);


			$rows = $dbc->getRowsAll($storage, $condition, '', $order, $limits);

			$c = $dbc->getRowCount($storage, $condition);

			// $result = array('success' => true, 'list' => $rows, 'total_count' => $c);
			$result = array('list' => $rows, 'total_count' => $c);				
		}
		else
		{
			$rows = $dbc->getRowsAll($storage, $condition, '', $order);
			
			// $result = array('success' => true, 'list' => $rows);
			$result = array('list' => $rows);
		}
		
		
		return $result;
	}
	
	/**
	* Returns all record data list or one page of record data list and its overall count from database queried by external key
	*
	* Method is used for interface grid-like components with dependencies.
	* Supports sorting and paging controlled by CGI parameters.
	*
	* If 'sort' CGI value specified, the list is sorted by that field name in order specified by 'dir' CGI value or 'ASC' by default.
	*
	* If 'paging' CGI value is true then:
	* - 'start' and 'limit' CGI values are applied as LIMITS if both are specified
	* - overall record count is returned in result as 'total_count'
	* - one page of list is returned in result as 'list'
	* otherwise:
	* - all record list is returned in result as 'list'
	*
	* @param string $storage "FROM" part of SQL query as for DBConnection::select() 
	* @param string $ext_key_name specifies external key field name and CGI value name for that field
	* @param string $default_order_by default "ORDER" part of SQL query as for DBConnection::select(), applied if no 'sort' CGI value specified
	* @param string|NULL $pri_key_name unused
	*
	* @return array array('list'[, 'total_count'])
	*
	* @see DBConnection::select()
	*/

	protected function getGridItemChildList($storage, $ext_key_name, $default_order_by = '', $pri_key_name = NULL)
	{
		$result = array();
		
		$order = $default_order_by;
		
		
		// Get values from CGI

		$ext_key_value = get_cgi_value($ext_key_name, 'i');

		
		$dbc = $this->getDefaultDBConnection();
		

		// Sorting if requested
		
		$sfield = get_cgi_value('sort');
		
		if (! is_null($sfield))
		{
			$sorder = get_cgi_value('dir');
			if (! in_array($sorder, array('ASC', 'DESC'))) $sorder = 'ASC';
			
			$order = '`' . SQL::escape($sfield) . '` ' . $sorder;
		}


		$paging = get_cgi_value('paging', 'b');
		
		if ($paging)
		{
			$start = get_cgi_value('start', 'i');
			$limit = get_cgi_value('limit', 'i');
			
			$limits = array();
			if (! is_null($start) && ! is_null($limit)) $limits = array($start, $limit);


			$rows = $dbc->getRowsAll($storage, array($ext_key_name => $ext_key_value), '', $order, $limits);

			$c = $dbc->getRowCount($storage, array($ext_key_name => $ext_key_value));

			$result = array('list' => $rows, 'total_count' => $c);
		}
		else
		{
			$rows = $dbc->getRowsAll($storage, array($ext_key_name => $ext_key_value), '', $order);

			$result = array('list' => $rows);
		}

		
		return $result;
	}
	
	
	/**
	* Used for site search by content of a module
	*
	* Should be implemented for particular module.
	*
	* @param string $raw_params string of parameters with ; and = separators
	*
	* @return array found data
	*/

	public function getContent($raw_params)
	{
		return '';
	}
	
	
	/**
	* Returns config entity list with options
	*
	* @return array config entity list
	*/

	protected function getEntityList()
	{
		$list = array();
		

		if (! is_null($this->dataModelConfig['entities']))
		{
			foreach ($this->dataModelConfig['entities'] as $entity_config)
			{
				// base properties
				$list_item = array
				(
					'name' => $entity_config['name'],
					'singular_title' => $entity_config['singular_title'],
					'plural_title' => $entity_config['plural_title'],
					'type' => $entity_config['type']
				);
				// options
				foreach (ApplicationEntity::$defaultOptions as $o_name => $o_def_value)
				{
					$list_item[$o_name] = $entity_config[$o_name];
				}
				
				
				$list[] = $list_item;
			}
		}

		
		return $list;
	}
	
	/**
	* Returns config entity list (list action handler)
	*
	* @return array
	*/

	public function onGetEntityList()
	{
		$result = array();
		
		
		$list = $this->getEntityList();
				
		$result = array('list' => $list);
		

		return $result;
	}
	
	/**
	* Returns config entity field list with options
	*
	* @param string $entity_name
	*
	* @return array config entity field list
	*/

	protected function getEntityFieldList($entity_name)
	{
		$list = array();
		

		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");
			
			
		$amEntity = $this->entity($entity_name);
		// $list = $this->dataModelConfig['entities'][$entity_name]['fields'];

		foreach ($amEntity->fields() as $amField)
		{
			// base properties
			$list_item = array
			(
				'name' => $amField->getName(),
				'type' => $amField->getType(),
				'title' => $amField->getTitle()
			);
			// options
			$list_item = array_merge($list_item, $amField->getOptions()); 
			
			
			$list[] = $list_item;
		}

		
		return $list;
	}
	
	/**
	* Returns config entity field list with options (list action handler)
	*
	* @param string $entity_name
	*
	* @return array config entity field list
	*/

	public function onGetEntityFieldList($params = array())
	{
		$result = array();
		
		
		$entity_name = array_get_value($params, 'entity_name', get_cgi_value('entity_name'));
	
		$list = $this->getEntityFieldList($entity_name);
				
		$result = array('list' => $list);
		

		return $result;
	}

	/**
	* Generates limits from CGI for getEntityItemDataList()
	*
	* @return array limits
	*/

	protected function getEntityItemDataList_applyLimits()
	{
		$limits = array();

		$start = get_cgi_value('start', 'i');
		$limit = get_cgi_value('limit', 'i');
		
		if (! is_null($start) || ! is_null($limit))
		{
			if (is_null($start))
			{
				$start = '0';
			}

			if (is_null($limit))
			{
				$limit = '18446744073709551615'; // largest MySQL INT value (BIGINT type, 8 bytes)
			}

			$limits = array($start, $limit);
		}
		
		return $limits;
	}

	/**
	* Generates ORDER from CGI for getEntityItemDataList()
	*
	* @return string
	*/

	protected function getEntityItemDataList_applyOrder()
	{
		$order = '';
		
		$sfield = get_cgi_value('sort');
		
		if (! is_null($sfield))
		{
			$sorder = get_cgi_value('dir');
			if (! in_array($sorder, array('ASC', 'DESC'))) $sorder = 'ASC';
			
			$order = '`' . SQL::escape($sfield) . '` ' . $sorder;
		}
		
		return $order;
	}
	
	/**
	* Generates GROUP BY from CGI for getEntityItemDataList()
	*
	* Default is empty string. Should be overridden in derived classes fro customization.
	*
	* @return string
	*/

	protected function getEntityItemDataList_applyGroup()
	{
		return '';
	}
	
	/**
	* Generates filtering condition from CGI for getEntityItemDataList()
	*
	* @param string $condition  base condition if any; set to 'false' (string) to turn off any filtering
	*
	* @return string condition with filtering applied if requested
	*/

	protected function getEntityItemDataList_applyFilters($condition)
	{
		$filters = get_cgi_value('filter', 'a');
		
		if ($condition != 'false' && ! is_null($filters))
		{
			if (is_null($condition)) $condition = 'true';
				
				
			foreach ($filters as $filter)
			{
				$f = $filter['field'];
				
				
				switch ($filter['data']['type'])
				{
					case 'list':
						$values = explode(',', $filter['data']['value']);
						
						$condition .= ' AND ' . SQL::in($f, $values);
					break;
					
					case 'string':
						$v = $filter['data']['value'];
						
						$condition .= " AND $f LIKE '{$v}%'";
					break;
				}
			}
		}
		
		return $condition;
	}
	
	/**
	* Returns item data list of specified entity
	*
	* Method is used for grid-like UI components.
	* Method can process some dependencies, supports filtering, grouping, sorting and paging controlled by CGI parameters,
	* triggers "beforeloadlist" and "afterloadlist" entity handlers
	*
	* Only these dependencies are processed: 1:n as child and n:n as child.
	*
	* If 'filter' CGI value specified (as array of filters), the list is filtered by every value for every field specified,
	* according to the value data type.
	*
	* No grouping is done by default. Derived classes may implement its own grouping.
	*
	* If 'sort' CGI value specified, the list is sorted by that field name in order specified by 'dir' CGI value or 'ASC' by default.
	*
	* 'start' and 'limit' CGI values are applied as LIMITS if both are specified.
	* If 'paging' CGI value is true then:
	* - overall record count is returned in result as 'total_count'
	* - one page of list is returned in result as 'list'
	* otherwise:
	* - all record list is returned in result as 'list'
	*
	* @param string $entity_name entity name
	*
	* @return array
	*/

	protected function getEntityItemDataList($entity_name, $depends = NULL)
	{
		$result = array();
		

		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity '{$entity_name}' not found");
			

		// $permissions = $this->getPermissionValues();

		// if ($permissions[$entity_name]['load'] != 1) throw new AMException("Operation 'load' not allowed on entity '{$entity_name}'");

		
		$entity_config = $this->dataModelConfig['entities'][$entity_name];


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();

		
		// $dm = $this->dataModel();
		// $amEntity = $dm->entity(ApplicationDataModel::makeModuleEntityName($this->moduleName, $entity_name));
		$amEntity = $this->entity($entity_name);
		

		$condition = NULL;
		
		$depends = ! is_null($depends) ? $depends : get_cgi_value('depends');
		if (! is_null($depends))
		{
			include_once('class.JSON.php');
			
			$depends = JSON::decode($depends, SERVICES_JSON_LOOSE_TYPE);
			

			$depends_conditions = array();
			foreach ($amEntity->linkEndPoints() as $p)
			{
				if ($p->isFrom())
				{
					/*
					$amLink = $p->link();
					$to_entity_name = $amLink->toEntity->getName();
					
					if (isset($depends[$to_entity_name]))
					{
						switch ($amLink->getType())
						{
							case: '1:n'
							break;
							
							case: 'n:n'
							break;
						}
						
						$filter[$entity_name]
					}
					*/
					
					// Experimental, reversed n:n

					$amLink = $p->link();
					$entity_name = strtolower($amLink->toEntity->getName());
					
					if (isset($depends[$entity_name]))
					{
						switch ($amLink->getType())
						{
							case '1:n':
								// not implemented
							break;
							
							case 'n:n':
								$_depends_conditions = $this->getEntityItemDataList_processNNLink($entity_name, $depends, $amLink, true); // reversed
								$depends_conditions = array_merge($depends_conditions, $_depends_conditions);
							break;
						}
					}
				}
				else // $p->isTo()
				{
					$amLink = $p->link();
					$entity_name = strtolower($amLink->fromEntity->getName());
					
					if (isset($depends[$entity_name]))
					{
						switch ($amLink->getType())
						{
							case '1:n':
								foreach ($amLink->DMLink()->fieldPairs() as $field_pair)
								{
									$from_field_name = $field_pair[0]->getName();
									$to_field_name = $field_pair[1]->getName();
									$to_field_prepared_name = $field_pair[1]->ORMField()->getPreparedName();
									
									if (! isset($depends[$entity_name][$from_field_name])) throw new AMException("Dependant entity {$entity_name} link field $from_field_name not specified.");
									
									$from_field_prepared_value = SQL::prepare_value($depends[$entity_name][$from_field_name]);

									// $condition[$to_field_name] = $depends[$entity_name][$from_field_name];
									$depends_conditions[] = "($to_field_prepared_name = $from_field_prepared_value)";
								}
							break;
							
							case 'n:n':
								$_depends_conditions = $this->getEntityItemDataList_processNNLink($entity_name, $depends, $amLink);
								$depends_conditions = array_merge($depends_conditions, $_depends_conditions);
							break;
						}
					}
				}
			}
			
			
			/*
			if (is_null($condition)) // if not modified - set to false, because depends is given, so no rows should be returned
			{
				$condition = 'false';
			}
			*/
			if (! empty($depends_conditions))
			{
				$condition = implode(' AND ', $depends_conditions);
			}
			else // if empty - set to false, because depends is given, so no rows should be returned
			{
				$condition = 'false';
			}
		}

		
		// Apply filters
		
		$condition = $this->getEntityItemDataList_applyFilters($condition);

		
		/*
		// Particular records request
		if (! is_null($pri_key_name))
		{
			$ids = get_cgi_value('ids', 'ai');
			
			if (! is_null($ids))
			{
				$condition = SQL::in($pri_key_name, $ids);
			}
		}
		*/
		
		
		// Apply grouping

		$group = $this->getEntityItemDataList_applyGroup();
		
		
		// Apply sorting

		$order = $this->getEntityItemDataList_applyOrder();
		

		// Apply limits
		
		$limits = $this->getEntityItemDataList_applyLimits();
			

		// Triggering events
		
		if (isset($handlers['beforeloadlist']))
		{
			$data = call_user_func($handlers['beforeloadlist'], $condition, $group, $order, $limits);
			
			$condition = $data['condition'];
			$group = $data['group'];
			$order = $data['order'];
			$limits = $data['limits'];
		}
		
		
		// Allow for paging 
		
		$paging = get_cgi_value('paging', 'b');
		
		if ($paging)
		{
			$result['total_count'] = $amEntity->DMEntity()->getItemCount($condition, $group);
		}

		
		// Load item data list		
		$result['list'] = $amEntity->DMEntity()->getItemDataListAll($condition, $group, $order, $limits);


		// Triggering events
		
		if (isset($handlers['afterloadlist']))
		{
			$result = call_user_func($handlers['afterloadlist'], $result);
		}
				
		
		return $result;
	}

	/**
	*
	*
	* @return array
	*/

	public function onGetEntityItemDataList($params = array())
	{
		$result = array();
		

		$entity_name = get_cgi_value('entity_name');
		dump('deprecated action call: onGetEntityItemDataList()' . (! is_null($entity_name) ? ' for entity ' . $this->moduleName . '::' . $entity_name : ''));	
		return $this->getEntityItemDataList($entity_name);
	}

	
	/**
	*
	*
	* @return array
	*/

	public function onExportEntityItemData()
	{
		$entity_name = get_cgi_value('entity_name');


		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");
		
		
		$this->ensureEntityPermission($entity_name, 'export');
		
		
		// $entity_id = get_cgi_value('entity_id', 'i');
		$record_ids = get_cgi_value('record_ids');
		

		$entity_item = $this->invokeModuleMethod('Modules', 'getModuleEntityItem', array($this->getName(), $entity_name));
		$entity_id = $entity_item['id'];
		
		
		if (! empty($record_ids))
		{
			$record_ids = explode(',', $record_ids);
		}
		else
		{
			$record_ids = array();
		}


		$this->invokeModuleMethod('Modules', 'exportModuleEntity', array(array($entity_id), $record_ids));
	}

	
	protected function ensureImageSetOptions(& $options)
	{
		// Set default options
		if (is_null($options)) $options = array(array('field_name_template' => '%image_name%_id'));
	}
	
	protected function applyImageProcessing($file_path, $processing, $use_copy = false)
	{
		include_once 'class.ImageTools.php';
		

		$frame_width  = $processing['frame_width'];
		$frame_height = $processing['frame_height'];
		
		
		$use_quality = false;
		if (isset($processing['q']) && $use_quality = true)
			ImageTools::setDefaultQuality(['jpeg' => $processing['q']]);
		

		// Apply processing
		
		$target_path = $file_path;
		if ($use_copy)
		{
			$target_path = tempnam('.', time());
			copy($file_path, $target_path);
		}
		
		
		switch ($processing['name'])
		{
			case 'fit':
				ImageTools::fitImageFile($target_path, $frame_width, $frame_height);
			break;
			
			case 'fit_reduce_only':
				ImageTools::fitReduceOnlyImageFile($target_path, $frame_width, $frame_height);
			break;
				
			case 'fit_crop':
				ImageTools::fitCropImageFile($target_path, $frame_width, $frame_height);
			break;
			
			default:
				trigger_error('Unknown processing name', E_USER_ERROR);
			break;
		}
		
		clearstatcache();
		
		if ($use_quality) ImageTools::restoreDefaultQuality();
		
		
		return $target_path;
	}

	/**
	* options format:
	* array
		(
			'<image name>' => array
			(
				'field_name_template' => '<template>' // %image_name% is replaced with $image_name
				[
					'processing' => array
					(
						'name' => '<processing name>',
						[
							'frame_width' => <frame_width>,
							'frame_height' => <frame_height>
						]
					)
				]
			),
			
			[next image options]
		)
	*
	* @param array $item_data
	* @param string $image_name
	* @param array|NULL $image_set_options
	*
	* @return bool
	*/

	public function updateItemMassstorageImageSet($item_data, $image_name, $image_set_options = NULL, $file_path = NULL, $custom_file_name = '')
	{
		$r = array();

		
		$this->ensureImageSetOptions($image_set_options);
		

		$from_cgi = false;
		if (is_null($file_path))
		{
			$from_cgi = true;
			$file_path = get_cgi_file($image_name);
		}
		
		if (! is_null($file_path))
		{
			$s = $this->getImageMassStorage(); // includes FileMassStorageHelper class


			if ($from_cgi)
			{
				$file_mime_type = $_FILES[$image_name]['type'];
				$file_name      = $_FILES[$image_name]['name'];
			}
			else // from passed file
			{
				$file_mime_type = FileMassStorageHelper::getMimeType($file_path);
				$file_name = ! empty($custom_file_name) ? $custom_file_name : basename($file_path);
			}


			$s->setDefaultMimeType($file_mime_type); // for 'create' purposes
			

			foreach ($image_set_options as $option)
			{
				// Make field name from template
				$field_name = str_replace('%image_name%', $image_name, $option['field_name_template']);
				$image_id = $item_data[$field_name];

				
				// Update/create file (with processing if any)
				

				$tmp_path = NULL;
				
				if (isset($option['processing'])) $tmp_path = $this->applyImageProcessing($file_path, $option['processing'], true);

				// Update/create image file
				
				$actual_path = $tmp_path ?: $file_path;
				
				if (! empty($image_id)) // update
				{
					$f = $s->getItem($image_id);
					$f->setDefaultMimeType($file_mime_type);
					$f->updateFromFile($actual_path, $file_name);
				}
				else // create
				{
					$f = $s->createItemFromFile($actual_path, $file_name);
					
					// Store file id with field name
					$r[$field_name] = $f->getId(); // schedule ext key (new file id) for update
				}


				// Cleanup
				if ($tmp_path) @unlink($tmp_path);
			}
		}
		
		
		return $r;
	}
	
	protected function updateItemMassstorageImageSetFromCGI($item_data, $image_name, $image_set_options = NULL)
	{
		return $this->updateItemMassstorageImageSet($item_data, $image_name, $image_set_options);
	}


	/**
	*
	*
	* @param string $image_name
	* @param array|NULL
	* @param string|NULL $file_path
	* @param string $custom_file_name
	*
	* @return array
	*/

	public function createItemMassstorageImageSet($image_name, $image_set_options = NULL, $file_path = NULL, $custom_file_name = '')
	{
		$r = array();


		$this->ensureImageSetOptions($image_set_options);


		$from_cgi = false;
		if (is_null($file_path))
		{
			$from_cgi = true;
			$file_path = get_cgi_file($image_name);
		}
		
		if (! is_null($file_path))
		{
			$s = $this->getImageMassStorage(); // includes FileMassStorageHelper class


			if ($from_cgi)
			{
				$file_mime_type = $_FILES[$image_name]['type'];
				$file_name      = $_FILES[$image_name]['name'];
			}
			else
			{
				$file_mime_type = FileMassStorageHelper::getMimeType($file_path);
				$file_name = ! empty($custom_file_name) ? $custom_file_name : basename($file_path);
			}
			
			
			$s->setDefaultMimeType($file_mime_type); // for 'create' purposes


			foreach ($image_set_options as $option)
			{
				// Make field name from template
				$field_name = str_replace('%image_name%', $image_name, $option['field_name_template']);
				

				// Create file (with processing if any)
				
				$tmp_path = NULL;
				
				if (isset($option['processing'])) $tmp_path = $this->applyImageProcessing($file_path, $option['processing'], true);
				

				// Create image file
				
				$f = $s->createItemFromFile($tmp_path ?: $file_path, $file_name);

				// Store file id with field name
				$r[$field_name] = $f->getId(); // schedule ext key (new file id) for insert
				
				
				// Cleanup
				if ($tmp_path) @unlink($tmp_path);
			}
		}
		else
		{
			foreach ($image_set_options as $option)
			{
				// Make field name from template
				$field_name = str_replace('%image_name%', $image_name, $option['field_name_template']);

				// Nullify values for insert
				$r[$field_name] = 0;
			}
		}
		
		
		return $r;
	}

	protected function createItemMassstorageImageSetFromCGI($image_name, $image_set_options = NULL)
	{
		return $this->createItemMassstorageImageSet($image_name, $image_set_options);
	}
	
	
	private function deleteItemMassstorageImageSet($item_data, $image_name, $image_set_options = NULL)
	{
		$r = array();


		// Set default options
		if (is_null($image_set_options)) $image_set_options = array(array('field_name_template' => '%image_name%_id'));
		

		$image_ids = array();
		
		
		// Collect image ids
		
		foreach ($image_set_options as $option)
		{
			// Make field name from template
			$field_name = str_replace('%image_name%', $image_name, $option['field_name_template']);

			// Get existing image id
			$image_id = $item_data[$field_name];
			
			// Schedule image deletion if any
			if (! empty($image_id))
			{
				$image_ids[] = $image_id; // schedule image id for deletion

				$r[$field_name] = 0; // schedule ext key for update
			}
		}


		// Delete images if any
		if (! empty($image_ids))
		{
			$s = $this->getImageMassStorage();

			$s->deleteItems($image_ids);
		}
		
		
		
		return $r;
	}

	

	/**
	*
	*
	* @param string $image_name
	* @param array $options
	* @param array $item_data
	* @param array $values
	*
	* @return array
	*/

	private function processImageOnUpdate($image_name, $options, $item_data, $values, & $modified)
	{
		$delete = get_cgi_value($image_name . '__delete', 'i', 0);
		if ($delete)
		{
			$image_values = $this->deleteItemMassstorageImageSet($item_data, $image_name, $options);
			
			// Update IDs
			$values = array_merge($values, $image_values);

			
			$modified = true;
		}
		else
		{
			$file_path = get_cgi_file($image_name);
			if (! is_null($file_path)) // if value has been provided
			{
				$original_field_name = $image_name . '_id';
	
				if (! empty($item_data[$original_field_name]))
				{
					$image_values = $this->updateItemMassstorageImageSetFromCGI($item_data, $image_name, $options);
				}
				else
				{
					$image_values = $this->createItemMassstorageImageSetFromCGI($image_name, $options);
				}
				
				// Update IDs
				$values = array_merge($values, $image_values);
				
				
				$modified = true;
			}
		}
		
		
		return $values;
	}

	/**
	*
	*
	* @param string $image_name
	* @param array $options
	* @param array $values
	*
	* @return array
	*/

	private function processImageOnCreate($image_name, $options, $values)
	{
		$delete = get_cgi_value($image_name . '_delete', 'i', 0); // delete checkbox
		if (! $delete)
		{
			$image_values = $this->createItemMassstorageImageSetFromCGI($image_name, $options);
			
			$values = array_merge($values, $image_values);
		}

		return $values;
	}
	
	/**
	*
	*
	* @param array $entity_config
	* @param array $data_field_values
	*
	* @return array
	*/

	public function processFilesAndImagesOnCreate($entity_config, $data_field_values)
	{
		if ($this->config['files_enabled'])
		{
			// $data_field_values =& $insert_data[$amEntity->getName()]['values'];

			// Process files

			foreach ($entity_config['fields'] as $field_config)
			{
				$field_type = $field_config['type'];
				$field_name = $field_config['name'];

				if ($field_type == 'massstoragefile') // catch 'massstoragefile' type
				{
					if (! is_null($data_field_values[$field_name])) // if value has been provided
					{
						$s = $this->getFileMassStorage();
						$s->setDefaultMimeType($_FILES[$field_name]['type']);
						$f = $s->createItemFromFile($data_field_values[$field_name], $_FILES[$field_name]['name']);
	
						$data_field_values[$field_name] = $f->getId(); // schedule ext key (new file id) for insert
					}
					else
					{
						// always skip special field name from direct update
						unset($data_field_values[$field_name]); // some sort of hack
						// $data_field_values[$name] = 0;
					}
				}
			}

			
			// Process images

			if (isset($entity_config['image_fields']))
			{
				foreach ($entity_config['image_fields'] as $image_name => $options)
				{
					$data_field_values = $this->processImageOnCreate($image_name, $options, $data_field_values);
				}
			}
		}
		
		
		return $data_field_values;
	}
	
	/**
	*
	*
	* @param array $entity_config
	* @param array $item_data
	* @param array $data_field_values
	*
	* @return array
	*/

	public function processFilesAndImagesOnUpdate($entity_config, $item_data, $data_field_values, & $modified)
	{
		if ($this->config['files_enabled'])
		{
			// Process files

			foreach ($entity_config['fields'] as $field_config)
			{
				$field_type = $field_config['type'];
				$field_name = $field_config['name'];
				// $amEntity->field($field_name);
				
				if ($field_type == 'massstoragefile') // catch 'massstoragefile' type
				{
					// dump("file field '{$field_name}'");
					if (! is_null($data_field_values[$field_name])) // if value has been provided
					{
						// dump("value provided");
						if (! empty($item_data[$field_name])) // if file attached
						{
							// Update file item
							
							$s = $this->getFileMassStorage();
							$fi = $s->getItem($item_data[$field_name]);
							
							$fi->setDefaultMimeType($_FILES[$field_name]['type']);
							$fi->updateFromFile($data_field_values[$field_name], $_FILES[$field_name]['name']);

							
							// always skip special field name from direct update
							unset($data_field_values[$field_name]); // some sort of hack
							
							
							$modified = true;
						}
						else
						{
							// Add new file item
							// dump("create new file");
							$s = $this->getFileMassStorage();
							$s->setDefaultMimeType($_FILES[$field_name]['type']);
							$f = $s->createItemFromFile($data_field_values[$field_name], $_FILES[$field_name]['name']);
		
							$data_field_values[$field_name] = $f->getId(); // schedule ext key (new file id) for insert

							
							$modified = true; // not really necessary, because field value is modified an that fact is catched after SQL UPDATE (affected rows)
						}
					}
					else
					{
						// always skip special field name from direct update
						unset($data_field_values[$field_name]); // some sort of hack
						// $data_field_values[$name] = 0;
					}
				}
			}
			
			
			// Process images

			if (isset($entity_config['image_fields']))
			{
				foreach ($entity_config['image_fields'] as $image_name => $options)
				{
					$data_field_values = $this->processImageOnUpdate($image_name, $options, $item_data, $data_field_values, $modified);
				}
			}
		}
		
		
		return $data_field_values;
	}

	
	protected function createManyToManyLinks($master_item_side, $amLink, $pri_key_values, $ids_list) // right 'pri key'; left 'ids list'
	{
		// Set links
		
		// Get new item ids
		
		$ids = ! empty($ids_list) ? explode(',', $ids_list) : array();
		

		// Create new links
		if (! empty($ids))
		{
			if ($master_item_side == 'right')
			{
				$from_pk_values = array();
				foreach ($ids as $id)
				{
					$from_pk_values[] = SQL::pk($id);
				}
	
				$amLink->createItemLinks($from_pk_values, $pri_key_values);
			}
			else // assume 'left'
			{
				$to_pk_values = array();
				foreach ($ids as $id)
				{
					$to_pk_values[] = SQL::pk($id);
				}
	
				$amLink->createItemLinks($pri_key_values, $to_pk_values);
			}
		}
	}
	
	protected function replaceManyToManyLinks($master_item_side, $amLink, $pri_key_values, $ids_list) // right 'pri key'; left 'ids list'
	{
		$modified = false;
		
		
		// Set links
		
		// Get new item ids
		
		$ids = ! empty($ids_list) ? explode(',', $ids_list) : array();
		

		$new_ids = $ids;

		sort($new_ids, SORT_NUMERIC);
		

		// Get existing item ids
		if ($master_item_side == 'right')
		{
			$existing_ids = $amLink->getLeftItemDataListRaw('e.id', $pri_key_values, NULL, '', '', NULL, '', 'id');
		}
		else // assume 'left'
		{
			$existing_ids = $amLink->getRightItemDataListRaw('e.id', $pri_key_values, NULL, '', '', NULL, '', 'id');
		}
		
		sort($existing_ids, SORT_NUMERIC);
		

		// Compare and update
		
		if ($new_ids != $existing_ids)
		{
			// Update links
			
			// Drop existing links
			if ($master_item_side == 'right')
			{
				$amLink->dropAllFromItemLinks($pri_key_values);
			}
			else // assume 'left'
			{
				$amLink->dropAllToItemLinks($pri_key_values);
			}
	
			// Create new links
			if (! empty($ids))
			{
				// Make PKs
				$pk_values = array();
				foreach ($ids as $id)
				{
					$pk_values[] = SQL::pk($id);
				}
				
				
				if ($master_item_side == 'right')
				{
					$amLink->createItemLinks($pk_values, $pri_key_values);
				}
				else // assue 'left'
				{
					$amLink->createItemLinks($pri_key_values, $pk_values);
				}
			}
			

			$modified = true;
		}
		
		
		return $modified;
	}


	/**
	*
	*/
	
	private function processOneToManyLinksBeforeInsert($amEntity, $insert_data)
	{
		// Process 'to' 1:n links (don't check for 'required' values as it will be checked on insert)

		$linkEndPoints = $amEntity->linkEndPoints();
		foreach ($linkEndPoints as $linkEndPoint) // Cycle through all link end points of the entity
		{
			if ($linkEndPoint->isTo()) // pick up only 'to'
			{
				$amLink = $linkEndPoint->link();
				
				$type = $amLink->getType();
				
				if ($type == '1:n') // pick up only '1:n' type
				{
					$amFromEntity = $amLink->fromEntity;
					// $required = $amLink->toRequired;
					
					
					// Make 'depends' array for insert
					
					$depends = array();
					$fieldPairs = $amLink->DMLink()->fieldPairs();
					foreach ($fieldPairs as $fieldPair)
					{
						$dmFromField = $fieldPair[0];
						$dmToField = $fieldPair[1];
						
						
						// Get CGI value if any
						
						$to_name = $dmToField->getName();
						$to_value = get_cgi_value($to_name, 'i');
						
						if (empty($to_value)) // break on no value
						{
							$depends = array();
							break;
						}
						
						
						// Store master pair
						
						$from_name = $dmFromField->getName();
						
						$depends[$from_name] = $to_value;
					}
					
					if (! empty($depends))
					{
						$insert_data[$amEntity->getName()]['depends'][$amFromEntity->getName()] = $depends;
					}
				}
			}
		}
		
		
		return $insert_data;
	}
	
	/**
	*
	*/
	
	private function processOneToManyLinksBeforeUpdate($amEntity, $pri_key_values, $data_field_values)
	{
		// Process 'to' 1:n links

		$linkEndPoints = $amEntity->linkEndPoints();
		foreach ($linkEndPoints as $linkEndPoint) // Cycle through all link end points of the entity
		{
			if ($linkEndPoint->isTo()) // pick up only 'to'
			{
				$amLink = $linkEndPoint->link();
				// dump($amLink->getName());
				$type = $amLink->getType();
				
				if ($type == '1:n') // pick up only '1:n' type
				{
					// Inject link values into values for update
					
					$link_values = array();
					$fieldPairs = $amLink->DMLink()->fieldPairs();
					foreach ($fieldPairs as $fieldPair)
					{
						$dmToField = $fieldPair[1];
						
						
						// Get CGI value if any
						
						$to_name = $dmToField->getName();
						$to_value = get_cgi_value($to_name, 'i');

						if (is_null($to_value)) // break on no value
						{
							$link_values = array();
							break;
						}
						
						
						// Store
						$link_values[$to_name] = $to_value;
					}
					
					if (! empty($link_values))
					{
						$data_field_values = array_merge($data_field_values, $link_values);
					}
				}
			}
		}
		
		
		return $data_field_values;
	}
	
	
	/**
	*
	*/
	
	private function processManyToManyLinksAfterInsert($amEntity, $pri_key_values)
	{
		// Process n:n links

		$linkEndPoints = $amEntity->linkEndPoints();
		foreach ($linkEndPoints as $linkEndPoint) // Cycle through all link end points of the entity
		{
			$amLink = $linkEndPoint->link();
			$type = $amLink->getType();
			
			if ($type == 'n:n') // only single field (id) keys supported
			{
				if ($linkEndPoint->isTo()) // pick up only 'to'
				{
					$amLeftEntity = $amLink->fromEntity;
					
					// Get CGI values if any
					
					$from_name = $amLeftEntity->getName() . '_ids_list';
					$from_ids_list = get_cgi_value($from_name);

					
					// Create links if any
					if (! is_null($from_ids_list))
					{
						$this->createManyToManyLinks('right', $amLink, $pri_key_values, $from_ids_list);
					}
				}
				else // assume 'from'
				{
					$amRightEntity = $amLink->toEntity;
					
					// Get CGI values if any
					
					$to_name = $amRightEntity->getName() . '_ids_list';
					$to_ids_list = get_cgi_value($to_name);

					
					// Create links if any
					if (! is_null($to_ids_list))
					{
						$this->createManyToManyLinks('left', $amLink, $pri_key_values, $to_ids_list);
					}
				}
			}
		}
	}

	/**
	*
	*/
	
	private function processManyToManyLinksAfterUpdate($amEntity, $pri_key_values)
	{
		$modified = false;
		
		
		// Process n:n links

		$linkEndPoints = $amEntity->linkEndPoints();
		foreach ($linkEndPoints as $linkEndPoint) // Cycle through all link end points of the entity
		{
			$amLink = $linkEndPoint->link(); 
			// dump('examine link "' . $amLink->getName() . '"');
			$type = $amLink->getType();
			
			if ($type == 'n:n') // only single field (id) keys supported
			{
				// dump('got n:n (otherwise skip)');
				if ($linkEndPoint->isTo()) // pick up 'to'
				{
					// dump('picked up "to"');
					$amLeftEntity = $amLink->fromEntity;
					
					// Get CGI values if any
					
					$from_name = $amLeftEntity->getName() . '_ids_list';
					// dump('expect name "'.$from_name.'"');
					$from_ids_list = get_cgi_value($from_name);
					

					// Create links if any
					if (! is_null($from_ids_list))
					{
						$m = $this->replaceManyToManyLinks('right', $amLink, $pri_key_values, $from_ids_list);
						$modified = $modified || $m;
						// dump('A:' . $modified);
					}
				}
				else // assume 'from'
				{
					// dump('picked up "from"');
					$amRightEntity = $amLink->toEntity;
					
					// Get CGI values if any
					
					$to_name = $amRightEntity->getName() . '_ids_list';
					// dump('expect name "'.$_name.'"');
					$to_ids_list = get_cgi_value($to_name);
					

					// Create links if any
					if (! is_null($to_ids_list))
					{
						$m = $this->replaceManyToManyLinks('left', $amLink, $pri_key_values, $to_ids_list);
						$modified = $modified || $m;
						// dump('B:' . $modified);
					}
				}
			}
		}
		
		
		return $modified;
	}


	/**
	*
	*
	* @param string $entity_name
	*/

	public function saveEntityItemDataFromCGI($entity_name)
	{
		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");


		$entity_config = $this->dataModelConfig['entities'][$entity_name];
		

		// $permissions = $this->getModulePermissions();
		// $permissions = $this->getPermissionValues();


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();
								

		$amEntity = $this->entity($entity_name);
		

		$pri_key_values = array();
		
		$_id = get_cgi_value('id', 'i');
		if (! is_null($_id)) $pri_key_values = array('id' => $_id);
		
		
		$data_field_values = array();
		foreach ($entity_config['fields'] as $field_config)
		{
			if ($field_config['name'] == 'id') continue;
			if ($field_config['managed'] == false) continue;
			
			$data_field_values[$field_config['name']] = $amEntity->field($field_config['name'])->getCGIValue();
		}

		if (isset($handlers['beforesave']) && isset($handlers['beforesave2']))
			throw new Exception("Both beforesave and beforesave2 handlers given which is forbidden. beforesave is deprecated, use beforesave2.");

		if (! empty($pri_key_values))
		{
			// Check permissions
			
			// if ($permissions[$entity_name]['edit'] != 1) throw new AMException("Operation 'edit' not allowed on entity '{$entity_name}'");
			$this->ensureEntityPermission($entity_name, 'edit');
	

			// Update existing item data

			
			// Check for existance
			
			$item_data = $amEntity->getItemDataAll($pri_key_values);
			
			if (empty($item_data)) throw new AMException("Entity '{$entity_name}' item  with PK '" . serialize($pri_key_values) . "' not found");
			

			// Process 'to' 1:n
			$data_field_values = $this->processOneToManyLinksBeforeUpdate($amEntity, $pri_key_values, $data_field_values);
			

			// Triggering events
			
			if (isset($handlers['beforeupdate']))
			{
				$data_field_values = call_user_func($handlers['beforeupdate'], $pri_key_values, $data_field_values, $item_data);
			}
			if (isset($handlers['beforesave'])) // deprecated
			{
				$data_field_values = call_user_func($handlers['beforesave'], $pri_key_values, $data_field_values, $item_data);
			}
			elseif (isset($handlers['beforesave2']))
			{
				$data_field_values = call_user_func($handlers['beforesave2'], $pri_key_values, $data_field_values, NULL, $item_data);
			}


			$modified = false;
			
			
			// Process files and images if any
			$m = false;
			$data_field_values = $this->processFilesAndImagesOnUpdate($entity_config, $item_data, $data_field_values, $m);
			$modified = $modified || $m;


			// Update item data if any
			if (! empty($data_field_values))
			{
				$affected = $amEntity->updateItemData($data_field_values, $pri_key_values);
				$modified = $modified || ($affected > 0);
			}

			
			// Process n:n links
			$m = $this->processManyToManyLinksAfterUpdate($amEntity, $pri_key_values);
			$modified = $modified || $m;


			// Triggering events
			
			if (isset($handlers['aftersave']))
			{
				$data_field_values = call_user_func($handlers['aftersave'], $pri_key_values, $data_field_values, NULL, $item_data);
			}
			if (isset($handlers['afterupdate']))
			{
				$data_field_values = call_user_func($handlers['afterupdate'], $pri_key_values, $data_field_values, $item_data, $modified);
			}
		}
		else
		{
			// Check permissions
			
			// if ($permissions[$entity_name]['add'] != 1) throw new AMException("Operation not allowed");
			$this->ensureEntityPermission($entity_name, 'add');
	

			// Add new item

			/*
			// Check parent item for existance
			
			$group = $dbc->getRowAll($group_table_name, array($group_table_pri_key_name => $ext_key_value));
			
			if (empty($group)) throw new AMNewsException("Group not found");
			*/

			// Add
			$insert_data = array
			(
				$amEntity->getName() => array
				(
					'values' => $data_field_values
					/*
					??????????????
					,
					'depends' => array
					(
						$parent_entity_name => array
						(
							'id' => 2
						)
					)
					*/
				)
			);

			
			// Process 'to' 1:n links (don't check for 'required' values as it will be checked on insert)
			$insert_data = $this->processOneToManyLinksBeforeInsert($amEntity, $insert_data);
			
			
			// Triggering events
			
			if (isset($handlers['beforeinsert']))
			{
				$insert_data = call_user_func($handlers['beforeinsert'], $insert_data);
			}
			if (isset($handlers['beforesave'])) // deprecated
			{
				$insert_data[$amEntity->getName()]['values'] = call_user_func($handlers['beforesave'], NULL, $insert_data[$amEntity->getName()]['values']);
			}
			elseif (isset($handlers['beforesave2']))
			{
				$insert_data[$amEntity->getName()]['values'] = call_user_func($handlers['beforesave2'], NULL, $insert_data[$amEntity->getName()]['values'], $insert_data, NULL);
			}
			

			// Process files and images if any
			$insert_data[$amEntity->getName()]['values'] = $this->processFilesAndImagesOnCreate($entity_config, $insert_data[$amEntity->getName()]['values']);


			// Insert item data
			$pri_key_values = $amEntity->insertItemData($insert_data);


			// Process n:n links
			$this->processManyToManyLinksAfterInsert($amEntity, $pri_key_values);


			// Triggering events
			
			if (isset($handlers['aftersave']))
			{
				$data_field_values = call_user_func($handlers['aftersave'], $pri_key_values, $data_field_values, $insert_data, NULL);
			}
			if (isset($handlers['afterinsert']))
			{
				call_user_func($handlers['afterinsert'], $pri_key_values, $data_field_values, $insert_data);
			}
		}
	}
	
	/**
	*
	*
	*/

	public function onSaveEntityItemData()
	{
		$entity_name = get_cgi_value('entity_name');
		
		
		$this->saveEntityItemDataFromCGI($entity_name);
	}


	/**
	*
	*
	* @param string $image_name
	* @param array $options
	* @param array $item_data
	*/

	private function processImageOnDelete($image_name, $options, $item_data)
	{
		// Collect image ids
		
		$image_ids = array();
		foreach ($options as $option)
		{
			// Make field name from template
			$field_name = str_replace('%image_name%', $image_name, $option['field_name_template']);

			if (! empty($item_data[$field_name])) $image_ids[] = $item_data[$field_name]; 
		}

		if (! empty($image_ids))
		{
			$s = $this->getImageMassStorage();

			$s->deleteItems($image_ids);
		}
	}

	/**
	*
	*
	* @param string $entity_name
	*/

	public function deleteEntityItemDataFromCGI($entity_name)
	{
		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");


		$entity_config = $this->dataModelConfig['entities'][$entity_name];
		

		// $permissions = $this->getModulePermissions();
		// $permissions = $this->getPermissionValues();


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();
								

		$amEntity = $this->entity($entity_name);
		
		/*
		$pri_key_values = array();
		
		$_id = get_cgi_value('id', 'i');
		if (! is_null($_id)) $pri_key_values = array('id' => $_id);
		*/
		$pri_keys = array();
		
		// single record
		
		$_id = get_cgi_value('id', 'i');
		if (! is_null($_id)) $pri_keys = array(array('id' => $_id));
		
		if (empty($pri_keys))
		{
			// multi record
			
			$_ids = get_cgi_value('ids');
			
			if (! is_null($_ids))
			{
				$_ids = explode(',', $_ids);
				foreach ($_ids as $_id)
				{
					$pri_keys[] = array('id' => $_id);
				}
			}
		}
		
		if (! empty($pri_keys))
		{
			// Check permissions
			
			// if ($permissions[$entity_name]['delete'] != 1) throw new AMException("Operation 'delete' not allowed on entity '{$entity_name}'");
			$this->ensureEntityPermission($entity_name, 'delete');
	

			foreach ($pri_keys as $pri_key_values)
			{
				// Check for existance
				
				$item_data = $amEntity->getItemDataAll($pri_key_values);
				
				if (empty($item_data)) throw new AMException("Entity '{$entity_name}' item  with PK '" . serialize($pri_key_values) . "' not found");
				
			
				// Triggering events
				
				if (isset($handlers['beforedelete']))
				{
					call_user_func($handlers['beforedelete'], $pri_key_values, $amEntity, $item_data);
				}
					
					
				// Delete
				$amEntity->deleteItemData($pri_key_values);
	
				
				// Triggering events
				
				if (isset($handlers['afterdelete']))
				{
					call_user_func($handlers['afterdelete'], $pri_key_values, $amEntity, $item_data);
				}
	
	
				if ($this->config['files_enabled'])
				{
					// Process files
	
					$file_ids = array();
					foreach ($entity_config['fields'] as $field_config)
					{
						$field_type = $field_config['type'];
						$field_name = $field_config['name'];
		
						if ($field_type == 'massstoragefile') // catch 'massstoragefile' type
						{
							if (! empty($item_data[$field_name])) // if file attached
							{
								$file_ids[] = $item_data[$field_name];
							}
						}
					}
					
					if (! empty($file_ids))
					{
						$s = $this->getFileMassStorage();
						$s->deleteItems($file_ids);
					}
					
					
					// Process images
					
					if (isset($entity_config['image_fields']))
					{
						foreach ($entity_config['image_fields'] as $image_name => $options)
						{
							$this->processImageOnDelete($image_name, $options, $item_data);
						}
					}
				}
			} // foreach ($pri_keys as $pri_key_values)
		} // if (! empty($pri_keys))
	}
	
	/**
	*
	*
	*/

	public function onDeleteEntityItemData()
	{
		$entity_name = get_cgi_value('entity_name');
		
		
		$this->deleteEntityItemDataFromCGI($entity_name);
	}
	
	
	/**
	*
	*
	* @param string $entity_name
	*
	* @return array
	*/

	public function loadEntityItemData($entity_name)
	{
		$result = array();

		
		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");
			

		$entity_config = $this->dataModelConfig['entities'][$entity_name];
		

		// $permissions = $this->getModulePermissions();
		// $permissions = $this->getPermissionValues();


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();
		

		$amEntity = $this->entity($entity_name);


		// Get pri key
		
		$pri_key_values = array();
		
		$_id = get_cgi_value('id', 'i');
		if (! is_null($_id)) $pri_key_values = array('id' => $_id);
		
		
		// Process action
		if (! empty($pri_key_values))
		{
			// Check permissions
			
			// if ($permissions[$entity_name]['load'] != 1) throw new AMException("Operation 'load' not allowed on entity '{$entity_name}'");
			$this->ensureEntityPermission($entity_name, 'load');
			
			
			// Get item data
			
			$item_data = $amEntity->getItemDataAll($pri_key_values);
			
			if (empty($item_data)) throw new AMException("Entity '{$entity_name}' item  with PK '" . serialize($pri_key_values) . "' not found");
			
		
			$result = array('data' => $item_data);
		}	


		return $result;
	}
	
	/**
	*
	*
	* @return array
	*/

	public function onLoadEntityItemData()
	{
		$result = array();
		
		
		$entity_name = get_cgi_value('entity_name');
	
		return $this->loadEntityItemData($entity_name);
	}


	/**
	*
	*
	* @param string $entity_name
	*
	* @return array
	*/

	public function loadEntityNodeData($entity_name)
	{
		$result = array();

		
		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");
			

		$entity_config = $this->dataModelConfig['entities'][$entity_name];
		

		// $permissions = $this->getModulePermissions();
		// $permissions = $this->getPermissionValues();


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();
		

		$amEntity = $this->entity($entity_name);


		// Get pri key
		
		$pri_key_values = array();
		
		$_id = get_cgi_value('id', 'i');
		if (! is_null($_id)) $pri_key_values = array('id' => $_id);
		
		
		// Process action
		if (! empty($pri_key_values))
		{
			// Check permissions
			
			// if ($permissions[$entity_name]['load'] != 1) throw new AMException("Operation 'load' not allowed on entity '{$entity_name}'");
			$this->ensureEntityPermission($entity_name, 'load');
			
			
			// Get node data
			
			$item_data = $amEntity->getItemDataAll($pri_key_values);
			
			if (empty($item_data)) throw new AMException("Entity '{$entity_name}' item  with PK '" . serialize($pri_key_values) . "' not found");
			
		
			$result = array('data' => $item_data);
		}	


		return $result;
	}
	
	/**
	*
	*
	* @return array
	*/

	public function onLoadEntityNodeData()
	{
		$result = array();
		
		
		$entity_name = get_cgi_value('entity_name');
	
		return $this->loadEntityNodeData($entity_name);
	}

	/**
	*
	*
	* @param string $entity_name
	*
	* @return array
	*/

	public function loadEntityNodeDataList($entity_name)
	{
		$response = array('text' => '', 'charset' => 'utf-8');

		
		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");
			

		$entity_config = $this->dataModelConfig['entities'][$entity_name];
		

		// $permissions = $this->getModulePermissions();
		// $permissions = $this->getPermissionValues();


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();
		

		$amEntity = $this->entity($entity_name);


		// Check permissions
			
		// if ($permissions[$entity_name]['load'] != 1) throw new AMException("Operation 'load' not allowed on entity '{$entity_name}'");
		$this->ensureEntityPermission($entity_name, 'load');
			
		
		$node_id = get_cgi_value('node');
		
		
		try
		{
			// Collect data field names
			
			$entity_title_field_name = 'id';
			$field_names = array();
			foreach ($entity_config['fields'] as $field_config)
			{
				if ($field_config['entity_title']) $entity_title_field_name = $field_config['name'];
				
				$field_names[] = $field_config['name'];
			}
			
			
			// Collect link field names if any

			foreach ($amEntity->linkEndPoints() as $p)
			{
				if ($p->isFrom())
				{
					// Not yet supported
				}
				else // $p->isTo()
				{
					$amLink = $p->link();
					$entity_name = strtolower($amLink->fromEntity->getName());
					
						switch ($amLink->getType())
						{
							case '1:n':
								foreach ($amLink->DMLink()->toFields() as $to_field)
								{
									$field_names[] = $to_field->getName();
								}
							break;
							
							case 'n:n':
								
								// Not yet supported
							break;
						}
				}
			}
			
			
			$left_index_name = DMCelkoEntity::left_index_field_name;
			$right_index_name = DMCelkoEntity::right_index_field_name;


			$pri_key_values = array('id' => $node_id);
			$max_depth = 1;
			$pre_condition = NULL;
			
			// Triggering events
			if (isset($handlers['beforeloadnodelist']))
			{
				$data = call_user_func($handlers['beforeloadnodelist'], array('id' => $node_id), $max_depth, $pre_condition);
				
				$pri_key_values = $data['pri_key_values'];
				$max_depth = $data['max_depth'];
				$pre_condition = $data['pre_condition'];
			}


			$children = $amEntity->getChildItemDataListAll($pri_key_values, $max_depth, $pre_condition);


			// Triggering events
			if (isset($handlers['afterloadnodelist']))
			{
				$children = call_user_func($handlers['afterloadnodelist'], $pri_key_values, $children);
			}
			
			
			$js_array = '[]';
			if (! empty($children))
			{
				$elements = array();
				foreach ($children as $child)
				{
					$leaf = $child[$right_index_name] - $child[$left_index_name] > 1 ? false : true;
					
					$vprops = array();
					
					foreach ($field_names as $n)
					{
						$vprops[] = $n . ': "' . escape_javascript_string($child[$n]) . '"';
					}
					
					$vprops = implode(",\n", $vprops);
					
					
					$props = array
					(
						'id: ' . $child['id'],
						// 'text: "' . escape_javascript_string($child[$entity_title_field_name]) . '"',
						'text: "' . escape_javascript_string(htmlspecialchars($child[$entity_title_field_name])) . '"',
						'leaf: false', // force async node
						'values: {' . $vprops . '}'
					);

					if ($leaf)
					{
						// prevent "plus" icon
						$props[] = 'expanded: true'; // expand by default 
						$props[] = 'children: []'; // prevent load from server
						 
						// override default icon
						$props[] = 'cls: "x-tree-node-collapsed"';
					}

					$elements[] = '{' . implode(",\n", $props) . '}';
				}
				
				$js_array = '[' . implode(',', $elements) . ']';
			}
	
			$response['text'] = $js_array;
		}
		catch (DBConnectionException $e)
		{
			$response['code'] = 500;
			$response['message'] = 'Server failure';
			$response['text'] = $e->getMessage();
		}
		catch (AMProductsException $e)
		{
			$response['code'] = 500;
			$response['message'] = 'Server failure';
			$response['text'] = $e->getMessage();
		}

		
		return $response;
	}
	
	/**
	*
	*
	* @return array
	*/

	public function onLoadEntityNodeDataList()
	{
		$result = array();
		
		
		$entity_name = get_cgi_value('entity_name');
	
		return $this->loadEntityNodeDataList($entity_name);
	}

	/**
	*
	*
	* @param string $entity_name
	*/

	public function moveEntityNodeAfter($entity_name)
	{
		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity not found");
			

		$entity_config = $this->dataModelConfig['entities'][$entity_name];
		

		// $permissions = $this->getModulePermissions();
		// $permissions = $this->getPermissionValues();


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();
		

		$dm = $this->dataModel();
		$amEntity = $this->entity($entity_name);


		// Check permissions
			
		// if ($permissions[$entity_name]['move'] != 1) throw new AMException("Operation 'move' not allowed on entity '{$entity_name}'");
		$this->ensureEntityPermission($entity_name, 'move');



		$node_id = get_cgi_value('node_id', 'i');
		$sibling_node_id = get_cgi_value('sibling_node_id', 'i');
		$as_child = get_cgi_value('as_child', 'b');


		// Process action
		
		$dm->beginTransaction();
		
		try
		{
			$parent_item_data = $amEntity->getParentItemDataAll(array('id' => $node_id));
			$old_parent_id = $parent_item_data['id'];
			
			
			// Triggering events
			
			if (isset($handlers['beforemove']))
			{
				// call_user_func($handlers['beforemove'], $node_id, $sibling_node_id, $as_child);
			}
				
				
			// Move celko node
			$amEntity->moveNodeAfter($node_id, $sibling_node_id, $as_child);

			
			// Triggering events
			
			if (isset($handlers['aftermove']))
			{
				call_user_func($handlers['aftermove'], $old_parent_id, $node_id, $sibling_node_id, $as_child);
			}
			
			
			$dm->commit();
		}
		catch (Exception $e)
		{
			$dm->rollBack();
			
			throw $e;
		}
	}
	
	/**
	*
	*
	* @return array
	*/

	public function onMoveEntityNodeAfter()
	{
		$result = array();
		
		
		$entity_name = get_cgi_value('entity_name');
	
		return $this->moveEntityNodeAfter($entity_name);
	}

	
	/**
	* @deprecated
	*/
	public function onSaveEntityField()
	{
		$entity_name = get_cgi_value('entity_name');

		
		$this->saveEntityField($entity_name);
	}
	
	/**
	* @deprecated
	*/
	public function onDeleteEntityField()
	{
		$entity_name = get_cgi_value('entity_name');


		$this->deleteEntityField($entity_name);
	}
	
	
	/**
	*
	*
	* @param string $type
	*
	* @return object
	*/

	protected function getMassStorage($type = 'file')
	{
		if (! $this->config['files_enabled']) throw new AMException("Could not get FileMassStorage for module {$this->moduleName} because files are NOT enabled for the module.");
			
			
		include_once('class.FileMassStorage.php');
		
		
		// new or old module ?
		$dmc = $this->getDataModelConfig();
		if (is_null($dmc))
		{
			// old module
			$table_name_prefix = '';
		}
		else
		{
			// new module
			$table_name_prefix = $this->getDBTablePrefix();
		}

		$table_name = $table_name_prefix . 'am_' . strtolower($this->moduleName) . '_filemassstorage';

		
		$c = $this->application->getConfig();
		$filestorage_root_dir = $c->get('application/filestorage_root_dir');
		$filestorage_dir = dirpath(dirpath($filestorage_root_dir) . 'am_' . strtolower($this->moduleName) . '/');
		

		$dbc = $this->getDefaultDBConnection();

		
		$fileStorageClassName = $type == 'image' ? 'ImageMassStorage' : 'FileMassStorage';
		
		
		return new $fileStorageClassName($dbc, $table_name, $filestorage_dir);
	}

	/**
	*
	*
	* @return object
	*/

	public function getFileMassStorage()
	{
		return $this->getMassStorage('file');
	}
	
	/**
	*
	*
	* @return object
	*/

	public function getImageMassStorage()
	{
		return $this->getMassStorage('image');
	}

	/**
	*
	*
	* @param string $disposition
	*/

	protected function getMassStorageFile($disposition = 'inline')
	{
		$id = get_cgi_value('id', 'i');
		
		
		try
		{
			$fms = $this->getFileMassStorage();
			$fms_item = $fms->getItem($id);
			$fms_item->send($disposition);
		}
		catch (FileMassStorageException $e)
		{
			echo $e->getMessage();
		}
		
		safe_exit();
	}

	/**
	*
	*
	*/

	public function onGetFile() // for backward compat.
	{
		$this->onGetFileInline();
	}

	/**
	*
	*
	*/

	public function onGetFileInline()
	{
		$this->getMassStorageFile();
	}

	/**
	*
	*
	*/

	public function onGetFileAttachment()
	{
		$this->getMassStorageFile('attachment');
	}
	
	

	/**
	*
	*
	*/

	public function onDefault($params)
	{
		$output = 'Not implemented';
		
		xbf_http_send_response($output, 'html');
	}
	
	

	/**
	* Generates limits from CGI for loadEntityItemDataList()
	*
	* @return array limits
	*/

	protected function loadEntityItemDataList_applyLimits($start = NULL, $limit = NULL)
	{
		$limits = array();

		if (! is_null($start) || ! is_null($limit))
		{
			if (is_null($start))
			{
				$start = '0';
			}

			if (is_null($limit))
			{
				$limit = '18446744073709551615'; // largest MySQL INT value (BIGINT type, 8 bytes)
			}

			$limits = array($start, $limit);
		}
		
		return $limits;
	}

	/**
	* Generates ORDER from CGI for loadEntityItemDataList()
	*
	* @return string
	*/

	protected function loadEntityItemDataList_applyOrder($sfield = NULL, $sorder = NULL)
	{
		$order = '';
		
		
		if (! is_null($sfield))
		{
			if (! in_array($sorder, array('ASC', 'DESC'))) $sorder = 'ASC';
			
			// $order = '`' . SQL::escape($sfield) . '` ' . $sorder;
			$order = SQL::prepare_name($sfield) . ' ' . $sorder;
		}
		
		return $order;
	}
	
	protected function loadEntityItemDataList_applyOrderCustom($sfield = NULL, $sorder = NULL)
	{
		$order = '';
		
		
		if (! is_null($sfield))
		{
			if (! in_array($sorder, array('ASC', 'DESC'))) $sorder = 'ASC';
			
			$order = $sfield . ' ' . $sorder;
		}
		
		return $order;
	}
	
	/**
	* Generates GROUP BY from CGI for loadEntityItemDataList()
	*
	* Default is empty string. Should be overridden in derived classes fro customization.
	*
	* @return string
	*/

	protected function loadEntityItemDataList_applyGroup()
	{
		return '';
	}
	
	/**
	* Generates filtering condition from CGI for getEntityItemDataListPipeline()
	*
	* @param string $condition  base condition if any; set to 'false' (string) to turn off any filtering
	* @param array  $filters    standard filters array 
	*
	* @return string condition with filtering applied if requested
	*/

	protected function loadEntityItemDataList_applyFilters($condition, $filters = NULL)
	{
		if ($condition != 'false' && ! is_null($filters))
		{
			if (is_null($condition)) $condition = 'true';
				
				
			foreach ($filters as $filter)
			{
				$f = $filter['field'];
				
				
				switch ($filter['data']['type'])
				{
					case 'list':
						$values = explode(',', $filter['data']['value']);
						
						$condition .= ' AND ' . SQL::in($f, $values);
					break;
					
					case 'string':
						$v = $filter['data']['value'];
						
						$condition .= " AND $f LIKE '{$v}%'";
					break;
				}
			}
		}
		
		return $condition;
	}
	

	protected function loadEntityItemDataList_processNNLink($entity_name, $depends, $amLink, $reverse = false)
	{
		$depends_conditions = array();
		
		
		if ($reverse)
		{
			$from_field_pairs = $amLink->DMLink()->toFieldPairs();
			$to_field_pairs   = $amLink->DMLink()->fromFieldPairs();
		}
		else
		{
			$from_field_pairs = $amLink->DMLink()->fromFieldPairs();
			$to_field_pairs   = $amLink->DMLink()->toFieldPairs();
		}

		
		$ormTable = $amLink->DMLink()->ORMTable();
		
		$link_condition = array();
		// foreach ($amLink->DMLink()->fromFieldPairs() as $field_pair)
		foreach ($from_field_pairs as $field_pair)
		{
			$from_field_name = $field_pair[0]->getName(); // DM
			$from_link_field_name = $field_pair[1]->getName(); // ORM

			if (! isset($depends[$entity_name][$from_field_name])) throw new AMException("Dependant entity {$entity_name} link field $from_field_name not specified.");
			
			$link_condition[$from_link_field_name] = $depends[$entity_name][$from_field_name];
		}

		$link_rows = $ormTable->getRowDataListAll($link_condition);
		
		if (! empty($link_rows))
		{
			$rows_conditions = array();
			
			foreach ($link_rows as $link_row)
			{
				$row_condition = array();
				
				// foreach ($amLink->DMLink()->toFieldPairs() as $field_pair)
				foreach ($to_field_pairs as $field_pair)
				{
					$to_field_name = $field_pair[0]->getName(); // DM
					$to_link_field_name = $field_pair[1]->getName(); // ORM
					
					
					$row_condition[] = $to_field_name .'='. $link_row[$to_link_field_name];
				}
				
				$rows_condition[] = implode(' AND ', $row_condition);
			}
			
		
			$depends_conditions[] = '(' . implode(') OR (', $rows_condition) . ')';
		}
		
		
		return $depends_conditions;
	}
	
	/**
	* Returns item data list of specified entity
	*
	* Method is used for grid-like UI components.
	* Method can process some dependencies, supports filtering, grouping, sorting and paging controlled by CGI parameters,
	* triggers "beforeloadlist" and "afterloadlist" entity handlers
	*
	* Only these dependencies are processed: 1:n as child and n:n as child.
	*
	* If 'filter' CGI value specified (as array of filters), the list is filtered by every value for every field specified,
	* according to the value data type.
	*
	* No grouping is done by default. Derived classes may implement its own grouping.
	*
	* If 'sort' CGI value specified, the list is sorted by that field name in order specified by 'dir' CGI value or 'ASC' by default.
	*
	* 'start' and 'limit' CGI values are applied as LIMITS if both are specified.
	* If 'paging' CGI value is true then:
	* - overall record count is returned in result as 'total_count'
	* - one page of list is returned in result as 'list'
	* otherwise:
	* - all record list is returned in result as 'list'
	*
	* @param string $entity_name entity name
	*
	* @return array
	*/

	protected function loadEntityItemDataList($entity_name, $depends, $filters, $sfield, $sorder, $start, $limit, $paging)
	{
		$result = array();


		if (is_null($this->dataModelConfig['entities']) || ! array_key_exists($entity_name, $this->dataModelConfig['entities'])) throw new AMException("Entity '{$entity_name}' not found");
			

		// $permissions = $this->getPermissionValues();

		// if ($permissions[$entity_name]['load'] != 1) throw new AMException("Operation 'load' not allowed on entity '{$entity_name}'");
		$this->ensureEntityPermission($entity_name, 'load');
		
		
		$entity_config = $this->dataModelConfig['entities'][$entity_name];


		$handlers = isset($entity_config['handlers']) ? $entity_config['handlers'] : array();

		
		// $dm = $this->dataModel();
		// $amEntity = $dm->entity(ApplicationDataModel::makeModuleEntityName($this->moduleName, $entity_name));
		$amEntity = $this->entity($entity_name);
		

		$condition = NULL;
		
		if (! is_null($depends))
		{
			include_once('class.JSON.php');
			
			$depends = JSON::decode($depends, SERVICES_JSON_LOOSE_TYPE);
			

			$depends_conditions = array();
			foreach ($amEntity->linkEndPoints() as $p)
			{
				if ($p->isFrom())
				{
					/*
					$amLink = $p->link();
					$to_entity_name = $amLink->toEntity->getName();
					
					if (isset($depends[$to_entity_name]))
					{
						switch ($amLink->getType())
						{
							case: '1:n'
							break;
							
							case: 'n:n'
							break;
						}
						
						$filter[$entity_name]
					}
					*/
					
					// Experimental, reversed n:n

					$amLink = $p->link();
					$entity_name = strtolower($amLink->toEntity->getName());
					
					if (isset($depends[$entity_name]))
					{
						switch ($amLink->getType())
						{
							case '1:n':
								// not implemented
							break;
							
							case 'n:n':
								$_depends_conditions = $this->loadEntityItemDataList_processNNLink($entity_name, $depends, $amLink, true); // reversed
								$depends_conditions = array_merge($depends_conditions, $_depends_conditions);
							break;
						}
					}
				}
				else // $p->isTo()
				{
					$amLink = $p->link();
					$entity_name = strtolower($amLink->fromEntity->getName());
					
					if (isset($depends[$entity_name]))
					{
						switch ($amLink->getType())
						{
							case '1:n':
								foreach ($amLink->DMLink()->fieldPairs() as $field_pair)
								{
									$from_field_name = $field_pair[0]->getName();
									$to_field_name = $field_pair[1]->getName();
									$to_field_prepared_name = $field_pair[1]->ORMField()->getPreparedName();
									
									if (! isset($depends[$entity_name][$from_field_name])) throw new AMException("Dependant entity {$entity_name} link field $from_field_name not specified.");
									
									$from_field_prepared_value = SQL::prepare_value($depends[$entity_name][$from_field_name]);

									// $condition[$to_field_name] = $depends[$entity_name][$from_field_name];
									$depends_conditions[] = "($to_field_prepared_name = $from_field_prepared_value)";
								}
							break;
							
							case 'n:n':
								/*
								$ormTable = $amLink->DMLink()->ORMTable();
								
								$link_condition = array();
								foreach ($amLink->DMLink()->fromFieldPairs() as $field_pair)
								{
									$from_field_name = $field_pair[0]->getName(); // DM
									$from_link_field_name = $field_pair[1]->getName(); // ORM

									if (! isset($depends[$entity_name][$from_field_name])) throw new AMException("Dependant entity {$entity_name} link field $from_field_name not specified.");
									
									$link_condition[$from_link_field_name] = $depends[$entity_name][$from_field_name];
								}

								$link_rows = $ormTable->getRowDataListAll($link_condition);
								
								if (! empty($link_rows))
								{
									$rows_conditions = array();
									
									foreach ($link_rows as $link_row)
									{
										$row_condition = array();
										
										foreach ($amLink->DMLink()->toFieldPairs() as $field_pair)
										{
											$to_field_name = $field_pair[0]->getName(); // DM
											$to_link_field_name = $field_pair[1]->getName(); // ORM
											
											
											$row_condition[] = $to_field_name .'='. $link_row[$to_link_field_name];
										}
										
										$rows_condition[] = implode(' AND ', $row_condition);
									}
									
								
									$depends_conditions[] = '(' . implode(') OR (', $rows_condition) . ')';
								}
								*/
								$_depends_conditions = $this->loadEntityItemDataList_processNNLink($entity_name, $depends, $amLink);
								$depends_conditions = array_merge($depends_conditions, $_depends_conditions);
							break;
						}
					}
				}
			}
			
			
			/*
			if (is_null($condition)) // if not modified - set to false, because depends is given, so no rows should be returned
			{
				$condition = 'false';
			}
			*/
			if (! empty($depends_conditions))
			{
				$condition = implode(' AND ', $depends_conditions);
			}
			else // if empty - set to false, because depends is given, so no rows should be returned
			{
				$condition = 'false';
			}
		}
		
		
		// Apply filters
		
		$condition = $this->loadEntityItemDataList_applyFilters($condition, $filters);
		
		
		/*
		// Particular records request
		if (! is_null($pri_key_name))
		{
			$ids = get_cgi_value('ids', 'ai');
			
			if (! is_null($ids))
			{
				$condition = SQL::in($pri_key_name, $ids);
			}
		}
		*/
		
		
		// Apply grouping

		$group = $this->loadEntityItemDataList_applyGroup();
		
		
		// Apply sorting

		$order = $this->loadEntityItemDataList_applyOrder($sfield, $sorder);
		

		// Apply limits
		
		$limits = $this->loadEntityItemDataList_applyLimits($start, $limit);
			

		// Triggering events
		
		if (isset($handlers['beforeloadlist']))
		{
			$data = call_user_func($handlers['beforeloadlist'], $condition, $group, $order, $limits);
			
			$condition = $data['condition'];
			$group = $data['group'];
			$order = $data['order'];
			$limits = $data['limits'];
		}
		
			
		// Allow for paging 
		
		if ($paging)
		{
			$result['total_count'] = $amEntity->DMEntity()->getItemCount($condition, $group);
		}

		
		// Load item data list		
		$result['list'] = $amEntity->DMEntity()->getItemDataListAll($condition, $group, $order, $limits);


		// Triggering events
		
		if (isset($handlers['afterloadlist']))
		{
			$result = call_user_func($handlers['afterloadlist'], $result);
		}
				
		
		return $result;
	}

	protected function getLoadEntityItemDataListParams($params)
	{
		$p = array();
		
		
		if (! empty($params))
		{
			$p['entity_name'] = array_get_value($params, 'entity_name');
			$p['depends'] = array_get_value($params, 'depends');
			$p['filters'] = array_get_value($params, 'filter');
			$p['sfield'] = array_get_value($params, 'sort');
			$p['sorder'] = array_get_value($params, 'dir');
			$p['start'] = array_get_value($params, 'start');
			if (! is_null($p['start'])) $p['start'] = intval($p['start']);
			$p['limit'] = array_get_value($params, 'limit');
			if (! is_null($p['limit'])) $p['limit'] = intval($p['limit']);
			$p['paging'] = array_get_value($params, 'paging');
			if (! is_null($p['paging'])) $p['paging'] = (bool)($p['paging']);
		}
		else
		{
			$p['entity_name'] = get_cgi_value('entity_name');
			$p['depends'] = get_cgi_value('depends');
			$p['filters'] = get_cgi_value('filter', 'a');
			$p['sfield'] = get_cgi_value('sort');
			$p['sorder'] = get_cgi_value('dir');
			$p['start'] = get_cgi_value('start', 'i');
			$p['limit'] = get_cgi_value('limit', 'i');
			$p['paging'] = get_cgi_value('paging', 'b');
		}
		
		
		return $p;
	}
	
	public function onLoadEntityItemDataList($params)
	{
		$p = $this->getLoadEntityItemDataListParams($params);

		return $this->loadEntityItemDataList($p['entity_name'], $p['depends'], $p['filters'], $p['sfield'], $p['sorder'], $p['start'], $p['limit'], $p['paging']);
	}
	
	
	public function checkURIMap($params)
	{
		return false;
	}
	
	
	protected $sfs = [];
	
	protected function registerServiceFunctions($names)
	{
		if (! is_array($names)) $names = [$names];
		$this->sfs = array_merge($this->sfs, $names);
		$this->sfs = array_unique($this->sfs);
	}	
	
	public function getServiceFunctionList()
	{
		return $this->sfs;
	}
}

?>