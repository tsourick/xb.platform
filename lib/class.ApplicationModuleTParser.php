<?php

/**
* XB.Platform Web Application Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/

class ApplicationModuleTParser
{
	private $module = NULL;
	private $tparser = NULL;
	private $defaultExtension = 'tpl';
	
	private $_oldData = NULL;
	
	
	/**
	*
	*
	* @param ApplicationModule $module
	*/

	public function __construct(ApplicationModule $module)
	{
		$this->module = $module;
		
		$this->tparser = $module->application()->getTParser();
	}
	
	
	/**
	* Magic PHP method which hooks all the calls to non-existant methods of this class.
	* This magic method translates calls to underlying TParser object.
	*/
	
	public function __call($m, $p)
	{
		if (method_exists($this->tparser, $m))
		{
			return call_user_func_array(array($this->tparser, $m), $p);
		}
		else
		{
			trigger_error('Call to undefined method ' . __CLASS__ . '::' . $m . '()', E_USER_ERROR);
		}
	}
	
	
	/**
	*
	*
	*/

	private function setModuleData($data)
	{
		$data = array_merge
		(
			array('module' => array('name' => $this->module->getName())),
			$data
		);
		
		
		// Save original data for every key provided in $data
		
		$old_data = array();
		foreach (array_keys($data) as $k)
		{
			$v = $this->tparser->get($k);
			
			if (! is_null($v))
			{
				$old_data[$k] = $v;
			}
		}
		
		$this->_oldData = $old_data;
		
		
		$this->tparser->set($data);		
	}

	/**
	*
	*
	*/

	private function resetModuleData()
	{
		$this->tparser->set($this->_oldData);
	}
	
	/**
	*
	*
	*/

	private function clearModuleData($names)
	{
		$names[] = 'module';
		
		$this->tparser->clear($names);
	}

	
	/**
	*
	*
	* @param string $template_contents
	*
	* @return string
	*/

	public function parseString($template_contents)
	{
		$r = '';
		
		
		$this->setModuleData();
		$r = $this->tparser->parseString($template_contents);
		// $this->clearModuleData();
		$this->resetModuleData();
		
		
		return $r; 
	}
	
	/**
	*
	*
	* @param string $template_path
	*
	* @return string
	*/

	private function parseTemplateFile($template_path)
	{
		$r = '';
		

		$this->setModuleData(array('this' => array('filename' => basename($template_path))));
		$r = $this->tparser->parseFile($template_path);
		// $this->clearModuleData(array('this'));
		$this->resetModuleData();
		
		
		return $r; 
	}
	
	/**
	*
	*
	* @param string $template_name
	*
	* @return string
	*/

	public function parseTemplate($template_name)
	{
		$template_path = dirpath($this->module->getName()) . $template_name . '.' . $this->defaultExtension;

		return $this->parseTemplateFile($template_path);
	}

	/**
	*
	*
	* @param string $template_filename
	*
	* @return string
	*/

	public function parseFile($template_filename)
	{
		$template_path = dirpath($this->module->getName()) . $template_filename;

		return $this->parseTemplateFile($template_path);
	}
}

?>
