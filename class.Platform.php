<?php

/**
* XB.Platform PHP Platform
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2014
*
* Platform initialization file
*
* {@internal Platform::init() is called at the end of file}}
*
* @package platform
*/


/**
* The platform class
*/

class Platform
{
	/**
	* @internal
	* @var string current working dir cached on init()
	*/
	
	private static $cwd;

	
	/**
	* @internal Hidden constructor, object of this class may not be instantiated
	*/

	private function __construct()
	{
	}

	
	/**
	* Returns dir of the platform
	*/
	
	public static function getCwd()
	{
		return self::$cwd;
	}
	
	
	/**
	* Includes core libraries and modifies include path so that require/include may be used without path
	*/

	public static function init()
	{
		// ini_set('include_path', '.'); // Prevent alien scripts from including while searching for files to include

		
		$cwd = dirname(realpath(__FILE__)) . DIRECTORY_SEPARATOR;
		

		// Init framework
		require_once($cwd . 'xb.framework/class.Framework.php');
		

		append_include_path($cwd . 'lib');
		
		
		self::$cwd = $cwd;
	}		


	/**
	* Includes the class source for further use
	*
	* Uses require_once() and absolute path to include the file from 'lib' directory.
	* Assumes that the file name is 'class.<class name>.php' and substitutes <class name> with the string given.
	*
	* @param string $class_name middle part of the file name
	*/
	
	public static function useClass($class_name)
	{
		require_once self::$cwd . 'lib' . DIRECTORY_SEPARATOR . 'class.' . $class_name . '.php';
	}
}

Platform::init();
?>
